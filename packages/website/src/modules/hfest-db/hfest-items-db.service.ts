import { Inject, Injectable, LOCALE_ID } from "@angular/core";

interface DbItem {
  id: number;
  name: {
    fr: string;
    en: string;
    es: string;
  };
  // TODO(demurgos): Rename to unlock
  required?: number;
}

const itemsDb: Map<number, DbItem> = new Map();

declare function require(id: string): any;

// tslint:disable-next-line:no-var-requires
for (const dbItem of require("./items-db.json") as DbItem[]) {
  itemsDb.set(dbItem.id, dbItem);
}

@Injectable()
export class HfestItemsDbService {
  private localeId: "en" | "es" | "fr";

  constructor(@Inject(LOCALE_ID) localeId: string) {
    if (/^fr(?:-|$)/.test(localeId)) {
      this.localeId = "fr";
    } else if (/^es(?:-|$)/.test(localeId)) {
      this.localeId = "es";
    } else {
      this.localeId = "en";
    }
  }

  getItemName(itemId: number): string {
    const dbItem: DbItem | undefined = itemsDb.get(itemId);
    if (dbItem === undefined) {
      console.warn("Item not found: " + itemId);
      return `Item #${itemId}`;
    }
    return dbItem.name[this.localeId];
  }

  getItemRarity(itemId: number): number {
    // TODO(demurgos): Implement this function
    return 6;
  }

  getItemUnlock(itemId: number): number {
    const dbItem: DbItem | undefined = itemsDb.get(itemId);
    if (dbItem === undefined) {
      console.warn("Item not found: " + itemId);
      return 10;
    }
    return typeof dbItem.required === "number" ? dbItem.required : 10;
  }

  getItemFamilyId(itemId: number): number {
    // TODO(demurgos): Implement this function
    return 1;
  }
}
