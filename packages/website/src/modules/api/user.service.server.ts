import { Inject, Injectable, NgZone } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options";
import { User } from "@eternalfest/api-core/lib/user/user";
import { Api as EfApi } from "@eternalfest/rest-server/lib/create-api-router";
import { Incident } from "incident";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex";

import { AUTH_CONTEXT, EF_API } from "../../server/tokens";
import { UserService } from "./user.service";
import { runUnzoned } from "./utils/run-unzoned.server";

@Injectable()
export class ServerUserService extends UserService {
  private readonly auth: AuthContext;
  private readonly efApi: EfApi;
  private readonly ngZone: NgZone;

  constructor(@Inject(AUTH_CONTEXT) auth: AuthContext, @Inject(EF_API) efApi: EfApi, ngZone: NgZone) {
    super();
    this.auth = auth;
    this.efApi = efApi;
    this.ngZone = ngZone;
  }

  async getUserById(userId: UuidHex): Promise<User | undefined> {
    return runUnzoned(this.ngZone, "ServerUserService#getUserById", () => this._getUserById(userId));
  }

  async updateUser(options: UpdateUserOptions): Promise<User> {
    throw new Incident("NotSupported", {action: "ServerUserService#updateUser"});
  }

  private async _getUserById(userId: UuidHex): Promise<User | undefined> {
    const error: Error | undefined = $UuidHex.testError(userId);
    if (error !== undefined) {
      throw error;
    }
    return this.efApi.user.getUserById(this.auth, userId);
  }
}
