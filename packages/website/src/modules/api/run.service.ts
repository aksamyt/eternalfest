import { Injectable } from "@angular/core";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options";
import { Run } from "@eternalfest/api-core/lib/run/run";
import { UuidHex } from "kryo/lib/uuid-hex";

@Injectable()
export abstract class RunService {
  abstract createRun(options: CreateRunOptions): Promise<Run>;

  abstract getRunById(gameId: UuidHex): Promise<Run | undefined>;
}
