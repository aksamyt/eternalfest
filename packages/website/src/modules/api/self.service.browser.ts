import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $AuthContext, $UserAuthContext, AuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import {
  $AuthHfestCredentials,
  $CreateSessionQuery,
  AuthHfestCredentials,
  AuthStrategy,
} from "@eternalfest/rest-server/lib/io/self";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer";
import { QS_VALUE_WRITER } from "kryo-qs/lib/qs-value-writer";
import { Observable } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import { SelfService } from "./self.service";
import { apiUri } from "./utils/api-uri";

@Injectable()
export class BrowserSelfService extends SelfService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  getAuth(): Observable<AuthContext> {
    return this.http.get(apiUri("self", "auth"), {withCredentials: true})
      .pipe(
        rxMap((raw) => $AuthContext.read(JSON_VALUE_READER, raw)),
      );
  }

  createWithHfestCredentials(options: AuthHfestCredentials): Observable<UserAuthContext> {
    const reqBody: any = $AuthHfestCredentials.write(JSON_VALUE_WRITER, options);
    const reqQuery: any = $CreateSessionQuery.write(QS_VALUE_WRITER, {strategy: AuthStrategy.HfestCredentials});
    return this.http.put(apiUri("self", "session"), reqBody, {params: reqQuery, withCredentials: true})
      .pipe(
        rxMap((raw) => $UserAuthContext.read(JSON_VALUE_READER, raw)),
      );
  }
}
