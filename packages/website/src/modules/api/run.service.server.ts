import { Inject, Injectable, NgZone } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { Run } from "@eternalfest/api-core/lib/run/run";
import { Api as EfApi } from "@eternalfest/rest-server/lib/create-api-router";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex";

import { AUTH_CONTEXT, EF_API } from "../../server/tokens";
import { RunService } from "./run.service";
import { runUnzoned } from "./utils/run-unzoned.server";

@Injectable()
export class ServerRunService extends RunService {
  private readonly auth: AuthContext;
  private readonly efApi: EfApi;
  private readonly ngZone: NgZone;

  constructor(@Inject(AUTH_CONTEXT) auth: AuthContext, @Inject(EF_API) efApi: EfApi, ngZone: NgZone) {
    super();
    this.auth = auth;
    this.efApi = efApi;
    this.ngZone = ngZone;
  }

  async createRun(): Promise<Run> {
    throw new Error("Unsupported: ServerRunService#createRun");
  }

  async getRunById(runId: UuidHex): Promise<Run | undefined> {
    return runUnzoned(this.ngZone, "ServerRunService#getRunById", () => this._getRunById(runId));
  }

  private async _getRunById(runId: UuidHex): Promise<Run | undefined> {
    const error: Error | undefined = $UuidHex.testError(runId);
    if (error !== undefined) {
      throw error;
    }
    return this.efApi.run.getRunById(this.auth, runId);
  }
}
