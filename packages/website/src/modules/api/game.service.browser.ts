import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $Game, Game } from "@eternalfest/api-core/lib/game/game";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { $ShortGame, ShortGame } from "@eternalfest/api-core/lib/game/short-game";
import {
  $UpdateGameOptions,
  ReadonlyUpdateGameOptions,
  UpdateGameOptions
} from "@eternalfest/api-core/lib/game/update-game-options";
import { $Leaderboard, Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer";
import { ArrayIoType, ArrayType } from "kryo/lib/array";
import { UuidHex } from "kryo/lib/uuid-hex";
import { map as rxMap } from "rxjs/operators";

import { GameService } from "./game.service";
import { apiUri } from "./utils/api-uri";

const $ShortGameArray: ArrayIoType<ShortGame> = new ArrayType({
  itemType: $ShortGame,
  maxLength: Infinity,
});

@Injectable()
export class BrowserGameService extends GameService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getGames(): Promise<readonly ShortGame[]> {
    return this.http
      .get(apiUri("games"), {withCredentials: true})
      .pipe(rxMap((raw): readonly ShortGame[] => {
        let result: readonly ShortGame[];
        try {
          result = $ShortGameArray.read(JSON_VALUE_READER, raw);
        } catch (err) {
          console.error(err);
          result = [];
        }
        return result;
      }))
      .toPromise();
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    return this.http
      .get(apiUri("games", gameId), {withCredentials: true})
      .pipe(rxMap((raw) => $Game.read(JSON_VALUE_READER, raw)))
      .toPromise();
  }

  async updateGame(gameId: UuidHex, options: ReadonlyUpdateGameOptions): Promise<Game> {
    const reqBody: any = $UpdateGameOptions.write(JSON_VALUE_WRITER, options as UpdateGameOptions);
    const raw: any = await this.http.patch(apiUri("games", gameId), reqBody).toPromise();
    return $Game.read(JSON_VALUE_READER, raw);
  }

  async getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined> {
    const leaderboardUri: URL = new URL(apiUri("games", gameId, "leaderboard"));
    leaderboardUri.searchParams.set("game_mode", gameMode);
    return this.http
      .get(leaderboardUri.toString(), {withCredentials: true})
      .pipe(rxMap((raw) => $Leaderboard.read(JSON_VALUE_READER, raw)))
      .toPromise();
  }
}
