import { Injectable } from "@angular/core";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game";
import { ReadonlyUpdateGameOptions } from "@eternalfest/api-core/lib/game/update-game-options";
import { Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard";
import { UuidHex } from "kryo/lib/uuid-hex";

@Injectable()
export abstract class GameService {
  abstract getGames(): Promise<readonly ShortGame[]>;

  abstract getGameById(gameId: UuidHex): Promise<Game | undefined>;

  abstract updateGame(gameId: UuidHex, options: ReadonlyUpdateGameOptions): Promise<Game>;

  abstract getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined>;
}
