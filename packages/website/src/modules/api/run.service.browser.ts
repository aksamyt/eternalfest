import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $CreateRunOptions, CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options";
import { $Run, Run } from "@eternalfest/api-core/lib/run/run";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer";
import { UuidHex } from "kryo/lib/uuid-hex";
import { map as rxMap } from "rxjs/operators";

import { RunService } from "./run.service";
import { apiUri } from "./utils/api-uri";

@Injectable()
export class BrowserRunService extends RunService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async createRun(options: CreateRunOptions): Promise<Run> {
    const reqBody: any = $CreateRunOptions.write(JSON_VALUE_WRITER, options);
    const raw: any = await this.http.post(apiUri("runs"), reqBody).toPromise();
    return $Run.read(JSON_VALUE_READER, raw);
  }

  async getRunById(runId: UuidHex): Promise<Run | undefined> {
    return this.http
      .get(apiUri("runs", runId), {withCredentials: true})
      .pipe(rxMap((raw) => $Run.read(JSON_VALUE_READER, raw)))
      .toPromise();
  }
}
