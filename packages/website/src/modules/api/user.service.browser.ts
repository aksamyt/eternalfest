import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $UpdateUserOptions, UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options";
import { $User, User } from "@eternalfest/api-core/lib/user/user";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer";
import { UuidHex } from "kryo/lib/uuid-hex";

import { UserService } from "./user.service";
import { apiUri } from "./utils/api-uri";

@Injectable()
export class BrowserUserService extends UserService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getUserById(userId: UuidHex): Promise<User | undefined> {
    const raw: any = await this.http.get(apiUri("users", userId)).toPromise();
    return $User.read(JSON_VALUE_READER, raw);
  }

  async updateUser(options: UpdateUserOptions): Promise<User> {
    const reqBody: any = $UpdateUserOptions.write(JSON_VALUE_WRITER, options);
    const raw: any = await this.http.patch(apiUri("users", options.userId!), reqBody).toPromise();
    return $User.read(JSON_VALUE_READER, raw);
  }
}
