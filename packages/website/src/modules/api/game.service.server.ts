import { Inject, Injectable, NgZone } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game";
import { ReadonlyUpdateGameOptions } from "@eternalfest/api-core/lib/game/update-game-options";
import { Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard";
import { Api as EfApi } from "@eternalfest/rest-server/lib/create-api-router";
import { Incident } from "incident";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex";

import { AUTH_CONTEXT, EF_API } from "../../server/tokens";
import { GameService } from "./game.service";
import { runUnzoned } from "./utils/run-unzoned.server";

@Injectable()
export class ServerGameService extends GameService {
  private readonly auth: AuthContext;
  private readonly efApi: EfApi;
  private readonly ngZone: NgZone;

  constructor(
    @Inject(AUTH_CONTEXT) auth: AuthContext,
    @Inject(EF_API) efApi: EfApi,
      ngZone: NgZone,
  ) {
    super();
    this.auth = auth;
    this.efApi = efApi;
    this.ngZone = ngZone;
  }

  async getGames(): Promise<readonly ShortGame[]> {
    return runUnzoned(this.ngZone, "ServerGameService#getGames", () => this._getGames());
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    return runUnzoned(this.ngZone, "ServerGameService#getGameById", () => this._getGameById(gameId));
  }

  async updateGame(gameId: UuidHex, options: ReadonlyUpdateGameOptions): Promise<Game> {
    throw new Incident("NotSupported", {action: "ServerGameService#updateGame"});
  }

  async getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined> {
    return runUnzoned(this.ngZone, "ServerGameService#getLeaderboardByGameId", () => this._getLeaderboardByGameId(gameId, gameMode));
  }

  private async _getGames(): Promise<readonly ShortGame[]> {
    return this.efApi.game.getGames(this.auth);
  }

  private async _getGameById(gameId: UuidHex): Promise<Game | undefined> {
    const error: Error | undefined = $UuidHex.testError(gameId);
    if (error !== undefined) {
      throw error;
    }
    return this.efApi.game.getGameById(this.auth, gameId);
  }

  private async _getLeaderboardByGameId(gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard | undefined> {
    const error: Error | undefined = $UuidHex.testError(gameId);
    if (error !== undefined) {
      throw error;
    }
    return this.efApi.leaderboard.getLeaderboard(this.auth, gameId, gameMode);
  }
}
