import { Injectable } from "@angular/core";
import { AuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { AuthHfestCredentials } from "@eternalfest/rest-server/lib/io/self";
import { Incident } from "incident";
import { Observable } from "rxjs";

import { SelfService } from "./self.service";

@Injectable()
export class ServerSelfService extends SelfService {
  getAuth(): Observable<AuthContext> {
    throw new Incident("NotSupported", {action: "ServerSelfService#getAuth"});
  }

  createWithHfestCredentials(options: AuthHfestCredentials): Observable<UserAuthContext> {
    throw new Incident("NotSupported", {action: "ServerSelfService#createWithHfestCredentials"});
  }
}
