import { Injectable } from "@angular/core";
import { UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options";
import { User } from "@eternalfest/api-core/lib/user/user";
import { UuidHex } from "kryo/lib/uuid-hex";

@Injectable()
export abstract class UserService {
  abstract getUserById(userId: UuidHex): Promise<User | undefined>;

  abstract updateUser(options: UpdateUserOptions): Promise<User>;
}
