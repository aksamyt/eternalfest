import { Pipe, PipeTransform } from "@angular/core";
import { hfestServerToUrl } from "@eternalfest/api-backend/lib/utils/hfest-server-url";
import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server";

@Pipe({
  name: "hfestUri",
  pure: true,
})
export class HfestUriPipe implements PipeTransform {
  transform(type: "public-profile", server: HfestServer, userId: string): string;
  transform(type: "login", server: HfestServer): string;
  transform(type: "host", server: HfestServer): string;
  transform(type: "root", server: HfestServer): string;
  transform(type: any, server: any, ...args: any[]): any {
    switch (type) {
      case "login":
        return getHfestLoginUri(server);
      case "host":
        return getHfestHostUri(server);
      case "public-profile":
        return getHfestPublicProfileUri(server, args[0]);
      case "root":
        return getHfestRootUri(server);
      default:
        console.error(`Unexpected type: ${type}`);
        return getHfestRootUri(server);
    }
  }
}

function getHfestRootUri(server: HfestServer): string {
  return hfestServerToUrl(server);
}

function getHfestHostUri(server: HfestServer): string {
  switch (server) {
    case HfestServer.En:
      return "hfest.net";
    case HfestServer.Es:
      return "hammerfest.es";
    case HfestServer.Fr:
      return "hammerfest.fr";
  }
}

function getHfestLoginUri(server: HfestServer): string {
  return `${hfestServerToUrl(server)}login.html`;
}

function getHfestPublicProfileUri(server: HfestServer, userId: string): string {
  return `${hfestServerToUrl(server)}user.html/${userId}`;
}
