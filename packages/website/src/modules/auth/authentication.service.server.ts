import { Inject, Injectable } from "@angular/core";
import { TransferState } from "@angular/platform-browser";
import { $AuthContext, AuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { AuthHfestCredentials } from "@eternalfest/rest-server/lib/io/self";
import { Incident } from "incident";
import { JsonValueWriter } from "kryo-json/lib/json-value-writer";
import { BehaviorSubject, Observable, of as rxOf } from "rxjs";

import { AUTH_CONTEXT } from "../../server/tokens";
import { Authentication } from "./authentication.service";
import { AUTH_CONTEXT_KEY, RawAuthContext } from "./state-keys";

const JSON_WRITER: JsonValueWriter = new JsonValueWriter();

@Injectable()
export class ServerAuthentication extends Authentication {
  private readonly auth: BehaviorSubject<AuthContext>;

  constructor(@Inject(AUTH_CONTEXT) auth: AuthContext, transferState: TransferState) {
    super();
    const rawAuthContext: RawAuthContext = $AuthContext.write(JSON_WRITER, auth);
    transferState.set(AUTH_CONTEXT_KEY, rawAuthContext);
    this.auth = new BehaviorSubject<AuthContext>(auth);
  }

  getAuth(): Observable<AuthContext> {
    return rxOf(this.auth.getValue());
  }

  async withHfestCredentials(options: AuthHfestCredentials): Promise<UserAuthContext> {
    throw new Incident("Unsupported", "ServerAuthentication#withHfestCredentials");
  }

  async logout(): Promise<void> {
    throw new Incident("Unsupported", "ServerAuthentication#logout");
  }
}
