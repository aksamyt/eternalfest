import { Injectable } from "@angular/core";
import { AuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { AuthHfestCredentials } from "@eternalfest/rest-server/lib/io/self";
import { Observable } from "rxjs";

@Injectable()
export abstract class Authentication {
  abstract getAuth(): Observable<AuthContext>;

  abstract async withHfestCredentials(options: AuthHfestCredentials): Promise<UserAuthContext>;

  abstract logout(): Promise<void>;
}
