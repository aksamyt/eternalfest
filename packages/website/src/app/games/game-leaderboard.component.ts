import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type";
import { $Leaderboard, Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard";
import { Observable } from "rxjs";
import { first as rxFirst } from "rxjs/operators";

import { Authentication } from "../../modules/auth/authentication.service";

@Component({
  selector: "ef-leaderboard",
  templateUrl: "./game-leaderboard.component.html",
  styleUrls: ["./game-leaderboard.component.scss"],
})
export class GameLeaderboardComponent implements OnInit
{
  private readonly authentication: Authentication;
  private readonly route: ActivatedRoute;
  public leaderboard: Leaderboard | undefined;

  constructor(authentication: Authentication, route: ActivatedRoute) {
    this.authentication = authentication;
    this.route = route;
    this.leaderboard = undefined;
  }

  ngOnInit(): void {
    const routeData$: Observable<{leaderboard: Leaderboard}> = this.route.data as any;
    routeData$
      .subscribe((data: {leaderboard: Leaderboard}) => {
        this.leaderboard = data.leaderboard;
      });
  }

  private async read(): Promise<void> {
    const auth: AuthContext = await this.authentication.getAuth().pipe(rxFirst()).toPromise();
    if (auth.type !== ActorType.User) {
      throw new Error("NotAuthenticated");
    }
  }
}
