import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HelpersModule } from "../../modules/helpers/helpers.module";
import { GameIconUriPipe } from "./game-icon-uri.pipe";
import { GameLeaderboardComponent } from "./game-leaderboard.component";
import { GameListComponent } from "./game-list.component";
import { GameModeNameComponent } from "./game-mode-name.component";
import { GameOptionNameComponent } from "./game-option-name.component";
import { GameViewComponent } from "./game-view.component";
import { GamesRoutingModule } from "./games-routing.module";

@NgModule({
  declarations: [
    GameIconUriPipe,
    GameLeaderboardComponent,
    GameListComponent,
    GameModeNameComponent,
    GameOptionNameComponent,
    GameViewComponent,
  ],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
    GamesRoutingModule,
  ],
})
export class GamesModule {
}
