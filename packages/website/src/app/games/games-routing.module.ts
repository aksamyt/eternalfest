import { Injectable, NgModule, NgZone } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes } from "@angular/router";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game";
import { Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard";

import { GameService } from "../../modules/api/game.service";
import { GameLeaderboardComponent } from "./game-leaderboard.component";
import { GameListComponent } from "./game-list.component";
import { GameViewComponent } from "./game-view.component";

@Injectable()
export class GameResolverService implements Resolve<Game> {
  private readonly game: GameService;
  private readonly ngZone: NgZone;
  private readonly router: Router;

  constructor(game: GameService, ngZone: NgZone, router: Router) {
    this.game = game;
    this.ngZone = ngZone;
    this.router = router;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Game | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId !== null) {
      const game: Game | undefined = await this.game.getGameById(gameId);
      if (game !== undefined) {
        return game;
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/games"));
    return undefined as never;
  }
}

@Injectable()
export class GamesResolverService implements Resolve<readonly ShortGame[]> {
  private readonly game: GameService;
  private readonly router: Router;

  constructor(router: Router, game: GameService) {
    this.router = router;
    this.game = game;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<readonly ShortGame[] | never> {
    return this.game.getGames();
  }
}

@Injectable()
export class GameLeaderboardResolverService implements Resolve<Leaderboard> {
  private readonly game: GameService;
  private readonly ngZone: NgZone;
  private readonly router: Router;

  constructor(game: GameService, ngZone: NgZone, router: Router) {
    this.ngZone = ngZone;
    this.router = router;
    this.game = game;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Leaderboard | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId !== null) {
      const game: Game | undefined = await this.game.getGameById(gameId);
      if (game !== undefined) {
        // TODO: Local tests have no mode, but have runs... So to remove once it's not possible to have no mode.
        // TODO: How to choose a different mode?
        const gameMode: GameModeKey = game.gameModes.length > 0 ? game.gameModes[0].key : "solo";
        const leaderboard: Leaderboard | undefined = await this.game.getLeaderboardByGameId(gameId, gameMode);
        if (leaderboard !== undefined) {
          return leaderboard;
        }
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/games/" + gameId + "/leaderboard"));
    return undefined as never;
  }
}

const routes: Routes = [
  {
    path: "",
    component: GameListComponent,
    pathMatch: "full",
    resolve: {
      games: GamesResolverService,
    },
  },
  {
    path: ":game_id",
    component: GameViewComponent,
    pathMatch: "full",
    resolve: {
      game: GameResolverService,
    },
  },
  {
    path: ":game_id/leaderboard",
    component: GameLeaderboardComponent,
    pathMatch: "full",
    resolve: {
      leaderboard: GameLeaderboardResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [GameResolverService, GamesResolverService, GameLeaderboardResolverService],
})
export class GamesRoutingModule {
}
