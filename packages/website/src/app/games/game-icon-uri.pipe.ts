import { Pipe, PipeTransform } from "@angular/core";
import { IdRef } from "@eternalfest/api-core/lib/types/id-ref";

import { apiUri } from "../../modules/api/utils/api-uri";

const DEFAULT_ICON_URI: string = "/assets/accounts/ef.x64.png";

@Pipe({
  name: "gameIconUri",
  pure: true,
})
export class GameIconUriPipe implements PipeTransform {
  transform(iconFile: IdRef | null | undefined): string {
    if (iconFile === null || iconFile === undefined) {
      return DEFAULT_ICON_URI;
    }
    return apiUri("files", iconFile.id, "raw");
  }
}
