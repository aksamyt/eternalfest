import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game";
import { Observable } from "rxjs";

@Component({
  selector: "ef-game-list",
  templateUrl: "./game-list.component.html",
  styleUrls: ["./game-list.component.scss"],
})
export class GameListComponent {
  public games: readonly ShortGame[] | undefined;

  private readonly route: ActivatedRoute;

  constructor(route: ActivatedRoute) {
    this.route = route;
    this.games = undefined;
  }

  ngOnInit(): void {
    const routeData$: Observable<{games: ShortGame[]}> = this.route.data as any;
    routeData$
      .subscribe((data: {games: readonly ShortGame[]}) => {
        this.games = data.games;
      });
  }
}
