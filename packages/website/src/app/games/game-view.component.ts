import { Component, NgZone, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { GameMode } from "@eternalfest/api-core/lib/game/game-mode";
import { GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state";
import { GameOptionState } from "@eternalfest/api-core/lib/game/game-option-state";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options";
import { Run } from "@eternalfest/api-core/lib/run/run";
import { $RunSettings, RunSettings } from "@eternalfest/api-core/lib/run/run-settings";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type";
import { UuidHex } from "kryo/lib/uuid-hex";
import { Observable } from "rxjs";
import { first as rxFirst, map as rxMap } from "rxjs/operators";

import { GameService } from "../../modules/api/game.service";
import { RunService } from "../../modules/api/run.service";
import { Authentication } from "../../modules/auth/authentication.service";
import { LocalStorageKey, LocalStorageService } from "../../modules/storage/local-storage.service";

const GAME_SETTINGS_KEY: LocalStorageKey<RunSettings> = new LocalStorageKey("user/game-settings", $RunSettings);

@Component({
  selector: "ef-game-view",
  templateUrl: "./game-view.component.html",
  styleUrls: ["./game-view.component.scss"],
})
export class GameViewComponent implements OnInit {
  public GameModeState: typeof GameModeState = GameModeState;
  public GameOptionState: typeof GameOptionState = GameOptionState;
  public game: Game | undefined;

  public get gameModeDisplayName(): string | undefined {
    return this.gameMode !== undefined ? this.gameMode.displayName : undefined;
  }

  public set gameModeKey(newVal: string | undefined) {
    if (this.game !== undefined) {
      for (const gameMode of this.game.gameModes) {
        if (gameMode.key === newVal) {
          const lockedOptions: string[] = gameMode.options
            .filter((o) => o.state === GameOptionState.Locked)
            .map((o) => o.key);
          this.gameMode = gameMode;
          this.options = new Set(lockedOptions);
          return;
        }
      }
    }
    this.gameMode = undefined;
  }

  public get gameModeKey(): string | undefined {
    return this.gameMode?.key;
  }

  public options: Set<string>;
  public detail: boolean;
  public shake: boolean;
  public sound: boolean;
  public music: boolean;
  public pending: boolean;
  public newPublicationDate: string;
  public isAdmin$: Observable<boolean>;

  private readonly authentication: Authentication;
  private readonly gameService: GameService;
  private readonly ngZone: NgZone;
  private readonly route: ActivatedRoute;
  private readonly router: Router;
  private readonly runService: RunService;
  private readonly localStorageService: LocalStorageService;
  public gameMode: GameMode | undefined;

  constructor(
    authentication: Authentication,
    gameService: GameService,
    ngZone: NgZone,
    route: ActivatedRoute,
    router: Router,
    runService: RunService,
    localStorageService: LocalStorageService,
  ) {
    this.authentication = authentication;
    this.gameService = gameService;
    this.ngZone = ngZone;
    this.route = route;
    this.router = router;
    this.runService = runService;
    this.localStorageService = localStorageService;
    this.isAdmin$ = authentication.getAuth()
      .pipe(rxMap((auth) => auth.type === ActorType.User && auth.isAdministrator));

    this.pending = false;
    this.game = undefined;
    this.gameMode = undefined;
    this.options = new Set();
    this.detail = true;
    this.shake = true;
    this.sound = true;
    this.music = true;
    this.newPublicationDate = (new Date()).toISOString();

    const settings: RunSettings | null = this.localStorageService.read(GAME_SETTINGS_KEY);
    if (settings !== null) {
      this.detail = settings.detail;
      this.shake = settings.shake;
      this.sound = settings.sound;
      this.music = settings.music;
    }
  }

  ngOnInit(): void {
    const routeData$: Observable<{game: Game}> = this.route.data as any;
    routeData$
      .subscribe((data: {game: Game}) => {
        this.game = data.game;
        for (const gameMode of this.game.gameModes) {
          if (gameMode.state === GameModeState.Enabled) {
            this.gameModeKey = gameMode.key;
            return;
          }
        }
        this.gameModeKey = undefined;
      });
  }

  async onSubmit(event: Event): Promise<void> {
    event.preventDefault();
    const createRunOptions: CreateRunOptions = await this.read();
    if (this.pending) {
      return;
    }
    this.pending = true;
    this.localStorageService.store(GAME_SETTINGS_KEY, createRunOptions.settings);
    try {
      const run: Run = await this.runService.createRun(createRunOptions);
      await this.ngZone.run(() => this.router.navigateByUrl(`/runs/${run.id}`));
    } catch (err) {
      console.error(err);
    }
    this.pending = false;
  }

  onOptionChange(event: Event): void {
    event.preventDefault();
    const target: HTMLInputElement = event.target as any;
    if (target.checked) {
      this.options.add(target.value);
    } else {
      this.options.delete(target.value);
    }
  }

  async setPublicationDate(val: string | null): Promise<void> {
    const publicationDate: Date | null = val !== null ? new Date(val) : null;
    if (this.game === undefined) {
      console.warn("Game is not loaded");
      return;
    }
    const gameId: UuidHex = this.game.id;
    await this.gameService.updateGame(gameId, {publicationDate});
    console.log("Done");
  }

  private async read(): Promise<CreateRunOptions> {
    const auth: AuthContext = await this.authentication.getAuth().pipe(rxFirst()).toPromise();
    if (auth.type !== ActorType.User) {
      throw new Error("NotAuthenticated");
    }
    return {
      userId: auth.userId,
      gameId: this.game!.id,
      gameMode: this.gameMode!.key,
      gameOptions: [...this.options],
      settings: {
        detail: this.detail,
        music: this.music,
        shake: this.shake,
        sound: this.sound,
        volume: 100,
      },
    };
  }

}
