import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { User } from "@eternalfest/api-core/lib/user/user";
import { Observable } from "rxjs";

@Component({
  selector: "ef-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent implements OnInit {
  public user: User | undefined;

  private readonly route: ActivatedRoute;

  constructor(route: ActivatedRoute) {
    this.route = route;
    this.user = undefined;
  }

  ngOnInit(): void {
    const data$: Observable<{user: User}> = this.route.data as any;
    data$.subscribe((data: {user: User}) => {
      this.user = data.user;
    });
  }
}
