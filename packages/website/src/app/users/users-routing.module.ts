import { Injectable, NgModule, NgZone } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes } from "@angular/router";
import { User } from "@eternalfest/api-core/lib/user/user";

import { UserService } from "../../modules/api/user.service";
import { UserComponent } from "./user.component";

@Injectable()
export class UserResolverService implements Resolve<User> {
  private readonly ngZone: NgZone;
  private readonly router: Router;
  private readonly user: UserService;

  constructor(ngZone: NgZone, router: Router, user: UserService) {
    this.ngZone = ngZone;
    this.router = router;
    this.user = user;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<User | never> {
    const userId: string | null = route.paramMap.get("user_id");
    if (userId !== null) {
      const user: User | undefined = await this.user.getUserById(userId);
      if (user !== undefined) {
        return user;
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/"));
    return undefined as never;
  }
}

const routes: Routes = [
  {
    path: ":user_id",
    component: UserComponent,
    pathMatch: "full",
    resolve: {
      user: UserResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [UserResolverService],
})
export class UsersRoutingModule {
}
