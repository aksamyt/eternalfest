import { Component, Input } from "@angular/core";
import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity";

declare function require(id: string): any;

@Component({
  selector: "ef-user-hfest-identity",
  templateUrl: "./hfest-identity.component.html",
  styleUrls: ["./hfest-identity.component.scss"],
})
export class HfestIdentityComponent {
  @Input("identity")
  identity!: HfestIdentity;

  readonly itemsDb: any[] = require("../../modules/hfest-db/items-db.json");
}
