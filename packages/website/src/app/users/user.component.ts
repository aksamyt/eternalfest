import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { IdentityType } from "@eternalfest/api-core/lib/types/user/identity-type";
import { User } from "@eternalfest/api-core/lib/user/user";
import { UuidHex } from "kryo/lib/uuid-hex";
import { Observable } from "rxjs";

import { UserService } from "../../modules/api/user.service";

@Component({
  selector: "ef-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"],
})
export class UserComponent implements OnInit {
  user: User | undefined;
  IdentityType: typeof IdentityType = IdentityType;

  private readonly route: ActivatedRoute;
  private readonly userService: UserService;

  constructor(activatedRoute: ActivatedRoute, userService: UserService) {
    this.route = activatedRoute;
    this.userService = userService;

    this.user = undefined;
  }

  async setTester(val: boolean): Promise<void> {
    if (this.user === undefined) {
      console.warn("User is not loaded");
      return;
    }
    const userId: UuidHex = this.user.id;
    await this.userService.updateUser({userId, isTester: val});
    console.log("Done");
  }

  ngOnInit(): void {
    const data$: Observable<{user: User}> = this.route.data as any;
    data$.subscribe((data: {user: User}) => {
      this.user = data.user;
    });
  }
}
