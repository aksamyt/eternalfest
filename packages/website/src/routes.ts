/**
 * List of routes corresponding to pages (not assets).
 */
export const ROUTES: string[] = [
  "/",
  "/games",
  "/games/:game_id",
  "/games/:game_id/leaderboard",
  "/login",
  "/runs/:run_id",
  "/settings",
  "/users/:user_id",
];
