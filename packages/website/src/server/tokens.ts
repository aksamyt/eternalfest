import { InjectionToken } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { Api as EfApi } from "@eternalfest/rest-server/lib/create-api-router";

export const AUTH_CONTEXT: InjectionToken<AuthContext> = new InjectionToken("AuthContext");

export const EF_API: InjectionToken<EfApi> = new InjectionToken("EfApi");
