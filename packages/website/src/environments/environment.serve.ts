import { Environment } from "./environment-type";

export const environment: Environment = {
  production: false,
  apiBase: "http://localhost:50314",
};
