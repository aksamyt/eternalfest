import { $UserDisplayName, UserDisplayName } from "@eternalfest/api-core/lib/user/user-display-name.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

export interface UpdateDisplayNameBody {
  displayName: UserDisplayName;
}

export const $UpdateDisplayNameBody: RecordIoType<UpdateDisplayNameBody> = new RecordType<UpdateDisplayNameBody>({
  properties: {
    displayName: {type: $UserDisplayName},
  },
  changeCase: CaseStyle.SnakeCase,
});
