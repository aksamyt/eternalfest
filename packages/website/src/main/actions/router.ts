import { OauthClientService } from "@eternal-twin/http-oauth-client";
import { hfestServerToUrl } from "@eternalfest/api-backend/lib/utils/hfest-server-url.js";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import hfestApi from "@eternalfest/hammerfest-api";
import hfestTypes from "@eternalfest/hammerfest-api/types/api.js";
import { $CreateSessionQuery, AuthStrategy, CreateSessionQuery } from "@eternalfest/rest-server/lib/io/self.js";
import { KoaAuth, SESSION_COOKIE } from "@eternalfest/rest-server/lib/koa-auth.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaMount from "koa-mount";
import koaRoute from "koa-route";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { $UuidHex } from "kryo/lib/uuid-hex.js";

import { $HfestCredentials, HfestCredentials } from "./hfest-credentials.js";
import { createLoginEtwinRouter } from "./login-etwin.js";
import { $UpdateDisplayNameBody, UpdateDisplayNameBody } from "./update-display-name.js";

const GUEST_AUTH_CONTEXT: AuthContext = {
  type: ActorType.Guest,
  scope: AuthScope.Default,
};

export interface Api {
  auth: AuthService;
  koaAuth: KoaAuth;
  hfestIdentity: HfestIdentityService;
  oauthClient: OauthClientService;
  session: SessionService;
  user: UserService;
}

export async function createActionsRouter(api: Api): Promise<Koa> {
  const router: Koa = new Koa();

  router.use(koaRoute.post("/logout", deleteSession));

  async function deleteSession(ctx: Koa.Context): Promise<void> {
    let auth: AuthContext;
    try {
      auth = await api.koaAuth.auth(ctx);
    } catch (err) {
      console.error(err);
      auth = GUEST_AUTH_CONTEXT;
    }

    try {
      const sessionId: string | undefined = ctx.cookies.get(SESSION_COOKIE);
      if (sessionId !== undefined && $UuidHex.test(sessionId)) {
        await api.session.deleteSessionById(auth, sessionId);
      }
    } finally {
      ctx.cookies.set(SESSION_COOKIE, undefined);
      ctx.response.redirect("/");
    }
  }

  router.use(koaRoute.post("/link_hfest", koaCompose([koaBodyParser(), linkHfest])));

  async function linkHfest(ctx: Koa.Context): Promise<void> {
    let auth: AuthContext;
    try {
      auth = await api.koaAuth.auth(ctx);
    } catch (err) {
      console.error(err);
      ctx.response.redirect("/");
      return;
    }

    let body: HfestCredentials;
    try {
      body = $HfestCredentials.read(JSON_VALUE_READER, ctx.request.body);
    } catch (err) {
      console.error(err);
      ctx.response.redirect("/");
      return;
    }

    try {
      const hfestSession: hfestTypes.Session = await hfestApi.DEFAULT_HAMMERFEST_API.createSession({
        server: hfestServerToUrl(body.server),
        username: body.login,
        password: body.password,
      });

      const identity: HfestIdentity = await api.hfestIdentity
        .createOrUpdateHfestIdentity(hfestApi.DEFAULT_HAMMERFEST_API.exportSession(hfestSession));
      await api.hfestIdentity.linkUser(auth, {identityId: identity.id});
    } catch (err) {
      console.error(err);
    } finally {
      ctx.response.redirect("/");
    }
  }

  router.use(koaRoute.post("/create_session", koaCompose([koaBodyParser(), createSession])));

  async function createSession(ctx: Koa.Context): Promise<void> {
    const query: CreateSessionQuery = $CreateSessionQuery.read(QS_VALUE_READER, ctx.request.query);
    switch (query.strategy) {
      case AuthStrategy.HfestCredentials: {
        await createSessionWithHfestCredentials(ctx);
        break;
      }
      default:
        console.warn(`Unknown AuthStrategy: ${query.strategy}`);
        ctx.response.redirect("/");
        return;
    }
  }

  async function createSessionWithHfestCredentials(ctx: Koa.Context): Promise<void> {
    let body: HfestCredentials;
    let password: Buffer;
    try {
      body = $HfestCredentials.read(JSON_VALUE_READER, ctx.request.body);
      password = Buffer.from(body.password);
    } catch (err) {
      console.error(err);
      ctx.response.redirect("/");
      return;
    }

    try {
      const auth: AuthContext = await api.auth.hfestCredentials(body.server, body.login, password);

      switch (auth.type) {
        case ActorType.User: {
          const session: Session = await api.session.createSession(auth, auth.userId);
          ctx.cookies.set(SESSION_COOKIE, session.id);
          break;
        }
        default:
          throw new Error("UnexpectedAuthType");
      }
    } catch (err) {
      console.error(err);
    } finally {
      ctx.response.redirect("/");
    }

  }

  router.use(koaRoute.post("/update_display_name", koaCompose([koaBodyParser(), updateDisplayName])));

  async function updateDisplayName(ctx: Koa.Context): Promise<void> {
    let auth: AuthContext;
    try {
      auth = await api.koaAuth.auth(ctx);
    } catch (err) {
      console.error(err);
      ctx.response.redirect("/");
      return;
    }

    let body: UpdateDisplayNameBody;
    try {
      body = $UpdateDisplayNameBody.read(JSON_VALUE_READER, ctx.request.body);
    } catch (err) {
      console.error(err);
      ctx.response.redirect("/");
      return;
    }

    try {
      await api.user.updateUser(auth, {displayName: body.displayName});
    } catch (err) {
      console.error(err);
    } finally {
      ctx.response.redirect("/");
    }
  }


  router.use(koaMount("/login/etwin", await createLoginEtwinRouter(api)));

  return router;
}
