import { OauthClientService } from "@eternal-twin/http-oauth-client/src/lib";
import { AuthService } from "@eternalfest/api-core/lib/auth/service";
import { KoaAuth } from "@eternalfest/rest-server/lib/koa-auth";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import url from "url";

export interface Api {
  auth: AuthService;
  koaAuth: KoaAuth;
  oauthClient: OauthClientService;
}

export async function createLoginEtwinRouter(api: Api): Promise<Koa> {
  const router: Koa = new Koa();

  router.use(koaRoute.post("/", koaCompose([koaBodyParser(), loginWithEtwin])));

  async function loginWithEtwin(cx: Koa.Context): Promise<void> {
    const csrfToken: string = await api.koaAuth.getOrCreateCsrfToken(cx);
    const state: string = await api.oauthClient.createStateJwt(csrfToken, "eternal-twin.net");
    const reqUrl: url.URL = await api.oauthClient.createAuthorizationRequest(state, []);
    cx.response.redirect(reqUrl.toString());
  }

  return router;
}
