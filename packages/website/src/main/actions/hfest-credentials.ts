import { $HfestLogin, HfestLogin } from "@eternalfest/api-core/lib/hfest-identity/hfest-login.js";
import { $HfestServer, HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export interface HfestCredentials {
  server: HfestServer;
  login: HfestLogin;
  password: string;
}

export const $HfestCredentials: RecordIoType<HfestCredentials> = new RecordType<HfestCredentials>({
  properties: {
    server: {type: $HfestServer},
    login: {type: $HfestLogin},
    password: {type: new Ucs2StringType({maxLength: 50})},
  },
  changeCase: CaseStyle.SnakeCase,
});
