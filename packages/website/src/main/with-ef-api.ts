import { EtwinClientService } from "@eternal-twin/core/lib/etwin-client/service.js";
import { HttpEtwinClient } from "@eternal-twin/etwin-client-http";
import { HttpOauthClientService, OauthClientService } from "@eternal-twin/http-oauth-client";
import { PgAuthService } from "@eternalfest/api-backend/lib/auth/pg-service.js";
import { PgBlobService } from "@eternalfest/api-backend/lib/blob/pg-service.js";
import { Database } from "@eternalfest/api-backend/lib/db/database.js";
import { PgFileService } from "@eternalfest/api-backend/lib/file/pg-service.js";
import { PgGameService } from "@eternalfest/api-backend/lib/game/pg-service.js";
import { PgHfestIdentityService } from "@eternalfest/api-backend/lib/hfest-identity/pg-service.js";
import { PgLeaderboardService } from "@eternalfest/api-backend/lib/leaderboard/pg-service.js";
import { ScryptPasswordService } from "@eternalfest/api-backend/lib/password/scrypt-service.js";
import { PgRunService } from "@eternalfest/api-backend/lib/run/pg-service.js";
import { PgSessionService } from "@eternalfest/api-backend/lib/session/pg-service.js";
import { PgUserService } from "@eternalfest/api-backend/lib/user/pg-service.js";
import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { LeaderboardService } from "@eternalfest/api-core/lib/leaderboard/service.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import bufferService from "@eternalfest/buffer-service/buffer-service.js";
import fsBufferService from "@eternalfest/buffer-service/fs-buffer-service.js";
import hfApi from "@eternalfest/hammerfest-api";
import hfApiTypes from "@eternalfest/hammerfest-api/types/api.js";
import { KoaAuth } from "@eternalfest/rest-server/lib/koa-auth.js";
import furi from "furi";
import pg from "pg";
import url from "url";
import urljoin from "url-join";

import { getLocalConfig, ServerConfig } from "./config.js";

export interface Api {
  auth: AuthService;
  blob: BlobService;
  etwinClient: EtwinClientService;
  file: FileService;
  game: GameService;
  hfestIdentity: HfestIdentityService;
  koaAuth: KoaAuth;
  oauthClient: OauthClientService;
  run: RunService;
  leaderboard: LeaderboardService;
  session: SessionService;
  user: UserService;
}

export async function withEfApi<R>(handler: (api: Api) => Promise<R>): Promise<R> {
  // TODO: Only load config once (it is currently loaded again in `withEfState`)
  const config: ServerConfig = await getLocalConfig();

  return withEfState(async ({buffer, db}): Promise<R> => {
    const blob: PgBlobService = new PgBlobService(db, buffer);
    const passwordService: ScryptPasswordService = new ScryptPasswordService(/* 0.1 */);
    const user: PgUserService = new PgUserService(db, passwordService);
    const file: PgFileService = new PgFileService(db, blob);
    const game: PgGameService = new PgGameService(db, user, file);
    const run: PgRunService = new PgRunService(db, game, user);
    const leaderboard: PgLeaderboardService = new PgLeaderboardService(db, game);
    const mockHammerfestApi: hfApiTypes.HammerfestApi = hfApi.DEFAULT_HAMMERFEST_API;
    const hfestIdentity: PgHfestIdentityService = new PgHfestIdentityService(db, mockHammerfestApi);
    const session: SessionService = new PgSessionService(db, 86400);
    const auth: PgAuthService = new PgAuthService(mockHammerfestApi, hfestIdentity, session, user);
    const kAuth: KoaAuth = new KoaAuth(session, user);
    const oauthClient: OauthClientService = new HttpOauthClientService(
      new url.URL(urljoin(config.etwinUri.toString(), "oauth/authorize")),
      new url.URL(urljoin(config.etwinUri.toString(), "oauth/token")),
      config.etwinClientId,
      config.etwinOauthSecret,
      new url.URL(urljoin(config.externalBaseUri.toString(), "oauth/callback")),
      Buffer.from(config.secret),
    );
    const etwinClient = new HttpEtwinClient(new url.URL("http://localhost:50320/"));
    const api: Api = {auth, blob, file, game, hfestIdentity, koaAuth: kAuth, run, leaderboard, session, user, oauthClient, etwinClient};
    return handler(api);
  });
}

export interface EfState {
  buffer: bufferService.BufferService;
  db: Database;
}

export async function withEfState<R>(handler: (state: EfState) => Promise<R>): Promise<R> {
  return withEfDb(async (db: Database): Promise<R> => {
    return withEfFsBufferRoot(async (root: url.URL): Promise<R> => {
      const buffer: fsBufferService.FsBufferService = new fsBufferService.FsBufferService(root);
      return handler({db, buffer});
    });
  });
}

async function withEfFsBufferRoot<R>(handler: (root: url.URL) => Promise<R>): Promise<R> {
  const {dataRoot} = await getLocalConfig();
  const fsBufferRoot: url.URL = furi.join(dataRoot, ["buffers"]);
  return handler(fsBufferRoot);
}

export async function withEfDb<R>(handler: (db: Database) => Promise<R>): Promise<R> {
  return withEfDbPool(async (pool: pg.Pool): Promise<R> => {
    const db: Database = new Database(pool);
    return handler(db);
  });
}

async function withEfDbPool<R>(handler: (pool: pg.Pool) => Promise<R>): Promise<R> {
  const config: ServerConfig = await getLocalConfig();

  const poolConfig: pg.PoolConfig = {
    user: config.user,
    password: config.password,
    host: config.host,
    port: config.port,
    database: config.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);

  let result: R;
  try {
    result = await handler(pool);
  } catch (err) {
    await pool.end();
    throw err;
  }
  await pool.end();
  return result;
}
