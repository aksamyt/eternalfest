import { AuthContext as EtwinAuthContext } from "@eternal-twin/core/lib/auth/auth-context.js";
import { AuthType } from "@eternal-twin/core/lib/auth/auth-type.js";
import { EtwinClientService } from "@eternal-twin/core/lib/etwin-client/service.js";
import { OauthClientService } from "@eternal-twin/http-oauth-client";
import { $AuthContext, AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { OauthAccessToken } from "@eternalfest/api-core/lib/oauth/oauth-access-token.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { KoaAuth, SESSION_COOKIE } from "@eternalfest/rest-server/lib/koa-auth.js";
import Koa from "koa";
import koaRoute from "koa-route";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";

export interface Api {
  auth: AuthService;
  etwinClient: EtwinClientService;
  koaAuth: KoaAuth;
  oauthClient: OauthClientService;
  session: SessionService;
}

export async function createOauthRouter(api: Api): Promise<Koa> {
  const router: Koa = new Koa();

  router.use(koaRoute.get("/callback", onAuthorizationGrant));

  async function onAuthorizationGrant(cx: Koa.Context): Promise<void> {
    if (cx.request.query.error !== undefined) {
      cx.response.body = {error: cx.request.query.error};
    }
    const code: unknown = cx.request.query.code;
    const state: unknown = cx.request.query.state;
    if (typeof code !== "string" || typeof state !== "string") {
      cx.response.body = {error: "InvalidRequest: Both `code` and `state` are required."};
      return;
    }

    let accessToken: OauthAccessToken;
    try {
      accessToken = await api.oauthClient.getAccessToken(code);
    } catch (e) {
      console.error(e);
      console.log(e.status);
      console.log(e.response?.body);
      if (e.status === 401 && e.response.body && e.response.body && e.response.body.error === "Unauthorized") {
        cx.response.body = {
          error: "Unauthorized",
          cause: e.response.body.cause === "CodeExpiredError" ? "CodeExpiredError" : "AuthorizationServerError",
        };
        cx.response.status = 401;
      } else {
        console.error(e);
        cx.response.status = 503;
      }
      return;
    }

    const etwinAuth: EtwinAuthContext = await api.etwinClient.getAuthSelf(accessToken.accessToken);
    if (etwinAuth.type !== AuthType.AccessToken) {
      throw new Error("UnexpectedEtwinAuthType");
    }

    const auth: AuthContext = await api.auth.etwinOauth(etwinAuth.user.id, etwinAuth.user.displayName.current.value);
    switch (auth.type) {
      case ActorType.User: {
        const session: Session = await api.session.createSession(auth, auth.userId);
        cx.cookies.set(SESSION_COOKIE, session.id);
        cx.response.body = $AuthContext.write(JSON_VALUE_WRITER, auth);
        break;
      }
      default:
        throw new Error("UnexpectedAuthType");
    }

    cx.redirect("/");
  }

  return router;
}
