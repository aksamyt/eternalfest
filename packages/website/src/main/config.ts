import { default as dotEnv } from "dotenv";
import findUp from "find-up";
import fs from "fs";
import furi from "furi";
import sysPath from "path";
import url from "url";

export interface ServerConfig {
  httpPort: number;
  externalBaseUri: url.URL;
  readonly user: string;
  readonly password: string;
  readonly host: string;
  readonly port: number;
  readonly name: string;
  readonly dataRoot: url.URL;
  secret: string;
  etwinClientId: string;
  etwinUri: url.URL;
  etwinOauthSecret: string;
}

const DEFAULT_HTTP_PORT: number = 50320;

export function getConfigFromEnv(cwd: url.URL, env: NodeJS.ProcessEnv): ServerConfig {
  let httpPort: number;
  if (typeof env.EF_HTTP_PORT === "string") {
    httpPort = parseInt(env.EF_HTTP_PORT, 10);
  } else {
    httpPort = DEFAULT_HTTP_PORT;
  }
  let externalBaseUri: url.URL;
  if (typeof env.EF_EXTERNAL_BASE_URI === "string") {
    externalBaseUri = new url.URL(env.EF_EXTERNAL_BASE_URI);
  } else {
    externalBaseUri = new url.URL("http://localhost");
    externalBaseUri.port = httpPort.toString(10);
  }
  let dbName: string;
  if (typeof env.EF_DB_NAME === "string") {
    dbName = env.EF_DB_NAME;
  } else {
    throw new Error("Missing EF_DB_NAME value");
  }
  let dbUser: string;
  if (typeof env.EF_DB_USER === "string") {
    dbUser = env.EF_DB_USER;
  } else {
    throw new Error("Missing EF_USER value");
  }
  let dbPassword: string;
  if (typeof env.EF_DB_PASSWORD === "string") {
    dbPassword = env.EF_DB_PASSWORD;
  } else {
    throw new Error("Missing EF_DB_PASSWORD value");
  }
  let dbHost: string;
  if (typeof env.EF_DB_HOST === "string") {
    dbHost = env.EF_DB_HOST;
  } else {
    throw new Error("Missing EF_DB_HOST value");
  }
  let dbPort: number;
  if (typeof env.EF_DB_PORT === "string") {
    dbPort = parseInt(env.EF_DB_PORT, 10);
  } else {
    throw new Error("Missing EF_DB_PORT value");
  }
  let dataRoot: url.URL;
  if (typeof env.EF_DATA_ROOT === "string") {
    const relativeRoot: string = env.EF_DATA_ROOT;
    const cwdSysPath: string = furi.toSysPath(cwd);
    const dataRootSysPath: string = sysPath.resolve(cwdSysPath, relativeRoot);
    dataRoot = furi.fromSysPath(dataRootSysPath);
  } else {
    throw new Error("Missing EF_DATA_ROOT value");
  }
  let secret: string;
  if (typeof env.EF_SECRET === "string") {
    secret = env.EF_SECRET;
  } else {
    throw new Error("Missing EF_SECRET value");
  }
  let etwinClientId: string;
  if (typeof env.EF_ETWIN_CLIENT_ID === "string") {
    etwinClientId = env.EF_ETWIN_CLIENT_ID;
  } else {
    throw new Error("Missing EF_ETWIN_CLIENT_ID value");
  }
  let etwinUri: url.URL;
  if (typeof env.EF_ETWIN_URI === "string") {
    etwinUri = new url.URL(env.EF_ETWIN_URI);
  } else {
    throw new Error("Missing EF_ETWIN_URI value");
  }
  let etwinOauthSecret: string;
  if (typeof env.EF_ETWIN_OAUTH_SECRET === "string") {
    etwinOauthSecret = env.EF_ETWIN_OAUTH_SECRET;
  } else {
    throw new Error("Missing EF_ETWIN_OAUTH_SECRET value");
  }

  return {
    httpPort,
    externalBaseUri,
    name: dbName,
    user: dbUser,
    password: dbPassword,
    host: dbHost,
    port: dbPort,
    dataRoot,
    secret,
    etwinClientId,
    etwinUri,
    etwinOauthSecret,
  };
}

export async function getLocalConfig(): Promise<ServerConfig> {
  const dotEnvPath: string | undefined = await findUp(".env", {cwd: furi.toSysPath(import .meta.url)});
  if (dotEnvPath !== undefined) {
    const dotEnvText: string = await fs.promises.readFile(dotEnvPath, {encoding: "utf-8"});
    const parsedDotEnv: Record<string, string> = dotEnv.parse(dotEnvText);
    for (const [key, value] of Object.entries(parsedDotEnv)) {
      Reflect.set(process.env, key, value);
    }
  }
  const cwdUri: url.URL = furi.fromSysPath(process.cwd());
  return getConfigFromEnv(cwdUri, process.env);
}
