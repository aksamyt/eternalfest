import findUp from "find-up";
import fs from "fs";
import furi from "furi";
import module from "module";
import url from "url";

import {
  DependencyResolutionMaxDepth,
  DependencyResolutionMaxIterLimit,
  PackageNotFound,
  PackageRootNotFound
} from "./errors.js";

interface PackageLocations {
  uri: url.URL;
  packageJsonUri: url.URL;
}

/**
 * Represents a resolved patchman package: either the root package or a `@patchman/` library.
 */
export interface ResolvedPatchmanPackage {
  /**
   * URI for the root directory of the package.
   *
   * The root directory is the directory containing the `package.json` file.
   */
  uri: url.URL;

  /**
   * URI for the `package.json` file.
   */
  packageJsonUri: url.URL;

  /**
   * Boolean indicating that this package is the root package (may be a non `@patchman/` package)
   */
  isRoot: boolean;

  /**
   * Full package name
   *
   * Example:
   * - `@patchman/merlin`.
   * - `@eternalfest-games/hackfest`
   */
  name: string;

  package: PackageJson;
}

/**
 * `package.json` data for any npm package
 */
export interface PackageJson {
  /**
   * Full name of the package
   *
   * Examples:
   * - `@eternalfest-games/hackfest`
   * - `@patchman/merlin`
   */
  name: string;

  /**
   * Package version (semver string)
   */
  version: string;

  dependencies?: DependencyRecord;
  devDependencies?: DependencyRecord;
  peerDependencies?: DependencyRecord;
}

export type DependencyRecord = Record<string, string>;

export class PatchmanDependencyGraph {
  private root: url.URL;
  private packages: Map<string, ResolvedPatchmanPackage>;
  private deps: Map<string, DependencyEdge[]>;
  private revDeps: Map<string, DependencyEdge[]>;

  public constructor(root: url.URL, packages: Map<string, ResolvedPatchmanPackage>, deps: Map<string, DependencyEdge[]>) {
    const revDeps = new Map();
    for (const edges of deps.values()) {
      for (const edge of edges) {
        let revEdges: DependencyEdge[] | undefined = revDeps.get(edge.dest.uri.toString());
        if (revEdges === undefined) {
          revEdges = [];
          revDeps.set(edge.dest.uri.toString(), revEdges);
        }
        revEdges.push(edge);
      }
    }
    this.root = root;
    this.packages = packages;
    this.deps = deps;
    this.revDeps = revDeps;
  }

  public getPackages(): ResolvedPatchmanPackage[] {
    return [...this.packages.values()];
  }

  public getRoot(): url.URL {
    return this.root;
  }

  public getDeps(pkg: url.URL): DependencyEdge[] {
    const deps: DependencyEdge[] | undefined = this.deps.get(pkg.toString());
    if (deps === undefined) {
      throw new Error(`UnknownPackage: ${pkg.toString()}`);
    }
    return deps;
  }

  public getRevDeps(pkg: url.URL): DependencyEdge[] {
    const revDeps: DependencyEdge[] | undefined = this.revDeps.get(pkg.toString());
    if (revDeps === undefined) {
      throw new Error(`UnknownPackage: ${pkg.toString()}`);
    }
    return revDeps;
  }
}

enum DependencyEdgeType {
  Simple,
  Dev,
  Peer,
}

interface DependencyEdge {
  source: PackageLocations;
  dest: PackageLocations;
  destName: string;
  version: string;
  kind: DependencyEdgeType;
}

export async function resolvePatchmanDependencies(base: url.URL): Promise<PatchmanDependencyGraph> {
  // Map from package `uri.toString()` to resolved package
  const resolved: Map<string, ResolvedPatchmanPackage> = new Map();
  const root: PackageLocations = await findPackageRoot(base);
  // Map from `uri.toString()` to forward edges
  const deps: Map<string, DependencyEdge[]> = new Map();

  // Map from `uri.toString` to package depth (shortest amount of edges to the root)
  const openSet: Map<string, {locs: PackageLocations, depth: number}> = new Map();
  openSet.set(root.uri.toString(), {locs: root, depth: 0});

  let iterCount: number = 0;
  const ITER_LIMT: number = 1000;
  const DEPTH_LIMT: number = 10;
  for (; iterCount < ITER_LIMT; iterCount++) {
    const next: [string, {locs: PackageLocations, depth: number}] | undefined = unshiftMap(openSet);
    if (next === undefined) {
      break;
    }
    const {locs: base, depth} = next[1];
    const {package: pkg, edges} = await loadPackage(base, depth === 0);
    resolved.set(base.uri.toString(), pkg);
    deps.set(base.uri.toString(), edges);
    for (const edge of edges) {
      if (resolved.has(edge.dest.uri.toString()) || openSet.has(edge.dest.uri.toString())) {
        continue;
      }
      if (depth >= DEPTH_LIMT) {
        throw new DependencyResolutionMaxDepth();
      }
      openSet.set(edge.dest.uri.toString(), {locs: edge.dest, depth: depth + 1});
    }
  }
  if (iterCount === ITER_LIMT) {
    throw new DependencyResolutionMaxIterLimit();
  }

  return new PatchmanDependencyGraph(root.uri, resolved, deps);
}

function unshiftMap<K, V>(map: Map<K, V>): [K, V] | undefined {
  const result: [K, V] | undefined = map[Symbol.iterator]().next().value;
  if (result !== undefined) {
    map.delete(result[0]);
  }
  return result;
}

async function loadPackage(base: PackageLocations, withDevDeps: boolean): Promise<{package: ResolvedPatchmanPackage, edges: DependencyEdge[]}> {
  const rawPkg: unknown = await readJsonFile(base.packageJsonUri);
  const pkg: PackageJson = readPackageJson(rawPkg);
  const edges: DependencyEdge[] = [];
  if (pkg.dependencies !== undefined) {
    for await (const edge of resolveDependencyRecord(base, pkg.dependencies, DependencyEdgeType.Simple)) {
      edges.push(edge);
    }
  }
  if (pkg.peerDependencies !== undefined) {
    for await (const edge of resolveDependencyRecord(base, pkg.peerDependencies, DependencyEdgeType.Peer)) {
      edges.push(edge);
    }
  }
  if (withDevDeps && pkg.devDependencies !== undefined) {
    for await (const edge of resolveDependencyRecord(base, pkg.devDependencies, DependencyEdgeType.Dev)) {
      edges.push(edge);
    }
  }
  const resolvedPackage: ResolvedPatchmanPackage = {
    ...base,
    isRoot: withDevDeps,
    package: pkg,
    name: pkg.name,
  };
  return {package: resolvedPackage, edges};
}

const PATCHMAN_DEPENDENCY: RegExp = /^@patchman\/\S+$/;

async function* resolveDependencyRecord(base: PackageLocations, record: DependencyRecord, kind: DependencyEdgeType): AsyncIterable<DependencyEdge> {
  for (const [depName, depVersion] of Object.entries(record)) {
    if (!PATCHMAN_DEPENDENCY.test(depName)) {
      continue;
    }
    const depLocations: PackageLocations = await resolvePackageFrom(base.uri, depName);
    const edge: DependencyEdge = {
      source: base,
      dest: depLocations,
      destName: depName,
      version: depVersion,
      kind,
    };
    yield edge;
  }
}

/**
 * Resolves the package `name` from the directory `base`.
 *
 * @param base Base directory from which the package is imported.
 * @param name Package to resolve.
 */
async function resolvePackageFrom(base: url.URL, name: string): Promise<PackageLocations> {
  const innerImporterUri: url.URL = furi.join(base, ["package.json"]);
  const packageJsonSpecifier: string = `${name}/package.json`;
  const requireFn: NodeRequire = module.createRequire(innerImporterUri);
  let resolvedPackagePath: string;
  try {
    resolvedPackagePath = requireFn.resolve(packageJsonSpecifier);
  } catch (err) {
    throw new PackageNotFound(name, base, err);
  }
  const packageJsonUri: url.URL = furi.fromSysPath(resolvedPackagePath);
  const uri: url.URL = furi.join(packageJsonUri, "..");
  return {uri, packageJsonUri};
}

async function findPackageRoot(base: url.URL): Promise<PackageLocations> {
  const packageJsonPath: string | undefined = await findUp("package.json", {cwd: furi.toSysPath(base)});
  if (packageJsonPath === undefined) {
    throw new PackageRootNotFound(base);
  }
  const packageJsonUri: url.URL = furi.fromSysPath(packageJsonPath);
  const uri: url.URL = furi.join(packageJsonUri, "..");
  return {uri, packageJsonUri};
}

// async function resolvePatchmanManifest(dirUrl: url.URL): Promise<ResolvedPatchmanDependency> {
//   const packageJsonUrl: url.URL = furi.join(dirUrl, ["package.json"]);
//   const packageJsonStr: string = await readTextFile(packageJsonUrl);
//   const packageJson: unknown = JSON.parse(packageJsonStr);
//   if (typeof packageJson !== "object" || packageJson === null) {
//     throw new Error(`InvalidPackageJson: Not a document: ${packageJsonUrl}`);
//   }
//   const name: unknown = Reflect.get(packageJson, "name");
//   if (name === undefined) {
//     throw new Error(`InvalidPackageJson: Missing "name" field: ${packageJsonUrl}`);
//   }
//   if (typeof name !== "string") {
//     throw new Error(`InvalidPackageJson: Not a string ("name"): ${packageJsonUrl}`);
//   }
//
//   const dependencies: ReadonlyMap<string, string> = readDependencies(packageJson, "dependencies");
//   const devDependencies: ReadonlyMap<string, string> = readDependencies(packageJson, "devDependencies");
//   const peerDependencies: ReadonlyMap<string, string> = readDependencies(packageJson, "peerDependencies");
//
//   const rawPatchmanData: unknown = Reflect.get(packageJson, "patchman");
//   let patchmanData: object;
//   if (rawPatchmanData === undefined) {
//     // throw new Error(`InvalidPackageJson: Missing "patchman" field: ${packageJsonUrl}`);
//     patchmanData = {};
//   } else if (typeof rawPatchmanData !== "object" || rawPatchmanData === null) {
//     throw new Error(`InvalidPackageJson: Not a document ("patchman"): ${packageJsonUrl}`);
//   } else {
//     patchmanData = rawPatchmanData;
//   }
//   const rawHaxeData: unknown = Reflect.get(patchmanData, "haxe");
//   const classPath: url.URL[] = [];
//   if (rawHaxeData === undefined) {
//     classPath.push(dirUrl);
//   } else if (typeof rawHaxeData === "string") {
//     classPath.push(new url.URL(urlJoin(dirUrl.toString(), rawHaxeData)));
//   } else {
//     throw new Error(`InvalidPackageJson: Not a string ("patchman.haxe"): ${packageJsonUrl}`);
//   }
//   return {url: dirUrl, name, haxe: {classPath}, dependencies, devDependencies, peerDependencies};
// }

function readPackageJson(raw: unknown): PackageJson {
  if (typeof raw !== "object" || raw === null) {
    throw new Error("InvalidPackageJson: Not a record");
  }
  const name: unknown = Reflect.get(raw, "name");
  if (typeof name !== "string") {
    if (name === undefined) {
      throw new Error("InvalidPackageJson: Missing \"name\" field");
    } else {
      throw new Error("InvalidPackageJson: \"name\" must be a string");
    }
  }
  const version: unknown = Reflect.get(raw, "version");
  if (typeof version !== "string") {
    if (version === undefined) {
      throw new Error("InvalidPackageJson: Missing \"version\" field");
    } else {
      throw new Error("InvalidPackageJson: \"version\" must be a string");
    }
  }
  // TODO: test dependency fields
  return raw as PackageJson;
}

async function readJsonFile(path: fs.PathLike): Promise<unknown> {
  const content: string = await fs.promises.readFile(path, {encoding: "utf-8"});
  return JSON.parse(content);
}

export function testPackageUniqueness(graph: PatchmanDependencyGraph): void {
  const packagesByName: Map<string, ResolvedPatchmanPackage[]> = new Map();
  for (const pkg of graph.getPackages()) {
    let nameGroup: ResolvedPatchmanPackage[] | undefined = packagesByName.get(pkg.name);
    if (nameGroup === undefined) {
      nameGroup = [];
      packagesByName.set(pkg.name, nameGroup);
    }
    nameGroup.push(pkg);
  }
  for (const [pkgName, pkgGroup] of packagesByName) {
    if (pkgGroup.length >= 2) {
      throw new Error(`DuplicatePackage: ${pkgName}: ${pkgGroup.map(p => p.uri.toString()).join(", ")}`);
    }
  }
}
