import url from "url";

export class PackageNotFound extends Error {
  /**
   * Full name of the missing package
   */
  packageName: string;

  /**
   * Base URI used to import the package
   */
  importerUri: url.URL;

  cause: Error;

  constructor(packageName: string, importerUri: url.URL, cause: Error) {
    super();
    this.packageName = packageName;
    this.importerUri = importerUri;
    this.cause = cause;
    this.message = `Failed to resolve ${packageName} from ${importerUri}`;
  }
}

/**
 * The package was found but failed validation
 */
export class InvalidPackage extends Error {
  packageName: string;

  uri: url.URL;

  packageJsonUri: url.URL;

  importerUri: url.URL;

  constructor(packageName: string, uri: url.URL, packageJsonUri: url.URL, importerUri: url.URL) {
    super();
    this.packageName = packageName;
    this.uri = uri;
    this.packageJsonUri = packageJsonUri;
    this.importerUri = importerUri;
  }
}

export class DependencyResolutionMaxDepth extends Error {
  constructor() {
    super();
    this.message = "Reached maximum dependency resolution depth";
  }
}

export class DependencyResolutionMaxIterLimit extends Error {
  constructor() {
    super();
    this.message = "Reached maximum dependency resolution iteration limit";
  }
}

export class PackageRootNotFound extends Error {
  uri: url.URL;

  constructor(uri: url.URL) {
    super();
    this.uri = uri;
    this.message = `Failed to resolve package root from ${uri}`;
  }
}
