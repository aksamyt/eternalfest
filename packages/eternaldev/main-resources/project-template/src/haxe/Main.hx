import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import patchman.IPatch;
import patchman.Patchman;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    debug: Debug,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
