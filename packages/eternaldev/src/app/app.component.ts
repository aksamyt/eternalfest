import { HttpClient } from "@angular/common/http";
import { Component } from "@angular/core";

@Component({
  selector: "efd-app",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  private http: HttpClient;

  constructor(http: HttpClient) {
    this.http = http;
  }

  async openGameBundler(): Promise<void> {
    console.log("bundler");
    await this.http.post("/api/v1/devkit/bundler", {}).toPromise();
  }

  async openLevelEditor(): Promise<void> {
    console.log("editor");
    await this.http.post("/api/v1/devkit/editor", {}).toPromise();
  }
}
