import { NgModule } from "@angular/core";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { ServerModule, ServerTransferStateModule } from "@angular/platform-server";

import { ServerApiModule } from "../modules/api/api.module.server";
import { AppComponent } from "./app.component";
import { AppModule } from "./app.module";

@NgModule({
  providers: [],
  imports: [
    AppModule,
    NoopAnimationsModule,
    ServerApiModule,
    ServerModule,
    ServerTransferStateModule,
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {
}
