import { ChangeDetectorRef, Component, EventEmitter, Input, Output } from "@angular/core";
import { GameMode } from "@eternalfest/api-core/lib/game/game-mode";
import { RunSettings } from "@eternalfest/api-core/lib/run/run-settings";
import { CreateDebugRunOptions } from "../../universal/create-debug-run-options";
import { ProfileKey } from "../../universal/profile-key";

@Component({
  selector: "efd-run-options",
  templateUrl: "./run-options.component.html",
  styleUrls: ["./run-options.component.scss"],
})
export class RunOptionsComponent {
  @Input("modes")
  modes: ReadonlyArray<GameMode> | undefined;

  @Input("profiles")
  profiles: ReadonlyArray<ProfileKey> | undefined;

  @Output()
  efdSubmit: EventEmitter<CreateDebugRunOptions>;

  set modeKey(modeKey: string | undefined) {
    this._modeKey = modeKey;
    this._mode = undefined;
    if (modeKey !== undefined && Array.isArray(this.modes)) {
      for (const mode of this.modes!) {
        if (mode.key === modeKey) {
          if (mode !== this._mode) {
            this.optionIds.clear();
          }
          this._mode = mode;
        }
      }
    }
    this.changeDetectorRef.detectChanges();
  }

  get modeKey(): string | undefined {
    return this._modeKey;
  }

  set mode(mode: GameMode | undefined) {
    if (mode !== this._mode) {
      this.optionIds.clear();
    }
    this._mode = mode;
    this._modeKey = mode === undefined ? undefined : mode.key;
  }

  get mode(): GameMode | undefined {
    return this._mode;
  }

  optionIds: Set<string>;

  detail: boolean;
  shake: boolean;
  sound: boolean;
  music: boolean;

  profileKey: string;

  private changeDetectorRef: ChangeDetectorRef;
  private _modeKey: string | undefined;
  private _mode: GameMode | undefined;

  constructor(changeDetectorRef: ChangeDetectorRef) {
    this.changeDetectorRef = changeDetectorRef;
    this.optionIds = new Set();
    this.efdSubmit = new EventEmitter();
    this.detail = false;
    this.shake = false;
    this.sound = false;
    this.music = false;
    this.profileKey = "default";
  }

  readOptions(): CreateDebugRunOptions {
    if (this._mode === undefined) {
      throw new Error("No mode");
    }
    const mode: GameMode = this._mode;
    const settings: RunSettings = {
      sound: this.sound,
      music: this.music,
      shake: this.shake,
      detail: this.detail,
      volume: 100,
    };

    const gameOptions: ReadonlyArray<string> = [...this.optionIds];

    return {
      gameId: "",
      userId: "",
      gameMode: mode.key,
      gameOptions,
      settings,
      profile: this.profileKey,
    };
  }

  onSubmit(event: Event): void {
    event.preventDefault();
    const result: CreateDebugRunOptions = this.readOptions();
    this.efdSubmit.next(result);
  }

  onOptionChange(event: Event): void {
    event.preventDefault();
    const target: HTMLInputElement = event.target as HTMLInputElement;
    if (target.checked) {
      this.optionIds.add(target.value);
    } else {
      this.optionIds.delete(target.value);
    }
  }
}
