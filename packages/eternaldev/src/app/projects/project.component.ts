import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options";
import { Run } from "@eternalfest/api-core/lib/run/run";
import { UuidHex } from "kryo/lib/uuid-hex";

import { GameService } from "../../modules/api/game.service";
import { ProjectService } from "../../modules/api/project.service";
import { RunService } from "../../modules/api/run.service";
import { CreateDebugRunOptions } from "../../universal/create-debug-run-options";
import { ProfileKey } from "../../universal/profile-key";
import { Project } from "../../universal/project";

@Component({
  selector: "efd-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.scss"],
})
export class ProjectComponent implements OnInit {
  project: Project | undefined;
  projectProfileKeys: ReadonlyArray<ProfileKey>;
  game: Game | undefined;
  runOptions: CreateRunOptions | undefined;
  run: Run | undefined;

  private readonly activatedRoute: ActivatedRoute;
  private readonly changeDetectorRef: ChangeDetectorRef;
  private readonly gameService: GameService;
  private readonly projectService: ProjectService;
  private readonly runService: RunService;

  constructor(
    activatedRoute: ActivatedRoute,
    changeDetectorRef: ChangeDetectorRef,
    gameService: GameService,
    projectService: ProjectService,
    runService: RunService,
  ) {
    this.activatedRoute = activatedRoute;
    this.gameService = gameService;
    this.changeDetectorRef = changeDetectorRef;
    this.projectService = projectService;
    this.runService = runService;

    this.project = undefined;
    this.projectProfileKeys = [];
    this.game = undefined;
    this.runOptions = undefined;
  }

  ngOnInit(): void {
    this.activatedRoute.params
      .subscribe(async (params: Params) => {
        const {project_id: UuidHex} = params;
        this.game = await this.gameService.getGameById(params.project_id);
        this.project = await this.projectService.getProjectById(params.project_id);
        this.projectProfileKeys = this.project !== undefined ? [...this.project.profiles.keys()] : [];
        this.changeDetectorRef.detectChanges();
      });
  }

  async onRunOptions(event: CreateDebugRunOptions): Promise<void> {
    this.runOptions = event;
    const createRunOptions: CreateDebugRunOptions = {
      ...event,
      userId: "00000000-0000-0000-0000-000000000000",
      gameId: this.game!.id,
    };
    this.run = await this.runService.createDebugRun(createRunOptions);
    // console.log("CREATED");
    // console.log(this.run);
    this.changeDetectorRef.detectChanges();
  }
}
