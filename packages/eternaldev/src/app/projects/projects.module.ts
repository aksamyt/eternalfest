import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HelpersModule } from "../../modules/helpers/helpers.module";
import { ConsoleModule } from "../console/console.module";
import { GameModule } from "../game/game.module";
import { ProjectComponent } from "./project.component";
import { ProjectsListComponent } from "./projects-list.component";
import { ProjectsRoutingModule } from "./projects-routing.module";
import { RunOptionsComponent } from "./run-options.component";

@NgModule({
  declarations: [ProjectComponent, ProjectsListComponent, RunOptionsComponent],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
    GameModule,
    ConsoleModule,
    ProjectsRoutingModule,
  ],
})
export class ProjectsModule {
}
