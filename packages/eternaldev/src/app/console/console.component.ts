import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";

import { ConsoleRow } from "../../modules/console/console-row";
import { ConsoleService } from "../../modules/console/console.service";

@Component({
  selector: "efd-console",
  templateUrl: "./console.component.html",
  styleUrls: ["./console.component.scss"],
})
export class ConsoleComponent implements OnInit, OnDestroy {
  rows: ReadonlyArray<ConsoleRow>;

  private readonly consoleService: ConsoleService;
  private rowsSubscription?: Subscription;

  constructor(consoleService: ConsoleService) {
    this.consoleService = consoleService;
    this.rows = [];
  }

  ngOnInit(): void {
    this.rowsSubscription = this.consoleService.observeRows()
      .subscribe((rows) => {
        this.rows = rows;
      });
  }

  ngOnDestroy(): void {
    if (this.rowsSubscription !== undefined) {
      this.rowsSubscription.unsubscribe();
    }
  }
}
