import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";

import { TextRow } from "../../modules/console/console-row";
import { ConsoleService } from "../../modules/console/console.service";

@Component({
  selector: "efd-print-console",
  templateUrl: "./print-console.component.html",
  styleUrls: ["./print-console.component.scss"],
})
export class PrintConsoleComponent implements OnInit, OnDestroy {
  rows: ReadonlyArray<TextRow>;

  private readonly consoleService: ConsoleService;
  private rowsSubscription?: Subscription;

  constructor(consoleService: ConsoleService) {
    this.consoleService = consoleService;
    this.rows = [];
  }

  ngOnInit(): void {
    this.rowsSubscription = this.consoleService.observePrintRows()
      .subscribe((rows) => {
        this.rows = rows;
      });
  }

  ngOnDestroy(): void {
    if (this.rowsSubscription !== undefined) {
      this.rowsSubscription.unsubscribe();
    }
  }
}
