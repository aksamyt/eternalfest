import { $GameModeKey, GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { $GameOptionKey, GameOptionKey } from "@eternalfest/api-core/lib/game/game-option-key.js";
import { $RunSettings, RunSettings } from "@eternalfest/api-core/lib/run/run-settings.js";
import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $ProfileKey, ProfileKey } from "./profile-key.js";

export interface CreateDebugRunOptions {
  readonly gameId: UuidHex;
  readonly userId: UuidHex;
  readonly gameMode: GameModeKey;
  readonly gameOptions: ReadonlyArray<GameOptionKey>;
  readonly settings: RunSettings;
  readonly profile?: ProfileKey;
}

export const $CreateDebugRunOptions: RecordIoType<CreateDebugRunOptions> = new RecordType<CreateDebugRunOptions>({
  properties: {
    gameId: {type: $UuidHex},
    userId: {type: $UuidHex},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 10})},
    settings: {type: $RunSettings},
    profile: {type: $ProfileKey, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
