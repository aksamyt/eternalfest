import { $FamiliesString, FamiliesString } from "@eternalfest/api-core/lib/game/families-string.js";
import { $GameDescription, GameDescription } from "@eternalfest/api-core/lib/game/game-description.js";
import { $GameDisplayName, GameDisplayName } from "@eternalfest/api-core/lib/game/game-display-name.js";
import { $GameEngines, GameEngines } from "@eternalfest/api-core/lib/game/game-engines.js";
import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $ManifestMode, ManifestMode } from "./manifest-mode.js";
import { $ManifestResource, ManifestResource } from "./manifest-resource.js";
import { $ProfileMap, ProfileMap } from "./profile-map.js";
import { $UrlFragment, UrlFragment } from "./url-fragment.js";

export interface ProjectManifest {
  remoteId?: UuidHex;
  displayName?: GameDisplayName;
  description?: GameDescription;
  icon?: UrlFragment;
  engines?: GameEngines;
  resources?: ReadonlyArray<ManifestResource>;
  families?: FamiliesString;
  modes?: readonly ManifestMode[];
  profiles?: ProfileMap;
}

export const $ProjectManifest: RecordIoType<ProjectManifest> = new RecordType<ProjectManifest>({
  properties: {
    remoteId: {type: $UuidHex, optional: true},
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    icon: {type: $UrlFragment, optional: true},
    engines: {type: $GameEngines, optional: true},
    resources: {
      type: new ArrayType({itemType: $ManifestResource, maxLength: Infinity}),
      optional: true,
    },
    families: {type: $FamiliesString, optional: true},
    modes: {type: new ArrayType({itemType: $ManifestMode, maxLength: Infinity}), optional: true},
    profiles: {type: $ProfileMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
  rename: {displayName: "name"},
});
