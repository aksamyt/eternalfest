import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

import { $UrlFragment, UrlFragment } from "./url-fragment.js";

export interface ManifestResource {
  readonly path: UrlFragment;
  readonly tag?: string;
  readonly priority?: string;
}

export const $ManifestResource: RecordIoType<ManifestResource> = new RecordType<ManifestResource>({
  properties: {
    path: {type: $UrlFragment},
    tag: {type: new Ucs2StringType({maxLength: 50}), optional: true},
    priority: {type: new Ucs2StringType({maxLength: 50}), optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
