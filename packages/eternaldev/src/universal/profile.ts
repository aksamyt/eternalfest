import { $RunItems, RunItems } from "@eternalfest/api-core/lib/run/run-items.js";
import { $UserRef, UserRef } from "@eternalfest/api-core/lib/user/user-ref.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

export interface Profile {
  readonly user: UserRef;
  readonly items: RunItems;
}

export const $Profile: RecordIoType<Profile> = new RecordType<Profile>({
  properties: {
    user: {type: $UserRef, optional: true},
    items: {type: $RunItems, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
