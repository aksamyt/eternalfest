import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type UrlFragment = string;

export const $UrlFragment: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
