import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export interface LocalFileRef {
  url: string;
}

export const $LocalFileRef: RecordIoType<LocalFileRef> = new RecordType<LocalFileRef>({
  properties: {
    url: {type: new Ucs2StringType({maxLength: Infinity})},
  },
  changeCase: CaseStyle.SnakeCase,
});
