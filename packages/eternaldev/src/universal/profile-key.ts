import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type ProfileKey = string;

export const $ProfileKey: Ucs2StringType = new Ucs2StringType({maxLength: 50});
