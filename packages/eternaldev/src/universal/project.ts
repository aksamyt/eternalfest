import { $FamiliesString, FamiliesString } from "@eternalfest/api-core/lib/game/families-string.js";
import { $GameDescription, GameDescription } from "@eternalfest/api-core/lib/game/game-description.js";
import { $GameDisplayName, GameDisplayName } from "@eternalfest/api-core/lib/game/game-display-name.js";
import { $GameEngines, GameEngines } from "@eternalfest/api-core/lib/game/game-engines.js";
import { $GameMode, GameMode } from "@eternalfest/api-core/lib/game/game-mode.js";
import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $LocalFileRef, LocalFileRef } from "./local-file-ref.js";
import { $ManifestResource, ManifestResource } from "./manifest-resource.js";
import { $ProfileMap, ProfileMap } from "./profile-map.js";

export interface Project {
  id: UuidHex;
  localUrl: string;
  displayName: GameDisplayName;
  description?: GameDescription;
  icon?: LocalFileRef;
  remoteId?: UuidHex;
  engines: GameEngines;
  resources: ReadonlyArray<ManifestResource>;
  families: FamiliesString;
  gameModes: ReadonlyArray<GameMode>;
  profiles: ProfileMap;
}

export const $Project: RecordIoType<Project> = new RecordType<Project>({
  properties: {
    id: {type: $UuidHex},
    localUrl: {type: new Ucs2StringType({maxLength: Infinity})},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription, optional: true},
    icon: {type: $LocalFileRef, optional: true},
    remoteId: {type: $UuidHex, optional: true},
    engines: {type: $GameEngines},
    resources: {type: new ArrayType({itemType: $ManifestResource, maxLength: Infinity})},
    families: {type: $FamiliesString},
    gameModes: {type: new ArrayType({itemType: $GameMode, maxLength: Infinity})},
    profiles: {type: $ProfileMap},
  },
  changeCase: CaseStyle.SnakeCase,
});
