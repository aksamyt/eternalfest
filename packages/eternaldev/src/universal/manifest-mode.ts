import { $GameModeDisplayName, GameModeDisplayName } from "@eternalfest/api-core/lib/game/game-mode-display-name.js";
import { $GameModeKey, GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { $GameModeState, GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state.js";
import { DeepReadonly } from "@eternalfest/api-core/lib/types/deep-readonly.js";
import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $ManifestOption, ManifestOption } from "./manifest-option.js";

export interface ManifestMode {
  key?: GameModeKey;
  id?: GameModeKey;
  displayName?: GameModeDisplayName;
  state: GameModeState;
  options: ManifestOption[];
}

export type ReadonlyManifestMode = DeepReadonly<ManifestMode>;

export const $ManifestMode: RecordIoType<ManifestMode> = new RecordType<ManifestMode>({
  properties: {
    key: {type: $GameModeKey, optional: true},
    id: {type: $GameModeKey, optional: true},
    displayName: {type: $GameModeDisplayName, optional: true},
    state: {type: $GameModeState},
    options: {type: new ArrayType({itemType: $ManifestOption, maxLength: 50})},
  },
  changeCase: CaseStyle.SnakeCase,
});
