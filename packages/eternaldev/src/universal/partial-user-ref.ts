import { $UserDisplayName, UserDisplayName } from "@eternalfest/api-core/lib/user/user-display-name.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

export interface PartialUserRef {
  id?: UuidHex;

  displayName?: UserDisplayName;
}

export const $PartialUserRef: RecordIoType<PartialUserRef> = new RecordType<PartialUserRef>({
  properties: {
    id: {type: $UuidHex, optional: true},
    displayName: {type: $UserDisplayName, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
