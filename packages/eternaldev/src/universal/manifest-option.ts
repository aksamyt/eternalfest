import { $GameOptionDisplayName, GameOptionDisplayName } from "@eternalfest/api-core/lib/game/game-option-display-name.js";
import { $GameOptionKey, GameOptionKey } from "@eternalfest/api-core/lib/game/game-option-key.js";
import { $GameOptionState, GameOptionState } from "@eternalfest/api-core/lib/game/game-option-state.js";
import { DeepReadonly } from "@eternalfest/api-core/lib/types/deep-readonly.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

export interface ManifestOption {
  key?: GameOptionKey;
  id?: GameOptionKey;
  displayName?: GameOptionDisplayName;
  state: GameOptionState;
}

export type ReadonlyManifestOption = DeepReadonly<ManifestOption>;

export const $ManifestOption: RecordIoType<ManifestOption> = new RecordType<ManifestOption>({
  properties: {
    key: {type: $GameOptionKey, optional: true},
    id: {type: $GameOptionKey, optional: true},
    displayName: {type: $GameOptionDisplayName, optional: true},
    state: {type: $GameOptionState},
  },
  changeCase: CaseStyle.SnakeCase,
});
