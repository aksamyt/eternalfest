declare module "express-session" {
  import {EventEmitter} from "events";
  import {Request as ExpressRequest, RequestHandler} from "express";

  namespace session {
    export interface RequestExtension<SessionData extends {} = {[key: string]: any}> {
      session: Session<SessionData>;
      sessionID: string;
    }

    export type Request<SessionData extends {} = {[key: string]: any},
      Base extends ExpressRequest = ExpressRequest> = Base & RequestExtension<SessionData>;

    export namespace Cookie {
      export interface Options {
        domain?: string;
        expires?: Date;
        httpOnly?: boolean;
        maxAge?: number | null;
        originalMaxAge?: number | null;
        path?: string;
        sameSite?: true | false | "lax" | "strict";
        secure?: boolean;
      }

      export interface Data extends Options {
        httpOnly: boolean;
        maxAge: number | null;
        originalMaxAge: number | null;
        path: string;
      }

      export interface Json {
        domain?: string;
        /**
         * ISO date
         */
        expires?: string;
        httpOnly: boolean;
        maxAge: number | null;
        originalMaxAge: number | null;
        path: string;
        sameSite?: true | false | "lax" | "strict";
        secure?: boolean;
      }
    }

    export class Cookie implements Cookie.Data {
      /**
       * Specifies the value for the `Domain` `Set-Cookie` attribute. By default, no domain
       * is set, and most clients will consider the cookie to apply to only the current domain.
       */
      domain?: string;

      /**
       * Specifies the `Date` object to be the value for the `Expires` `Set-Cookie` attribute. By default, no
       * expiration is set, and most clients will consider this a "non-persistent cookie" and will delete it on a
       * condition like exiting a web browser application.
       */
      expires?: Date;

      /**
       * Specifies the `boolean` value for the `HttpOnly` `Set-Cookie` attribute. When truthy, the HttpOnly attribute
       * is set, otherwise it is not. By default, the HttpOnly attribute is set.
       */
      httpOnly: boolean;

      /**
       * Specifies the `number` (in milliseconds) to use when calculating the `Expires` `Set-Cookie` attribute. This
       * is done by taking the current server time and adding `maxAge` milliseconds to the value to calculate an
       * `Expires` datetime. By default, no maximum age is set.
       *
       * This value depends on the creation date of the cookie and current date.
       */
      maxAge: number | null;

      /**
       * Same as `maxAge` but does not change over time, this corresponds to the `maxAge` passed at the construction.
       */
      originalMaxAge: number | null;

      /**
       * Specifies the value for the `Path` `Set-Cookie`. By default, this is set to `'/'`, which is the root path of
       * the domain.
       */
      path: string;

      /**
       * Specifies the `boolean` or `string` to be the value for the `SameSite` `Set-Cookie` attribute.
       *
       * - `true` will set the `SameSite` attribute to `Strict` for strict same site enforcement.
       * - `false` will not set the `SameSite` attribute.
       * - `'lax'` will set the `SameSite` attribute to `Lax` for lax same site enforcement.
       * - `'strict'` will set the SameSite attribute to `Strict` for strict same site enforcement.
       *
       * More information about the different enforcement levels can be found in the specification
       * <https://tools.ietf.org/html/draft-west-first-party-cookies-07#section-4.1.1>
       *
       * **Note** This is an attribute that has not yet been fully standardized, and may change in the future. This
       * also means many clients may ignore this attribute until they understand it.
       */
      sameSite?: true | false | "lax" | "strict";

      /**
       * Specifies the `boolean` value for the `Secure` `Set-Cookie` attribute. When truthy, the `Secure` attribute
       * is set, otherwise it is not. By default, the `Secure` attribute is not set.
       *
       * **Note** be careful when setting this to true, as compliant clients will not send the cookie back to the
       * server in the future if the browser does not have an HTTPS connection.
       */
      secure?: boolean;

      constructor(options: Cookie.Options);

      /**
       * Serialize a cookie name-value pair into a `Set-Cookie` header string.
       *
       * @param name name for the cookie
       * @param value value to set the cookie to
       */
      serialize(name: string, value: string): string;

      /**
       * Return the JSON representation of this cookie.
       *
       * [In fact no, `expires` is a Date instead of a `string`]
       */
      toJSON(): Cookie.Data;
    }

    export interface Options {
      /**
       * Settings object for the session ID cookie.
       * The default value is `{ path: '/', httpOnly: true, secure: false, maxAge: null }`.
       */
      cookie?: Cookie.Options;

      /**
       * Function to call to generate a new session ID. Provide a function that returns a string that will be used as
       * a session ID. The function is given `req` as the first argument if you want to use some value attached to
       * `req` when generating the ID.
       *
       * The default value is a function which uses the `uid-safe` library to generate IDs.
       *
       * **NOTE** be careful to generate unique IDs so your sessions do not conflict.
       *
       * ```javascript
       * app.use(session({
       *   genid: function(req) {
       *     return genuuid() // use UUIDs for session IDs
       *   },
       *   secret: 'keyboard cat'
       * }))
       * ```
       */
      genid?: (req: Request) => string;

      /**
       * The name of the session ID cookie to set in the response (and read from in the request).
       *
       * The default value is `'connect.sid'`.
       *
       * **Note** if you have multiple apps running on the same hostname (this is just the name, i.e. `localhost` or
       * `127.0.0.1`; different schemes and ports do not name a different hostname), then you need to separate the
       * session cookies from each other. The simplest method is to simply set different names per app.
       */
      name?: string;

      /**
       * Trust the reverse proxy when setting secure cookies (via the "X-Forwarded-Proto" header).
       *
       * The default value is `undefined`.
       *
       * - `true` The "X-Forwarded-Proto" header will be used.
       * - `false` All headers are ignored and the connection is considered secure only if there is a direct
       *   TLS/SSL connection.
       * - `undefined` Uses the "trust proxy" setting from express
       */
      proxy?: boolean;

      /**
       * Forces the session to be saved back to the session store, even if the session was never modified during the
       * request. Depending on your store this may be necessary, but it can also create race conditions where a client
       * makes two parallel requests to your server and changes made to the session in one request may get overwritten
       * when the other request ends, even if it made no changes (this behavior also depends on what store you're
       * using).
       *
       * The default value is `true`, but using the default has been deprecated, as the default will change in the
       * future. Please research into this setting and choose what is appropriate to your use-case. Typically, you'll
       * want `false`.
       *
       * How do I know if this is necessary for my store? The best way to know is to check with your store if it
       * implements the `touch` method. If it does, then you can safely set `resave: false`. If it does not implement
       * the touch method and your store sets an expiration date on stored sessions, then you likely need
       * `resave: true`.
       */
      resave: boolean;

      /**
       * Force a session identifier cookie to be set on every response. The expiration is reset to the original
       * `maxAge`, resetting the expiration countdown.
       *
       * The default value is `false`.
       *
       * **Note** When this option is set to `true` but the `saveUninitialized` option is set to `false`, the cookie
       * will not be set on a response with an uninitialized session.
       */
      rolling?: boolean;

      /**
       * Forces a session that is "uninitialized" to be saved to the store. A session is uninitialized when it is new
       * but not modified. Choosing `false` is useful for implementing login sessions, reducing server storage usage,
       * or complying with laws that require permission before setting a cookie. Choosing `false` will also help with
       * race conditions where a client makes multiple parallel requests without a session.
       *
       * The default value is `true`, but using the default has been deprecated, as the default will change in the
       * future. Please research into this setting and choose what is appropriate to your use-case.
       *
       * **Note** if you are using Session in conjunction with PassportJS, Passport will add an empty Passport object
       * to the session for use after a user is authenticated, which will be treated as a modification to the session,
       * causing it to be saved. This has been fixed in PassportJS 0.3.0
       */
      saveUninitialized: boolean;

      /**
       * This is the secret used to sign the session ID cookie. This can be either a string for a single secret, or
       * an array of multiple secrets. If an array of secrets is provided, only the first element will be used to sign
       * the session ID cookie, while all the elements will be considered when verifying the signature in requests.
       */
      secret: string | string[];

      /**
       * The session store instance, defaults to a new `MemoryStore` instance.
       */
      store?: Store;

      /**
       * Control the result of unsetting req.session (through delete, setting to null, etc.).
       *
       * The default value is 'keep'.
       *
       * - `'destroy'` The session will be destroyed (deleted) when the response ends.
       * - `'keep'` The session in the store will be kept, but modifications made during the request are ignored
       *   and not saved.
       */
      unset?: "destroy" | "keep";
    }

    export namespace SessionBase {
      export interface Json {
        id: string;
        cookie: Cookie.Json;
      }
    }

    export namespace SessionBase {
      export interface Data {
        /**
         * Each session has a unique ID associated with it. This property is an alias of `req.sessionID` and cannot be
         * modified. It has been added to make the `session` ID accessible from the session object.
         */
        readonly id: string;

        /**
         * Each session has a unique cookie object accompany it. This allows you to alter the session cookie per
         * visitor. For example we can set `req.session.cookie.expires` to `false` to enable the cookie to remain for
         * only the duration of the user-agent.
         *
         * [Note: use `undefined` if you want this behavior]
         */
        cookie: Cookie;
      }
    }

    export interface SessionBase extends SessionBase.Data {
      /**
       * To regenerate the session simply invoke the method. Once complete, a new SID and Session instance will be
       * initialized at req.session and the callback will be invoked.
       *
       * ```javascript
       * req.session.regenerate(function(err) {
       *   // will have a new session here
       * })
       * ```
       */
      regenerate: (callback: (err: any) => void) => void;

      /**
       * Destroys the session and will unset the req.session property. Once complete, the callback will be invoked.
       *
       * ```javascript
       * req.session.destroy(function(err) {
       *   // cannot access session here
       * })
       * ```
       */
      destroy: (callback: (err: any) => void) => void;

      /**
       * Reloads the session data from the store and re-populates the `req.session` object. Once complete, the
       * `callback` will be invoked.
       *
       * ```javascript
       * req.session.reload(function(err) {
       *   // session updated
       * })
       */
      reload: (callback: (err: any) => void) => void;

      /**
       * Save the session back to the store, replacing the contents on the store with the contents in memory (though
       * a store may do something else--consult the store's documentation for exact behavior).
       *
       * This method is automatically called at the end of the HTTP response if the session data has been altered
       * (though this behavior can be altered with various options in the middleware constructor). Because of this,
       * typically this method does not need to be called.
       *
       * There are some cases where it is useful to call this method, for example, long- lived requests or in
       * WebSockets.
       *
       * ```javascript
       * req.session.save(function(err) {
       *   // session saved
       * })
       * ```
       */
      save: (callback: (err: any) => void) => void;

      /**
       * Updates the `.maxAge` property. Typically this is not necessary to call, as the session middleware does this
       * for you.
       */
      touch: (callback: (err: any) => void) => void;
    }

    export namespace Session {
      export type Json <SessionData extends {} = {[key: string]: any}> = SessionData & SessionBase.Json;
    }
    export type Session<SessionData extends {} = {[key: string]: any}> = SessionData & SessionBase;

    /* tslint:disable-next-line:variable-name */
    export const Session: {
      new <SessionData extends {} = {[key: string]: any}>(
        req: {sessionID: string, session?: Session<SessionData>},
        session: SessionBase.Data & SessionData,
      ): Session<SessionData>;
    };

    export interface StoreInterface<D extends {} = {[key: string]: any}> extends EventEmitter {
      /**
       * This recommended method is used to "touch" a given session given a session ID (sid) and session (session)
       * object.
       */
      touch?: (sid: string, session: Session<D>, cb: (err: any) => void) => void;

      /**
       * This optional method is used to get all sessions in the store as an array.
       */
      all?: (cb: (err: any) => void) => void;

      /**
       * This optional method is used to get the count of all sessions in the store.
       */
      length?: (cb: (err: any, length: number) => void) => void;

      /**
       * This optional method is used to delete all sessions from the store.
       */
      clear?: (cb: (err: any) => void) => void;

      /**
       * This required method is used to get a session from the store given a session ID (sid).
       */
      get(sid: string, cb: (err: any, session: Session.Json<D> | null) => void): void;

      /**
       * This required method is used to upsert a session into the store given a session ID (sid) and session
       * (session) object.
       */
      set(sid: string, session: Session<D>, cb: (err: any) => void): void;

      /**
       * This required method is used to destroy/delete a session from the store given a session ID (sid).
       */
      destroy(sid: string, cb: (err: any) => void): void;

      /**
       * Re-generate the given requests's session.
       */
      regenerate (req: Request, cb: (err: any) => void): void;

      /**
       * Load a `Session` instance via the given `sid` and invoke the callback `cb(err, session)`.
       */
      load (sid: string, cb: (err: any, session: Session<D>) => void): void;

      /**
       * Create session from JSON `session` data.
       */
      createSession (req: {sessionID: string, session?: Session<D>}, session: Session.Json<D>): Session<D>;
    }

    /**
     * Abstract base class for session stores.
     */
    export abstract class Store<D extends {} = {[key: string]: any}> extends EventEmitter {
      constructor();

      /**
       * Re-generate the given requests's session.
       */
      regenerate(req: Request, cb: (err: any) => void): void;

      /**
       * Load a `Session` instance via the given `sid` and invoke the callback `cb(err, session)`.
       */
      load(sid: string, cb: (err: any, session: Session<D>) => void): void;

      /**
       * Create session from JSON `session` data.
       */
      createSession(req: {sessionID: string, session?: Session<D>}, session: Session.Json<D>): Session<D>;
    }

    export class MemoryStore<D extends {} = {[key: string]: any}> extends Store<D> implements StoreInterface<D> {
      touch?: (sid: string, session: Session<D>, cb: (err: any) => void) => void;
      all?: (cb: (err: any) => void) => void;
      length?: (cb: (err: any, length: number) => void) => void;
      clear?: (cb: (err: any) => void) => void;

      get(sid: string, cb: (err: any, session: Session.Json<D>) => void): void;

      set(sid: string, session: Session<D>, cb: (err: any) => void): void;

      destroy(sid: string, cb: (err: any) => void): void;
    }
  }

  function session(options?: session.Options): RequestHandler;

  export = session;
}
