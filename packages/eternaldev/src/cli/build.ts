import efLoader from "@eternalfest-types/loader";
import { LocaleId } from "@eternalfest/api-core/lib/types/locale-id.js";
import { CompilerResult as Jcr } from "@eternalfest/efc/compiler-result";
import efcConfig from "@eternalfest/efc/java/config.js";
import efcProject from "@eternalfest/efc/project.js";
import furi from "furi";
import rxOp from "rxjs/operators/index.js";
import url from "url";

import { ManifestContent } from "../main/api/manifest/manifest-content.js";
import { ManifestLevelKey } from "../main/api/manifest/manifest-level-key.js";
import { ManifestLevel } from "../main/api/manifest/manifest-level.js";
import { ManifestLocale } from "../main/api/manifest/manifest-locale.js";
import { Manifest } from "../main/api/manifest/manifest.js";
import { CompilerResult as Cr } from "../main/api/project/compiler-result.js";
import { observeManifestByDir } from "../main/api/project/local-service.js";
import { outputFileAsync } from "../main/fs-utils.js";

export async function build(dir: url.URL): Promise<void> {
  const manifestCr: Cr<Manifest | undefined> = await observeManifestByDir(dir).pipe(rxOp.first()).toPromise();
  if (!manifestCr.hasValue()) {
    throw new Error(`NonProjectDirectory: ${dir}`);
  }
  const manifest: Manifest | undefined = manifestCr.unwrap();
  if (manifest === undefined) {
    throw new Error(`NonProjectDirectory: ${dir}`);
  }

  if (manifest.content === undefined) {
    throw new Error("Missing manifest section: content");
  }

  const content: ManifestContent = manifest.content;

  const assets: efcConfig.EfcAssetsConfig = {
    useBaseAssets: true,
    roots: [],
  };
  if (manifest.assets !== undefined) {
    (assets.roots as url.URL[]).push(furi.join(dir, manifest.assets));
  }

  const efcConfig: efcConfig.EfcConfig = {
    root: dir,
    levels: resolveSets(dir, content.levels),
    patcherData: furi.join(dir, content.data !== undefined ? content.data : "datas"),
    scoreItems: furi.join(dir, content.scoreItems),
    specialItems: furi.join(dir, content.specialItems),
    dimensions: furi.join(dir, content.dimensions),
    locales: resolveLocales(dir, manifest.locales),
    assets,
    prettyOutput: false,
    obf: resolveObf(dir, manifest.obf),
  };

  const compiledCr: Jcr<efcProject.Compiled> = await efcProject.compile(efcConfig).pipe(rxOp.first()).toPromise();
  if (!compiledCr.hasValue()) {
    console.error(compiledCr);
  }
  const compiled: efcProject.Compiled = compiledCr.unwrap();

  const contentBuild: url.URL = furi.join(dir, content.build);
  await outputFileAsync(contentBuild, Buffer.from(`${compiled.content}\n`));
}

function resolveSets(dir: url.URL, levels: ManifestContent["levels"]): efcConfig.EfcConfig["levels"] {
  if (typeof levels === "string") {
    return furi.join(dir, levels);
  }
  const sets: Map<ManifestLevelKey, efcConfig.EfcLevelSetConfig> = new Map();
  for (const [setKey, levelConfig] of levels) {
    sets.set(setKey, resolveSet(dir, levelConfig));
  }
  return sets;
}

function resolveSet(dir: url.URL, set: ManifestLevel): efcConfig.EfcLevelSetConfig {
  return {
    dir: furi.join(dir, set.levels),
    did: set.did,
    varName: set.var,
    padding: set.padding !== undefined ? set.padding : false,
    equalSize: set.sameSize !== undefined ? set.sameSize : false,
  };
}

function resolveLocales(dir: url.URL, locales: Manifest["locales"]): efcConfig.EfcConfig["locales"] {
  const resolved: Map<LocaleId, efcConfig.EfcLocaleConfig> = new Map();
  if (locales !== undefined) {
    for (const [localeId, locale] of locales) {
      resolved.set(localeId, resolveLocale(dir, locale));
    }
  }
  return resolved;
}

function resolveLocale(dir: url.URL, locale: ManifestLocale): efcConfig.EfcLocaleConfig {
  if (typeof locale.content === "string") {
    return {
      items: furi.join(dir, locale.content, "items.xml"),
      keys: furi.join(dir, locale.content, "keys.xml"),
      lands: furi.join(dir, locale.content, "lands.xml"),
      statics: furi.join(dir, locale.content, "statics.xml"),
    };
  } else {
    return {
      items: furi.join(dir, locale.content.items),
      keys: furi.join(dir, locale.content.keys),
      lands: furi.join(dir, locale.content.lands),
      statics: furi.join(dir, locale.content.statics),
    };
  }
}

function resolveObf(_dir: url.URL, obf: Manifest["obf"]): efcConfig.EfcConfig["obf"] {
  if (obf === undefined) {
    return undefined;
  }
  const parentMap: url.URL = efLoader.getObfuMapUri();
  return {key: obf.key, parentMap};
}
