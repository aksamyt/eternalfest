import fs from "fs";
import furi from "furi";
import inquirer from "inquirer";
import sysPath from "path";
import url from "url";

import { mkdirpAsync, outputFileAsync } from "../main/fs-utils.js";

interface InitConfig {
  name: string;
  patcher: Patcher;
  assets: boolean;
}

enum Patcher {
  None,
  Patchman,
  External,
}

export async function init(dir: url.URL): Promise<void> {
  const state: InitDirState = await checkInitDir(dir);
  if (state === InitDirState.NotADirectory) {
    console.error(`Failed to init: Not a directory: ${dir}`);
    return;
  } else if (state === InitDirState.NonEmptyDir) {
    console.error(`Failed to init: Non empty directory: ${dir}`);
    return;
  }

  console.info(`Collecting information to initialize directory: ${dir}`);
  const defaultName: string = sysPath.basename(furi.toSysPath(dir));

  const answer: {name: string, patcher: "Patchman" | "External" | "None", assets: boolean} = await inquirer.prompt([
    {
      name: "name",
      type: "input",
      message: "What is the project name? (lowercase ascii and dashes)",
      default: defaultName,
    },
    {
      name: "patcher",
      type: "list",
      message: "Which patcher framework do you want to use?",
      choices: [
        "Patchman",
        "External",
        "None",
      ],
    },
    {
      name: "assets",
      type: "confirm",
      message: "Add support for custom assets?",
    },
  ]);

  const config: InitConfig = {
    name: answer.name,
    patcher: answer.patcher === "Patchman"
      ? Patcher.Patchman
      : (answer.patcher === "External" ? Patcher.External : Patcher.None),
    assets: answer.assets,
  };

  await innerInit(dir, config);
}

enum InitDirState {
  DoesNotExist,
  NotADirectory,
  EmptyDir,
  NonEmptyDir,
}

async function checkInitDir(dir: url.URL): Promise<InitDirState> {
  let stats: fs.Stats;
  try {
    stats = await statAsync(dir);
  } catch (err) {
    if (err.code === "ENOENT") {
      return InitDirState.DoesNotExist;
    } else {
      throw err;
    }
  }
  if (!stats.isDirectory()) {
    return InitDirState.NotADirectory;
  }
  const content: fs.Dirent[] = await readdirAsync(dir);
  return content.length > 0 ? InitDirState.NonEmptyDir : InitDirState.EmptyDir;
}

async function statAsync(path: url.URL): Promise<fs.Stats> {
  return new Promise<fs.Stats>((resolve, reject) => {
    fs.stat(path, (err: Error | null, stats: fs.Stats) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(stats);
      }
    });
  });
}

async function readdirAsync(path: url.URL): Promise<fs.Dirent[]> {
  return new Promise<fs.Dirent[]>((resolve, reject) => {
    fs.readdir(path, {withFileTypes: true}, (err: Error | null, ents: fs.Dirent[]) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(ents);
      }
    });
  });
}

export async function innerInit(dir: url.URL, config: InitConfig): Promise<void> {
  console.info(`Initializing directory: ${dir}`);
  await mkdirpAsync(dir);
  await outputFileAsync(furi.join(dir, "package.json"), Buffer.from(getPackageJson(config)));
  await outputFileAsync(furi.join(dir, "eternalfest.toml"), Buffer.from(getManifestToml(config)));
  await outputFileAsync(furi.join(dir, "config.xml"), Buffer.from(getConfigXml(config)));
  await copyProjectTemplateFile(dir, [".gitattributes"]);
  await copyProjectTemplateFile(dir, [".npmrc"]);
  await copyProjectTemplateFile(dir, ["musics", "silence.mp3"]);
  await copyProjectTemplateFile(dir, ["src", "dimensions.xml"]);
  await copyProjectTemplateFile(dir, ["src", "score_items.xml"]);
  await copyProjectTemplateFile(dir, ["src", "special_items.xml"]);
  if (config.assets) {
    await copyProjectTemplateFile(dir, ["assets.xml"]);
    await copyProjectTemplateFile(dir, ["tsconfig.json"]);
    await copyProjectTemplateFile(dir, ["src", "assets", "xml", "fonds.xml"]);
    await copyProjectTemplateFile(dir, ["src", "assets", "xml", "plateformes.xml"]);
    await copyProjectTemplateFile(dir, ["src", "assets", "xml", "rayons.xml"]);
    await copyProjectTemplateFile(dir, ["src", "assets", "xml", "sprites.xml"]);
    const buildGameTs: string = await readProjectTemplateText(["tools", "build-game.ts.template"]);
    await outputFileAsync(furi.join(dir, ["tools", "build-game.ts"]), Buffer.from(buildGameTs));
  }
  await copyProjectTemplateFile(dir, ["src", "haxe", "Main.hx"]);
  await copyProjectTemplateFile(dir, ["src", "levels", "adventure", "000.lvl"]);
  await copyProjectTemplateFile(dir, ["src", "levels", "adventure", "001.lvl"]);
  await copyProjectTemplateFile(dir, ["src", "levels", "deepnight", "dim0", "000.lvl"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "items.xml"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "keys.xml"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "lands.xml"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "statics.xml"]);
  const gitignore: string = await readProjectTemplateText(["dot-gitignore"]);
  await outputFileAsync(furi.join(dir, [".gitignore"]), Buffer.from(gitignore));
  console.info("Initialization complete");
}

export function getPackageScripts(config: InitConfig): any {
  const scripts: any = {};

  const simpleBuildSteps: string[] = [];
  const buildSteps: string[] = [];

  scripts["build:content"] = "eternalfest build";
  buildSteps.push("build:content");

  switch (config.patcher) {
    case Patcher.External:
      scripts["build:external"] = "eternalfest-external";
      buildSteps.unshift("build:external");
      simpleBuildSteps.unshift("build:external");
      break;
    case Patcher.Patchman:
      scripts["build:patchman"] = "patchman";
      buildSteps.unshift("build:patchman");
      simpleBuildSteps.unshift("build:patchman");
      scripts["idea"] = "patchman --idea";
      break;
    default:
      break;
  }

  if (config.assets) {
    scripts["build:assets"] = "swfmill simple assets.xml ./build/assets.swf";
    scripts["build:engine"] = "ts-node tools/build-game.ts";
    buildSteps.push("build:assets");
    buildSteps.push("build:engine");
  }

  scripts["build"] = buildSteps.map((x: string) => `yarn run ${x}`).join(" && ");
  if (simpleBuildSteps.length !== buildSteps.length) {
    scripts["simple"] = simpleBuildSteps.map((x: string) => `yarn run ${x}`).join(" && ");
  }
  scripts["prepare"] = "yarn run build";

  return sortByKeys(scripts);
}

export function getPackageDependencies(config: InitConfig): any {
  const dependencies: any = {};
  dependencies["@eternalfest/eternaldev"] = "^0.3.0";
  switch (config.patcher) {
    case Patcher.External:
      dependencies["@eternalfest/external"] = "^3.0.17";
      break;
    case Patcher.Patchman:
      dependencies["@patchman/debug"] = "^0.8.1";
      dependencies["@patchman/keyboard"] = "^0.8.0";
      dependencies["@patchman/merlin"] = "^0.12.2";
      dependencies["@patchman/patchman"] = "^0.8.2";
      break;
    default:
      break;
  }
  if (config.assets) {
    dependencies["swf-emitter"] = "^0.10.0";
    dependencies["swf-merge"] = "^0.10.0";
    dependencies["swf-parser"] = "^0.10.0";
    dependencies["swf-types"] = "^0.10.0";
    dependencies["ts-node"] = "^8.5.4";
    dependencies["typescript"] = "^3.7.4";
  }
  return sortByKeys(dependencies);
}

function sortByKeys<T extends {}>(obj: T): T {
  const entries: [string, any][] = Object.entries(obj);
  entries.sort((left, right) => {
    if (left[0] === right[0]) {
      return 0;
    } else {
      return left[0] < right[0] ? -1 : 1;
    }
  });

  const result: Partial<T> = {};
  for (const [key, value] of entries) {
    Reflect.set(result, key, value);
  }
  return result as T;
}

function getPackageJson(config: InitConfig): string {
  const name: string = config.name;
  const pkg = {
    "name": `@eternalfest-games/${name}`,
    "version": "1.0.0",
    "homepage": `https://gitlab.com/eternalfest/games/${name}`,
    "description": `Project for the game \`${name}\``,
    "repository": {
      type: "git",
      url: `git://gitlab.com:eternalfest/games/${name}.git`,
    },
    "scripts": getPackageScripts(config),
    "publishConfig": {
      access: "restricted",
      registry: "https://npm.eternalfest.net/",
    },
    "pre-commit": {
      run: [
        "build",
      ],
    },
    "keywords": ["game"],
    "license": "UNLICENSED",
    "patchman": getPatchmanConfig(),
    "engines": {
      node: ">=14.0.0",
    },
    "dependencies": getPackageDependencies(config),
  };

  function getPatchmanConfig(): object | undefined {
    if (config.patcher === Patcher.Patchman) {
      return {
        "haxe": "src/haxe",
        "haxeMain": "Main",
        "outFile": "./build/patchman.swf"
      };
    } else {
      return undefined;
    }
  }

  return `${JSON.stringify(pkg, null, 2)}\n`;
}

function getManifestToml(config: InitConfig): string {
  return [
    `display_name = ${JSON.stringify(config.name)}`,
    `description = ${JSON.stringify(`Description for the game ${config.name}`)}`,
    "# icon = \"./icon.png\"",
    "# remote_id = \"00000000-0000-0000-0000-000000000000\"",
    `repository = ${JSON.stringify(getRepository(config))}`,
    "families = \"0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030\"",
    "main_locale = \"fr-FR\"",
    `${config.assets ? "" : "# "}hf = "./build/game.swf"`,
    "musics = [",
    "  \"./musics/silence.mp3\",",
    "  \"./musics/silence.mp3\",",
    "  \"./musics/silence.mp3\",",
    "]",
    `${config.assets ? "" : "# "}assets = "./src/assets"`,
    "",
    "[obf]",
    "key = \"TODO: Do not use a hardcoded key\"",
    "",
    "[content]",
    "build = \"./build/game.xml\"",
    "score_items = \"./src/score_items.xml\"",
    "special_items = \"./src/special_items.xml\"",
    "dimensions = \"./src/dimensions.xml\"",
    "data = \"./src/data\"",
    "",
    "[content.levels.0]",
    "levels = \"./src/levels/adventure\"",
    "var = \"xml_adventure\"",
    "did = \"0\"",
    "[content.levels.1]",
    "levels = \"./src/levels/deepnight\"",
    "var = \"xml_deepnight\"",
    "padding = true",
    "did = \"1\"",
    "",
    "[patchman]",
    "build = \"./build/patchman.swf\"",
    "",
    "[modes.solo]",
    "display_name = \"Aventure\"",
    "state = \"enabled\"",
    "[modes.solo.options]",
    "mirror = {display_name = \"Miroir\", state = \"enabled\"}",
    "nightmare = {display_name = \"Cauchemar\", state = \"enabled\"}",
    "ninja = {display_name = \"Ninjutsu\", state = \"enabled\"}",
    "# nojutsu = {display_name = \"Nojutsu\", state = \"enabled\"}",
    "bombexpert = {display_name = \"Explosifs Instables\",state = \"enabled\"}",
    "boost = {display_name = \"Tornade\", state = \"enabled\"}",
    "",
    "[modes.multicoop]",
    "display_name = \"Multi Coopératif\"",
    "state = \"enabled\"",
    "[modes.multicoop.options]",
    "mirrormulti = {display_name = \"Miroir\", state = \"enabled\"}",
    "nightmaremulti = {display_name = \"Cauchemar\", state = \"enabled\"}",
    "bombexpert = {display_name = \"Explosifs Instables\", state = \"enabled\"}",
    "boost = {display_name = \"Tornade\", state = \"enabled\"}",
    "lifesharing = {display_name = \"Partage de vies\", state = \"enabled\"}",
    "",
    "[locales.fr-FR]",
    "build = \"./build/locales.fr-FR.json\"",
    "[locales.fr-FR.content]",
    "keys = \"./src/locales/fr-FR/keys.xml\"",
    "items = \"./src/locales/fr-FR/items.xml\"",
    "lands = \"./src/locales/fr-FR/lands.xml\"",
    "statics = \"./src/locales/fr-FR/statics.xml\"",
    "",
  ].join("\n");
}

function getConfigXml(config: InitConfig): string {
  const lines: string[] = [];
  lines.push("<config>");
  if (config.assets) {
    lines.push("  <assets>");
    lines.push("    <asset>src/assets</asset>");
    lines.push("  </assets>");
  }
  lines.push("</config>");
  lines.push("");
  return lines.join("\n");
}

function getRepository(config: InitConfig) {
  return `https://gitlab.com/eternalfest/games/${config.name}`;
}

async function readProjectTemplateText(parts: readonly string[]): Promise<string> {
  const furi: url.URL = resolveProjectTemplateFuri(parts);
  return fs.promises.readFile(furi, {encoding: "utf-8"});
}

async function copyProjectTemplateFile(outDir: url.URL, parts: readonly string[]): Promise<void> {
  const src: url.URL = resolveProjectTemplateFuri(parts);
  const dest: url.URL = furi.join(outDir, parts);
  const destDir: url.URL = furi.join(dest, "..");
  await fs.promises.mkdir(destDir, {recursive: true});
  await fs.promises.copyFile(src, dest);
}

const RESOURCE_DIR: url.URL = furi.join(import.meta.url, "../../../main-resources");

function resolveProjectTemplateFuri(parts: readonly string[]): url.URL {
  return furi.join(RESOURCE_DIR, "project-template", parts);
}
