import { $UserAuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { $Blob, Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { $CreateBlobOptions } from "@eternalfest/api-core/lib/blob/create-blob-options.js";
import { $CreateUploadSessionOptions } from "@eternalfest/api-core/lib/blob/create-upload-session-options.js";
import { $UploadBytesOptions } from "@eternalfest/api-core/lib/blob/upload-bytes-options.js";
import { $UploadSession, UploadSession } from "@eternalfest/api-core/lib/blob/upload-session.js";
import { $CreateFileOptions } from "@eternalfest/api-core/lib/file/create-file-options.js";
import { $Drive, Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { $File, File } from "@eternalfest/api-core/lib/file/file.js";
import { $CreateGameOptions, CreateGameOptions } from "@eternalfest/api-core/lib/game/create-game-options.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { $Game, Game } from "@eternalfest/api-core/lib/game/game.js";
import { InputGameResource } from "@eternalfest/api-core/lib/game/input-game-resource.js";
import { $UpdateGameOptions, UpdateGameOptions } from "@eternalfest/api-core/lib/game/update-game-options.js";
import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { $AuthHfestCredentials, $CreateSessionQuery, AuthStrategy } from "@eternalfest/rest-server/lib/io/self.js";
import fs from "fs";
import furi from "furi";
import incident from "incident";
import { IoType } from "kryo";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_WRITER } from "kryo-qs/lib/qs-value-writer.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import sysPath from "path";
import readline from "readline";
import superagent from "superagent";
import url from "url";
import urlJoin from "url-join";

import { LocalFileService } from "../main/api/file/local-service.js";
import { LocalProjectService } from "../main/api/project/local-service.js";
import { Project } from "../universal/project.js";

export async function uploadProject(apiBase: string): Promise<void> {
  const apiBaseUrl: url.URL = new url.URL(apiBase);
  const cwdUrl: url.URL = furi.fromSysPath(process.cwd());
  const fileService: LocalFileService = new LocalFileService();
  const projectService: LocalProjectService = new LocalProjectService(fileService, [cwdUrl]);

  const project: Project | undefined = await projectService.getProjectByRoot(cwdUrl);
  if (project === undefined) {
    throw new Error(`NonProjectDirectory: ${cwdUrl}`);
  }
  console.log(`API endpoint: ${apiBase}`);
  console.log(`Project: ${project.displayName}`);
  console.log(`Local URL: ${cwdUrl}`);
  if (project.remoteId === undefined) {
    console.log("Remote id: (none)");
    const answer: string = await prompt("Do you want to upload a new game or update an existing game? (new/update) ");
    if (answer === "update") {
      console.log("Specify the remote id (`remote_id` field) in the project manifest then run the upload again.");
      return;
    } else if (answer !== "new") {
      console.error("Invalid response, aborting.");
      return;
    }
    console.log("Uploading a NEW game");
    await createGameFromProject(apiBaseUrl, project);
  } else {
    console.log(`Remote id: ${project.remoteId}`);
    const answer: string = await prompt("Do you want to upload a new game or update an existing game? (new/update) ");
    if (answer === "new") {
      console.log("Remove the remote id (`remote_id` field) from the project manifest then run the upload again.");
      return;
    } else if (answer !== "update") {
      console.error("Invalid response, aborting.");
      return;
    }
    console.log("UPDATING an existing game");
    await updateGameFromProject(apiBaseUrl, project.remoteId, project);
  }
}

type Agent = superagent.SuperAgent<superagent.SuperAgentRequest>;

class ApiClient {
  private readonly agent: Agent;
  private readonly apiBase: url.URL;

  constructor(apiBase: url.URL) {
    this.agent = superagent.agent();
    this.apiBase = apiBase;
  }

  async get<Res>(
    url: string,
    resType: IoType<Res>,
  ): Promise<Res> {
    const res: superagent.Response = await this.agent.get(this.resolve(url).toString()).send();
    try {
      return readJsonResponse(resType, res.body);
    } catch (e) {
      console.error(`Response (${res.status}):`);
      console.error(JSON.stringify(res.body));
      throw e;
    }
  }

  async patch<Req, Res>(
    url: string,
    req: Req,
    reqType: IoType<Req>,
    resType: IoType<Res>,
  ): Promise<Res> {
    const rawReq: any = reqType.write(JSON_VALUE_WRITER, req);
    const res: superagent.Response = await this.agent.patch(this.resolve(url).toString()).send(rawReq);
    try {
      return readJsonResponse(resType, res.body);
    } catch (e) {
      console.error("Request:");
      console.error(JSON.stringify(rawReq));
      console.error(`Response (${res.status}):`);
      console.error(JSON.stringify(res.body));
      throw e;
    }
  }

  async post<Req, Res>(
    url: string,
    req: Req,
    reqType: IoType<Req>,
    resType: IoType<Res>,
  ): Promise<Res> {
    const rawReq: any = reqType.write(JSON_VALUE_WRITER, req);
    const res: superagent.Response = await this.agent.post(this.resolve(url).toString()).send(rawReq);
    try {
      return readJsonResponse(resType, res.body);
    } catch (e) {
      console.error("Request:");
      console.error(JSON.stringify(rawReq));
      console.error(`Response (${res.status}):`);
      console.error(JSON.stringify(res.body));
      throw e;
    }
  }

  async put<Query, Req, Res>(
    url: string,
    query: Query,
    queryType: IoType<Query>,
    req: Req,
    reqType: IoType<Req>,
    resType: IoType<Res>,
  ): Promise<Res> {
    const rawQuery: any = queryType.write(QS_VALUE_WRITER, query);
    const rawReq: any = reqType.write(JSON_VALUE_WRITER, req);
    console.log(this.resolve(url).toString());
    const res: superagent.Response = await this.agent.put(this.resolve(url).toString())
      .query(rawQuery)
      .send(rawReq);
    try {
      return readJsonResponse(resType, res.body);
    } catch (e) {
      console.error("Request:");
      console.error(JSON.stringify(rawReq));
      console.error(`Response (${res.status}):`);
      console.error(JSON.stringify(res.body));
      throw e;
    }
  }

  private resolve(uri: string): url.URL {
    return new url.URL(urlJoin(this.apiBase.toString(), uri));
  }
}

async function createGameFromProject(apiBase: url.URL, project: Project) {
  const client: ApiClient = new ApiClient(apiBase);
  const credentials: Credentials = await promptCredentials();
  console.log("Start Upload");
  const auth: UserAuthContext = await authenticate(client, credentials);
  console.log("Authenticated");
  const drive: Drive = await getDrive(client, auth.userId);
  console.log("Drive");

  const prefix: string = `tmp-${(new Date()).toISOString().replace(/[^\d]/g, "")}-`;
  const filePathToId: Map<string, UuidHex> = new Map();

  const resources: InputGameResource[] = [];

  for (const {path, tag} of project.resources) {
    let fileId: UuidHex | undefined = filePathToId.get(path);
    if (fileId === undefined) {
      const baseName: string = sysPath.posix.basename(path);
      let mediaType: string;
      if (/\.swf$/.test(path)) {
        mediaType = "application/x-shockwave-flash";
      } else if (/\.xml$/.test(path)) {
        mediaType = "application/xml";
      } else if (/\.mp3$/.test(path)) {
        mediaType = "audio/mp3";
      } else if (/\.png$/.test(path)) {
        mediaType = "image/png";
      } else {
        throw new Error(`Unknown extension for: ${path}`);
      }
      const file: File = await uploadFile(client, drive.root.id, `${prefix}-${baseName}`, mediaType, path);
      fileId = file.id;
      filePathToId.set(path, fileId);
    }
    resources.push({fileId, tag});
  }

  let iconFileId: UuidHex | undefined;
  if (project.icon !== undefined) {
    const icon: File = await uploadFile(client, drive.root.id, `${prefix}-icon.png`, "image/png", project.icon.url);
    iconFileId = icon.id;
  }

  console.log("Files");

  const options: CreateGameOptions = {
    displayName: project.displayName,
    description: project.description !== undefined ? project.description : "",
    mainLocale: "fr-FR",
    engines: {
      loader: "3.0.0",
    },
    iconFileId,
    resources,
    gameModes: [...project.gameModes],
    families: project.families,
    category: GameCategory.Small,
    isPrivate: false,
  };

  const game: Game = await client.post("/games", options, $CreateGameOptions, $Game);

  console.log(`Done: ${game.displayName}`);
}

async function updateGameFromProject(apiBase: url.URL, gameId: UuidHex, project: Project) {
  const client: ApiClient = new ApiClient(apiBase);
  const credentials: Credentials = await promptCredentials();
  console.log("Start Upload");
  const auth: UserAuthContext = await authenticate(client, credentials);
  console.log("Authenticated");
  const drive: Drive = await getDrive(client, auth.userId);
  console.log("Drive");

  const prefix: string = `tmp-${(new Date()).toISOString().replace(/[^\d]/g, "")}-`;
  const filePathToId: Map<string, UuidHex> = new Map();

  const resources: InputGameResource[] = [];

  for (const {path, tag} of project.resources) {
    let fileId: UuidHex | undefined = filePathToId.get(path);
    if (fileId === undefined) {
      const baseName: string = sysPath.posix.basename(path);
      let mediaType: string;
      if (/\.swf$/.test(path)) {
        mediaType = "application/x-shockwave-flash";
      } else if (/\.xml$/.test(path)) {
        mediaType = "application/xml";
      } else if (/\.mp3$/.test(path)) {
        mediaType = "audio/mp3";
      } else if (/\.png$/.test(path)) {
        mediaType = "image/png";
      } else {
        throw new Error(`Unknown extension for: ${path}`);
      }
      const file: File = await uploadFile(client, drive.root.id, `${prefix}-${baseName}`, mediaType, path);
      fileId = file.id;
      filePathToId.set(path, fileId);
    }
    resources.push({fileId, tag});
  }

  let iconFileId: UuidHex | undefined;
  if (project.icon !== undefined) {
    const icon: File = await uploadFile(client, drive.root.id, `${prefix}-icon.png`, "image/png", project.icon.url);
    iconFileId = icon.id;
  }

  console.log("Files");

  const options: UpdateGameOptions = {
    displayName: project.displayName,
    description: project.description,
    // engines: {
    //   loader: "3.0.0",
    // },
    iconFileId,
    resources,
    gameModes: [...project.gameModes],
    families: project.families,
    category: GameCategory.Small,
    isPrivate: false,
  };
  const game: Game = await client.patch(`/games/${gameId}`, options, $UpdateGameOptions, $Game);

  console.log(`Done: ${game.displayName}`);
}

export interface Credentials {
  username: string;
  password: string;
}

export async function promptCredentials(): Promise<Credentials> {
  const cliInterface: readline.ReadLine = readline.createInterface(
    process.stdin,
    process.stdout,
  );

  const username: string = await new Promise<string>(
    (resolve: (res: string) => void, _reject: (err: any) => void): void => {
      cliInterface.question("Username? ", resolve);
    },
  );

  const password: string = await new Promise<string>(
    (resolve: (res: string) => void, _reject: (err: any) => void): void => {
      cliInterface.question("Password? ", resolve);
    },
  );

  const result: Promise<Credentials> = new Promise(
    (resolve: (res: Credentials) => void, reject: (err: Error) => void): void => {
      cliInterface.once("error", (err: Error): void => {
        reject(err);
      });
      cliInterface.once("close", (): void => {
        resolve({username, password});
      });
    },
  );

  cliInterface.close();
  return result;
}

export async function prompt(message: string): Promise<string> {
  const cliInterface: readline.ReadLine = readline.createInterface(
    process.stdin,
    process.stdout,
  );

  const answer: string = await new Promise<string>(
    (resolve: (res: string) => void, _reject: (err: any) => void): void => {
      cliInterface.question(message, resolve);
    },
  );

  const result: Promise<string> = new Promise(
    (resolve: (res: string) => void, reject: (err: Error) => void): void => {
      cliInterface.once("error", (err: Error): void => {
        reject(err);
      });
      cliInterface.once("close", (): void => {
        resolve(answer);
      });
    },
  );

  cliInterface.close();
  return result;
}

async function authenticate(client: ApiClient, credentials: Credentials): Promise<UserAuthContext> {
  return client.put(
    "/self/session",
    {strategy: AuthStrategy.HfestCredentials},
    $CreateSessionQuery,
    {
      server: HfestServer.Fr,
      username: credentials.username,
      password: Buffer.from(credentials.password),
    },
    $AuthHfestCredentials,
    $UserAuthContext,
  );
}

async function getDrive(client: ApiClient, userId: UuidHex): Promise<Drive> {
  return client.get(`/users/${userId}/drive`, $Drive);
}

const CHUNK_SIZE: number = 200 * 1024;

async function uploadFile(client: ApiClient, dirId: string, name: string, mediaType: string, filePath: string) {
  console.log(`Start upload: ${name}`);
  const buffer: Buffer = await readFile(filePath);
  let blob: Blob;
  if (buffer.length <= CHUNK_SIZE) {
    blob = await client.post("/blobs", {mediaType, data: buffer}, $CreateBlobOptions, $Blob);
  } else {
    let uploadSession: UploadSession = await client.post(
      "/upload-sessions",
      {mediaType, byteSize: buffer.length},
      $CreateUploadSessionOptions,
      $UploadSession,
    );
    while (uploadSession.remainingRange.start < uploadSession.remainingRange.end) {
      console.log(uploadSession.remainingRange.start / buffer.length);
      uploadSession = await reliableUploadNextChunk(client, uploadSession, buffer);
    }
    blob = uploadSession.blob!;
  }
  console.log(1);
  return client.post(
    "/files", {
      parentId: dirId,
      displayName: name,
      blobId: blob.id,
    },
    $CreateFileOptions,
    $File,
  );
}

async function reliableUploadNextChunk(client: ApiClient, uploadSession: UploadSession, buffer: Buffer) {
  for (; ;) {
    try {
      return await uploadNextChunk(client, uploadSession, buffer);
    } catch (err) {
      if (err.code === "ECONNRESET") {
        continue;
      }
      console.error(err);
    }
  }
}

async function uploadNextChunk(
  client: ApiClient,
  uploadSession: UploadSession,
  buffer: Buffer,
): Promise<UploadSession> {
  return client.patch(
    `/upload-sessions/${uploadSession.id}`,
    {
      data: buffer.slice(
        uploadSession.remainingRange.start,
        Math.min(uploadSession.remainingRange.start + CHUNK_SIZE, uploadSession.remainingRange.end),
      ),
      offset: uploadSession.remainingRange.start,
    },
    $UploadBytesOptions,
    $UploadSession,
  );
}

async function readFile(filePath: string): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(resolvePath(filePath), (err, buff) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(buff);
      }
    });
  });
}

function resolvePath(relPath: string): string {
  return sysPath.join(process.cwd(), relPath);
}

export function readJsonResponse<T>(type: IoType<T>, raw: any): T {
  if (typeof raw.error === "string") {
    throw new incident.Incident(raw.error, raw);
  } else {
    return type.read(JSON_VALUE_READER, raw);
  }
}
