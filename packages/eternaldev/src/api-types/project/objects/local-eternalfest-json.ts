import { GameEngines } from "@eternalfest/api-core/game/game-engines";
import { GameMode } from "@eternalfest/api-core/game/game-mode";

export type ResourcesDictionary = Record<string, string | null>;

export interface Resource {
  path: string;
  tag?: string;
  priority?: string;
}

export type Resources = ResourcesDictionary | Resource[];

export interface LocalEternalfestJson {
  name?: string;
  description?: string;
  icon?: string;
  engines?: Partial<GameEngines>;
  resources?: Resources;
  families?: string;
  modes?: ReadonlyArray<GameMode>;
}
