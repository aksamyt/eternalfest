import {PosixPath} from "../../scalars/posix-path";
import {Uuid} from "../../scalars/uuid";

export interface PartialProject {
  id: Uuid;
  _rootPath: PosixPath;
}
