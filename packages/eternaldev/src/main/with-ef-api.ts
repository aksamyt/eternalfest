import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Api as EfApi } from "@eternalfest/rest-server/lib/create-api-router.js";
import { KoaAuth } from "@eternalfest/rest-server/lib/koa-auth.js";
import fs from "fs";
import furi from "furi";
import url from "url";

import { InMemoryAuthService } from "./api/auth/in-memory-service.js";
import { InMemoryBlobService } from "./api/blob/in-memory-service.js";
import { LocalFileService } from "./api/file/local-service.js";
import { LocalGameService } from "./api/game/local-service.js";
import { InMemoryHfestIdentityService } from "./api/hfest-identity/in-memory-service.js";
import { InMemoryLeaderboardService } from "./api/leaderboard/in-memory-service.js";
import { LocalProjectService } from "./api/project/local-service.js";
import { InMemoryRunService } from "./api/run/in-memory-service.js";
import { InMemorySessionService } from "./api/session/in-memory-service.js";
import { InMemoryUserService } from "./api/user/in-memory-service.js";
import { getRecentProjects, touchRecentProject } from "./lib/recent-projects.js";

export interface LocalEfApi extends EfApi {
  project: LocalProjectService;
  run: InMemoryRunService;
}

export async function withEfApi<R>(handler: (efApi: LocalEfApi) => Promise<R>): Promise<R> {
  const localProjects: readonly url.URL[] = await getLocalProjects();
  const file: LocalFileService = new LocalFileService();
  const project: LocalProjectService = new LocalProjectService(file, localProjects);
  const blob: InMemoryBlobService = new InMemoryBlobService();
  const user: InMemoryUserService = new InMemoryUserService();
  const game: LocalGameService = new LocalGameService(file, project);
  const run: InMemoryRunService = new InMemoryRunService(game, user);
  const leaderboard: InMemoryLeaderboardService = new InMemoryLeaderboardService();
  const session: SessionService = new InMemorySessionService();
  const hfestIdentity: HfestIdentityService = new InMemoryHfestIdentityService();
  const auth: InMemoryAuthService = new InMemoryAuthService();
  const koaAuth: KoaAuth = new KoaAuth(session, user);
  const efApi: LocalEfApi = {auth, blob, file, game, hfestIdentity, koaAuth, run, leaderboard, session, user, project};
  return handler(efApi);
}

async function getLocalProjects(): Promise<url.URL[]> {
  const cwdProject: url.URL | undefined = await tryGetCwdProject();
  if (cwdProject !== undefined) {
    await touchRecentProject(cwdProject);
  }
  return getRecentProjects();
}

export async function tryGetCwdProject(): Promise<url.URL | undefined> {
  const cwdUrl: url.URL = furi.fromSysPath(process.cwd());
  if (fs.existsSync(furi.join(cwdUrl, "eternalfest.toml"))) {
    return cwdUrl;
  }
  return undefined;
}
