import findUp from "find-up";
import fs from "fs";
import meta from "./meta.js";

const packagePath: string | undefined = findUp.sync("package.json", {cwd: meta.dirname});

if (packagePath === undefined) {
  throw new Error("Cannot find `package.json`");
}

const pkg: any = JSON.parse(fs.readFileSync(packagePath, {encoding: "utf-8"}));

export const VERSION: string = pkg.version;
