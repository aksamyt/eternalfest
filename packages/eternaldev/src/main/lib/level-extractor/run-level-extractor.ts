import {levelExtractor} from "@eternalfest/devkit";
import {Readable} from "stream";

export async function extractLevels(_levelsString: string): Promise<any[]> {
  return [levelExtractor.getPath()];
}

export async function extractLevelsFromStream(_levelsStringStream: Readable): Promise<any[]> {
  return [levelExtractor.getPath()];
}

export async function extractLevelsFromFile(_levelsStringPath: string): Promise<any[]> {
  return [levelExtractor.getPath()];
}
