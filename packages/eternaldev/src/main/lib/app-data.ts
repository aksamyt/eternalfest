import appDatPath from "appdata-path";
import fs from "fs";
import furi from "furi";
import sysPath from "path";
import url from "url";

const APP_NAME: string = "eternalfest";
const APP_BASE: url.URL = furi.fromSysPath(appDatPath(APP_NAME));

function getAppUrl(components: ReadonlyArray<string>): url.URL {
  return furi.join(APP_BASE, components);
}

export async function readAppFile(components: ReadonlyArray<string>): Promise<Buffer> {
  return readFileAsync(getAppUrl(components));
}

export async function writeAppFile(components: ReadonlyArray<string>, data: Buffer): Promise<void> {
  return outputFileAsync(getAppUrl(components), data);
}

export async function readAppText(components: ReadonlyArray<string>): Promise<string> {
  return (await readAppFile(components)).toString("utf-8");
}

export async function writeAppText(components: ReadonlyArray<string>, text: string): Promise<void> {
  return writeAppFile(components, Buffer.from(text));
}

export async function readAppJson(components: ReadonlyArray<string>): Promise<any> {
  return JSON.parse(await readAppText(components));
}

export async function writeAppJson(components: ReadonlyArray<string>, data: any): Promise<void> {
  return writeAppText(components, JSON.stringify(data, null, 2));
}

async function outputFileAsync(filePath: url.URL, data: Buffer): Promise<void> {
  await mkdirpAsync(furi.fromSysPath(sysPath.dirname(furi.toSysPath(filePath.toString()))));
  await writeFileAsync(filePath, data);
}

async function mkdirpAsync(dirPath: url.URL): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.mkdir(dirPath, {recursive: true}, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

async function writeFileAsync(filePath: url.URL, data: Buffer): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

async function readFileAsync(filePath: url.URL): Promise<Buffer> {
  return new Promise<Buffer>((resolve, reject) => {
    fs.readFile(filePath, (err: NodeJS.ErrnoException | null, data: Buffer) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
