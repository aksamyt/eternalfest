import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { Game } from "@eternalfest/api-core/lib/game/game.js";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { RunItems } from "@eternalfest/api-core/lib/run/run-items.js";
import { RunStart } from "@eternalfest/api-core/lib/run/run-start.js";
import { Run } from "@eternalfest/api-core/lib/run/run.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { SetRunResultOptions } from "@eternalfest/api-core/lib/run/set-run-result-options.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { UserRef } from "@eternalfest/api-core/lib/user/user-ref.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import uuidjs from "uuidjs";

import { CreateDebugRunOptions } from "../../../universal/create-debug-run-options.js";
import { PartialProfile } from "../../../universal/partial-profile.js";
import { ProfileKey } from "../../../universal/profile-key.js";
import { Profile } from "../../../universal/profile.js";
import { LocalGameService } from "../game/local-service.js";
import { DEFAULT_USER_DISPLAY_NAME, DEFAULT_USER_ID } from "../user/in-memory-service.js";

export class InMemoryRunService implements RunService {
  private readonly game: LocalGameService;
  private readonly user: UserService;
  private readonly idToRun: Map<UuidHex, Run>;
  private readonly runIdToItems: Map<UuidHex, RunItems>;

  constructor(game: LocalGameService, user: UserService) {
    this.game = game;
    this.user = user;
    this.idToRun = new Map();
    this.runIdToItems = new Map();
  }

  async createDebugRun(auth: AuthContext, options: CreateDebugRunOptions): Promise<Run> {
    const id: UuidHex = uuidjs.genV4().toString();
    const game: Game | undefined = await this.game.getGameById(auth, options.gameId);
    if (game === undefined) {
      throw new Error("GameNotFound");
    }
    const profile: Profile = await this.getDebugProfile(options.gameId, options.profile);

    const user: User | undefined = await this.user.getUserById(auth, options.userId);
    if (user === undefined) {
      throw new Error("UserNotFound");
    }
    const run: Run = {
      id,
      game: {id: game.id, displayName: game.displayName},
      user: profile.user,
      gameOptions: options.gameOptions,
      gameMode: options.gameMode,
      createdAt: new Date(),
      startedAt: null,
      result: null,
      settings: options.settings,
    };
    this.idToRun.set(id, run);
    this.runIdToItems.set(id, profile.items);
    return run;
  }

  async createRun(auth: AuthContext, options: CreateRunOptions): Promise<Run> {
    return this.createDebugRun(auth, options);
  }

  async getRunById(_auth: AuthContext, runId: UuidHex): Promise<Run> {
    return this.idToRun.get(runId)!;
  }

  async startRun(auth: AuthContext, runId: UuidHex): Promise<RunStart> {
    const run: Run | undefined = this.idToRun.get(runId);
    if (run === undefined) {
      throw new Error("RunNotFound");
    }
    const game: Game | undefined = await this.game.getGameById(auth, run.game.id);
    if (game === undefined) {
      throw new Error("GameNotFound");
    }
    const key: UuidHex = uuidjs.genV4().toString();

    const items: RunItems = this.runIdToItems.get(runId)!;

    const runStart: RunStart = {
      families: game.families,
      key,
      run: {id: runId},
      items,
    };

    const newRun: Run = {...run, startedAt: new Date()};
    this.idToRun.set(runId, newRun);
    return runStart;
  }

  async setRunResult(
    _auth: AuthContext,
    runId: UuidHex,
    options: SetRunResultOptions,
  ): Promise<Run> {
    console.log("Run result:");
    console.log(options);
    return this.idToRun.get(runId)!;
  }

  private async getDebugProfile(gameId: UuidHex, profileKey?: ProfileKey): Promise<Profile> {
    const partial: PartialProfile = await this.game.getDebugProfile(gameId, profileKey);
    const user: UserRef = {
      id: partial.user !== undefined && partial.user.id !== undefined ? partial.user.id : DEFAULT_USER_ID,
      type: ActorType.User as ActorType.User,
      displayName: partial.user !== undefined && partial.user.displayName !== undefined
        ? partial.user.displayName
        : DEFAULT_USER_DISPLAY_NAME,
    };
    const items: RunItems = partial.items !== undefined ? partial.items : new Map();
    return {user, items};
  }
}
