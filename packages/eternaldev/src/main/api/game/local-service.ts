import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { File } from "@eternalfest/api-core/lib/file/file.js";
import { CreateGameOptions } from "@eternalfest/api-core/lib/game/create-game-options.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { GameMode } from "@eternalfest/api-core/lib/game/game-mode.js";
import { GameResource } from "@eternalfest/api-core/lib/game/game-resource.js";
import { Game } from "@eternalfest/api-core/lib/game/game.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { UpdateGameOptions } from "@eternalfest/api-core/lib/game/update-game-options.js";
import { IdRef } from "@eternalfest/api-core/lib/types/id-ref.js";
import furi from "furi";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import url from "url";

import { PartialProfile } from "../../../universal/partial-profile.js";
import { ProfileKey } from "../../../universal/profile-key.js";
import { Project } from "../../../universal/project.js";
import { LocalFileService } from "../file/local-service.js";
import { LocalProjectService } from "../project/local-service.js";
import { DEFAULT_USER_DISPLAY_NAME, DEFAULT_USER_ID } from "../user/in-memory-service.js";

export class LocalGameService implements GameService {
  private readonly file: LocalFileService;
  private readonly project: LocalProjectService;

  constructor(file: LocalFileService, project: LocalProjectService) {
    this.file = file;
    this.project = project;
  }

  createGame(_auth: AuthContext, _options: CreateGameOptions): Promise<Game> {
    throw new incident.Incident("NotImplemented: LocalGameService#createGame");
  }

  deleteGameById(_auth: AuthContext, _gameId: UuidHex): Promise<void> {
    throw new incident.Incident("NotImplemented: LocalGameService#deleteGameById");
  }

  async getGameById(_auth: AuthContext, gameId: UuidHex): Promise<Game | undefined> {
    const project: Project | undefined = await this.project.getProjectById(gameId);
    if (project === undefined) {
      return undefined;
    }
    return this.gameFromProject(project);
  }

  async getGames(_auth: AuthContext): Promise<readonly Game[]> {
    throw new Error("NotImplemented");
    // const games: Game[] = [];
    // const projects: ShortProject[] = await this.project.getProjects();
    // for (const project of projects) {
    //   games.push(await this.gameFromProject(project));
    // }
    // return games;
  }

  async updateGame(_auth: AuthContext, _gameId: UuidHex, _options: UpdateGameOptions): Promise<Game> {
    throw new incident.Incident("NotImplemented: LocalGameService#updateGame");
  }

  async getDebugProfile(gameId: UuidHex, profileKey?: ProfileKey): Promise<PartialProfile> {
    const project: Project | undefined = await this.project.getProjectById(gameId);
    if (project === undefined) {
      throw new Error("ProjectNotFound");
    }
    const result: PartialProfile | undefined = profileKey !== undefined ? project.profiles.get(profileKey) : undefined;
    return result !== undefined ? result : {};
  }

  private async gameFromProject(project: Project): Promise<Game> {
    let iconFile: IdRef | undefined;
    if (project.icon !== undefined) {
      const resolvedUrl: url.URL = furi.join(project.localUrl, project.icon.url.split("/"));
      iconFile = await this.file.createFileFromUrl(resolvedUrl);
    }

    const resources: GameResource[] = [];
    for (const res of project.resources) {
      const resolvedUrl: url.URL = furi.join(project.localUrl, res.path.split("/"));
      const file: File = await this.file.createFileFromUrl(resolvedUrl);
      resources.push({
        file,
        priority: "high",
        tag: res.tag,
      });
    }

    return {
      id: project.id,
      key: null,
      createdAt: new Date(),
      updatedAt: new Date(),
      publicationDate: null,
      author: {
        id: DEFAULT_USER_ID,
        displayName: DEFAULT_USER_DISPLAY_NAME,
      },
      mainLocale: "fr-FR",
      displayName: project.displayName,
      description: project.description || "No description",
      category: GameCategory.Other,
      iconFile,
      isPrivate: false,
      resources,
      engines: project.engines,
      families: project.families,
      gameModes: project.gameModes as GameMode[],
      locales: new Map(),
    };
  }
}
