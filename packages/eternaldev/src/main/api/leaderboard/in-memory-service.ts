import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard.js";
import { LeaderboardService } from "@eternalfest/api-core/lib/leaderboard/service.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";

export class InMemoryLeaderboardService implements LeaderboardService {
  getLeaderboard(_auth: AuthContext, _gameId: UuidHex, _gameMode: GameModeKey): Promise<Leaderboard> {
    throw new incident.Incident("NotImplemented: InMemoryLeaderboardService#getGameResults");
  }
}
