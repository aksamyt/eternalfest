import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { HfestLogin } from "@eternalfest/api-core/lib/hfest-identity/hfest-login.js";
import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { UserDisplayName } from "@eternalfest/api-core/lib/user/user-display-name.js";
import { UserId } from "@eternalfest/api-core/lib/user/user-id.js";
import incident from "incident";

export class InMemoryAuthService implements AuthService {
  async etwinOauth(_userId: UserId, _userDisplayName: UserDisplayName): Promise<AuthContext> {
    throw new incident.Incident("NotImplemented: InMemoryAuthService#etwinOauth");
  }

  async hfestCredentials(_server: HfestServer, _login: HfestLogin, _password: Uint8Array): Promise<AuthContext> {
    throw new incident.Incident("NotImplemented: InMemoryAuthService#hfestCredentials");
  }

  async hfestSession(_hfestSessionString: string): Promise<AuthContext> {
    throw new incident.Incident("NotImplemented: InMemoryAuthService#hfestSession");
  }

  async session(_sessionId: string): Promise<AuthContext> {
    throw new incident.Incident("NotImplemented: InMemoryAuthService#session");
  }
}
