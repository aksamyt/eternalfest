import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { CreateUserWithCredentialsOptions } from "@eternalfest/api-core/lib/user/create-user-with-credentials-options.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options.js";
import { UserDisplayName } from "@eternalfest/api-core/lib/user/user-display-name.js";
import { UserRef } from "@eternalfest/api-core/lib/user/user-ref.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";

export const DEFAULT_USER_ID: UuidHex = "00000000-0000-0000-0000-000000000000";
export const DEFAULT_USER_DISPLAY_NAME: UserDisplayName = "Eternaldev";

export class InMemoryUserService implements UserService {
  async createUserWithCredentials(
    _auth: AuthContext,
    _options: CreateUserWithCredentialsOptions,
  ): Promise<User> {
    return {
      id: DEFAULT_USER_ID,
      type: ActorType.User as ActorType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async createUserWithHfestIdentity(
    _auth: AuthContext,
    _hfestIdentity: HfestIdentity,
  ): Promise<User> {
    return {
      id: DEFAULT_USER_ID,
      type: ActorType.User as ActorType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async getUserById(_auth: AuthContext, _userId: UuidHex): Promise<User | undefined> {
    return {
      id: DEFAULT_USER_ID,
      type: ActorType.User as ActorType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async getUsers(_auth: AuthContext): Promise<readonly Partial<User>[]> {
    return [{
      id: DEFAULT_USER_ID,
      type: ActorType.User as ActorType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    }];
  }

  async updateUser(_auth: AuthContext, _options: UpdateUserOptions): Promise<User> {
    return {
      id: DEFAULT_USER_ID,
      type: ActorType.User as ActorType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async getUserRefById(
    _auth: AuthContext,
    userId: string,
  ): Promise<UserRef | undefined> {
    if (userId !== DEFAULT_USER_ID) {
      return undefined;
    }
    return {
      id: DEFAULT_USER_ID,
      type: ActorType.User as ActorType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
    };
  }

  async getUserRefsById(
    auth: AuthContext,
    _userIds: ReadonlySet<UuidHex>,
  ): Promise<Map<UuidHex, UserRef | undefined>> {
    const user: UserRef | undefined = await this.getUserRefById(auth, DEFAULT_USER_ID);
    return new Map([[DEFAULT_USER_ID, user]]);
  }

  async getOrCreateUserWithEtwin(_auth: AuthContext, userId: UuidHex, userDisplay: UserDisplayName): Promise<User> {
    return {
      id: userId,
      type: ActorType.User as ActorType.User,
      displayName: userDisplay,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }
}
