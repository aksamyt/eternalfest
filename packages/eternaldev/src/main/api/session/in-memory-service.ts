import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";

export class InMemorySessionService implements SessionService {
  async createSession(_authContext: AuthContext, _userId: UuidHex): Promise<Session> {
    return {
      id: "00000000-0000-0000-0000-000000000000",
      user: {
        id: "00000000-0000-0000-0000-000000000000",
      },
      createdAt: new Date(),
      updatedAt: new Date(),
    };
  }

  async deleteSessionById(_authContext: AuthContext, _sessionId: UuidHex): Promise<void> {
  }

  async getAndTouchSessionById(_authContext: AuthContext, _sessionId: UuidHex): Promise<Session | undefined> {
    return {
      id: "00000000-0000-0000-0000-000000000000",
      user: {
        id: "00000000-0000-0000-0000-000000000000",
      },
      createdAt: new Date(),
      updatedAt: new Date(),
    };
  }

  async getSessionById(_authContext: AuthContext, _sessionId: UuidHex): Promise<Session | undefined> {
    return {
      id: "00000000-0000-0000-0000-000000000000",
      user: {
        id: "00000000-0000-0000-0000-000000000000",
      },
      createdAt: new Date(),
      updatedAt: new Date(),
    };
  }
}
