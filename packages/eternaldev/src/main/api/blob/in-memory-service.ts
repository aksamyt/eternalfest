import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { CreateBlobOptions } from "@eternalfest/api-core/lib/blob/create-blob-options.js";
import { CreateUploadSessionOptions } from "@eternalfest/api-core/lib/blob/create-upload-session-options.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { UploadBytesOptions } from "@eternalfest/api-core/lib/blob/upload-bytes-options.js";
import { UploadSession } from "@eternalfest/api-core/lib/blob/upload-session.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import stream from "stream";

export class InMemoryBlobService implements BlobService {
  async createBlob(_auth: AuthContext, _options: CreateBlobOptions): Promise<Blob> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#createBlob");
  }

  async createUploadSession(_auth: AuthContext, _options: CreateUploadSessionOptions): Promise<UploadSession> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#createUploadSession");
  }

  async getBlobById(_auth: AuthContext, _blobId: UuidHex): Promise<Blob> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#getBlobById");
  }

  async readBlobData(_auth: AuthContext, _blobId: UuidHex): Promise<stream.Readable> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#readBlobData");
  }

  uploadBytes(_auth: AuthContext, _uploadSessionId: UuidHex, _options: UploadBytesOptions): Promise<UploadSession> {
    throw new incident.Incident("NotImplemented: InMemoryBlobService#uploadBytes");
  }
}
