import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $Ucs2String } from "kryo/lib/ucs2-string.js";

export interface ManifestObf {
  key: string;
}

export const $ManifestObf: RecordIoType<ManifestObf> = new RecordType<ManifestObf>({
  properties: {
    key: {type: $Ucs2String},
  },
  changeCase: CaseStyle.SnakeCase,
});
