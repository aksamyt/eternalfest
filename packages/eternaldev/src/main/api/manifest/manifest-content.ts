import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TryUnionType } from "kryo/lib/try-union.js";

import { $UrlFragment, UrlFragment } from "../../../universal/url-fragment.js";
import { $ManifestLevelMap, ManifestLevelMap } from "./manifest-level-map.js";

export interface ManifestContent {
  build: UrlFragment;
  scoreItems: UrlFragment;
  specialItems: UrlFragment;
  dimensions: UrlFragment;
  data?: UrlFragment;
  levels: UrlFragment | ManifestLevelMap;
}

export const $ManifestContent: RecordIoType<ManifestContent> = new RecordType<ManifestContent>({
  properties: {
    build: {type: $UrlFragment},
    scoreItems: {type: $UrlFragment},
    specialItems: {type: $UrlFragment},
    dimensions: {type: $UrlFragment},
    data: {type: $UrlFragment, optional: true},
    levels: {type: new TryUnionType({variants: [$UrlFragment, $ManifestLevelMap]})},
  },
  changeCase: CaseStyle.SnakeCase,
});
