import { $GameModeKey, GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { MapType } from "kryo/lib/map.js";

import { $ManifestMode, ManifestMode } from "./manifest-mode.js";

export type ManifestModeMap = Map<GameModeKey, ManifestMode>;

export const $ManifestModeMap: MapType<GameModeKey, ManifestMode> = new MapType({
  keyType: $GameModeKey,
  valueType: $ManifestMode,
  maxSize: Infinity,
  assumeStringKey: true,
});
