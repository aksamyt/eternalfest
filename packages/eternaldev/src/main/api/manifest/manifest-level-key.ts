import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type ManifestLevelKey = string;

export const $ManifestLevelKey: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
