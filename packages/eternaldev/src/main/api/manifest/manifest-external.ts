import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $UrlFragment, UrlFragment } from "../../../universal/url-fragment.js";

export interface ManifestExternal {
  build: UrlFragment;
}

export const $ManifestExternal: RecordIoType<ManifestExternal> = new RecordType<ManifestExternal>({
  properties: {
    build: {type: $UrlFragment},
  },
  changeCase: CaseStyle.SnakeCase,
});
