import { $GameDescription, GameDescription } from "@eternalfest/api-core/lib/game/game-description.js";
import { $GameDisplayName, GameDisplayName } from "@eternalfest/api-core/lib/game/game-display-name.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TryUnionType } from "kryo/lib/try-union.js";

import { $UrlFragment, UrlFragment } from "../../../universal/url-fragment.js";
import { $ManifestLocaleContent, ManifestLocaleContent } from "./manifest-locale-content.js";

export interface ManifestLocale {
  build: UrlFragment;
  displayName?: GameDisplayName;
  description?: GameDescription;
  content: UrlFragment | ManifestLocaleContent;
}

export const $ManifestLocale: RecordIoType<ManifestLocale> = new RecordType<ManifestLocale>({
  properties: {
    build: {type: $UrlFragment},
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    content: {type: new TryUnionType({variants: [$UrlFragment, $ManifestLocaleContent]})},
  },
  changeCase: CaseStyle.SnakeCase,
});
