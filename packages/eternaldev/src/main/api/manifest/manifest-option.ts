import { $GameOptionDisplayName, GameOptionDisplayName } from "@eternalfest/api-core/lib/game/game-option-display-name.js";
import { $GameOptionState, GameOptionState } from "@eternalfest/api-core/lib/game/game-option-state.js";
import { DeepReadonly } from "@eternalfest/api-core/lib/types/deep-readonly.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { GameOptionDescription } from "./game-option-description.js";

export interface ManifestOption {
  state: GameOptionState;
  displayName: GameOptionDisplayName;
  description?: GameOptionDescription;
}

export type ReadonlyManifestOption = DeepReadonly<ManifestOption>;

export const $ManifestOption: RecordIoType<ManifestOption> = new RecordType<ManifestOption>({
  properties: {
    displayName: {type: $GameOptionDisplayName},
    description: {type: $GameOptionDisplayName, optional: true},
    state: {type: $GameOptionState},
  },
  changeCase: CaseStyle.SnakeCase,
});
