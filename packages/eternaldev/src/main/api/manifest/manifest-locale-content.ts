import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $UrlFragment, UrlFragment } from "../../../universal/url-fragment.js";

export interface ManifestLocaleContent {
  lands: UrlFragment;
  keys: UrlFragment;
  items: UrlFragment;
  statics: UrlFragment;
}

export const $ManifestLocaleContent: RecordIoType<ManifestLocaleContent> = new RecordType<ManifestLocaleContent>({
  properties: {
    lands: {type: $UrlFragment},
    keys: {type: $UrlFragment},
    items: {type: $UrlFragment},
    statics: {type: $UrlFragment},
  },
  changeCase: CaseStyle.SnakeCase,
});
