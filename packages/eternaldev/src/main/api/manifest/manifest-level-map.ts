import { MapType } from "kryo/lib/map.js";

import { $ManifestLevelKey, ManifestLevelKey } from "./manifest-level-key.js";
import { $ManifestLevel, ManifestLevel } from "./manifest-level.js";

export type ManifestLevelMap = Map<ManifestLevelKey, ManifestLevel>;

export const $ManifestLevelMap: MapType<ManifestLevelKey, ManifestLevel> = new MapType({
  keyType: $ManifestLevelKey,
  valueType: $ManifestLevel,
  maxSize: Infinity,
  assumeStringKey: true,
});
