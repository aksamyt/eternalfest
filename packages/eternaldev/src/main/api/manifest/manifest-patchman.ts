import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $UrlFragment, UrlFragment } from "../../../universal/url-fragment.js";

export interface ManifestPatchman {
  build: UrlFragment;
}

export const $ManifestPatchman: RecordIoType<ManifestPatchman> = new RecordType<ManifestPatchman>({
  properties: {
    build: {type: $UrlFragment},
  },
  changeCase: CaseStyle.SnakeCase,
});
