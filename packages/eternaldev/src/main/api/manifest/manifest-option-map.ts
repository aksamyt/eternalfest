import { $GameOptionKey, GameOptionKey } from "@eternalfest/api-core/lib/game/game-option-key.js";
import { MapType } from "kryo/lib/map.js";

import { $ManifestOption, ManifestOption } from "./manifest-option.js";

export type ManifestOptionMap = Map<GameOptionKey, ManifestOption>;

export const $ManifestOptionMap: MapType<GameOptionKey, ManifestOption> = new MapType({
  keyType: $GameOptionKey,
  valueType: $ManifestOption,
  maxSize: Infinity,
  assumeStringKey: true,
});
