import { GameDescription } from "@eternalfest/api-core/lib/game/game-description.js";
import { GameDisplayName } from "@eternalfest/api-core/lib/game/game-display-name.js";
import { $GameModeDisplayName } from "@eternalfest/api-core/lib/game/game-mode-display-name.js";
import { $GameModeState, GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $GameModeDescription } from "./game-mode-description.js";
import { $ManifestOptionMap, ManifestOptionMap } from "./manifest-option-map.js";

export interface ManifestMode {
  state: GameModeState;
  displayName: GameDisplayName;
  description?: GameDescription;
  options?: ManifestOptionMap;
}

export const $ManifestMode: RecordIoType<ManifestMode> = new RecordType<ManifestMode>({
  properties: {
    state: {type: $GameModeState},
    displayName: {type: $GameModeDisplayName},
    description: {type: $GameModeDescription, optional: true},
    options: {type: $ManifestOptionMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
