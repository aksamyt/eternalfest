import { $LocaleId, LocaleId } from "@eternalfest/api-core/lib/types/locale-id.js";
import { MapType } from "kryo/lib/map.js";

import { $ManifestLocale, ManifestLocale } from "./manifest-locale.js";

export type ManifestLocaleMap = Map<LocaleId, ManifestLocale>;

export const $ManifestLocaleMap: MapType<LocaleId, ManifestLocale> = new MapType({
  keyType: $LocaleId,
  valueType: $ManifestLocale,
  maxSize: Infinity,
  assumeStringKey: true,
});
