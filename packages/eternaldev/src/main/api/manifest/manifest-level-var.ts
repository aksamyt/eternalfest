import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type ManifestLevelVar = string;

export const $ManifestLevelVar: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
