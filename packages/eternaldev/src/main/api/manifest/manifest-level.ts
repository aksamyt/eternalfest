import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/lib/boolean.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $Ucs2String } from "kryo/lib/ucs2-string.js";

import { $UrlFragment, UrlFragment } from "../../../universal/url-fragment.js";
import { $ManifestLevelVar, ManifestLevelVar } from "./manifest-level-var.js";

export interface ManifestLevel {
  levels: UrlFragment;
  var: ManifestLevelVar;
  padding?: boolean;
  sameSize?: boolean;
  did?: string;
}

export const $ManifestLevel: RecordIoType<ManifestLevel> = new RecordType<ManifestLevel>({
  properties: {
    levels: {type: $UrlFragment},
    var: {type: $ManifestLevelVar},
    padding: {type: $Boolean, optional: true},
    sameSize: {type: $Boolean, optional: true},
    did: {type: $Ucs2String, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
