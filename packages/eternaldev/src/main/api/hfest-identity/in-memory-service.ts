import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity.js";
import { LinkUserOptions } from "@eternalfest/api-core/lib/hfest-identity/link-user-options.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { UnlinkUserOptions } from "@eternalfest/api-core/lib/hfest-identity/unlink-user-options.js";
import incident from "incident";

export class InMemoryHfestIdentityService implements HfestIdentityService {
  createOrUpdateHfestIdentity(_hfestSessionString: string): Promise<HfestIdentity> {
    throw new incident.Incident("NotImplemented: InMemoryHfestIdentityService#createOrUpdateHfestIdentity");
  }

  linkUser(_auth: AuthContext, _linkUserOptions: LinkUserOptions): Promise<void> {
    throw new incident.Incident("NotImplemented: InMemoryHfestIdentityService#linkUser");
  }

  unlinkUser(_auth: AuthContext, _unlinkUserOptions: UnlinkUserOptions): Promise<void> {
    throw new incident.Incident("NotImplemented: InMemoryHfestIdentityService#unlinkUser");
  }
}
