import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { MediaType } from "@eternalfest/api-core/lib/blob/media-type.js";
import { CreateDirectoryOptions } from "@eternalfest/api-core/lib/file/create-directory-options.js";
import { CreateFileOptions } from "@eternalfest/api-core/lib/file/create-file-options.js";
import { Directory } from "@eternalfest/api-core/lib/file/directory.js";
import { DriveItemDisplayName } from "@eternalfest/api-core/lib/file/drive-item-display-name.js";
import { DriveItemType } from "@eternalfest/api-core/lib/file/drive-item-type.js";
import { DriveItem } from "@eternalfest/api-core/lib/file/drive-item.js";
import { Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { File } from "@eternalfest/api-core/lib/file/file.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { toUuid } from "@eternalfest/api-core/lib/utils/helpers.js";
import fs from "fs";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import stream from "stream";
import url from "url";

interface LocalFile {
  url: url.URL;
}

async function fileFromLocalFile(localFile: LocalFile): Promise<File> {
  const id: UuidHex = toUuid(localFile.url.toString());
  let mediaType: MediaType;
  if (localFile.url.toString().match(/\.swf$/)) {
    mediaType = "application/x-shockwave-flash";
  } else if (localFile.url.toString().match(/\.xml$/)) {
    mediaType = "application/xml";
  }  else if (localFile.url.toString().match(/\.mp3$/)) {
    mediaType = "audio/mp3";
  }  else if (localFile.url.toString().match(/\.png$/)) {
    mediaType = "image/png";
  } else {
    mediaType = "application/octet-stream";
  }

  const stats: fs.Stats = fs.statSync(localFile.url);

  return {
    id,
    displayName: "File",
    createdAt: stats.ctime,
    updatedAt: stats.mtime,
    type: DriveItemType.File,
    byteSize: stats.size,
    mediaType: mediaType,
  };
}

export class LocalFileService implements FileService {
  private readonly idToLocalFile: Map<UuidHex, LocalFile>;

  constructor() {
    this.idToLocalFile = new Map();
  }

  async createFileFromUrl(fileUrl: url.URL): Promise<File> {
    const id: UuidHex = toUuid(fileUrl.toString());
    const localFile: LocalFile = {url: fileUrl};
    this.idToLocalFile.set(id, localFile);
    return fileFromLocalFile(localFile);
  }

  createDirectory(_auth: AuthContext, _options: CreateDirectoryOptions): Promise<Directory> {
    throw new incident.Incident("NotImplemented: LocalFileService#createDirectory");
  }

  createFile(_auth: AuthContext, _options: CreateFileOptions): Promise<File> {
    throw new incident.Incident("NotImplemented: LocalFileService#createFile");
  }

  deleteItem(_auth: AuthContext, _itemId: UuidHex): Promise<void> {
    throw new incident.Incident("NotImplemented: LocalFileService#deleteItem");
  }

  getDirectoryById(_auth: AuthContext, _directoryId: UuidHex): Promise<Directory> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDirectoryById");
  }

  async getDirectoryChildrenById(_auth: AuthContext, _directoryId: UuidHex): Promise<readonly DriveItem[]> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDirectoryChildrenById");
  }

  getDriveById(_auth: AuthContext, _driveId: UuidHex): Promise<Drive> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDriveById");
  }

  getDriveByOwnerId(_auth: AuthContext, _ownerId: UuidHex): Promise<Drive> {
    throw new incident.Incident("NotImplemented: LocalFileService#getDriveByOwnerId");
  }

  getFileById(_auth: AuthContext, fileId: UuidHex): Promise<File> {
    const localFile: LocalFile | undefined = this.idToLocalFile.get(fileId);
    if (localFile === undefined) {
      throw new Error("FileNotFound");
    }
    return fileFromLocalFile(localFile);
  }

  getItemByPath(
    _auth: AuthContext,
    _driveId: UuidHex,
    _itemPath: ReadonlyArray<DriveItemDisplayName>,
  ): Promise<DriveItem> {
    throw new incident.Incident("NotImplemented: LocalFileService#getItemByPath");
  }

  async readFileContent(_auth: AuthContext, fileId: UuidHex): Promise<stream.Readable> {
    const localFile: LocalFile | undefined = this.idToLocalFile.get(fileId);
    if (localFile === undefined) {
      throw new Error("FileNotFound");
    }
    return fs.createReadStream(localFile.url);
  }
}
