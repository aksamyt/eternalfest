import { $GameDescription, GameDescription } from "@eternalfest/api-core/lib/game/game-description.js";
import { $GameDisplayName, GameDisplayName } from "@eternalfest/api-core/lib/game/game-display-name.js";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

export interface ShortProject {
  id: UuidHex;
  localUrl: string;
  displayName: GameDisplayName;
  description?: GameDescription;
}

export const $ShortProject: RecordIoType<ShortProject> = new RecordType<ShortProject>({
  properties: {
    id: {type: $UuidHex},
    localUrl: {type: new Ucs2StringType({maxLength: Infinity})},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
