import { FamiliesString } from "@eternalfest/api-core/lib/game/families-string.js";
import { GameDescription } from "@eternalfest/api-core/lib/game/game-description.js";
import { GameDisplayName } from "@eternalfest/api-core/lib/game/game-display-name.js";
import { GameEngines } from "@eternalfest/api-core/lib/game/game-engines.js";
import { GameModeDisplayName } from "@eternalfest/api-core/lib/game/game-mode-display-name.js";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { GameMode } from "@eternalfest/api-core/lib/game/game-mode.js";
import { GameOptionDisplayName } from "@eternalfest/api-core/lib/game/game-option-display-name.js";
import { GameOptionKey } from "@eternalfest/api-core/lib/game/game-option-key.js";
import { toUuid } from "@eternalfest/api-core/lib/utils/helpers.js";
import fs from "fs";
import furi from "furi";
import incident from "incident";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import toml from "toml";
import url from "url";

import { LocalFileRef } from "../../../universal/local-file-ref.js";
import { ManifestResource } from "../../../universal/manifest-resource.js";
import { ProfileMap } from "../../../universal/profile-map.js";
import { $ProjectManifest, ProjectManifest } from "../../../universal/project-manifest.js";
import { Project } from "../../../universal/project.js";

export async function loadLocalProject(root: url.URL, allowWarning: boolean): Promise<Project> {
  const manifest: ProjectManifest = await readManifest(root);
  return resolveProject(manifest, root, allowWarning);
}

const DEFAULT_GAME_ENGINES: Readonly<GameEngines> = {loader: "3.0.0"};

const DEFAULT_FAMILIES: FamiliesString = "0,7,1000,1001,1002,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19";

async function resolveProject(manifest: ProjectManifest, root: url.URL, allowWarning: boolean): Promise<Project> {
  const id: UuidHex = toUuid(root.toString());
  const remoteId: UuidHex | undefined = manifest.remoteId;
  const localUrl: string = root.toString();
  const displayName: GameDisplayName = manifest.displayName !== undefined ? manifest.displayName : "Untitled";
  const description: GameDescription = manifest.description !== undefined ? manifest.description : "No description";

  const engines: GameEngines = manifest.engines !== undefined
    ? {...DEFAULT_GAME_ENGINES, ...manifest.engines}
    : DEFAULT_GAME_ENGINES;

  const resources: ReadonlyArray<ManifestResource> = manifest.resources !== undefined ? manifest.resources : [];
  const icon: LocalFileRef | undefined = manifest.icon !== undefined ? {url: manifest.icon} : undefined;
  const gameModes: GameMode[] = [];
  if (manifest.modes !== undefined) {
    for (const manifestMode of manifest.modes) {
      let key: GameModeKey;
      if (manifestMode.key !== undefined) {
        key = manifestMode.key;
      } else if (manifestMode.id !== undefined) {
        key = manifestMode.id;
        if (allowWarning) {
          console.warn(`Deprecated: Use \`key\` instead of \`id\` to define the game mode \`${key}\` (${root.href})`);
        }
      } else {
        throw new Error("Game mode in manifest file is missing the required `key` property");
      }
      let displayName: GameModeDisplayName;
      if (manifestMode.displayName !== undefined) {
        displayName = manifestMode.displayName;
      } else {
        displayName = gameModeKeyToDisplayName(key);
        if (allowWarning) {
          console.warn(`Deprecated: Missing \`display_name\` in game mode \`${key}\` (${displayName}) (${root.href})`);
        }
      }

      const gameMode: GameMode = {key, displayName, state: manifestMode.state, options: [], locales: new Map()};

      for (const manifestOption of manifestMode.options) {
        let key: GameOptionKey;
        if (manifestOption.key !== undefined) {
          key = manifestOption.key;
        } else if (manifestOption.id !== undefined) {
          key = manifestOption.id;
          if (allowWarning) {
            // tslint:disable-next-line:max-line-length
            console.warn(`Deprecated: Use \`key\` instead of \`id\` to define the game option \`${key}\` (${root.href})`);
          }
        } else {
          throw new Error("Game option in manifest file is missing the required `key` property");
        }
        let displayName: GameModeDisplayName;
        if (manifestOption.displayName !== undefined) {
          displayName = manifestOption.displayName;
        } else {
          displayName = gameOptionKeyToDisplayName(key);
          if (allowWarning) {
            console.warn(`Deprecated: Missing \`display_name\` in option \`${key}\` (${displayName}) (${root.href})`);
          }
        }
        gameMode.options.push({key, displayName, state: manifestOption.state, locales: new Map()});
      }
      gameModes.push(gameMode);
    }
  }
  const families: FamiliesString = manifest.families !== undefined ? manifest.families : DEFAULT_FAMILIES;
  const profiles: ProfileMap = manifest.profiles !== undefined ? manifest.profiles : new Map();

  return {
    id,
    remoteId,
    localUrl,
    displayName,
    description,
    icon,
    engines,
    resources,
    families,
    gameModes,
    profiles,
  };
}

export async function readManifest(root: url.URL): Promise<ProjectManifest> {
  {
    const resolved: url.URL = furi.join(root, ["eternalfest.toml"]);
    const text: string | undefined = await tryReadTextAsync(resolved);
    if (text !== undefined) {
      return $ProjectManifest.read(JSON_VALUE_READER, toml.parse(text));
    }
  }
  {
    const resolved: url.URL = furi.join(root, ["eternalfest.json"]);
    const text: string | undefined = await tryReadTextAsync(resolved);
    if (text !== undefined) {
      return $ProjectManifest.read(JSON_VALUE_READER, JSON.parse(text));
    }
  }
  throw new incident.Incident("ManifestNotFound", {root: root.toString()});
}

async function tryReadTextAsync(filePath: url.URL): Promise<string | undefined> {
  return new Promise<string | undefined>((resolve, _reject) => {
    fs.readFile(filePath, {encoding: "utf-8"}, (err: NodeJS.ErrnoException | null, data: string) => {
      if (err !== null) {
        if (err.code !== "ENOENT") {
          console.warn(err);
        }
        resolve(undefined);
      } else {
        resolve(data);
      }
    });
  });
}

export function gameModeKeyToDisplayName(key: GameModeKey): GameModeDisplayName {
  switch (key) {
    case "bossrush":
      return "Boss Rush";
    case "multicoop":
      return "Multi Coopératif";
    case "multitime":
      return "TA Multi";
    case "soccer":
      return "Soccerfest";
    case "soccer_0":
      return "Gazon maudit";
    case "soccer_1":
      return "Temple du Ballon";
    case "soccer_2":
      return "VolleyFest";
    case "soccer_3":
      return "Maitrise aérienne";
    case "soccer_4":
      return "Piste temporelle";
    case "soccer_5":
      return "Stade de Basalte";
    case "soccer_6":
      return "Coupe de glace";
    case "soccer_7":
      return "Carton noir";
    case "soccer_8":
      return "Terrain oublié";
    case "soccer_9":
      return "Orange gardien";
    case "soccer_10":
      return "Chaudron cracheur";
    case "soccer_11":
      return "Salle à pics";
    case "soccer_12":
      return "Kiwi Arena";
    case "soccer_13":
      return "Cages inviolées";
    case "soccer_14":
      return "Dojo Fulguro";
    case "soccer_alea":
      return "Terrain aléatoire";
    case "solo":
      return "Aventure";
    case "timeattack":
      return "Time Attack";
    case "tutorial":
      return "Apprentissage";
    default:
      return key;
  }
}

export function gameOptionKeyToDisplayName(key: GameOptionKey): GameOptionDisplayName {
  switch (key) {
    case "bombcontrol": return "Bombotopia";
    case "bombexpert": return "Explosifs Instables";
    case "boost": return "Tornade";
    case "ice": return "Âge de Glace";
    case "insight": return "Intuition";
    case "kickcontrol": return "Contrôle avancé du ballon";
    case "lava": return "Lave";
    case "lifesharing": return "Partage de vies";
    case "mirror": return "Miroir";
    case "mirrormulti": return "Miroir";
    case "nightmare": return "Cauchemar";
    case "nightmaremulti": return "Cauchemar";
    case "ninja": return "Ninjutsu";
    case "nojutsu": return "Nojutsu";
    case "randomfest": return "Randomfest";
    case "soccerbomb": return "Bombes";
    case "soccer_short": return "Durée de match courte";
    case "soccer_normal": return "Durée de match normale";
    case "soccer_long": return "Durée de match longue";
    default: return key;
  }
}
