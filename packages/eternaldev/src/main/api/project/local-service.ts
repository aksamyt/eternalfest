import { FileService } from "@eternalfest/api-core/lib/file/service";
import { FamiliesString } from "@eternalfest/api-core/lib/game/families-string.js";
import { GameDisplayName } from "@eternalfest/api-core/lib/game/game-display-name.js";
import { $GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state.js";
import { GameMode } from "@eternalfest/api-core/lib/game/game-mode.js";
import { $GameOptionState } from "@eternalfest/api-core/lib/game/game-option-state.js";
import { GameOption } from "@eternalfest/api-core/lib/game/game-option.js";
import { toUuid } from "@eternalfest/api-core/lib/utils/helpers.js";
import furi from "furi";
import incident from "incident";
import { JsonValueReader } from "kryo-json/lib/json-value-reader.js";
import { JsonWriter } from "kryo-json/lib/json-writer.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import rx from "rxjs";
import rxOp from "rxjs/operators/index.js";
import toml from "toml";
import { fileURLToPath, URL } from "url";

import { LocalFileRef } from "../../../universal/local-file-ref.js";
import { ManifestResource } from "../../../universal/manifest-resource.js";
import { ProfileMap } from "../../../universal/profile-map.js";
import { Project } from "../../../universal/project.js";
import { readTextAsync } from "../../fs-utils.js";
import { LocalFileService } from "../file/local-service.js";
import { ManifestContent } from "../manifest/manifest-content.js";
import { ManifestExternal } from "../manifest/manifest-external.js";
import { ManifestLevel } from "../manifest/manifest-level.js";
import { ManifestLocaleMap } from "../manifest/manifest-locale-map.js";
import { ManifestModeMap } from "../manifest/manifest-mode-map.js";
import { ManifestPatchman } from "../manifest/manifest-patchman.js";
import { $Manifest, Manifest } from "../manifest/manifest.js";
import { CompilerResult as Cr, error as crError, of as crOf } from "./compiler-result.js";
import { getObservableFileVersion } from "./observable-fs.js";
import { ShortProject } from "./short-project.js";

const JSON_VALUE_READER: JsonValueReader = new JsonValueReader();
const JSON_WRITER: JsonWriter = new JsonWriter();

const DEFAULT_FAMILIES: FamiliesString = "0,7,1000,1001,1002,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19";

export class LocalProjectService {
  private readonly fileService: LocalFileService;
  private readonly furiToManifest: Map<string, rx.Observable<Cr<Manifest | undefined>>>;
  private readonly idToFuri: Map<UuidHex, URL>;
  private readonly furiToId: Map<string, UuidHex>;

  public constructor(fileService: LocalFileService, projectDirs: Iterable<URL>) {
    const furiToManifest: Map<string, rx.Observable<Cr<Manifest | undefined>>> = new Map();
    const idToFuri: Map<UuidHex, URL> = new Map();
    const furiToId: Map<string, UuidHex> = new Map();

    for (const projectDir of projectDirs) {
      const projectDirStr: string = projectDir.toString();
      if (!furiToManifest.has(projectDirStr)) {
        furiToManifest.set(projectDirStr, observeManifestByDir(projectDir));
        const id: UuidHex = toUuid(projectDirStr);
        idToFuri.set(id, projectDir);
        furiToId.set(projectDirStr, id);
      }
    }

    this.fileService = fileService;
    this.furiToManifest = furiToManifest;
    this.idToFuri = idToFuri;
    this.furiToId = furiToId;
  }

  public async getProjects(): Promise<ShortProject[]> {
    const shortProjects: rx.Observable<(ShortProject | undefined)>[] = [];
    for (const [dir, manifest$] of this.furiToManifest) {
      const shortProject$: rx.Observable<ShortProject | undefined> = manifest$
        .pipe(rxOp.map((manifestCr: Cr<Manifest | undefined>): ShortProject | undefined => {
          if (!manifestCr.hasValue()) {
            // An unexpected error occurred
            return undefined;
          }
          const manifest: Manifest | undefined = manifestCr.unwrap();
          if (manifest === undefined) {
            // Manifest not found
            return undefined;
          }
          const localUrl: string = dir.toString();
          const projectId: UuidHex | undefined = this.furiToId.get(localUrl);
          if (projectId === undefined) {
            throw new Error("AssertionError: Expected known id for project URI");
          }
          const displayName: GameDisplayName = manifest.displayName !== undefined ? manifest.displayName : "Untitled";
          const project: ShortProject = {
            id: projectId,
            localUrl,
            displayName,
            description: manifest.description,
          };
          return project;
        }));
      shortProjects.push(shortProject$);
    }

    const projects: ShortProject[] = await rx.combineLatest(shortProjects)
      .pipe(rxOp.map((projects: (ShortProject | undefined)[]): ShortProject[] => {
        const result: ShortProject[] = [];
        for (const p of projects) {
          if (p !== undefined) {
            result.push(p);
          }
        }
        return result;
      }))
      .pipe(rxOp.first())
      .toPromise();
    return projects;
  }

  public async getProjectById(projectId: UuidHex): Promise<Project | undefined> {
    const dir: URL | undefined = this.idToFuri.get(projectId);
    if (dir === undefined) {
      return undefined;
    }
    const manifest$: rx.Observable<Cr<Manifest | undefined>> | undefined = this.furiToManifest.get(dir.toString());
    if (manifest$ === undefined) {
      throw new Error("AssertionError: Expected known manifest for project URI");
    }
    const manifestCr: Cr<Manifest | undefined> = await manifest$.pipe(rxOp.first()).toPromise();
    const manifest: Manifest | undefined = manifestCr.unwrap();
    if (manifest === undefined) {
      return undefined;
    }
    return resolveProject(projectId, dir, this.fileService, manifest);
  }

  public async getProjectByRoot(root: URL): Promise<Project | undefined> {
    const id: UuidHex | undefined = this.furiToId.get(root.toString());
    if (id === undefined) {
      return undefined;
    }
    return this.getProjectById(id);
  }
}

function assertNonBareUrl(url: string, name: string): void {
  if (!url.startsWith("./") && !url.startsWith("../")) {
    throw new Error(`Invalid ${name}: must start with "./" or "../": ${JSON.stringify(url)}`);
  }
}

async function resolveProject(id: UuidHex, dir: URL, _fileService: FileService, manifest: Manifest): Promise<Project> {
  const displayName: GameDisplayName = manifest.displayName !== undefined ? manifest.displayName : "Untitled";

  let icon: LocalFileRef | undefined;
  if (manifest.icon !== undefined) {
    assertNonBareUrl(manifest.icon, "icon");
    icon = {url: manifest.icon};
  }

  const resources: ManifestResource[] = [];

  if (manifest.hf !== undefined) {
    assertNonBareUrl(manifest.hf, "hf");
    resources.push({path: manifest.hf, tag: "game-swf"});
  }

  if (manifest.content !== undefined) {
    assertNonBareUrl(manifest.content.build, "content.build");
    resources.push({path: manifest.content.build, tag: "game-xml"});
  }

  if (manifest.musics !== undefined) {
    for (const [i, music] of manifest.musics.entries()) {
      assertNonBareUrl(music, `musics[${i}]`);
      resources.push({path: music, tag: "music"});
    }
  }

  if (manifest.patchman !== undefined) {
    assertNonBareUrl(manifest.patchman.build, "patchman.build");
    resources.push({path: manifest.patchman.build, tag: "global-engine-patcher"});
  } else if (manifest.external !== undefined) {
    assertNonBareUrl(manifest.external.build, "external.build");
    resources.push({path: manifest.external.build, tag: "global-engine-patcher"});
  }

  const gameModes: GameMode[] = [];
  if (manifest.modes !== undefined) {
    for (const [key, mode] of manifest.modes) {
      const options: GameOption[] = [];
      if (mode.options !== undefined) {
        for (const [optionKey, option] of mode.options) {
          options.push({
            key: optionKey,
            displayName: option.displayName,
            state: option.state,
            locales: new Map(), // TODO
          });
        }
        gameModes.push({
          key,
          displayName: mode.displayName,
          state: mode.state,
          options,
          locales: new Map(), // TODO
        });
      }
    }
  }

  const families: FamiliesString = manifest.families !== undefined ? manifest.families : DEFAULT_FAMILIES;

  const profiles: ProfileMap = manifest.profiles !== undefined ? manifest.profiles : new Map();

  return {
    id,
    localUrl: dir.toString(),
    displayName,
    description: manifest.description,
    icon,
    remoteId: manifest.remoteId,
    engines: {loader: "3.0.0"},
    resources,
    families,
    gameModes,
    profiles,
  };
}

export function observeManifestByDir(path: URL): rx.Observable<Cr<Manifest | undefined>> {
  return rx.combineLatest([
    observeTomlManifest(furi.join(path, ["eternalfest.toml"])),
    observeJsonManifest(furi.join(path, ["eternalfest.json"])),
  ])
    .pipe(rxOp.map(([tomlCr, jsonCr]): Cr<Manifest | undefined> => {
      if (tomlCr.hasValue()) {
        const tomlManifest: Manifest | undefined = tomlCr.unwrap();
        if (tomlManifest !== undefined) {
          return tomlCr;
        }
      } else if (jsonCr.hasValue()) {
        const jsonManifest: Manifest | undefined = jsonCr.unwrap();
        if (jsonManifest !== undefined) {
          return jsonCr;
        }
      }
      if (fileURLToPath(path) === process.cwd()) {
        console.warn(tomlCr);
        console.warn(jsonCr);
      }
      return crError(new incident.Incident("ManifestNotFound", {path: path.toString()}));
    }));
}

function observeJsonManifest(path: URL): rx.Observable<Cr<Manifest | undefined>> {
  return getObservableFileVersion(path).pipe(rxOp.share(), rxOp.switchMap(() => getJsonManifest(path)));
}

async function getJsonManifest(path: URL): Promise<Cr<Manifest | undefined>> {
  let text: string;
  try {
    text = await readTextAsync(path);
  } catch (err) {
    if (err.code === "ENOENT") {
      return crOf(undefined);
    } else {
      throw err;
    }
  }
  let manifest: Manifest;
  try {
    const raw: any = JSON.parse(text);
    manifest = $Manifest.read(JSON_VALUE_READER, raw);
  } catch (err) {
    return crError(err);
  }
  return crOf(manifest);
}

function observeTomlManifest(path: URL): rx.Observable<Cr<Manifest | undefined>> {
  return getObservableFileVersion(path).pipe(rxOp.switchMap(() => getTomlManifest(path)));
}

async function getTomlManifest(path: URL): Promise<Cr<Manifest | undefined>> {
  let text: string;
  try {
    text = await readTextAsync(path);
  } catch (err) {
    if (err.code === "ENOENT") {
      return crOf(undefined);
    } else {
      throw err;
    }
  }
  let manifest: Manifest;
  try {
    const raw: any = toml.parse(text);
    manifest = $Manifest.read(JSON_VALUE_READER, raw);
  } catch (err) {
    return crError(err);
  }
  return crOf(manifest);
}

export function emitTomlManifest(manifest: Manifest): string {
  const lines: string[] = [];
  if (manifest.displayName !== undefined) {
    lines.push(`display_name = ${JSON.stringify(manifest.displayName)}`);
  }
  if (manifest.description !== undefined) {
    lines.push(`description = ${JSON.stringify(manifest.description)}`);
  }
  if (manifest.icon !== undefined) {
    lines.push(`icon = ${JSON.stringify(manifest.icon)}`);
  }
  if (manifest.remoteId !== undefined) {
    lines.push(`remote_id = ${JSON.stringify(manifest.remoteId)}`);
  }
  if (manifest.repository !== undefined) {
    lines.push(`repository = ${JSON.stringify(manifest.repository)}`);
  }
  if (manifest.mainLocale !== undefined) {
    lines.push(`main_locale = ${JSON.stringify(manifest.mainLocale)}`);
  }
  if (manifest.families !== undefined) {
    lines.push(`families = ${JSON.stringify(manifest.families)}`);
  }
  if (manifest.hf !== undefined) {
    lines.push(`hf = ${JSON.stringify(manifest.hf)}`);
  }
  if (manifest.musics !== undefined) {
    lines.push(`musics = ${JSON.stringify(manifest.musics, null, 2)}`);
  }
  if (manifest.content !== undefined) {
    lines.push("");
    lines.push("[content]");
    lines.push(emitTomlManifestContent(manifest.content));
  }
  if (manifest.patchman !== undefined) {
    lines.push("");
    lines.push("[patchman]");
    lines.push(emitTomlManifestPatchman(manifest.patchman));
  }
  if (manifest.external !== undefined) {
    lines.push("");
    lines.push("[external]");
    lines.push(emitTomlManifestExternal(manifest.external));
  }
  if (manifest.modes !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestModes(manifest.modes));
  }
  if (manifest.profiles !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestProfiles(manifest.profiles));
  }
  if (manifest.locales !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestLocales(manifest.locales));
  }
  lines.push("");
  return lines.join("\n");
}

function emitTomlManifestContent(content: ManifestContent): string {
  const lines: string[] = [];
  if (content.build !== undefined) {
    lines.push(`build = ${JSON.stringify(content.build)}`);
  }
  if (content.scoreItems !== undefined) {
    lines.push(`score_items = ${JSON.stringify(content.scoreItems)}`);
  }
  if (content.specialItems !== undefined) {
    lines.push(`special_items = ${JSON.stringify(content.specialItems)}`);
  }
  if (content.dimensions !== undefined) {
    lines.push(`dimensions = ${JSON.stringify(content.dimensions)}`);
  }
  if (content.data !== undefined) {
    lines.push(`data = ${JSON.stringify(content.data)}`);
  }
  if (content.levels !== undefined) {
    if (typeof content.levels === "string") {
      lines.push(`levels = ${JSON.stringify(content.levels)}`);
    } else {
      for (const [key, level] of content.levels) {
        lines.push(`[content.levels.${key}]`);
        lines.push(emitTomlManifestLevel(level));
      }
    }
  }
  return lines.join("\n");
}

function emitTomlManifestLevel(level: ManifestLevel): string {
  const lines: string[] = [];
  if (level.levels !== undefined) {
    lines.push(`levels = ${JSON.stringify(level.levels)}`);
  }
  if (level.var !== undefined) {
    lines.push(`var = ${JSON.stringify(level.var)}`);
  }
  if (level.padding !== undefined) {
    lines.push(`padding = ${JSON.stringify(level.padding)}`);
  }
  if (level.sameSize !== undefined) {
    lines.push(`same_size = ${JSON.stringify(level.sameSize)}`);
  }
  if (level.did !== undefined) {
    lines.push(`did = ${JSON.stringify(level.did)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestPatchman(pm: ManifestPatchman): string {
  const lines: string[] = [];
  if (pm.build !== undefined) {
    lines.push(`build = ${JSON.stringify(pm.build)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestExternal(external: ManifestExternal): string {
  const lines: string[] = [];
  if (external.build !== undefined) {
    lines.push(`build = ${JSON.stringify(external.build)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestModes(modes: ManifestModeMap): string {
  const lines: string[] = [];
  for (const [key, mode] of modes) {
    lines.push(`[modes.${key}]`);
    lines.push(`display_name = ${JSON.stringify(mode.displayName)}`);
    lines.push(`state = ${$GameModeState.write(JSON_WRITER, mode.state)}`);
    if (mode.options !== undefined) {
      lines.push(`[modes.${key}.options]`);
      for (const [okey, opt] of mode.options) {
        lines.push(`${okey} = {display_name = ${JSON.stringify(opt.displayName)}, state = ${$GameOptionState.write(JSON_WRITER, opt.state)}}`);
      }
    }
  }
  return lines.join("\n");
}

function emitTomlManifestProfiles(profiles: ProfileMap): string {
  const lines: string[] = [];
  for (const [key, profile] of profiles) {
    lines.push(`[profiles.${key}]`);
    if (profile.user !== undefined) {
      lines.push(`user = {id = ${JSON.stringify(profile.user.id)}, display_name = ${JSON.stringify(profile.user.displayName)}}`);
    }
    if (profile.items !== undefined) {
      const itemDict: string[] = [];
      for (const [id, count] of profile.items) {
        itemDict.push(`${id} = ${count}`);
      }
      lines.push(`items = {${itemDict.join(", ")}}`);
    }
  }
  return lines.join("\n");
}

function emitTomlManifestLocales(locales: ManifestLocaleMap): string {
  const lines: string[] = [];
  for (const [key, locale] of locales) {
    lines.push(`[locales.${key}]`);
    if (locale.build !== undefined) {
      lines.push(`build = ${JSON.stringify(locale.build)}`);
    }
    if (typeof locale.content === "string") {
      lines.push(`content = ${JSON.stringify(locale.content)}`);
    } else {
      lines.push(`[locales.${key}.content]`);
      lines.push(`keys = ${JSON.stringify(locale.content.keys)}`);
      lines.push(`items = ${JSON.stringify(locale.content.items)}`);
      lines.push(`lands = ${JSON.stringify(locale.content.lands)}`);
      lines.push(`statics = ${JSON.stringify(locale.content.statics)}`);
    }
  }
  return lines.join("\n");
}
