import efGame from "@eternalfest/game";
import efLoader from "@eternalfest/loader";
import furi from "furi";
import Koa from "koa";
import koaLogger from "koa-logger";
import koaMount from "koa-mount";
import koaRoute from "koa-route";
import koaSend from "koa-send";
import sysPath from "path";
import url from "url";

import { createLocalApiRouter } from "./api-router.js";
import { createNgRouter } from "./ng-router.js";
import { LocalEfApi } from "./with-ef-api.js";

const LOADER_SYS_PATH: string = furi.toSysPath(efLoader.getLoaderUri(efLoader.Version.Flash8));
const GAME_SYS_PATH: string = efGame.getGamePath();

export async function createApp(efApi: LocalEfApi, defaultNgAppDir: url.URL): Promise<Koa> {
  const apiRouter: Koa = await createLocalApiRouter(efApi);
  const defaultNgRouter: Koa = await createNgRouter(efApi, defaultNgAppDir);

  const app: Koa = new Koa();
  app.use(koaLogger());
  app.use(koaMount("/api/v1", apiRouter));
  app.use(koaRoute.get("/assets/loader.swf", sendLoader));
  app.use(koaRoute.get("/assets/game.swf", sendGame));
  app.use(koaMount(defaultNgRouter));

  async function sendLoader(ctx: Koa.Context): Promise<void> {
    await koaSend(ctx, sysPath.basename(LOADER_SYS_PATH), {root: sysPath.dirname(LOADER_SYS_PATH)});
  }

  async function sendGame(ctx: Koa.Context): Promise<void> {
    await koaSend(ctx, sysPath.basename(GAME_SYS_PATH), {root: sysPath.dirname(GAME_SYS_PATH)});
  }

  return app;
}
