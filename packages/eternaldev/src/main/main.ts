#!/usr/bin/env node

import devkit from "@eternalfest/devkit";
import efcCli from "@eternalfest/efc/cli.js";
import furi from "furi";
import { URL } from "url";
import yargs from "yargs";

import { build } from "../cli/build.js";
import { init } from "../cli/init.js";
import { upgradeProject } from "../cli/upgrade-project.js";
import { uploadProject } from "../cli/upload-project.js";
import { startServer } from "./main.server.js";
import { VERSION } from "./version.js";

const startCommand: yargs.CommandModule<any, any> = {
  command: ["start"],
  aliases: [],
  builder: {},
  describe: "Start Eternaldev in the current directory",
  handler: (_args: any) => {
    console.log("Starting development server...");
    startServer()
      .catch((err: Error): never => {
        console.error(err);
        process.exit(1);
      });
  },
};

const DEFAULT_API_BASE: string = "https://eternalfest.net/api/v1";

const buildCommand: yargs.CommandModule<any, any> = {
  command: ["build"],
  aliases: [],
  describe: "Build the current project",
  handler: async (_args: yargs.Arguments): Promise<never> => {
    try {
      await build(furi.fromSysPath(process.cwd()));
      process.exit(0);
    } catch (err) {
      console.error(err.stack);
      process.exit(1);
    }
  },
};

const initCommand: yargs.CommandModule<any, any> = {
  command: ["init [name]"],
  aliases: [],
  describe: "Initialize the provided directory",
  handler: async (args: yargs.Arguments<{name?: string}>): Promise<never> => {
    try {
      const name: string | undefined = args.name;
      const cwd: URL = furi.fromSysPath(process.cwd());
      const dir: URL = name !== undefined ? furi.join(cwd, name) : cwd;
      await init(dir);
      process.exit(0);
    } catch (err) {
      console.error(err.stack);
      process.exit(1);
    }
  },
};

const uploadCommand: yargs.CommandModule<any, any> = {
  command: ["upload"],
  aliases: [],
  builder: {
    api: {
      type: "string",
      default: DEFAULT_API_BASE,
    },
  },
  describe: "Upload the current game",
  handler: async (args: yargs.Arguments): Promise<never> => {
    try {
      const {api} = args;
      if (typeof api !== "string") {
        throw new Error("InvalidCliArguments: expected --api to be a string");
      }
      await uploadProject(api);
      process.exit(0);
    } catch (err) {
      console.error(err.stack);
      process.exit(1);
    }
  },
};

const upgradeCommand: yargs.CommandModule<any, any> = {
  command: ["upgrade"],
  aliases: [],
  describe: "Upgrade the current project",
  handler: async (_args: yargs.Arguments): Promise<never> => {
    try {
      await upgradeProject();
      process.exit(0);
    } catch (err) {
      console.error(err.stack);
      process.exit(1);
    }
  },
};

const gameBundlerCommand: yargs.CommandModule<any, any> = {
  command: ["game-bundler"],
  aliases: ["bundler"],
  describe: "Start the game bundler",
  handler: (_args: yargs.Argv) => {
    devkit.gameBundler.exec({args: [], cwd: process.cwd(), stdio: "inherit"});
  },
};

const levelEditorCommand: yargs.CommandModule<any, any> = {
  command: ["level-editor"],
  aliases: ["editor"],
  describe: "Start the level editor",
  handler: (_args: yargs.Argv) => {
    devkit.levelEditor.exec({args: [], cwd: process.cwd(), stdio: "inherit"});
  },
};

const levelExtractorCommand: yargs.CommandModule<any, any> = {
  command: ["level-extractor"],
  aliases: ["extractor"],
  describe: "Start the level extractor",
  handler: (_args: yargs.Argv) => {
    devkit.levelExtractor.exec({args: [], cwd: process.cwd(), stdio: "inherit"});
  },
};

const deobfuscatorCommand: yargs.CommandModule<any, any> = {
  command: ["deobfuscator"],
  aliases: [],
  describe: "Start the deobfuscator",
  handler: (_args: yargs.Argv) => {
    devkit.deobfuscator.exec({args: [], cwd: process.cwd(), stdio: "inherit"});
  },
};

const mapperCommand: yargs.CommandModule<any, any> = {
  command: ["mapper"],
  aliases: [],
  describe: "Start the mapper",
  handler: (_args: yargs.Argv) => {
    devkit.mapper.exec({args: [], cwd: process.cwd(), stdio: "inherit"});
  },
};

const ARG_PARSER: yargs.Argv = yargs() as any;

ARG_PARSER
  .command(startCommand)
  .command(initCommand)
  .command(gameBundlerCommand)
  .command(levelEditorCommand)
  .command(levelExtractorCommand)
  .command(deobfuscatorCommand)
  .command(mapperCommand)
  .command(buildCommand)
  .command(upgradeCommand)
  .command(uploadCommand)
  .command("*", <any> false, {}, () => yargs.showHelp())
  .scriptName("eternalfest")
  .help()
  .locale("en");

async function main() {
  switch (process.argv[2]) {
  case "--version":
    console.log(VERSION);
    break;
  case "game-bundler":
  case "bundler":
    await devkit.gameBundler.exec({args: process.argv.slice(3), cwd: process.cwd(), stdio: "inherit"}).toPromise();
    break;
  case "level-editor":
  case "editor":
    await devkit.levelEditor.exec({args: process.argv.slice(3), cwd: process.cwd(), stdio: "inherit"}).toPromise();
    break;
  case "level-extractor":
  case "extractor":
    await devkit.levelExtractor.exec({args: process.argv.slice(3), cwd: process.cwd(), stdio: "inherit"}).toPromise();
    break;
  case "deobfuscator":
    await devkit.deobfuscator.exec({args: process.argv.slice(3), cwd: process.cwd(), stdio: "inherit"}).toPromise();
    break;
  case "mapper":
    await devkit.mapper.exec({args: process.argv.slice(3), cwd: process.cwd(), stdio: "inherit"}).toPromise();
    break;
  case "efc": {
    const args: string[] = process.argv.slice(3);
    const cwd: string = furi.fromSysPath(process.cwd()).toString();
    const returnCode: number = await efcCli.execCli(args, cwd, process);
    if (returnCode !== 0) {
      process.exit(returnCode);
    }
    break;
  }
  default:
    ARG_PARSER.parse(process.argv.slice(2));
  }
}

main()
  .then(
    undefined,
    (err: Error): never => {
      console.error(err);
      return process.exit(1) as never;
    });
