import { Api as EfApi } from "@eternalfest/rest-server/lib/create-api-router.js";
import furi from "furi";
import Koa from "koa";
import koaRoute from "koa-route";
import koaSend from "koa-send";
import koaStatic from "koa-static";
import sysPath from "path";
import url from "url";

import { ROUTES } from "../routes.js";

export async function createNgRouter(_efApi: EfApi, appDir: url.URL): Promise<Koa> {
  const staticRoot: url.URL = furi.join(appDir, ["browser"]);
  const templatePath: url.URL = furi.join(appDir, ["browser", "index.html"]);
  const templateSysPath: string = furi.toSysPath(templatePath.toString());

  const app: Koa = new Koa();

  app.use(koaRoute.get(ROUTES, sendNg));

  async function sendNg(ctx: Koa.Context): Promise<void> {
    await koaSend(ctx, sysPath.basename(templateSysPath), {root: sysPath.dirname(templateSysPath)});
  }

  app.use(koaStatic(furi.toSysPath(staticRoot.toString())));

  return app;
}
