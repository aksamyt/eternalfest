import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { $Run, Run } from "@eternalfest/api-core/lib/run/run.js";
import efDevkit from "@eternalfest/devkit";
import { createApiRouter } from "@eternalfest/rest-server/lib/create-api-router.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaMount from "koa-mount";
import koaRoute from "koa-route";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import {
  $CreateDebugRunOptions,
  CreateDebugRunOptions,
} from "../universal/create-debug-run-options.js";
import { $Project, Project } from "../universal/project.js";
import { $ShortProject, ShortProject } from "./api/project/short-project.js";
import { LocalEfApi } from "./with-ef-api.js";

async function createProjectsRouter(api: LocalEfApi): Promise<Koa> {
  const router: Koa = new Koa();

  router.use(koaRoute.get("/", getProjects));

  async function getProjects(ctx: Koa.Context): Promise<void> {
    const projects: ShortProject[] = await api.project.getProjects();
    ctx.response.body = projects.map((project) => $ShortProject.write(JSON_VALUE_WRITER, project));
  }

  router.use(koaRoute.get("/:project_id", getProjectById));

  async function getProjectById(ctx: Koa.Context, rawProjectId: string): Promise<void> {
    // const auth: AuthContext = await api.koaAuth.auth(ctx);
    const projectId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawProjectId);
    const project: Project | undefined = await api.project.getProjectById(projectId);
    if (project === undefined) {
      ctx.status = 404;
      return;
    }
    ctx.response.body = $Project.write(JSON_VALUE_WRITER, project);
  }

  return router;
}

async function createDevkitRouter(): Promise<Koa> {
  const router: Koa = new Koa();

  router.use(koaRoute.post("/bundler", openBundler));

  async function openBundler(ctx: Koa.Context): Promise<void> {
    ctx.status = 202;
    efDevkit.gameBundler.exec({
      args: [],
      cwd: process.cwd(),
      stdio: "inherit",
    });
  }

  router.use(koaRoute.post("/editor", openEditor));

  async function openEditor(ctx: Koa.Context): Promise<void> {
    ctx.status = 202;
    efDevkit.levelEditor.exec({
      args: [],
      cwd: process.cwd(),
      stdio: "inherit",
    });
  }

  return router;
}

export async function createLocalApiRouter(efApi: LocalEfApi): Promise<Koa> {
  const router: Koa = new Koa();

  router.use(koaMount("/devkit", await createDevkitRouter()));
  router.use(koaMount("/projects", await createProjectsRouter(efApi)));

  const baseApiRouter: Koa = createApiRouter(efApi);
  const apiRouter: Koa = new Koa();

  router.use(koaRoute.post("/runs", koaCompose([koaBodyParser(), createRun])));

  async function createRun(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await efApi.koaAuth.auth(ctx);
    let options: CreateDebugRunOptions;
    try {
      options = $CreateDebugRunOptions.read(JSON_VALUE_READER, ctx.request.body);
    } catch (err) {
      console.error(err);
      ctx.response.status = 422;
      ctx.response.body = {error: "InvalidRequest"};
      return;
    }
    const run: Run = await efApi.run.createDebugRun(auth, options);
    ctx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  apiRouter.use(koaMount(baseApiRouter));

  router.use(koaMount(apiRouter));

  return router;
}
