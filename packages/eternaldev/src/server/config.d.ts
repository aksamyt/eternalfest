import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { KoaAuth } from "@eternalfest/rest-server/lib/koa-auth.js";
import url from "url";

interface Api {
  auth: AuthService;
  blob: BlobService;
  file: FileService;
  game: GameService;
  hfestIdentity: HfestIdentityService;
  koaAuth: KoaAuth;
  run: RunService;
  session: SessionService;
  user: UserService;
}

export interface ServerAppConfig {
  externalBaseUri?: url.URL;
  isIndexNextToServerMain: boolean;
  isProduction: boolean;
  api: Api;
}
