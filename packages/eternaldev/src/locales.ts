export type Locale = "de-DE" | "en-US" | "es-SP" | "fr-FR";

export const LOCALES: ReadonlySet<Locale> = new Set([
  "de-DE" as "de-DE",
  "en-US" as "en-US",
  "es-SP" as "es-SP",
  "fr-FR" as "fr-FR",
]);

export const DEFAULT_LOCALE: Locale = "en-US";
