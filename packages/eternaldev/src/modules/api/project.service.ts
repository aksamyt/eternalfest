import { Injectable } from "@angular/core";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { UuidHex } from "kryo/lib/uuid-hex";

import { ShortProject } from "../../main/api/project/short-project";
import { Project } from "../../universal/project";

@Injectable()
export abstract class ProjectService {
  abstract getProjects(acceptCached: boolean): Promise<ShortProject[]>;

  abstract getProjectById(projectId: UuidHex): Promise<Project>;

  abstract getGameByProjectId(projectId: UuidHex): Promise<Game>;
}
