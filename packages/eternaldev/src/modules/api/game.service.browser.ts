import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $Game, Game } from "@eternalfest/api-core/lib/game/game";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader";
import { ArrayIoType, ArrayType } from "kryo/lib/array";
import { UuidHex } from "kryo/lib/uuid-hex";

import { GameService } from "./game.service";

const $GameArray: ArrayIoType<Game> = new ArrayType({itemType: $Game, maxLength: Infinity});

@Injectable()
export class BrowserGameService extends GameService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getGames(): Promise<ReadonlyArray<Game>> {
    const raw: any = await this.http.get("/api/v1/games").toPromise();
    let result: ReadonlyArray<Game>;
    try {
      result = $GameArray.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      result = [];
    }
    return result;
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    const raw: any = await this.http.get(`/api/v1/games/${gameId}`).toPromise();
    let result: Game;
    try {
      result = $Game.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      throw err;
    }
    return result;
  }
}
