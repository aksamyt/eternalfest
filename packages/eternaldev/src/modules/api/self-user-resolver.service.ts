import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type";
import { User } from "@eternalfest/api-core/lib/user/user";
import { Observable, of as rxOf } from "rxjs";

@Injectable()
export class SelfUserResolverService implements Resolve<User> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User | never> {
    return rxOf({
      id: "00000000-0000-0000-0000-000000000000",
      type: ActorType.User as ActorType.User,
      displayName: "EternalDev",
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    });
  }
}
