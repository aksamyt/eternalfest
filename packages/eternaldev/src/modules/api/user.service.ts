import { Injectable } from "@angular/core";
import { User } from "@eternalfest/api-core/lib/user/user";
import { UuidHex } from "kryo/lib/uuid-hex";

@Injectable()
export abstract class UserService {
  abstract getUserById(userId: UuidHex): Promise<User | undefined>;
}
