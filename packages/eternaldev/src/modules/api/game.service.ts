import { Injectable } from "@angular/core";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game";
import { UuidHex } from "kryo/lib//uuid-hex";

@Injectable()
export abstract class GameService {
  abstract getGames(): Promise<readonly ShortGame[]>;

  abstract getGameById(gameId: UuidHex): Promise<Game | undefined>;
}
