import { Inject, Injectable, NgZone } from "@angular/core";
import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game";
import { Api as EfApi } from "@eternalfest/rest-server/lib/create-api-router";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex";

import { AUTH_CONTEXT, EF_API } from "../../main/ng/tokens";
import { GameService } from "./game.service";
import { runUnzoned } from "./utils/run-unzoned.server";

@Injectable()
export class ServerGameService extends GameService {
  private readonly auth: AuthContext;
  private readonly efApi: EfApi;
  private readonly ngZone: NgZone;

  constructor(@Inject(AUTH_CONTEXT) auth: AuthContext, @Inject(EF_API) efApi: EfApi, ngZone: NgZone) {
    super();
    this.auth = auth;
    this.efApi = efApi;
    this.ngZone = ngZone;
  }

  async getGames(): Promise<readonly ShortGame[]> {
    return runUnzoned(this.ngZone, "ServerGameService#getGames", () => this._getGames());
  }

  async getGameById(gameId: UuidHex): Promise<Game | undefined> {
    return runUnzoned(this.ngZone, "ServerGameService#getGameById", () => this._getGameById(gameId));
  }

  private async _getGames(): Promise<readonly ShortGame[]> {
    return this.efApi.game.getGames(this.auth);
  }

  private async _getGameById(gameId: UuidHex): Promise<Game | undefined> {
    const error: Error | undefined = $UuidHex.testError(gameId);
    if (error !== undefined) {
      throw error;
    }
    return this.efApi.game.getGameById(this.auth, gameId);
  }
}
