import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game";

import { GameService } from "./game.service";

@Injectable()
export class GamesResolverService implements Resolve<readonly ShortGame[]> {
  private readonly game: GameService;
  private readonly router: Router;

  constructor(router: Router, game: GameService) {
    this.router = router;
    this.game = game;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<readonly ShortGame[] | never> {
    return this.game.getGames();
  }
}
