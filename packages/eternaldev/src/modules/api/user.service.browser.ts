import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { $User, User } from "@eternalfest/api-core/lib/user/user";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader";
import { UuidHex } from "kryo/lib/uuid-hex";

import { UserService } from "./user.service";

@Injectable()
export class BrowserUserService extends UserService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async getUserById(userId: UuidHex): Promise<User | undefined> {
    const raw: any = await this.http.get(`/api/v1/users/${userId}`).toPromise();
    return $User.read(JSON_VALUE_READER, raw);
  }
}
