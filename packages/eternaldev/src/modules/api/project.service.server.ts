import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Game } from "@eternalfest/api-core/lib/game/game";
import { UuidHex } from "kryo/lib/uuid-hex";

import { ShortProject } from "../../main/api/project/short-project";
import { Project } from "../../universal/project";
import { ProjectService } from "./project.service";

@Injectable()
export class ServerProjectService extends ProjectService {
  constructor(http: HttpClient) {
    super();
  }

  getGameByProjectId(projectId: UuidHex): Promise<Game> {
    throw new Error("NotImplemented");
  }

  getProjectById(projectId: UuidHex): Promise<Project> {
    throw new Error("NotImplemented");
  }

  getProjects(acceptCached: boolean): Promise<ShortProject[]> {
    throw new Error("NotImplemented");
  }
}
