import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options";
import { $Run, Run } from "@eternalfest/api-core/lib/run/run";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer";
import { UuidHex } from "kryo/lib/uuid-hex";

import {
  $CreateDebugRunOptions,
  CreateDebugRunOptions,
} from "../../universal/create-debug-run-options";
import { RunService } from "./run.service";

@Injectable()
export class BrowserRunService extends RunService {
  private http: HttpClient;

  constructor(http: HttpClient) {
    super();
    this.http = http;
  }

  async createRun(options: CreateRunOptions): Promise<Run> {
    return this.createDebugRun(options);
  }

  async createDebugRun(options: CreateDebugRunOptions): Promise<Run> {
    let result: Run;
    try {
      const reqBody: any = $CreateDebugRunOptions.write(JSON_VALUE_WRITER, options);
      const raw: any = await this.http.post("/api/v1/runs", reqBody).toPromise();
      result = $Run.read(JSON_VALUE_READER, raw);
    } catch (err) {
      console.error(err);
      throw err;
    }
    return result;
  }

  async getRunById(runId: UuidHex): Promise<Run | undefined> {
    const raw: any = await this.http.get(`/api/v1/runs/${runId}`).toPromise();
    return $Run.read(JSON_VALUE_READER, raw);
  }
}
