import { Injectable } from "@angular/core";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options";
import { Run } from "@eternalfest/api-core/lib/run/run";
import { UuidHex } from "kryo/lib/uuid-hex";

import { CreateDebugRunOptions } from "../../universal/create-debug-run-options";

@Injectable()
export abstract class RunService {
  abstract createRun(options: CreateRunOptions): Promise<Run>;

  abstract createDebugRun(options: CreateDebugRunOptions): Promise<Run>;

  abstract getRunById(gameId: UuidHex): Promise<Run | undefined>;
}
