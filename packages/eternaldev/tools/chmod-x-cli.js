const chmod = require("chmod");
const {root} = require("../webpack/helpers");
chmod(root("build/cli/main.cli.js"), {read: true, execute: true});
