#!/bin/bash

#This script provides better access to eternal's commandline tools
#Put it in a folder in your $PATH to use these tools anywhere

#Path to eternaldev : modify as needed
ETERNALPATH="/absolute/path/to/eternaldev/folder"

#Path to the devkit : modify as needed
DEVKITPATH="/absolute/path/to/devkit/folder"

#Path to python command : modify as needed
PYTHONPATH="python"

NAMES[0]="compile"
PROGS[0]="$PYTHONPATH $ETERNALPATH/external/compile.py"
DESCS[0]="Compile current project's external.swf"

NAMES[1]="deobfu"
PROGS[1]="java -jar $DEVKITPATH/utils/Deobfuscator.jar"
DESCS[1]="Use the deobfuscator"

NAMES[2]="extract"
PROGS[2]="java -jar $DEVKITPATH/utils/levelExtractor.jar"
DESCS[2]="Extract levels from base64 code"

NAMES[3]="map"
PROGS[3]="java -jar $DEVKITPATH/utils/mapCreator.jar"
DESCS[3]="Create a 2D map from .png and/or .lvl files"

TYPE=$1
shift

for((i=0;i<${#NAMES[@]};i++)); do
    NAME=${NAMES[$i]}
    if [ "$TYPE" = "$NAME" ]; then
        ${PROGS[$i]} "$@"
        exit $?;
    fi
done

if [ -z $TYPE ]; then
    >&2 echo "You must specify a program to execute"
else
    >&2 echo "The program $TYPE doesn't exist"
fi

>&2 echo "Programs :"

for((i=0;i<${#NAMES[@]};i++)); do
    >&2 echo "    ${NAMES[$i]}: ${DESCS[$i]}"
done
