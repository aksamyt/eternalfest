# API REST Server

[![build status](https://gitlab.com/eternalfest/api-rest-server/badges/master/build.svg)](https://gitlab.com/eternalfest/api-rest-server/commits/master)

REST Server for the Eternalfest API

## Changelog

See [CHANGELOG.md](./CHANGELOG.md).

## License

[MIT License © 2018 Eternalfest](./LICENSE.md)
