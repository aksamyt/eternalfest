import { PgAuthService } from "@eternalfest/api-backend/lib/auth/pg-service.js";
import { PgBlobService } from "@eternalfest/api-backend/lib/blob/pg-service.js";
import { Database } from "@eternalfest/api-backend/lib/db/database.js";
import { PgFileService } from "@eternalfest/api-backend/lib/file/pg-service.js";
import { PgGameService } from "@eternalfest/api-backend/lib/game/pg-service.js";
import { PgHfestIdentityService } from "@eternalfest/api-backend/lib/hfest-identity/pg-service.js";
import { ScryptPasswordService } from "@eternalfest/api-backend/lib/password/scrypt-service.js";
import { PgRunService } from "@eternalfest/api-backend/lib/run/pg-service.js";
import { PgLeaderboardService } from "@eternalfest/api-backend/lib/leaderboard/pg-service.js";
import { PgSessionService } from "@eternalfest/api-backend/lib/session/pg-service.js";
import { PgUserService } from "@eternalfest/api-backend/lib/user/pg-service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import fsBufferService from "@eternalfest/buffer-service/fs-buffer-service.js";
import hfestApi from "@eternalfest/hammerfest-api";
import hfestTypes from "@eternalfest/hammerfest-api/types/api.js";
import koaCors from "@koa/cors";
import furi from "furi";
import Koa from "koa";
import koaLogger from "koa-logger";
import koaMount from "koa-mount";
import path from "path";
import pg from "pg";

import { createApiRouter } from "../lib/create-api-router.js";
import { KoaAuth } from "../lib/koa-auth.js";
import { Config, getLocalConfigOrDie } from "./config.js";

async function main(): Promise<void> {
  const config: Config = getLocalConfigOrDie();
  const bufferRoot: string = path.posix.join(config.dataRoot, "buffers");

  const poolConfig: pg.PoolConfig = {
    user: config.user,
    password: config.password,
    host: config.host,
    port: config.port,
    database: config.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);
  const db: Database = new Database(pool);

  const bufferService: fsBufferService.FsBufferService = new fsBufferService.FsBufferService(furi.fromSysPath(bufferRoot));
  const blob: PgBlobService = new PgBlobService(db, bufferService);
  const passwordService: ScryptPasswordService = new ScryptPasswordService(/* 0.1 */);
  const user: PgUserService = new PgUserService(db, passwordService);
  const file: PgFileService = new PgFileService(db, blob);
  const game: PgGameService = new PgGameService(db, user, file);
  const run: PgRunService = new PgRunService(db, game, user);
  const leaderboard: PgLeaderboardService = new PgLeaderboardService(db, game);
  const mockHammerfestApi: hfestTypes.HammerfestApi = hfestApi.DEFAULT_HAMMERFEST_API;
  const hfestIdentity: PgHfestIdentityService = new PgHfestIdentityService(db, mockHammerfestApi);
  const session: SessionService = new PgSessionService(db, 86400);
  const auth: PgAuthService = new PgAuthService(mockHammerfestApi, hfestIdentity, session, user);
  const koaAuth: KoaAuth = new KoaAuth(session, user);
  const apiRouter: Koa = createApiRouter({auth, blob, file, game, hfestIdentity, koaAuth, run, leaderboard, session, user});

  const app: Koa = new Koa();
  const port: number = 50314;

  app.use(koaLogger());
  app.use(koaCors({origin: "http://localhost:4200", credentials: true}));
  app.use(koaMount("/", apiRouter));

  app.listen(port, () => {
    console.log(`Listening on http://localhost:${port}`);
  });
}

main()
  .catch((err: Error): never => {
    console.error(err.stack);
    return process.exit(1) as never;
  });
