import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { $CreateRunOptions, CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { $RunStart, RunStart } from "@eternalfest/api-core/lib/run/run-start.js";
import { $Run, Run } from "@eternalfest/api-core/lib/run/run.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { $SetRunResultOptions, SetRunResultOptions } from "@eternalfest/api-core/lib/run/set-run-result-options.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  run: RunService;
}

export function createRunsRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.post("/", koaCompose([koaBodyParser(), createRun])));

  async function createRun(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    let options: CreateRunOptions;
    try {
      options = $CreateRunOptions.read(JSON_VALUE_READER, ctx.request.body);
    } catch (err) {
      console.error(err);
      ctx.response.status = 422;
      ctx.response.body = {error: "InvalidRequest"};
      return;
    }
    const run: Run = await api.run.createRun(auth, options);
    ctx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  router.use(koaRoute.get("/:run_id", getRunById));

  async function getRunById(ctx: Koa.Context, rawRunId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    let runId: UuidHex;
    try {
      runId = $UuidHex.read(QS_VALUE_READER, rawRunId);
    } catch (err) {
      ctx.response.status = 422;
      ctx.response.body = {error: "InvalidRequest", message: "Invalid run id"};
      return;
    }

    let run: Run;
    try {
      run = await api.run.getRunById(auth, runId);
    } catch (err) {
      switch (err.name) {
        case "RunNotFound":
          ctx.response.status = 404;
          ctx.response.body = {error: "ResourceNotFound"};
          break;
        case "Unauthenticated":
          ctx.response.status = 401;
          ctx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          ctx.response.status = 403;
          ctx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("getRunById");
          console.error(err);
          ctx.response.status = 500;
          break;
      }
      return;
    }

    ctx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  router.use(koaRoute.post("/:run_id/start", startRun));

  async function startRun(ctx: Koa.Context, rawRunId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const runId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawRunId);
    const runStart: RunStart = await api.run.startRun(auth, runId);
    ctx.response.body = $RunStart.write(JSON_VALUE_WRITER, runStart);
  }

  router.use(koaRoute.post("/:run_id/result", setRunResult));

  async function setRunResult(ctx: Koa.Context, rawRunId: string): Promise<void> {
    return koaCompose([koaBodyParser(), innerSetRunResult])(ctx, undefined as any);

    async function innerSetRunResult(ctx: Koa.Context): Promise<void> {
      const auth: AuthContext = await api.koaAuth.auth(ctx);
      let runId: UuidHex;
      let options: SetRunResultOptions;
      try {
        runId = $UuidHex.read(QS_VALUE_READER, rawRunId);
        if (ctx.request.body.flash === "true") {
          for (const [key, value] of Object.entries(ctx.request.body)) {
            ctx.request.body[key] = JSON.parse(value as any);
          }
        }
        options = $SetRunResultOptions.read(JSON_VALUE_READER, ctx.request.body);
      } catch (err) {
        console.error(err);
        ctx.response.status = 422;
        ctx.response.body = {error: "InvalidRequest"};
        return;
      }
      const run: Run = await api.run.setRunResult(auth, runId, options);
      ctx.response.body = $Run.write(JSON_VALUE_WRITER, run);
    }
  }

  return router;
}
