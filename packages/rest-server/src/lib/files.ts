import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import {
  $CreateFileOptions,
  CreateFileOptions
} from "@eternalfest/api-core/lib/file/create-file-options.js";
import { $File, File } from "@eternalfest/api-core/lib/file/file.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  file: FileService;
}

export function createFilesRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.post("/", koaCompose([koaBodyParser(), createFile])));

  async function createFile(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const options: CreateFileOptions = $CreateFileOptions.read(JSON_VALUE_READER, ctx.request.body);
    const file: File = await api.file.createFile(auth, options);
    ctx.response.body = $File.write(JSON_VALUE_WRITER, file);
  }

  router.use(koaRoute.get("/:file_id", getFileById));

  async function getFileById(ctx: Koa.Context, rawFileId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const fileId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawFileId);
    const file: File = await api.file.getFileById(auth, fileId);
    ctx.response.body = $File.write(JSON_VALUE_WRITER, file);
  }

  router.use(koaRoute.get("/:file_id/raw", getRawFileById));

  async function getRawFileById(ctx: Koa.Context, rawFileId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const fileId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawFileId);
    const file: File = await api.file.getFileById(auth, fileId);
    ctx.response.set("Content-Length", file.byteSize.toString(10));
    ctx.response.set("Content-Type", file.mediaType);
    ctx.response.body = await api.file.readFileContent(auth, fileId);
  }

  return router;
}
