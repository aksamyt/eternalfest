import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { LeaderboardService } from "@eternalfest/api-core/lib/leaderboard/service.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import Koa from "koa";
import koaMount from "koa-mount";

import { createBlobsRouter } from "./blobs.js";
import { createFilesRouter } from "./files.js";
import { createGamesRouter } from "./games.js";
import { KoaAuth } from "./koa-auth.js";
import { createRunsRouter } from "./runs.js";
import { createSelfRouter } from "./self.js";
import { createUploadSessionsRouter } from "./upload-sessions.js";
import { createUsersRouter } from "./users.js";

export interface Api {
  auth: AuthService;
  blob: BlobService;
  file: FileService;
  game: GameService;
  hfestIdentity: HfestIdentityService;
  koaAuth: KoaAuth;
  run: RunService;
  leaderboard: LeaderboardService;
  session: SessionService;
  user: UserService;
}

export function createApiRouter(api: Api): Koa {
  const app: Koa = new Koa();

  app.use(koaMount("/blobs", createBlobsRouter(api)));
  app.use(koaMount("/files", createFilesRouter(api)));
  app.use(koaMount("/games", createGamesRouter(api)));
  app.use(koaMount("/runs", createRunsRouter(api)));
  app.use(koaMount("/self", createSelfRouter(api)));
  app.use(koaMount("/upload-sessions", createUploadSessionsRouter(api)));
  app.use(koaMount("/users", createUsersRouter(api)));
  app.use((ctx: Koa.Context) => {
    ctx.response.status = 404;
    ctx.body = {error: "ResourceNotFound"};
  });

  return app;
}
