import { AuthContext, GuestAuthContext, SystemAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { createUserNotFoundError } from "@eternalfest/api-core/lib/errors/user-not-found.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import crypto from "crypto";
import Koa from "koa";
import { $UuidHex } from "kryo/lib/uuid-hex.js";

export const SESSION_COOKIE: string = "sid";
export const CSRF_TOKEN_COOKIE: string = "XSRF-TOKEN";
export const CSRF_TOKEN_HEADER: string = "X-XSRF-TOKEN";

const GUEST_AUTH_CONTEXT: GuestAuthContext = {
  type: ActorType.Guest,
  scope: AuthScope.Default,
};

const SYSTEM_AUTH_CONTEXT: SystemAuthContext = {
  type: ActorType.System,
};

export class KoaAuth {
  private readonly session: SessionService;
  private readonly user: UserService;

  constructor(session: SessionService, user: UserService) {
    this.session = session;
    this.user = user;
  }

  public async auth(cx: Koa.Context): Promise<AuthContext> {
    const maybeSessionId: string | undefined = cx.cookies.get(SESSION_COOKIE);
    if (!$UuidHex.test(maybeSessionId as any)) {
      return GUEST_AUTH_CONTEXT;
    }
    const sessionId: string = maybeSessionId!;

    const session: Session | undefined = await this.session.getAndTouchSessionById(SYSTEM_AUTH_CONTEXT, sessionId);
    if (session === undefined) {
      return GUEST_AUTH_CONTEXT;
    }
    const user: User | undefined = await this.user.getUserById(SYSTEM_AUTH_CONTEXT, session.user.id);
    if (user === undefined) {
      throw createUserNotFoundError(session.user.id);
    }
    return {
      type: ActorType.User,
      userId: user.id,
      displayName: user.displayName,
      isAdministrator: user.isAdministrator,
      isTester: user.isTester!,
      scope: AuthScope.Default,
    };
  }

  public async getOrCreateCsrfToken(cx: Koa.Context): Promise<string> {
    let token: string | undefined = cx.cookies.get(CSRF_TOKEN_COOKIE);
    if (token === undefined) {
      token = await this.createCsrfToken();
      this.setCsrfToken(cx, token);
    }
    return token;
  }

  private setCsrfToken(cx: Koa.Context, value: string): void {
    cx.cookies.set(CSRF_TOKEN_COOKIE, value, {httpOnly: true, sameSite: "lax"});
  }

  private async createCsrfToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      crypto.randomBytes(16, (err: Error | null, buffer: Buffer): void => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(buffer.toString("hex"));
        }
      });
    });
  }
}
