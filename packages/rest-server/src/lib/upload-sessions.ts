import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import {
  $CreateUploadSessionOptions,
  CreateUploadSessionOptions
} from "@eternalfest/api-core/lib/blob/create-upload-session-options.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { $UploadBytesOptions, UploadBytesOptions } from "@eternalfest/api-core/lib/blob/upload-bytes-options.js";
import { $UploadSession, UploadSession } from "@eternalfest/api-core/lib/blob/upload-session.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  blob: BlobService;
}

export function createUploadSessionsRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.post("/", koaCompose([koaBodyParser(), createUploadSession])));

  async function createUploadSession(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const options: CreateUploadSessionOptions = $CreateUploadSessionOptions.read(JSON_VALUE_READER, ctx.request.body);
    const uploadSession: UploadSession = await api.blob.createUploadSession(auth, options);
    ctx.response.body = $UploadSession.write(JSON_VALUE_WRITER, uploadSession);
  }

  router.use(koaRoute.patch("/:upload_session_id", uploadBytes));

  async function uploadBytes(ctx: Koa.Context, rawUploadSessionId: string): Promise<void> {
    return koaCompose([koaBodyParser(), innerUploadBytes])(ctx, undefined as any);

    async function innerUploadBytes(ctx: Koa.Context): Promise<void> {
      const auth: AuthContext = await api.koaAuth.auth(ctx);
      let uploadSessionId: UuidHex;
      let options: UploadBytesOptions;
      try {
        uploadSessionId = $UuidHex.read(QS_VALUE_READER, rawUploadSessionId);
        options = $UploadBytesOptions.read(JSON_VALUE_READER, ctx.request.body);
      } catch (err) {
        console.error(err);
        ctx.response.status = 422;
        ctx.response.body = {error: "InvalidRequest"};
        return;
      }
      const uploadSession: UploadSession = await api.blob.uploadBytes(auth, uploadSessionId, options);
      ctx.response.body = $UploadSession.write(JSON_VALUE_WRITER, uploadSession);
    }
  }

  return router;
}
