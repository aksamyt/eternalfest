import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { $Blob, Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import {
  $CreateBlobOptions,
  CreateBlobOptions
} from "@eternalfest/api-core/lib/blob/create-blob-options.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  blob: BlobService;
}

export function createBlobsRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.post("/", koaCompose([koaBodyParser(), createBlob])));

  async function createBlob(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const options: CreateBlobOptions = $CreateBlobOptions.read(JSON_VALUE_READER, ctx.request.body);
    const blob: Blob = await api.blob.createBlob(auth, options);
    ctx.response.body = $Blob.write(JSON_VALUE_WRITER, blob);
  }

  router.use(koaRoute.get("/:blob_id", getFileById));

  async function getFileById(ctx: Koa.Context, rawBlobId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const blobId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawBlobId);
    const blob: Blob = await api.blob.getBlobById(auth, blobId);
    ctx.response.body = $Blob.write(JSON_VALUE_WRITER, blob);
  }

  router.use(koaRoute.get("/:file_id/raw", getRawBlobById));

  async function getRawBlobById(ctx: Koa.Context, rawBlobId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const blobId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawBlobId);
    const blob: Blob = await api.blob.getBlobById(auth, blobId);
    ctx.response.set("Content-Length", blob.byteSize.toString(10));
    ctx.response.set("Content-Type", blob.mediaType);
    ctx.response.body = await api.blob.readBlobData(auth, blobId);
  }

  return router;
}
