import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import {
  $CreateGameOptions,
  CreateGameOptions
} from "@eternalfest/api-core/lib/game/create-game-options.js";
import { $GameModeKey, GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { $Game, Game } from "@eternalfest/api-core/lib/game/game.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { $ShortGame, ShortGame } from "@eternalfest/api-core/lib/game/short-game.js";
import {
  $UpdateGameOptions,
  UpdateGameOptions
} from "@eternalfest/api-core/lib/game/update-game-options.js";
import { $Leaderboard, Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard.js";
import { LeaderboardService } from "@eternalfest/api-core/lib/leaderboard/service.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import { CaseStyle } from "kryo";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { KoaAuth } from "./koa-auth.js";

export interface Api {
  koaAuth: KoaAuth;
  game: GameService;
  leaderboard: LeaderboardService;
}

interface GameModeQuery {
  gameMode: GameModeKey;
}

const $GameModeQuery: RecordIoType<GameModeQuery> = new RecordType<GameModeQuery>({
  properties: {
    gameMode: {type: $GameModeKey},
  },
  changeCase: CaseStyle.SnakeCase,
});

export function createGamesRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.get("/", getGames));

  async function getGames(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const games: readonly ShortGame[] = await api.game.getGames(auth);
    ctx.response.body = games.map(user => $ShortGame.write(JSON_VALUE_WRITER, user));
  }

  router.use(koaRoute.post("/", koaCompose([koaBodyParser(), createGame])));

  async function createGame(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    let options: CreateGameOptions;
    try {
      options = $CreateGameOptions.read(JSON_VALUE_READER, ctx.request.body);
    } catch (err) {
      ctx.response.status = 422;
      ctx.response.body = {error: "InvalidRequest"};
      return;
    }
    let game: Game;
    try {
      game = await api.game.createGame(auth, options);
    } catch (err) {
      switch (err.name) {
        case "Unauthenticated":
          ctx.response.status = 401;
          ctx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          ctx.response.status = 403;
          ctx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("createGameError");
          console.error(err);
          ctx.response.status = 500;
          break;
      }
      return;
    }
    ctx.response.body = $Game.write(JSON_VALUE_WRITER, game);
  }

  router.use(koaRoute.get("/:game_id", getGameById));

  async function getGameById(ctx: Koa.Context, rawGameId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const gameId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawGameId);
    const game: Game | undefined = await api.game.getGameById(auth, gameId);
    if (game === undefined) {
      ctx.status = 404;
      ctx.response.body = {error: "ResourceNotFound"};
      return;
    }
    ctx.response.body = $Game.write(JSON_VALUE_WRITER, game);
  }

  router.use(koaRoute.patch("/:game_id", updateGameById));

  async function updateGameById(ctx: Koa.Context, rawGameId: string): Promise<void> {
    return koaCompose([koaBodyParser(), innerUpdateGameById])(ctx, undefined as any);

    async function innerUpdateGameById(ctx: Koa.Context): Promise<void> {
      const auth: AuthContext = await api.koaAuth.auth(ctx);
      let options: UpdateGameOptions;
      let gameId: UuidHex;
      try {
        options = $UpdateGameOptions.read(JSON_VALUE_READER, ctx.request.body);
        gameId = $UuidHex.read(QS_VALUE_READER, rawGameId);
      } catch (err) {
        console.error(err);
        ctx.response.status = 422;
        ctx.response.body = {error: "InvalidRequest"};
        return;
      }
      const game: Game = await api.game.updateGame(auth, gameId, options);
      ctx.response.body = $Game.write(JSON_VALUE_WRITER, game);
    }
  }

  router.use(koaRoute.get("/:game_id/leaderboard", getLeaderboardByGameId));

  async function getLeaderboardByGameId(ctx: Koa.Context, rawGameId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const gameId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawGameId);
    const gameMode: GameModeKey = $GameModeQuery.read(QS_VALUE_READER, ctx.request.query).gameMode;
    try {
      const leaderboard: Leaderboard = await api.leaderboard.getLeaderboard(auth, gameId, gameMode);
      ctx.response.body = $Leaderboard.write(JSON_VALUE_WRITER, leaderboard);
    } catch (err) {
      console.error(err);
      // TODO: Some better error?
      ctx.response.status = 500;
      ctx.response.body = {error: "UnknownError"};
      return;
    }
  }

  return router;
}
