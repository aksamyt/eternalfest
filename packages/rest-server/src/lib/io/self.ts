import { $HfestLogin, HfestLogin } from "@eternalfest/api-core/lib/hfest-identity/hfest-login.js";
import { $HfestServer, HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { CaseStyle } from "kryo";
import { $Bytes } from "kryo/lib/bytes.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TsEnumType } from "kryo/lib/ts-enum.js";

export enum AuthStrategy {
  HfestCredentials,
}

export const $AuthStrategy: TsEnumType<AuthStrategy> = new TsEnumType<AuthStrategy>({
  enum: AuthStrategy,
  changeCase: CaseStyle.KebabCase,
});

export interface CreateSessionQuery {
  strategy: AuthStrategy;
}

export const $CreateSessionQuery: RecordIoType<CreateSessionQuery> = new RecordType<CreateSessionQuery>({
  properties: {
    strategy: {type: $AuthStrategy},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface AuthHfestCredentials {
  server: HfestServer;
  username: HfestLogin;
  password: Uint8Array;
}

export const $AuthHfestCredentials: RecordIoType<AuthHfestCredentials> = new RecordType<AuthHfestCredentials>({
  properties: {
    server: {type: $HfestServer},
    username: {type: $HfestLogin},
    password: {type: $Bytes},
  },
  changeCase: CaseStyle.SnakeCase,
});
