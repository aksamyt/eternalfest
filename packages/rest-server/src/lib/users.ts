import { AuthContext, SystemAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { $Drive, Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import {
  $CreateUserWithCredentialsOptions,
  CreateUserWithCredentialsOptions
} from "@eternalfest/api-core/lib/user/create-user-with-credentials-options.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { $UpdateUserOptions, UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options.js";
import { $User, User } from "@eternalfest/api-core/lib/user/user.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import { CaseStyle } from "kryo";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { KoaAuth, SESSION_COOKIE } from "./koa-auth.js";

const SYSTEM_AUTH_CONTEXT: SystemAuthContext = {
  type: ActorType.System,
};

const $PartialUser: RecordIoType<Partial<User>> = new RecordType<Partial<User>>({
  properties: {
    id: {type: $User.properties.id.type, optional: true},
    type: {type: $User.properties.type.type, optional: true},
    displayName: {type: $User.properties.displayName.type, optional: true},
    createdAt: {type: $User.properties.createdAt.type, optional: true},
    updatedAt: {type: $User.properties.updatedAt.type, optional: true},
    identities: {type: $User.properties.identities.type, optional: true},
    isAdministrator: {type: $User.properties.isAdministrator.type, optional: true},
    isTester: {type: $User.properties.isTester!.type, optional: true},
    hasPassword: {type: $User.properties.hasPassword!.type, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface Api {
  koaAuth: KoaAuth;
  file: FileService;
  user: UserService;
  session: SessionService;
}

export function createUsersRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.get("/", getUsers));

  async function getUsers(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const users: ReadonlyArray<Partial<User>> = await api.user.getUsers(auth);
    ctx.response.body = users.map(user => $PartialUser.write(JSON_VALUE_WRITER, user));
  }

  router.use(koaRoute.post("/", koaCompose([koaBodyParser(), createUser])));

  async function createUser(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    let options: CreateUserWithCredentialsOptions;
    try {
      options = $CreateUserWithCredentialsOptions.read(JSON_VALUE_READER, ctx.request.body);
    } catch (err) {
      console.error(err);
      ctx.response.status = 422;
      ctx.response.body = {error: "InvalidRequest"};
      return;
    }
    const user: User = await api.user.createUserWithCredentials(auth, options);
    const session: Session = await api.session.createSession(SYSTEM_AUTH_CONTEXT, user.id);
    ctx.cookies.set(SESSION_COOKIE, session.id);
    ctx.response.body = $User.write(JSON_VALUE_WRITER, user);
  }

  router.use(koaRoute.get("/:user_id", getUserById));

  async function getUserById(ctx: Koa.Context, rawUserId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const userId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawUserId);
    const user: User | undefined = await api.user.getUserById(auth, userId);
    if (user === undefined) {
      ctx.response.status = 404;
      return;
    }
    ctx.response.body = $User.write(JSON_VALUE_WRITER, user);
  }

  router.use(koaRoute.patch("/:user_id", updateUserById));

  async function updateUserById(ctx: Koa.Context, rawUserId: string): Promise<void> {
    return koaCompose([koaBodyParser(), innerUpdateUserById])(ctx, undefined as any);

    async function innerUpdateUserById(ctx: Koa.Context): Promise<void> {
      const auth: AuthContext = await api.koaAuth.auth(ctx);
      let options: UpdateUserOptions;
      let userId: UuidHex;
      try {
        options = $UpdateUserOptions.read(JSON_VALUE_READER, ctx.request.body);
        userId = $UuidHex.read(QS_VALUE_READER, rawUserId);
      } catch (err) {
        console.error(err);
        ctx.response.status = 422;
        ctx.response.body = {error: "InvalidRequest"};
        return;
      }
      const user: User = await api.user.updateUser(auth, {...options, userId});
      ctx.response.body = $User.write(JSON_VALUE_WRITER, user);
    }
  }

  router.use(koaRoute.get("/:user_id/drive", getUserDrive));

  async function getUserDrive(ctx: Koa.Context, rawUserId: string): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const userId: UuidHex = $UuidHex.read(QS_VALUE_READER, rawUserId);
    const drive: Drive = await api.file.getDriveByOwnerId(auth, userId);
    ctx.response.body = $Drive.write(JSON_VALUE_WRITER, drive);
  }

  return router;
}
