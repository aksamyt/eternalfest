import { $AuthContext, AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import koaRoute from "koa-route";
import { JSON_VALUE_READER } from "kryo-json/lib/json-value-reader.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { QS_VALUE_READER } from "kryo-qs/lib/qs-value-reader.js";

import {
  $AuthHfestCredentials,
  $CreateSessionQuery,
  AuthHfestCredentials,
  AuthStrategy,
  CreateSessionQuery,
} from "./io/self.js";
import { KoaAuth, SESSION_COOKIE } from "./koa-auth.js";

export interface Api {
  auth: AuthService;
  koaAuth: KoaAuth;
  session: SessionService;
}

export function createSelfRouter(api: Api): Koa {
  const router: Koa = new Koa();

  router.use(koaRoute.get("/auth", getSelfAuth));

  async function getSelfAuth(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    ctx.response.body = $AuthContext.write(JSON_VALUE_WRITER, auth);
  }

  router.use(koaRoute.put("/session", koaCompose([koaBodyParser(), createSession])));

  async function createSession(ctx: Koa.Context): Promise<void> {
    const query: CreateSessionQuery = $CreateSessionQuery.read(QS_VALUE_READER, ctx.request.query);
    switch (query.strategy) {
      case AuthStrategy.HfestCredentials: {
        await createSessionWithHfestCredentials(ctx);
        break;
      }
      default:
        throw new Error("UnknownStrategy");
    }
  }

  async function createSessionWithHfestCredentials({request, response, cookies}: Koa.Context): Promise<void> {
    const options: AuthHfestCredentials = $AuthHfestCredentials.read(JSON_VALUE_READER, request.body);
    const auth: AuthContext = await api.auth.hfestCredentials(options.server, options.username, options.password);
    switch (auth.type) {
      case ActorType.User: {
        const session: Session = await api.session.createSession(auth, auth.userId);
        cookies.set(SESSION_COOKIE, session.id);
        response.body = $AuthContext.write(JSON_VALUE_WRITER, auth);
        break;
      }
      default:
        throw new Error("UnexpectedAuthType");
    }
  }

  return router;
}
