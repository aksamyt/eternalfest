import {
  HammerfestApiState,
  InMemoryHfestApiService
} from "@eternalfest/api-backend/lib/hfest-api/in-memory-service.js";
import getServer from "@eternalfest/hammerfest-api/api/get-server.js";

export function createMockHammerfestApi(): InMemoryHfestApiService {
  const INITIAL_STATE: HammerfestApiState = {
    servers: new Map([
      [
        getServer.SERVER_FR,
        {
          users: new Map([
            [
              "50313",
              {
                id: "50313",
                username: "bob",
                password: "BOB",
              },
            ],
          ]),
          sessions: new Map(),
        },
      ],
    ]),
  };

  return new InMemoryHfestApiService(INITIAL_STATE);
}
