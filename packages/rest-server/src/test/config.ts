import dotenv from "dotenv";
import fs from "fs";
import path from "path";

const REQUIRED_VARIABLES: string[] = [
  "EF_DB_USER",
  "EF_DB_PASSWORD",
  "EF_DB_HOST",
  "EF_DB_PORT",
  "EF_DB_NAME",
];

export interface Config {
  readonly user: string;
  readonly password: string;
  readonly host: string;
  readonly port: number;
  readonly name: string;
  readonly dataRoot: string;
}

export function getLocalConfigOrDie(): Config {
  const cwd: string = process.cwd();
  let dotEnvContent: string;
  try {
    dotEnvContent = fs.readFileSync(path.resolve(cwd, ".env"), {encoding: "utf8"});
  } catch (err) {
    if (err.code === "ENOENT") {
      console.error("Cannot start the server, missing `.env` file in the current working directory.");
      console.error(`Current working directory: ${cwd}`);
    } else {
      console.error(err.stack);
    }
    return process.exit(1) as never;
  }

  const parsedDotEnv: { [name: string]: string } = dotenv.parse(dotEnvContent);
  for (const key in parsedDotEnv) {
    if (!(key in process.env)) {
      process.env[key] = parsedDotEnv[key];
    }
  }

  const missingVariables: string[] = REQUIRED_VARIABLES.filter(envVar => !(envVar in process.env));
  if (missingVariables.length > 0) {
    console.error(`Missing environmnet variable: ${missingVariables.join(", ")}`);
    process.exit(1);
  }

  return {
    user: process.env["EF_DB_USER"]!,
    password: process.env["EF_DB_PASSWORD"]!,
    host: process.env["EF_DB_HOST"]!,
    port: parseInt(process.env["EF_DB_PORT"]!, 10),
    name: process.env["EF_DB_NAME"]!,
    dataRoot: path.resolve(cwd, "test-data"),
  };
}
