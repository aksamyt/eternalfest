import { $Blob, Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { $CreateBlobOptions } from "@eternalfest/api-core/lib/blob/create-blob-options.js";
import { MediaType } from "@eternalfest/api-core/lib/blob/media-type.js";
import { $CreateFileOptions } from "@eternalfest/api-core/lib/file/create-file-options.js";
import { $Drive, Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { $File, File } from "@eternalfest/api-core/lib/file/file.js";
import { $CreateGameOptions, CreateGameOptions } from "@eternalfest/api-core/lib/game/create-game-options.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state.js";
import { GameOptionState } from "@eternalfest/api-core/lib/game/game-option-state.js";
import { $Game, Game } from "@eternalfest/api-core/lib/game/game.js";
import { $ShortGame, ShortGame } from "@eternalfest/api-core/lib/game/short-game.js";
import { $UpdateGameOptions } from "@eternalfest/api-core/lib/game/update-game-options.js";
import { $Leaderboard, Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard.js";
import { $CreateRunOptions, CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { $Run, Run } from "@eternalfest/api-core/lib/run/run.js";
import { $SetRunResultOptions } from "@eternalfest/api-core/lib/run/set-run-result-options.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";

import { getTestResource } from "../get-test-resource.js";
import { readJsonResponse } from "../helpers.js";
import { populateWithUsers } from "../populate/users.js";
import { DbState } from "../test-db.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/games", function () {
  it("From an empty server, upload `sous-la-colline`", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const {alice: aliceUser, aliceAgent} = await populateWithUsers(server);
      const aliceDrive: Drive = await getDriveByOwnerId(aliceAgent, aliceUser.id);

      const {
        icon: iconFile,
        gameSwf: gameSwfFile,
        gameXml: gameXmlFile,
        music: musicFile,
      } = await uploadSousLaCollineFiles(aliceAgent, aliceDrive.root.id);

      const options: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Sous la colline",
        description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
        engines: {
          loader: "3.0.0",
        },
        iconFileId: iconFile.id,
        resources: [
          {fileId: gameSwfFile.id, tag: "game-swf"},
          {fileId: gameXmlFile.id, tag: "game-xml"},
          {fileId: musicFile.id, tag: "music"},
          {fileId: musicFile.id, tag: "music"},
          {fileId: musicFile.id, tag: "music"},
        ],
        gameModes: [
          {
            key: "solo",
            displayName: "Aventure",
            state: GameModeState.Enabled,
            options: [
              {key: "mirror", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmare", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "ninja", displayName: "Ninjutsu", state: GameOptionState.Enabled, locales: new Map()},
              {
                key: "bombexpert",
                displayName: "Explosifs Instables",
                state: GameOptionState.Enabled,
                locales: new Map(),
              },
              {key: "boost", displayName: "Tornade", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
          {
            key: "multicoop",
            displayName: "Multi coopératif",
            state: GameModeState.Enabled,
            options: [
              {key: "mirrormulti", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmaremulti", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "lifesharing", displayName: "Partage de vies", state: GameOptionState.Enabled, locales: new Map()},
              {key: "bombotopia", displayName: "Bombotopia", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
        ],
        // tslint:disable-next-line:max-line-length
        families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
        category: GameCategory.Small,
        isPrivate: true,
        locales: new Map(),
      };
      const res: ChaiHttp.Response = await aliceAgent.post("/games")
        .send($CreateGameOptions.write(JSON_VALUE_WRITER, options));
      const actual: Game = readJsonResponse($Game, res.body);

      chai.assert.isString(actual.id);
      chai.assert.instanceOf(actual.createdAt, Date);
      chai.assert.instanceOf(actual.updatedAt, Date);
      chai.assert.isTrue(actual.updatedAt.getTime() >= actual.createdAt.getTime());

      const shortExpected: ShortGame = {
        id: actual.id,
        key: null,
        createdAt: actual.createdAt,
        updatedAt: actual.updatedAt,
        publicationDate: null,
        author: {
          id: aliceUser.id,
          displayName: aliceUser.displayName,
        },
        mainLocale: "fr-FR",
        displayName: "Sous la colline",
        description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
        iconFile: {
          id: iconFile.id,
        },
        category: GameCategory.Small,
        isPrivate: true,
        locales: new Map(),
      };

      const expected: Game = {
        ...shortExpected,
        engines: {
          loader: "3.0.0",
        },
        resources: [
          {file: gameSwfFile, tag: "game-swf", priority: "high"},
          {file: gameXmlFile, tag: "game-xml", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
        ],
        gameModes: [
          {
            key: "solo",
            displayName: "Aventure",
            state: GameModeState.Enabled,
            options: [
              {key: "mirror", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmare", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "ninja", displayName: "Ninjutsu", state: GameOptionState.Enabled, locales: new Map()},
              {
                key: "bombexpert",
                displayName: "Explosifs Instables",
                state: GameOptionState.Enabled,
                locales: new Map(),
              },
              {key: "boost", displayName: "Tornade", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
          {
            key: "multicoop",
            displayName: "Multi coopératif",
            state: GameModeState.Enabled,
            options: [
              {key: "mirrormulti", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmaremulti", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "lifesharing", displayName: "Partage de vies", state: GameOptionState.Enabled, locales: new Map()},
              {key: "bombotopia", displayName: "Bombotopia", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
        ],
        // tslint:disable-next-line:max-line-length
        families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
      };

      chai.assert.deepEqual(actual, expected);

      const gamesRes: ChaiHttp.Response = await aliceAgent.get("/games");
      const actualGames: readonly ShortGame[] = gamesRes.body.map((g: any) => readJsonResponse($ShortGame, g));
      chai.assert.deepEqual(actualGames, [shortExpected]);
    });
  });

  it("Authors should be able to edit games (resources)", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const {bob, bobAgent} = await populateWithUsers(server);
      const {alpha} = await createSampleGames(bobAgent);
      const bobDrive: Drive = await getDriveByOwnerId(bobAgent, bob.id);

      chai.assert.isEmpty(alpha.resources);

      const {
        icon: iconFile,
        gameSwf: gameSwfFile,
        gameXml: gameXmlFile,
        music: musicFile,
      } = await uploadSousLaCollineFiles(bobAgent, bobDrive.root.id);

      let updatedAlpha: Game;
      {
        const res: ChaiHttp.Response = await bobAgent.patch(`/games/${alpha.id}`)
          .send($UpdateGameOptions.write(JSON_VALUE_WRITER, {
            iconFileId: iconFile.id,
            resources: [
              {fileId: gameSwfFile.id, tag: "game-swf", priority: "high"},
              {fileId: gameXmlFile.id, tag: "game-xml", priority: "high"},
              {fileId: musicFile.id, tag: "music", priority: "high"},
              {fileId: musicFile.id, tag: "music", priority: "high"},
              {fileId: musicFile.id, tag: "music", priority: "high"},
            ],
          }));
        updatedAlpha = readJsonResponse($Game, res.body);
      }

      const expectedUpdatedAlpha: Game = {
        ...alpha,
        iconFile: {id: iconFile.id},
        updatedAt: updatedAlpha.updatedAt,
        resources: [
          {file: gameSwfFile, tag: "game-swf", priority: "high"},
          {file: gameXmlFile, tag: "game-xml", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
        ],
      };

      chai.assert.deepEqual(updatedAlpha, expectedUpdatedAlpha);
    });
  });

  it("Administrators can set the `publicationDate` value", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const {aliceAgent} = await populateWithUsers(server);
      const {alpha} = await createSampleGames(aliceAgent);

      chai.assert.propertyVal(alpha, "publicationDate", null);
      const now: Date = new Date();
      let updatedAlpha: Game;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/games/${alpha.id}`)
          .send($UpdateGameOptions.write(JSON_VALUE_WRITER, {publicationDate: now}));
        updatedAlpha = readJsonResponse($Game, res.body);
      }
      chai.assert.deepEqual(updatedAlpha.id, alpha.id);
      chai.assert.deepEqual(updatedAlpha.publicationDate, now);
      let updatedAlpha2: Game;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/games/${alpha.id}`)
          .send($UpdateGameOptions.write(JSON_VALUE_WRITER, {publicationDate: null}));
        updatedAlpha2 = readJsonResponse($Game, res.body);
      }
      chai.assert.deepEqual(updatedAlpha2.id, alpha.id);
      chai.assert.deepEqual(updatedAlpha2.publicationDate, null);
    });
  });

  it("Can give the leaderboard of a game", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const {alice, aliceAgent} = await populateWithUsers(server);
      const {alpha} = await createSampleGames(aliceAgent);

      // TODO: Replace with a run populate function when there is one.
      const createRunOptions: CreateRunOptions = {
        userId: alice.id,
        gameId: alpha.id,
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: false,
          volume: 100,
        },
      };
      const runRes: ChaiHttp.Response = await aliceAgent.post("/runs")
        .send($CreateRunOptions.write(JSON_VALUE_WRITER, createRunOptions));
      const actualRun: Run = readJsonResponse($Run, runRes.body);
      await aliceAgent.post(`/runs/${actualRun.id}/start`).send({});
      await aliceAgent.post(`/runs/${actualRun.id}/result`)
        .send($SetRunResultOptions.write(JSON_VALUE_WRITER, {
          isVictory: true,
          maxLevel: 10,
          scores: [1500],
          items: new Map(),
          stats: {}
        }));

      const res: ChaiHttp.Response = await aliceAgent.get(`/games/${alpha.id}/leaderboard?game_mode=solo`);
      const leaderboard: Leaderboard = readJsonResponse($Leaderboard, res.body);

      const expectedLeaderboard: Leaderboard = {
        game: {id: alpha.id, displayName: "Alpha"},
        gameMode: "solo",
        results: [{
          score: 1500,
          user: {type: ActorType.User, id: alice.id, displayName: "Alice"},
          run: {id: actualRun.id, maxLevel: 10, gameOptions: ["boost"]}
        }]
      };
      chai.assert.deepEqual(leaderboard, expectedLeaderboard);
    });
  });
});

interface SampleGames {
  alpha: Game;
  beta: Game;
}

// TODO: Remove this function and use `populateWithGames`
async function createSampleGames(agent: ChaiHttp.Agent): Promise<SampleGames> {
  let alpha: Game;
  {
    const options: CreateGameOptions = {
      mainLocale: "fr-FR",
      displayName: "Alpha",
      description: "Alpha game description.",
      engines: {
        loader: "3.0.0",
      },
      iconFileId: undefined,
      resources: [],
      gameModes: [
        {key: "solo", displayName: "Aventure", state: GameModeState.Enabled, options: [], locales: new Map()},
      ],
      families: "1,2,4",
      category: GameCategory.Big,
      isPrivate: true,
    };
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(JSON_VALUE_WRITER, options));
    alpha = readJsonResponse($Game, res.body);
  }
  let beta: Game;
  {
    const options: CreateGameOptions = {
      mainLocale: "fr-FR",
      displayName: "Beta",
      description: "Beta game description.",
      engines: {
        loader: "3.0.0",
      },
      iconFileId: undefined,
      resources: [],
      gameModes: [
        {key: "solo", displayName: "Aventure", state: GameModeState.Enabled, options: [], locales: new Map()},
      ],
      families: "10,20,40",
      category: GameCategory.Big,
      isPrivate: true,
    };
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(JSON_VALUE_WRITER, options));
    beta = readJsonResponse($Game, res.body);
  }
  return {alpha, beta};
}

interface SousLaCollineFiles {
  icon: File;
  gameSwf: File;
  gameXml: File;
  music: File;
}

async function uploadSousLaCollineFiles(
  agent: ChaiHttp.Agent,
  directoryId: UuidHex,
): Promise<SousLaCollineFiles> {
  async function createFile(components: string[], mediaType: MediaType): Promise<File> {
    const buffer: Buffer = await getTestResource(["example-games", "sous-la-colline", ...components]);
    const blobRes: ChaiHttp.Response = await agent.post("/blobs")
      .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType, data: buffer}));
    const blob: Blob = readJsonResponse($Blob, blobRes.body);
    const fileRes: ChaiHttp.Response = await agent.post("/files")
      .send($CreateFileOptions.write(
        JSON_VALUE_WRITER,
        {
          parentId: directoryId,
          displayName: components[components.length - 1],
          blobId: blob.id,
        }));
    return readJsonResponse($File, fileRes.body);
  }

  const icon: File = await createFile(["icon.png"], "image/png");
  const gameSwf: File = await createFile(["game.swf"], "application/x-shockwave-flash");
  const gameXml: File = await createFile(["game.xml"], "application/xml");
  const music: File = await createFile(["music", "rourou.mp3"], "music/mp3");

  return {icon, gameSwf, gameXml, music};
}

async function getDriveByOwnerId(agent: ChaiHttp.Agent, userId: string): Promise<Drive> {
  const res: ChaiHttp.Response = await agent.get(`/users/${userId}/drive`);
  return readJsonResponse($Drive, res.body);
}
