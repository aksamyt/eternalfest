import { $Blob, Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { $CreateBlobOptions } from "@eternalfest/api-core/lib/blob/create-blob-options.js";
import { $CreateFileOptions } from "@eternalfest/api-core/lib/file/create-file-options.js";
import { DriveItemType } from "@eternalfest/api-core/lib/file/drive-item-type.js";
import { $Drive, Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { $File, File } from "@eternalfest/api-core/lib/file/file.js";
import { $CreateUserWithCredentialsOptions } from "@eternalfest/api-core/lib/user/create-user-with-credentials-options.js";
import { $User, User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";

import { getTestResource } from "../get-test-resource.js";
import { readJsonResponse } from "../helpers.js";
import { DbState } from "../test-db.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/games", function () {
  it("Upload sous-la-colline/icon.png", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);

    return withTestServer(DbState.Empty, async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users").send($CreateUserWithCredentialsOptions.write(
          JSON_VALUE_WRITER,
          {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
        ));
        aliceUser = readJsonResponse($User, res.body);
      }
      let aliceDrive: Drive;
      {
        const res: ChaiHttp.Response = await aliceAgent.get(`/users/${aliceUser.id}/drive`);
        aliceDrive = readJsonResponse($Drive, res.body);
      }

      const buffer: Buffer = await getTestResource(["example-games", "sous-la-colline", "icon.png"]);
      const blobRes: ChaiHttp.Response = await aliceAgent.post("/blobs")
        .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType: "image/png", data: buffer}));
      const blob: Blob = readJsonResponse($Blob, blobRes.body);

      const fileRes: ChaiHttp.Response = await aliceAgent.post("/files")
        .send($CreateFileOptions.write(
          JSON_VALUE_WRITER,
          {parentId: aliceDrive.root.id, displayName: "icon.png", blobId: blob.id},
        ));
      const actual: File = readJsonResponse($File, fileRes.body);

      const expected: File = {
        type: DriveItemType.File,
        id: actual.id,
        createdAt: actual.createdAt,
        updatedAt: actual.updatedAt,
        displayName: "icon.png",
        byteSize: 80805,
        mediaType: "image/png",
      };

      chai.assert.deepEqual(actual, expected);
    });
  });

  it("Retrieve sous-la-colline/icon.png", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);

    return withTestServer(DbState.Empty, async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users").send($CreateUserWithCredentialsOptions.write(
          JSON_VALUE_WRITER,
          {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
        ));
        aliceUser = readJsonResponse($User, res.body);
      }
      let aliceDrive: Drive;
      {
        const res: ChaiHttp.Response = await aliceAgent.get(`/users/${aliceUser.id}/drive`);
        aliceDrive = readJsonResponse($Drive, res.body);
      }
      let iconFile: File;
      {
        const buffer: Buffer = await getTestResource(["example-games", "sous-la-colline", "icon.png"]);
        const blobRes: ChaiHttp.Response = await aliceAgent.post("/blobs")
          .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType: "image/png", data: buffer}));
        const blob: Blob = readJsonResponse($Blob, blobRes.body);

        const fileRes: ChaiHttp.Response = await aliceAgent.post("/files")
          .send($CreateFileOptions.write(
            JSON_VALUE_WRITER,
            {parentId: aliceDrive.root.id, displayName: "icon.png", blobId: blob.id},
          ));
        iconFile = readJsonResponse($File, fileRes.body);
      }

      const res: ChaiHttp.Response = await aliceAgent.get(`/files/${iconFile.id}`);
      const actual: File = readJsonResponse($File, res.body);

      const expected: File = {
        type: DriveItemType.File,
        id: iconFile.id,
        createdAt: iconFile.createdAt,
        updatedAt: iconFile.updatedAt,
        displayName: "icon.png",
        byteSize: 80805,
        mediaType: "image/png",
      };

      chai.assert.deepEqual(actual, expected);
    });
  });

  it("Download sous-la-colline/icon.png", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);

    return withTestServer(DbState.Empty, async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users").send($CreateUserWithCredentialsOptions.write(
          JSON_VALUE_WRITER,
          {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
        ));
        aliceUser = readJsonResponse($User, res.body);
      }
      let aliceDrive: Drive;
      {
        const res: ChaiHttp.Response = await aliceAgent.get(`/users/${aliceUser.id}/drive`);
        aliceDrive = readJsonResponse($Drive, res.body);
      }
      const iconBuffer: Buffer = await getTestResource(["example-games", "sous-la-colline", "icon.png"]);
      let iconFile: File;
      {
        const blobRes: ChaiHttp.Response = await aliceAgent.post("/blobs")
          .send($CreateBlobOptions.write(JSON_VALUE_WRITER, {mediaType: "image/png", data: iconBuffer}));
        const blob: Blob = readJsonResponse($Blob, blobRes.body);

        const fileRes: ChaiHttp.Response = await aliceAgent.post("/files")
          .send($CreateFileOptions.write(
            JSON_VALUE_WRITER,
            {parentId: aliceDrive.root.id, displayName: "icon.png", blobId: blob.id},
          ));
        iconFile = readJsonResponse($File, fileRes.body);
      }

      const res: ChaiHttp.Response = await aliceAgent.get(`/files/${iconFile.id}/raw`);

      chai.assert.strictEqual(res.type, "image/png");
      chai.assert.deepEqual(res.body, iconBuffer);
    });
  });
});
