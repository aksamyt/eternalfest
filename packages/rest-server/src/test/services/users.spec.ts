import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { $CreateUserWithCredentialsOptions } from "@eternalfest/api-core/lib/user/create-user-with-credentials-options.js";
import { $UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options.js";
import { $User, User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";

import { readJsonResponse } from "../helpers.js";
import { DbState } from "../test-db.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/users", function () {
  it("should be possible to create the first user", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const agent: ChaiHttp.Agent = chai.request.agent(server);
      {
        const res: ChaiHttp.Response = await agent.get("/users");
        chai.assert.deepEqual(res.body, []);
      }
      {
        const res: ChaiHttp.Response = await agent.post("/users").send($CreateUserWithCredentialsOptions.write(
          JSON_VALUE_WRITER,
          {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
        ));

        const user: User = readJsonResponse($User, res.body);

        const expected: User = {
          id: user.id,
          type: ActorType.User,
          displayName: "Alice",
          hasPassword: true,
          identities: [],
          isAdministrator: true,
          isTester: true,
          createdAt: user.createdAt,
          updatedAt: user.updatedAt,
        };

        chai.assert.deepEqual(user, expected);

        await agent.get(`/users/${user.id}`).send();
      }
    });
  });

  it("Create a user and changes his display name", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users")
          .send($CreateUserWithCredentialsOptions.write(
            JSON_VALUE_WRITER,
            {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
          ));
        aliceUser = readJsonResponse($User, res.body);
      }
      let updatedUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${aliceUser.id}`)
          .send($UpdateUserOptions.write(
            JSON_VALUE_WRITER,
            {displayName: "Bob"},
          ));
        updatedUser = readJsonResponse($User, res.body);
      }
      const expected: User = {
        id: aliceUser.id,
        type: ActorType.User,
        createdAt: aliceUser.createdAt,
        updatedAt: updatedUser.updatedAt,
        identities: [],
        isAdministrator: true,
        isTester: true,
        hasPassword: true,
        displayName: "Bob",
      };
      chai.assert.deepEqual(updatedUser, expected);
    });
  });

  it("Administrators can set the `isTester` value for users", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      const bobAgent: ChaiHttp.Agent = chai.request.agent(server);
      {
        await aliceAgent.post("/users")
          .send($CreateUserWithCredentialsOptions.write(
            JSON_VALUE_WRITER,
            {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
          ));
      }
      let bobUser: User;
      {
        const res: ChaiHttp.Response = await bobAgent.post("/users")
          .send($CreateUserWithCredentialsOptions.write(
            JSON_VALUE_WRITER,
            {displayName: "Bob", login: "bob", password: Buffer.from("BOB")},
          ));
        bobUser = readJsonResponse($User, res.body);
      }

      chai.assert.isFalse(bobUser.isTester);

      let updatedBob: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${bobUser.id}`)
          .send($UpdateUserOptions.write(
            JSON_VALUE_WRITER,
            {isTester: true},
          ));
        updatedBob = readJsonResponse($User, res.body);
      }

      chai.assert.strictEqual(updatedBob.id, bobUser.id);
      chai.assert.isTrue(updatedBob.isTester);

      let updatedBob2: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${bobUser.id}`)
          .send($UpdateUserOptions.write(
            JSON_VALUE_WRITER,
            {isTester: false},
          ));
        updatedBob2 = readJsonResponse($User, res.body);
      }

      chai.assert.strictEqual(updatedBob2.id, bobUser.id);
      chai.assert.isFalse(updatedBob2.isTester);
    });
  });

  it("The `isTester` value cannot be set to `false` for administrators", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
      let aliceUser: User;
      {
        const res: ChaiHttp.Response = await aliceAgent.post("/users")
          .send($CreateUserWithCredentialsOptions.write(
            JSON_VALUE_WRITER,
            {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
          ));
        aliceUser = readJsonResponse($User, res.body);
      }
      const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${aliceUser.id}`)
        .send($UpdateUserOptions.write(JSON_VALUE_WRITER, {isTester: false}));
      chai.assert.strictEqual(res.status, 500);
    });
  });

  it("The `isTester` value cannot be set to `false` for administrators", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const bobAgent: ChaiHttp.Agent = chai.request.agent(server);
      {
        const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
        await aliceAgent.post("/users")
          .send($CreateUserWithCredentialsOptions.write(
            JSON_VALUE_WRITER,
            {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
          ));
      }
      let bobUser: User;
      {
        const res: ChaiHttp.Response = await bobAgent.post("/users")
          .send($CreateUserWithCredentialsOptions.write(
            JSON_VALUE_WRITER,
            {displayName: "Bob", login: "bob", password: Buffer.from("BOB")},
          ));
        bobUser = readJsonResponse($User, res.body);
      }
      const res: ChaiHttp.Response = await bobAgent.patch(`/users/${bobUser.id}`)
        .send($UpdateUserOptions.write(JSON_VALUE_WRITER, {isTester: true}));
      chai.assert.strictEqual(res.status, 500);
    });
  });
});
