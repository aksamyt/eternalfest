import chai from "chai";
import chaiHttp from "chai-http";

import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/self", function () {
  it(
    "should be possible to create a session with the `hfest-credentials` strategy",
    async function (this: Mocha.Context) {
      this.timeout(TIMEOUT);
      this.skip(); // AuthMigration
      // return withTestServer(DbState.Empty, async server => {
      //   const bobAgent: ChaiHttp.Agent = chai.request.agent(server);
      //   let bobAuth: UserAuthContext;
      //   {
      //     const res: ChaiHttp.Response = await bobAgent.put("/self/session")
      //       .query($CreateSessionQuery.write(QS_WRITER, {strategy: AuthStrategy.HfestCredentials}))
      //       .send($AuthHfestCredentials.write(JSON_WRITER, {
      //         server: HfestServer.Fr,
      //         username: "bob",
      //         password: Buffer.from("BOB"),
      //       }));
      //     bobAuth = readJsonResponse($UserAuthContext, res.body);
      //     const expectedAuth: UserAuthContext = {
      //       type: ActorType.User,
      //       scope: AuthScope.Default,
      //       userId: bobAuth.userId,
      //       displayName: "bob",
      //       isAdministrator: true,
      //       isTester: true,
      //     };
      //     chai.assert.deepEqual(bobAuth, expectedAuth);
      //   }
      // });
    },
  );
});
