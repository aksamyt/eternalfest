import { $CreateRunOptions, CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { $RunStart, RunStart } from "@eternalfest/api-core/lib/run/run-start.js";
import { $Run, Run } from "@eternalfest/api-core/lib/run/run.js";
import { $SetRunResultOptions } from "@eternalfest/api-core/lib/run/set-run-result-options.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import chai from "chai";
import chaiHttp from "chai-http";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";

import { readJsonResponse } from "../helpers.js";
import { populateWithGames } from "../populate/games.js";
import { populateWithUsers } from "../populate/users.js";
import { DbState } from "../test-db.js";
import { withTestServer } from "../test-server.js";
import { TIMEOUT } from "../timeout.js";

chai.use(chaiHttp);

describe("/runs", function () {
  it("Start a \"Sous la colline\" run", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withTestServer(DbState.Empty, async server => {
      const {alice, aliceAgent} = await populateWithUsers(server);
      const {alpha} = await populateWithGames(aliceAgent);

      const createRunOptions: CreateRunOptions = {
        userId: alice.id,
        gameId: alpha.id,
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      const runRes: ChaiHttp.Response = await aliceAgent.post("/runs")
        .send($CreateRunOptions.write(JSON_VALUE_WRITER, createRunOptions));
      const actualRun: Run = readJsonResponse($Run, runRes.body);

      const expectedRun: Run = {
        id: actualRun.id,
        createdAt: actualRun.createdAt,
        startedAt: null,
        result: null,
        user: {id: alice.id, type: ActorType.User, displayName: alice.displayName},
        game: {id: alpha.id, displayName: alpha.displayName},
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      chai.assert.deepEqual(actualRun, expectedRun);

      const runStartRes: ChaiHttp.Response = await aliceAgent.post(`/runs/${actualRun.id}/start`).send({});
      const actualRunStart: RunStart = readJsonResponse($RunStart, runStartRes.body);

      const expectedRunStart: RunStart = {
        run: {
          id: actualRun.id,
        },
        key: actualRunStart.key,
        families: alpha.families,
        items: new Map(),
      };

      chai.assert.deepEqual(actualRunStart, expectedRunStart);

      const runWithResultRes: ChaiHttp.Response = await aliceAgent.post(`/runs/${actualRun.id}/result`)
        .send($SetRunResultOptions.write(
          JSON_VALUE_WRITER,
          {isVictory: true, maxLevel: 10, scores: [1500], items: new Map([["1000", 10]]), stats: {jumps: 10}}),
        );
      const actualRunWithResult: Run = readJsonResponse($Run, runWithResultRes.body);

      const expectedRunWithResult: Run = {
        id: actualRun.id,
        createdAt: actualRun.createdAt,
        startedAt: actualRunWithResult.startedAt,
        result: {
          createdAt: actualRunWithResult.result!.createdAt,
          isVictory: true,
          maxLevel: 10,
          scores: [1500],
          items: new Map([["1000", 10]]),
          stats: {jumps: 10},
        },
        user: {id: alice.id, type: ActorType.User, displayName: alice.displayName},
        game: {id: alpha.id, displayName: alpha.displayName},
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      chai.assert.deepEqual(actualRunWithResult, expectedRunWithResult);
    });
  });
});
