import { PgAuthService } from "@eternalfest/api-backend/lib/auth/pg-service.js";
import { PgBlobService } from "@eternalfest/api-backend/lib/blob/pg-service.js";
import { PgFileService } from "@eternalfest/api-backend/lib/file/pg-service.js";
import { PgGameService } from "@eternalfest/api-backend/lib/game/pg-service.js";
import { InMemoryHfestApiService } from "@eternalfest/api-backend/lib/hfest-api/in-memory-service.js";
import { PgHfestIdentityService } from "@eternalfest/api-backend/lib/hfest-identity/pg-service.js";
import { ScryptPasswordService } from "@eternalfest/api-backend/lib/password/scrypt-service.js";
import { PgRunService } from "@eternalfest/api-backend/lib/run/pg-service.js";
import { PgLeaderboardService } from "@eternalfest/api-backend/lib/leaderboard/pg-service.js";
import { PgSessionService } from "@eternalfest/api-backend/lib/session/pg-service.js";
import { PgUserService } from "@eternalfest/api-backend/lib/user/pg-service.js";
import http from "http";
import Koa from "koa";

import { createApiRouter } from "../lib/create-api-router.js";
import { KoaAuth } from "../lib/koa-auth.js";
import { createMockHammerfestApi } from "./mock-hammerfest-api.js";
import { DbState, StateService, withTestState } from "./test-db.js";

export async function withTestServer<R>(state: DbState, handler: (server: http.Server) => Promise<R>): Promise<R> {
  return withTestState(state, async ({db, buffer}: StateService): Promise<R> => {
    const blob: PgBlobService = new PgBlobService(db, buffer);
    const password: ScryptPasswordService = new ScryptPasswordService(/* 0.1 */);
    const user: PgUserService = new PgUserService(db, password);
    const file: PgFileService = new PgFileService(db, blob);
    const game: PgGameService = new PgGameService(db, user, file);
    const run: PgRunService = new PgRunService(db, game, user);
    const leaderboard: PgLeaderboardService = new PgLeaderboardService(db, game);
    const mockHammerfestApi: InMemoryHfestApiService = createMockHammerfestApi();
    const hfestIdentity: PgHfestIdentityService = new PgHfestIdentityService(db, mockHammerfestApi);
    const session: PgSessionService = new PgSessionService(db, 86400);
    const auth: PgAuthService = new PgAuthService(mockHammerfestApi, hfestIdentity, session, user);
    const koaAuth: KoaAuth = new KoaAuth(session, user);
    const app: Koa = createApiRouter({auth, blob, file, game, hfestIdentity, koaAuth, run, leaderboard, session, user});

    const server: http.Server = http.createServer(app.callback());

    async function closeServer(): Promise<void> {
      return new Promise<void>(resolve => {
        server.close(() => {
          resolve();
        });
      });
    }

    return new Promise<R>((resolve, reject): void => {
      async function onListening(): Promise<void> {
        server.removeListener("error", onError);

        let result: R;
        try {
          result = await handler(server);
        } catch (err) {
          await closeServer();
          reject(err);
          return;
        }
        await closeServer();
        resolve(result);
      }

      function onError(err: Error): void {
        server.removeListener("listening", onListening);
        reject(err);
      }

      server.once("listening", onListening);
      server.once("error", onError);

      server.listen();
    });
  });
}
