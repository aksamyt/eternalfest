import { $CreateUserWithCredentialsOptions } from "@eternalfest/api-core/lib/user/create-user-with-credentials-options.js";
import { $UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options.js";
import { $User, User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";
import http from "http";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";

import { readJsonResponse } from "../helpers.js";

export interface PopulateUsers {
  alice: User;
  aliceAgent: ChaiHttp.Agent;
  bob: User;
  bobAgent: ChaiHttp.Agent;
  charlie: User;
  charlieAgent: ChaiHttp.Agent;
}

export async function populateWithUsers(server: http.Server): Promise<PopulateUsers> {
  const aliceAgent: ChaiHttp.Agent = chai.request.agent(server);
  let alice: User;
  {
    const res: ChaiHttp.Response = await aliceAgent.post("/users")
      .send($CreateUserWithCredentialsOptions.write(
        JSON_VALUE_WRITER,
        {displayName: "Alice", login: "alice", password: Buffer.from("ALICE")},
      ));

    alice = readJsonResponse($User, res.body);
  }
  const bobAgent: ChaiHttp.Agent = chai.request.agent(server);
  let bob: User;
  {
    const res: ChaiHttp.Response = await bobAgent.post("/users")
      .send($CreateUserWithCredentialsOptions.write(
        JSON_VALUE_WRITER,
        {displayName: "Bob", login: "bob", password: Buffer.from("BOB")},
      ));

    const simpleBob: User = readJsonResponse($User, res.body);
    {
      const res: ChaiHttp.Response = await aliceAgent.patch(`/users/${simpleBob.id}`)
        .send($UpdateUserOptions.write(JSON_VALUE_WRITER, {isTester: true}));
      bob = readJsonResponse($User, res.body);
    }
  }
  const charlieAgent: ChaiHttp.Agent = chai.request.agent(server);
  let charlie: User;
  {
    const res: ChaiHttp.Response = await charlieAgent.post("/users")
      .send($CreateUserWithCredentialsOptions.write(
        JSON_VALUE_WRITER,
        {displayName: "Charlie", login: "charlie", password: Buffer.from("CHARLIE")},
      ));

    charlie = readJsonResponse($User, res.body);
  }

  return {alice, aliceAgent, bob, bobAgent, charlie, charlieAgent};
}
