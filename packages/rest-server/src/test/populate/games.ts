import { $CreateGameOptions } from "@eternalfest/api-core/lib/game/create-game-options.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state.js";
import { $Game, Game } from "@eternalfest/api-core/lib/game/game.js";
import { JSON_VALUE_WRITER } from "kryo-json/lib/json-value-writer.js";

import { readJsonResponse } from "../helpers.js";

export interface SampleGames {
  alpha: Game;
  beta: Game;
}

export async function populateWithGames(agent: ChaiHttp.Agent): Promise<SampleGames> {
  let alpha: Game;
  {
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(
        JSON_VALUE_WRITER,
        {
          mainLocale: "fr-FR",
          displayName: "Alpha",
          description: "Alpha game description.",
          engines: {
            loader: "3.0.0",
          },
          iconFileId: undefined,
          resources: [],
          gameModes: [
            {key: "solo", displayName: "Aventure", state: GameModeState.Enabled, options: [], locales: new Map()},
          ],
          families: "1,2,4",
          category: GameCategory.Big,
          isPrivate: true,
        },
      ));

    alpha = readJsonResponse($Game, res.body);
  }
  let beta: Game;
  {
    const res: ChaiHttp.Response = await agent.post("/games")
      .send($CreateGameOptions.write(
        JSON_VALUE_WRITER,
        {
          mainLocale: "fr-FR",
          displayName: "Beta",
          description: "Beta game description.",
          engines: {
            loader: "3.0.0",
          },
          iconFileId: undefined,
          resources: [],
          gameModes: [
            {key: "solo", displayName: "Aventure", state: GameModeState.Enabled, options: [], locales: new Map()},
          ],
          families: "10,20,40",
          category: GameCategory.Big,
          isPrivate: true,
        },
      ));

    beta = readJsonResponse($Game, res.body);
  }

  return {alpha, beta};
}
