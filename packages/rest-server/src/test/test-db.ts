import { Database } from "@eternalfest/api-backend/lib/db/database.js";
import bufferService from "@eternalfest/buffer-service/buffer-service.js";
import fsBufferService from "@eternalfest/buffer-service/fs-buffer-service.js";
import { LATEST_DB_VERSION, setState as setDbState } from "@eternalfest/eternalfest-db/lib/index.js";
import furi from "furi";
import sysPath from "path";
import pg from "pg";
import url from "url";

import { Config, getLocalConfigOrDie } from "./config.js";
import meta from "./meta.js";

const CONFIG: Config = getLocalConfigOrDie();

const DATA_ROOT: string = sysPath.posix.join(CONFIG.dataRoot, "test");
if (DATA_ROOT.indexOf("/rest-server/") < 0) {
  console.error("Unsafe test data directory: it must be a subdirectory of `rest-server`");
  process.exit(1);
}

async function withTestDbPool<R>(handler: (pool: pg.Pool) => Promise<R>): Promise<R> {
  const poolConfig: pg.PoolConfig = {
    user: CONFIG.user,
    password: CONFIG.password,
    host: CONFIG.host,
    port: CONFIG.port,
    database: CONFIG.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);

  let result: R;
  try {
    result = await handler(pool);
  } catch (err) {
    await pool.end();
    throw err;
  }
  await pool.end();
  return result;
}

export enum DbState {
  Void,
  Empty,
}

export async function withTestDb<R>(state: DbState, handler: (db: Database) => Promise<R>): Promise<R> {
  return withTestDbPool(async (pool: pg.Pool): Promise<R> => {
    const db: Database = new Database(pool);
    await setDbState(db, LATEST_DB_VERSION, state === DbState.Void);
    return handler(db);
  });
}

export interface StateService {
  db: Database;
  buffer: bufferService.BufferService;
}

export async function withTestState<R>(dbState: DbState, handler: (state: StateService) => Promise<R>): Promise<R> {
  return withTestDb(dbState, async (db: Database): Promise<R> => {
    return withTestFsBufferRoot(async (root: url.URL): Promise<R> => {
      const buffer: fsBufferService.FsBufferService = new fsBufferService.FsBufferService(root);
      return handler({db, buffer});
    });
  });
}

async function withTestFsBufferRoot<R>(handler: (root: url.URL) => Promise<R>): Promise<R> {
  const fsBufferRoot: url.URL = furi.join(furi.fromSysPath(meta.dirname), ["fs-buffer"]);
  return handler(fsBufferRoot);
}
