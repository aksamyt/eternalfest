import { DriveItemType } from "@eternalfest/api-core/lib/file/drive-item-type.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { LocaleId } from "@eternalfest/api-core/lib/types/locale-id.js";
import { IdentityType } from "@eternalfest/api-core/lib/types/user/identity-type.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import incident from "incident";

export function dbEnumFromDriveItemType(driveItemType: DriveItemType): dbSchema.DriveItemType {
  switch (driveItemType) {
    case DriveItemType.Directory:
      return dbSchema.DRIVE_ITEM_TYPE_DIRECTORY;
    case DriveItemType.File:
      return dbSchema.DRIVE_ITEM_TYPE_FILE;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${driveItemType}`);
  }
}

export function dbEnumToDriveItemType(dbEnum: dbSchema.DriveItemType): DriveItemType {
  switch (dbEnum) {
    case dbSchema.DRIVE_ITEM_TYPE_DIRECTORY:
      return DriveItemType.Directory;
    case dbSchema.DRIVE_ITEM_TYPE_FILE:
      return DriveItemType.File;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${dbEnum}`);
  }
}

export function dbEnumFromGameCategory(gameCategory: GameCategory): dbSchema.GameCategory {
  switch (gameCategory) {
    case GameCategory.Big:
      return dbSchema.GAME_CATEGORY_BIG;
    case GameCategory.Challenge:
      return dbSchema.GAME_CATEGORY_CHALLENGE;
    case GameCategory.Fun:
      return dbSchema.GAME_CATEGORY_FUN;
    case GameCategory.Lab:
      return dbSchema.GAME_CATEGORY_LAB;
    case GameCategory.Puzzle:
      return dbSchema.GAME_CATEGORY_PUZZLE;
    case GameCategory.Other:
      return dbSchema.GAME_CATEGORY_OTHER;
    case GameCategory.Small:
      return dbSchema.GAME_CATEGORY_SMALL;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${gameCategory}`);
  }
}

export function dbEnumToGameCategory(dbEnum: dbSchema.GameCategory): GameCategory {
  switch (dbEnum) {
    case dbSchema.GAME_CATEGORY_BIG:
      return GameCategory.Big;
    case dbSchema.GAME_CATEGORY_CHALLENGE:
      return GameCategory.Challenge;
    case dbSchema.GAME_CATEGORY_FUN:
      return GameCategory.Fun;
    case dbSchema.GAME_CATEGORY_LAB:
      return GameCategory.Lab;
    case dbSchema.GAME_CATEGORY_PUZZLE:
      return GameCategory.Puzzle;
    case dbSchema.GAME_CATEGORY_OTHER:
      return GameCategory.Other;
    case dbSchema.GAME_CATEGORY_SMALL:
      return GameCategory.Small;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${dbEnum}`);
  }
}

export function dbEnumFromLocaleId(localeId: LocaleId): dbSchema.LocaleId {
  switch (localeId) {
    case "en-US":
      return dbSchema.LOCALE_ID_EN_US;
    case "es-SP":
      return dbSchema.LOCALE_ID_ES_SP;
    case "fr-FR":
      return dbSchema.LOCALE_ID_FR_FR;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${localeId}`);
  }
}

export function dbEnumToLocaleId(dbEnum: dbSchema.LocaleId): LocaleId {
  switch (dbEnum) {
    case dbSchema.LOCALE_ID_EN_US:
      return "en-US";
    case dbSchema.LOCALE_ID_ES_SP:
      return "es-SP";
    case dbSchema.LOCALE_ID_FR_FR:
      return "fr-FR";
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${dbEnum}`);
  }
}

export function dbEnumFromHfestServer(hfestServer: HfestServer): dbSchema.HfestServer {
  switch (hfestServer) {
    case HfestServer.En:
      return dbSchema.HFEST_SERVER_EN;
    case HfestServer.Es:
      return dbSchema.HFEST_SERVER_ES;
    case HfestServer.Fr:
      return dbSchema.HFEST_SERVER_FR;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${hfestServer}`);
  }
}

export function dbEnumToHfestServer(dbEnum: dbSchema.HfestServer): HfestServer {
  switch (dbEnum) {
    case dbSchema.HFEST_SERVER_EN:
      return HfestServer.En;
    case dbSchema.HFEST_SERVER_ES:
      return HfestServer.Es;
    case dbSchema.HFEST_SERVER_FR:
      return HfestServer.Fr;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${dbEnum}`);
  }
}

export function dbEnumFromIdentityType(identityType: IdentityType): dbSchema.IdentityType {
  switch (identityType) {
    case IdentityType.Ef:
      return dbSchema.IDENTITY_TYPE_EF;
    case IdentityType.Hfest:
      return dbSchema.IDENTITY_TYPE_HFEST;
    default:
      throw new incident.Incident(`UnknownEnumVariant: ${identityType}`);
  }
}
