import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
// import hfApiServer from "@eternalfest/hammerfest-api/api/get-server.js";

export const SERVER_EN: string = "http://www.hfest.net/";
export const SERVER_ES: string = "http://www.hammerfest.es/";
export const SERVER_FR: string = "http://www.hammerfest.fr/";

export function hfestServerFromUrl(hfestServerUrl: string): HfestServer {
  switch (hfestServerUrl) {
    case SERVER_EN:
      return HfestServer.En;
    case SERVER_ES:
      return HfestServer.Es;
    case SERVER_FR:
      return HfestServer.Fr;
    default:
      throw new Error(`UnknownEnumVariant: ${hfestServerUrl}`);
  }
}

export function hfestServerToUrl(hfestServer: HfestServer): string {
  switch (hfestServer) {
    case HfestServer.En:
      return SERVER_EN;
    case HfestServer.Es:
      return SERVER_ES;
    case HfestServer.Fr:
      return SERVER_FR;
    default:
      throw new Error(`UnknownEnumVariant: ${hfestServer}`);
  }
}
