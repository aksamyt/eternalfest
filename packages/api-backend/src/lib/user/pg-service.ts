import { AuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { createForbiddenError } from "@eternalfest/api-core/lib/errors/forbidden.js";
import { createUnauthenticatedError } from "@eternalfest/api-core/lib/errors/unauthenticated.js";
import { createUserNotFoundError } from "@eternalfest/api-core/lib/errors/user-not-found.js";
import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity.js";
import { PasswordService } from "@eternalfest/api-core/lib/password/service.js";
import { Email } from "@eternalfest/api-core/lib/types/email.js";
import { IdRef } from "@eternalfest/api-core/lib/types/id-ref.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { IdentityType } from "@eternalfest/api-core/lib/types/user/identity-type.js";
import { Identity } from "@eternalfest/api-core/lib/types/user/identity.js";
import { ItemCount } from "@eternalfest/api-core/lib/types/user/item-count.js";
import { CreateUserWithCredentialsOptions } from "@eternalfest/api-core/lib/user/create-user-with-credentials-options.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { UpdateUserOptions } from "@eternalfest/api-core/lib/user/update-user-options.js";
import { UserDisplayName } from "@eternalfest/api-core/lib/user/user-display-name";
import { UserId } from "@eternalfest/api-core/lib/user/user-id";
import { UserRef } from "@eternalfest/api-core/lib/user/user-ref.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import squel from "squel";
import uuidjs from "uuidjs";

import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionContext, TransactionMode } from "../db/transaction.js";
import { collectAsyncIterable } from "../utils/collect-async-iterable.js";
import { dbEnumToHfestServer } from "../utils/db-enum.js";

const PG_SQUEL: squel.PostgresSquel = squel.useFlavour("postgres");

export class PgUserService implements UserService {
  private readonly db: Database;
  private readonly passwordService: PasswordService;

  constructor(db: Database, passwordService: PasswordService) {
    this.db = db;
    this.passwordService = passwordService;
  }

  async createUserWithCredentials(_auth: AuthContext, options: CreateUserWithCredentialsOptions): Promise<User> {
    const params: CreateUserWithCredentialParameters = {
      passwordService: this.passwordService,
    };
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createUserWithCredentials(txCtx, params, options),
    );
  }

  async createUserWithHfestIdentity(auth: AuthContext, hfestIdentity: HfestIdentity): Promise<User> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createUserWithHfestIdentity(txCtx, auth, hfestIdentity),
    );
  }

  getOrCreateUserWithEtwin(auth: AuthContext, userId: UserId, userDisplay: UserDisplayName): Promise<User> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => getOrCreateUserWithEtwin(txCtx, auth, userId, userDisplay),
    );
  }

  async getUserById(auth: AuthContext, userId: UuidHex): Promise<User | undefined> {
    return this.db.transaction(TransactionMode.ReadOnly, async txCtx => getUserById(auth, txCtx, userId));
  }

  async getUserRefById(auth: AuthContext, userId: UuidHex): Promise<UserRef | undefined> {
    return this.db.transaction(TransactionMode.ReadOnly, async txCtx => getUserRefById(auth, txCtx, userId));
  }

  async getUserRefsById(auth: AuthContext, userIds: ReadonlySet<UuidHex>): Promise<Map<UuidHex, UserRef | undefined>> {
    return this.db.transaction(TransactionMode.ReadOnly, async txCtx => getUserRefsById(auth, txCtx, userIds));
  }

  async getUsers(auth: AuthContext): Promise<readonly  Partial<User>[]> {
    return this.db.transaction(TransactionMode.ReadOnly, txCtx => getAllUsers(auth, txCtx));
  }

  async updateUser(auth: AuthContext, options: UpdateUserOptions): Promise<User> {
    return this.db.transaction(TransactionMode.ReadWrite, async (txCtx: TransactionContext) => {
      const updatedId: IdRef = await updateUser(auth, txCtx, options);
      const updated: User | undefined = await getUserById(auth, txCtx, updatedId.id);
      if (updated === undefined) {
        throw createUserNotFoundError(updatedId.id);
      }
      return updated;
    });
  }
}

async function getUserById(
  auth: AuthContext,
  queryable: Queryable,
  userId: UuidHex,
): Promise<User | undefined> {
  const partialUser: Partial<User> | undefined = await getPartialUserById(auth, queryable, userId);
  if (partialUser === undefined) {
    return undefined;
  }
  const identities: Identity[] = await collectAsyncIterable(getIdentitiesByUserId(queryable, userId));
  return {
    id: partialUser.id!,
    type: ActorType.User,
    displayName: partialUser.displayName!,
    createdAt: partialUser.createdAt!,
    updatedAt: partialUser.updatedAt!,
    identities,
    isAdministrator: partialUser.isAdministrator!,
    isTester: partialUser.isTester,
    hasPassword: partialUser.hasPassword,
  };
}

async function getUserRefById(
  _auth: AuthContext,
  queryable: Queryable,
  userId: UuidHex,
): Promise<UserRef | undefined> {
  // tslint:disable-next-line:max-line-length
  type Row = Pick<dbSchema.User, "user_id" | "display_name">;

  const row: Row | undefined = await queryable.oneOrNone(
    `SELECT user_id, display_name
    FROM users
    WHERE users.user_id = $1::uuid;`,
    [userId],
  );
  if (row === undefined) {
    return undefined;
  }

  return {
    id: row.user_id,
    type: ActorType.User,
    displayName: row.display_name,
  };
}

async function getUserRefsById(
  auth: AuthContext,
  queryable: Queryable,
  userIds: ReadonlySet<UuidHex>,
): Promise<Map<UuidHex, UserRef | undefined>> {
  const userRefs: Map<UuidHex, UserRef | undefined> = new Map();
  for (const userId of userIds) {
    const userRef: UserRef | undefined = await getUserRefById(auth, queryable, userId);
    userRefs.set(userId, userRef);
  }
  return userRefs;
}

async function getPartialUserById(
  auth: AuthContext,
  queryable: Queryable,
  userId: UuidHex,
): Promise<Partial<User> | undefined> {
  // tslint:disable-next-line:max-line-length
  type Row = Pick<dbSchema.User, "user_id" | "display_name" | "created_at" | "updated_at" | "password_hash" | "is_administrator" | "is_tester">;

  const row: Row | undefined = await queryable.oneOrNone(
    `SELECT user_id, display_name, created_at, updated_at, is_administrator, is_tester, password_hash
    FROM users
    WHERE users.user_id = $1::uuid;`,
    [userId],
  );
  if (row === undefined) {
    return undefined;
  }

  let isTester: boolean | undefined = undefined;
  let hasPassword: boolean | undefined = undefined;
  if (auth.type === ActorType.System || auth.type === ActorType.User && (auth.isTester || auth.isAdministrator)) {
    isTester = row.is_tester;
    hasPassword = row.password_hash !== null;
  }

  return {
    id: row.user_id,
    type: ActorType.User,
    displayName: row.display_name,
    createdAt: row.created_at,
    updatedAt: row.updated_at,
    isAdministrator: row.is_administrator,
    isTester,
    hasPassword,
  };
}

async function getAllUsers(
  auth: AuthContext,
  queryable: Queryable,
): Promise<readonly Partial<User>[]> {
  // tslint:disable-next-line:max-line-length
  type Row = Pick<dbSchema.User, "user_id" | "display_name" | "created_at" | "updated_at" | "is_administrator" | "is_tester">;

  const rows: AsyncIterable<Row> = queryable.many(
    `SELECT user_id, display_name, created_at, updated_at, is_administrator, is_tester
    FROM users`,
    [],
  );

  const users: Partial<User>[] = [];
  for await(const row of rows) {
    let isTester: boolean | undefined = undefined;
    if (auth.type === ActorType.User && (auth.isTester || auth.isAdministrator)) {
      isTester = row.is_tester;
    }

    users.push({
      id: row.user_id,
      type: ActorType.User,
      displayName: row.display_name,
      createdAt: row.created_at,
      updatedAt: row.updated_at,
      isAdministrator: row.is_administrator,
      isTester,
    });
  }
  return users;
}

async function getItemsByIdentityId(
  queryable: Queryable,
  identityId: UuidHex,
): Promise<Map<string, ItemCount>> {
  type Row = Pick<dbSchema.Inventory, "hfest_item_id" | "count" | "max_count">;

  const rows: AsyncIterable<Row> = queryable.many(
    `SELECT hfest_item_id, count, max_count
    FROM inventories
    WHERE inventories.identity_id = $1::uuid;`,
    [identityId],
  );

  const items: Map<string, ItemCount> = new Map();

  for await (const row of rows) {
    items.set(row.hfest_item_id.toString(10), {count: row.count, maxCount: row.max_count});
  }

  return items;
}

async function* getIdentitiesByUserId(
  queryable: Queryable,
  userId: UuidHex,
  withItems: boolean = true,
): AsyncIterable<Identity> {
  // tslint:disable-next-line:max-line-length
  type Row = Pick<dbSchema.CompleteHfestIdentity, "identity_id" | "created_at" | "updated_at" | "user_id" | "server" | "hfest_id" | "username" | "email" | "best_score" | "best_level" | "game_completed">;

  const rows: AsyncIterable<Row> = queryable.many(
    `SELECT identities.*, hfest_identities.*
    FROM users INNER JOIN identities USING (user_id) INNER JOIN hfest_identities USING (identity_id)
    WHERE users.user_id = $1::uuid;`,
    [userId],
  );

  for await (const row of rows) {
    yield {
      id: row.identity_id,
      type: IdentityType.Hfest,
      createdAt: row.created_at,
      updatedAt: row.updated_at,
      userId: row.user_id,
      server: dbEnumToHfestServer(row.server),
      hfestId: row.hfest_id.toString(10),
      username: row.username,
      email: row.email !== null ? row.email : undefined,
      bestScore: row.best_score,
      bestLevel: row.best_level,
      gameCompleted: row.game_completed,
      items: withItems ? await getItemsByIdentityId(queryable, row.identity_id) : undefined,
    };
  }
}

async function updateUser(auth: AuthContext, queryable: Queryable, options: UpdateUserOptions): Promise<IdRef> {
  if (auth.type !== ActorType.User) {
    throw createUnauthenticatedError({action: "updateUser"});
  }

  const userId: UuidHex = options.userId !== undefined ? options.userId : auth.userId;
  const authErr: Error | undefined = testUpdateUserAuthorization(auth, userId, options);
  if (authErr !== undefined) {
    throw authErr;
  }

  if (options.email !== undefined || options.password !== undefined || options.login !== undefined) {
    throw new incident.Incident("NotImplemented", {options});
  }

  type Fields = Partial<Pick<dbSchema.User, "display_name" | "is_tester">>;
  const fields: Fields = Object.create(null);
  if (options.displayName !== undefined) {
    fields.display_name = options.displayName;
  }
  if (options.isTester !== undefined) {
    fields.is_tester = options.isTester;
  }

  if (Object.keys(fields).length > 0) {
    const query: squel.ParamString = PG_SQUEL.update()
      .table("users")
      .setFields(fields)
      .set("updated_at", PG_SQUEL.str("NOW()"))
      .where("users.user_id = ?::UUID", userId)
      .toParam();
    await queryable.countOne(query.text, query.values);
  }

  return {id: userId};
}

function testUpdateUserAuthorization(
  auth: UserAuthContext,
  userId: UuidHex,
  options: UpdateUserOptions,
): Error | undefined {
  if (userId !== auth.userId && !auth.isAdministrator) {
    return createForbiddenError({
      action: "updateUser",
      message: "Non-administrators cannot update other users",
    });
  }
  if (options.isTester !== undefined && !auth.isAdministrator) {
    return createForbiddenError({
      action: "updateUser",
      message: "Non-administrators cannot set the `isTester` value",
    });
  }
  if (options.password !== undefined && userId !== auth.userId) {
    return createForbiddenError({
      action: "updateUser",
      message: "Only the user itself can set its password",
    });
  }

  return undefined;
}

export interface CreateUserWithCredentialParameters {
  readonly passwordService: PasswordService;
}

/**
 * Create a new user with the provided display name.
 *
 * Does not check the validity of data (display name)
 * Does not deal with transaction: you have to rollback yourself in case of error.
 *
 * @param queryable The database client to use
 * @param params Additional parameters
 * @param options The options for the new user
 */
export async function createUserWithCredentials(
  queryable: Queryable,
  params: CreateUserWithCredentialParameters,
  options: CreateUserWithCredentialsOptions,
): Promise<User> {
  const userId: UuidHex = uuidjs.genV4().toString();
  const email: Email | null = options.email !== undefined ? options.email : null;
  const passwordBuffer: Uint8Array = Buffer.from(options.password);
  const passwordHash: Buffer = asBuffer(await params.passwordService.hash(passwordBuffer));

  type Row = Pick<dbSchema.User, "created_at" | "updated_at" | "is_administrator" | "is_tester">;
  const row: Row = await queryable.one(
    `WITH administrator_exists AS (SELECT 1 FROM users WHERE is_administrator)
    INSERT INTO users (
      user_id, created_at, updated_at, display_name,
      is_administrator,
      is_tester,
      email, username, password_hash
    )
    VALUES (
      $1::UUID, NOW(), NOW(), $2::VARCHAR,
      (NOT EXISTS (SELECT 1 FROM administrator_exists)),
      (NOT EXISTS (SELECT 1 FROM administrator_exists)),
      $3::VARCHAR, $4::VARCHAR, $5::BYTEA
    )
    RETURNING created_at, updated_at, is_administrator, is_tester;`,
    [userId, options.displayName, email, options.login, passwordHash],
  );

  return {
    id: userId,
    type: ActorType.User,
    displayName: options.displayName,
    createdAt: row.created_at,
    updatedAt: row.updated_at,
    identities: [],
    isAdministrator: row.is_administrator,
    isTester: row.is_tester,
    hasPassword: true,
  };
}

function asBuffer(buffer: Uint8Array): Buffer {
  return buffer instanceof Buffer ? buffer : Buffer.from(buffer);
}

export async function createUserWithHfestIdentity(
  queryable: Queryable,
  auth: AuthContext,
  hfestIdentity: HfestIdentity,
): Promise<User> {
  if (auth.type !== ActorType.System) {
    throw new incident.Incident("Unhautorized");
  }

  const userId: UuidHex = uuidjs.genV4().toString();

  type Row = Pick<dbSchema.User, "created_at" | "updated_at" | "is_administrator" | "is_tester">;
  const row: Row = await queryable.one(
    `WITH administrator_exists AS (SELECT 1 FROM users WHERE is_administrator)
    INSERT INTO users (
      user_id, created_at, updated_at, display_name,
      is_administrator,
      is_tester,
      email, username, password_hash
    )
    VALUES (
      $1::UUID, NOW(), NOW(), $2::VARCHAR,
      (NOT EXISTS (SELECT 1 FROM administrator_exists)),
      (NOT EXISTS (SELECT 1 FROM administrator_exists)),
      NULL, NULL, NULL
    )
    RETURNING created_at, updated_at, is_administrator, is_tester;`,
    [userId, hfestIdentity.username],
  );

  await queryable.countOne(
    `UPDATE identities
    SET
      user_id = $2::UUID
    WHERE
      identity_id = $1::UUID AND user_id IS NULL;`,
    [hfestIdentity.id, userId],
  );

  const updatedHfestIdentity: HfestIdentity = {
    ...hfestIdentity,
    userId,
  };

  return {
    id: userId,
    type: ActorType.User,
    displayName: hfestIdentity.username,
    createdAt: row.created_at,
    updatedAt: row.updated_at,
    identities: [updatedHfestIdentity],
    isAdministrator: row.is_administrator,
    isTester: row.is_tester,
    hasPassword: false,
  };
}

export async function getOrCreateUserWithEtwin(
  queryable: Queryable,
  auth: AuthContext,
  userId: UserId,
  displayName: UserDisplayName,
): Promise<User> {
  if (auth.type !== ActorType.System) {
    throw new incident.Incident("Unhautorized");
  }

  type Row = Pick<dbSchema.User, "created_at" | "updated_at" | "is_administrator" | "is_tester">;
  const row: Row = await queryable.one(
    `WITH administrator_exists AS (SELECT 1 FROM users WHERE is_administrator)
    INSERT INTO users (
      user_id, created_at, updated_at, display_name,
      is_administrator,
      is_tester,
      email, username, password_hash
    )
    VALUES (
      $1::UUID, NOW(), NOW(), $2::VARCHAR,
      (NOT EXISTS (SELECT 1 FROM administrator_exists)),
      (NOT EXISTS (SELECT 1 FROM administrator_exists)),
      NULL, NULL, NULL
    )
    ON CONFLICT (user_id)
      DO UPDATE SET
      display_name = $2::VARCHAR
    RETURNING created_at, updated_at, is_administrator, is_tester;`,
    [userId, displayName],
  );

  return {
    id: userId,
    type: ActorType.User,
    displayName,
    createdAt: row.created_at,
    updatedAt: row.updated_at,
    identities: [],
    isAdministrator: row.is_administrator,
    isTester: row.is_tester,
    hasPassword: false,
  };
}
