import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { createForbiddenError } from "@eternalfest/api-core/lib/errors/forbidden.js";
import { createUnauthenticatedError } from "@eternalfest/api-core/lib/errors/unauthenticated.js";
import { Game } from "@eternalfest/api-core/lib/game/game.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { HfestItemId } from "@eternalfest/api-core/lib/hfest-api/hfest-item-id.js";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { $RunItems, RunItems } from "@eternalfest/api-core/lib/run/run-items.js";
import { RunResult } from "@eternalfest/api-core/lib/run/run-result.js";
import { RunStart } from "@eternalfest/api-core/lib/run/run-start.js";
import { Run } from "@eternalfest/api-core/lib/run/run.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { SetRunResultOptions } from "@eternalfest/api-core/lib/run/set-run-result-options.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { UserDisplayName } from "@eternalfest/api-core/lib/user/user-display-name.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import incident from "incident";
import { JsonValueReader } from "kryo-json/lib/json-value-reader.js";
import { JsonValueWriter } from "kryo-json/lib/json-value-writer.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import uuidjs from "uuidjs";

import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionMode } from "../db/transaction.js";

const JSON_VALUE_READER: JsonValueReader = new JsonValueReader();
const JSON_VALUE_WRITER: JsonValueWriter = new JsonValueWriter();

export class PgRunService implements RunService {
  private readonly db: Database;
  private readonly game: GameService;
  private readonly user: UserService;

  constructor(db: Database, game: GameService, user: UserService) {
    this.db = db;
    this.game = game;
    this.user = user;
  }

  async createRun(auth: AuthContext, options: CreateRunOptions): Promise<Run> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createRun(auth, txCtx, this.game, this.user, options),
    );
  }

  async getRunById(auth: AuthContext, runId: UuidHex): Promise<Run> {
    return this.db.transaction(
      TransactionMode.ReadOnly,
      async txCtx => getRunById(auth, txCtx, this.game, this.user, runId),
    );
  }

  async startRun(auth: AuthContext, runId: UuidHex): Promise<RunStart> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => startRun(auth, txCtx, runId),
    );
  }

  async setRunResult(auth: AuthContext, runId: UuidHex, result: SetRunResultOptions): Promise<Run> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => setRunResult(auth, txCtx, this.game, this.user, runId, result),
    );
  }
}

async function createRun(
  auth: AuthContext,
  queryable: Queryable,
  gameService: GameService,
  userService: UserService,
  options: CreateRunOptions,
): Promise<Run> {
  if (auth.type !== ActorType.User || options.userId !== auth.userId) {
    throw createUnauthenticatedError({action: "createRun"});
  }

  // Check authorization (throw if invalid) and if game exists
  const game: Game | undefined = await gameService.getGameById(auth, options.gameId);
  if (game === undefined) {
    throw new incident.Incident("GameNotFound", {gameId: options.gameId});
  }

  const runId: UuidHex = uuidjs.genV4().toString();
  const gameOptions: string[] = [...options.gameOptions];

  type Row = Pick<dbSchema.Run, "created_at">;
  const row: Row = await queryable.one(
    `INSERT INTO runs(
      run_id, game_id, user_id,
      created_at, started_at,
      game_mode, game_options,
      detail, music, shake,
      sound, volume
    )
    VALUES (
      $1::UUID, $2::UUID, $3::UUID,
      NOW(), NULL,
      $4::VARCHAR, $5::JSON,
      $6::BOOLEAN, $7::BOOLEAN, $8::BOOLEAN,
      $9::BOOLEAN, $10::INT
    )
    RETURNING created_at;`,
    [
      runId, options.gameId, options.userId,
      options.gameMode, JSON.stringify(gameOptions),
      options.settings.detail, options.settings.music, options.settings.shake,
      options.settings.sound, options.settings.volume,
    ],
  );

  let userDisplayName: UserDisplayName;
  if (options.userId === auth.userId) {
    userDisplayName = auth.displayName;
  } else {
    const user: User | undefined = await userService.getUserById(auth, options.userId);
    if (user === undefined) {
      throw new incident.Incident("UserLinkedToRunNotFound", {userId: options.userId, runId});
    }
    userDisplayName = user.displayName;
  }

  return {
    id: runId,
    game: {id: game.id, displayName: game.displayName},
    user: {id: options.userId, type: ActorType.User, displayName: userDisplayName},
    createdAt: row.created_at,
    startedAt: null,
    result: null,
    gameMode: options.gameMode,
    gameOptions,
    settings: {
      detail: options.settings.detail,
      music: options.settings.music,
      shake: options.settings.shake,
      sound: options.settings.sound,
      volume: options.settings.volume,
    },
  };
}

/**
 * Make all properties in T nullable
 */
type NullableProperties<T> = {
  [P in keyof T]: T[P] | null;
};

async function getRunById(
  auth: AuthContext,
  queryable: Queryable,
  gameService: GameService,
  userService: UserService,
  runId: UuidHex,
): Promise<Run> {
  type Row = Pick<dbSchema.Run,
    "run_id" | "game_id" | "user_id"
    | "created_at" | "started_at"
    | "game_mode" | "game_options"
    | "detail" | "music" | "shake"
    | "sound" | "volume">
    & NullableProperties<Pick<dbSchema.RunResult, "is_victory" | "max_level" | "scores" | "items" | "stats">
    & { run_result_created_at: Date | null }>;
  const row: Row | undefined = await queryable.oneOrNone(
    `SELECT
      run_id, game_id, user_id,
      runs.created_at, started_at,
      game_mode, game_options,
      detail, music, shake,
      sound, volume,
      is_victory, max_level, scores, items, stats,
      run_results.created_at AS run_result_created_at
    FROM runs LEFT OUTER JOIN run_results USING (run_id)
    WHERE run_id = $1::UUID;`,
    [runId],
  );
  if (row === undefined) {
    throw new incident.Incident("RunNotFound", {runId});
  }

  // Check authorization (throw if invalid)
  const game: Game | undefined = await gameService.getGameById(auth, row.game_id);
  if (game === undefined) {
    throw new incident.Incident("GameLinkedToRunNotFound", {gameId: row.game_id, runId: row.run_id});
  }

  let userDisplayName: UserDisplayName;
  if (auth.type === ActorType.User && row.user_id === auth.userId) {
    userDisplayName = auth.displayName;
  } else {
    const user: User | undefined = await userService.getUserById(auth, row.user_id);
    if (user === undefined) {
      throw new incident.Incident("UserLinkedToRunNotFound", {userId: row.user_id, runId});
    }
    userDisplayName = user.displayName;
  }

  let result: RunResult | null = null;
  if (
    row.run_result_created_at !== null
    && row.is_victory !== null
    && row.max_level !== null
    && row.scores !== null
    && row.items !== null
  ) {
    result = {
      createdAt: row.run_result_created_at,
      isVictory: row.is_victory,
      maxLevel: row.max_level,
      scores: row.scores,
      items: $RunItems.read(JSON_VALUE_READER, row.items),
      stats: row.stats,
    };
  }

  return {
    id: runId,
    game: {id: game.id, displayName: game.displayName},
    user: {id: row.user_id, type: ActorType.User, displayName: userDisplayName},
    createdAt: row.created_at,
    startedAt: row.started_at,
    result,
    gameMode: row.game_mode,
    gameOptions: [...row.game_options],
    settings: {
      detail: row.detail,
      music: row.music,
      shake: row.shake,
      sound: row.sound,
      volume: row.volume,
    },
  };
}

async function startRun(
  auth: AuthContext,
  queryable: Queryable,
  runId: UuidHex,
): Promise<RunStart> {
  if (auth.type !== ActorType.User) {
    throw createUnauthenticatedError({action: "startRun"});
  }
  type Row = Pick<dbSchema.Run, "started_at" | "user_id"> & Pick<dbSchema.Game, "game_id" | "families">;
  const row: Row | undefined = await queryable.oneOrNone(
    `SELECT user_id, game_id, families, started_at
    FROM games INNER JOIN runs USING (game_id)
    WHERE run_id = $1::UUID;`,
    [runId],
  );
  if (row === undefined) {
    throw new incident.Incident("RunNotFound", {runId});
  }
  if (row.user_id !== auth.userId || row.started_at !== null) {
    throw createForbiddenError({action: "startRun"});
  }

  const items: RunItems = await getUserItems(queryable, row.user_id, row.game_id);

  await queryable.countOne(
    `UPDATE runs
    SET
      started_at = NOW()
    WHERE run_id = $1::UUID AND started_at IS NULL;`,
    [runId],
  );

  return {
    run: {id: runId},
    key: "00000000-0000-0000-0000-000000000000",
    families: row.families,
    items,
  };
}

export async function getUserItems(
  queryable: Queryable,
  userId: UuidHex,
  gameId: UuidHex,
): Promise<RunItems> {
  const items: Map<HfestItemId, number> = new Map();
  type Row = Pick<dbSchema.UserItem, "item_id" | "item_count">;
  const rows: AsyncIterable<Row> = queryable.many(
    `SELECT item_id, SUM(run_items.item_count)::INT AS item_count
    FROM (
      SELECT
        run_id, item.key::TEXT AS item_id, item.value::TEXT::INT AS item_count
      FROM (
        SELECT run_id, items
        FROM run_results
          INNER JOIN runs USING (run_id)
        WHERE user_id = $1::UUID AND game_id = $2::UUID
      ) AS run_infos,
        LATERAL json_each(run_infos.items) AS item
    ) AS run_items
    GROUP BY item_id
    ORDER BY item_id;`,
    [userId, gameId],
  );
  for await (const row of rows) {
    items.set(row.item_id, row.item_count);
  }
  return items;
}

async function setRunResult(
  auth: AuthContext,
  queryable: Queryable,
  gameService: GameService,
  userService: UserService,
  runId: UuidHex,
  result: SetRunResultOptions,
): Promise<Run> {
  if (auth.type !== ActorType.User) {
    // TODO: Check that the user id matches
    throw createUnauthenticatedError({action: "setRunResult"});
  }
  // type Row = Pick<DbRunResult, "created_at">;
  await queryable.one(
    `INSERT INTO run_results (
      run_id, created_at, is_victory, max_level, scores, items, stats
    )
    VALUES (
      $1::UUID, NOW(), $2::BOOLEAN, $3::INT, $4::JSON, $5::JSON, $6::JSON
    )
    RETURNING created_at;`,
    [
      runId,
      result.isVictory,
      result.maxLevel,
      JSON.stringify(result.scores),
      JSON.stringify($RunItems.write(JSON_VALUE_WRITER, result.items as Map<HfestItemId, number>)),
      JSON.stringify(result.stats),
    ],
  );

  return getRunById(auth, queryable, gameService, userService, runId);
}
