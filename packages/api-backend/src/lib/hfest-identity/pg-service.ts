import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { createUnauthenticatedError } from "@eternalfest/api-core/lib/errors/unauthenticated.js";
import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity.js";
import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { LinkUserOptions } from "@eternalfest/api-core/lib/hfest-identity/link-user-options.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { UnlinkUserOptions } from "@eternalfest/api-core/lib/hfest-identity/unlink-user-options.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { IdentityType } from "@eternalfest/api-core/lib/types/user/identity-type.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import hfApiTypes from "@eternalfest/hammerfest-api/types/api.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import pg from "pg";

// import uuidjs from "uuidjs";
import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionMode } from "../db/transaction.js";
import { dbEnumFromHfestServer, dbEnumFromIdentityType } from "../utils/db-enum.js";
import { hfestServerFromUrl } from "../utils/hfest-server-url.js";

export class PgHfestIdentityService implements HfestIdentityService {
  private readonly db: Database;
  private readonly hfestApi: hfApiTypes.HammerfestApi;

  constructor(db: Database, hfestApi: hfApiTypes.HammerfestApi) {
    this.db = db;
    this.hfestApi = hfestApi;
  }

  async createOrUpdateHfestIdentity(hfestSessionString: string): Promise<HfestIdentity> {
    const hfestSession: hfApiTypes.Session = this.hfestApi.importSession(hfestSessionString);
    if (!(await this.hfestApi.isSessionActive(hfestSession))) {
      throw new Error("InvalidSession");
    }

    const server: HfestServer = hfestServerFromUrl(hfestSession.server);

    const profile: hfApiTypes.Profile = await this.hfestApi.getPublicProfile(hfestSession);
    const items: Map<number, number> = await this.hfestApi.getItems(hfestSession);

    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createOrUpdateHfestIdentity(txCtx, server, profile, items),
    );
  }

  async linkUser(auth: AuthContext, linkUserOptions: LinkUserOptions): Promise<void> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => linkUser(auth, txCtx, linkUserOptions),
    );
  }

  async unlinkUser(_auth: AuthContext, _unlinkUserOptions: UnlinkUserOptions): Promise<void> {
    throw new Error("NotImplemented");
  }
}

async function createOrUpdateHfestIdentity(
  queryable: Queryable,
  hfestServer: HfestServer,
  profile: hfApiTypes.Profile,
  items: Map<number, number>,
): Promise<HfestIdentity> {
  // AuthMigration: Allow user creation (currently only updates are supported)
  type HfestRow = Pick<dbSchema.HfestIdentity, "identity_id">;
  const hfestRow: HfestRow = await queryable.one(
    `UPDATE hfest_identities
      SET
        username = $3::VARCHAR,
        email = $4::VARCHAR,
        best_score = $5::INT,
        best_level = $6::INT,
        game_completed = $7::BOOLEAN
      WHERE server = $1::HFEST_SERVER AND hfest_id = $2::INT
      RETURNING identity_id;`,
    [
      dbEnumFromHfestServer(hfestServer),
      profile.id,
      profile.username,
      profile.email,
      profile.bestScore,
      profile.bestLevel,
      profile.carrotRecovered,
    ],
  );

  const identityId: UuidHex = hfestRow.identity_id;

  type IdentityRow = Pick<dbSchema.Identity, "created_at" | "updated_at" | "user_id">;
  const identityRow: IdentityRow = await queryable.one(
    `INSERT INTO identities(
        identity_id, type, created_at, updated_at, user_id
      )
      VALUES (
        $1::UUID, $2::IDENTITY_TYPE, NOW(), NOW(), NULL
      )
      ON CONFLICT (identity_id)
      DO UPDATE SET
        updated_at = NOW()
      RETURNING created_at, updated_at, user_id;`,
    [
      identityId,
      dbEnumFromIdentityType(IdentityType.Hfest),
    ],
  );

  await createOrUpdateInventory(queryable, identityId, items);

  return {
    id: identityId,
    type: IdentityType.Hfest,
    createdAt: identityRow.created_at,
    updatedAt: identityRow.updated_at,
    bestLevel: profile.bestLevel,
    bestScore: profile.bestScore,
    email: profile.email !== null ? profile.email : undefined,
    gameCompleted: profile.carrotRecovered,
    hfestId: profile.id,
    server: hfestServer,
    items: undefined,
    userId: identityRow.user_id,
    username: profile.username,
  };
}

/**
 * Create or update inventory for an identity
 *
 * Does not check the validity of data (dindentity_id, inventory)
 * Does not deal with transaction: you have to rollback yourself in case of error.
 *
 * @param queryable The database client to use
 * @param indentityId Id of the specific user account
 * @param inventory map of (itemId, itemCount)
 */
async function createOrUpdateInventory(
  queryable: Queryable,
  indentityId: UuidHex,
  inventory: Map<number, number>,
): Promise<void> {
  for (const [itemId, itemCount] of Array.from(inventory.entries())) {
    await queryable.query(
      `INSERT INTO inventories(hfest_item_id, identity_id, count, max_count)
      VALUES ($1::int, $2::uuid, $3::int, $3::int)
      ON CONFLICT (hfest_item_id, identity_id)
      DO UPDATE SET
        count = $3::int,
        max_count = GREATEST(inventories.max_count, $3::int);`,
      [itemId, indentityId, itemCount],
    );
  }
}

async function linkUser(
  auth: AuthContext,
  queryable: Queryable,
  options: LinkUserOptions,
) {
  // The function first adds the identity and then checks for server uniqueness
  // to avoid invalid state caused by data races.

  if (auth.type !== ActorType.User) {
    throw createUnauthenticatedError({action: "linkUser"});
  }

  const identityId: UuidHex = options.identityId;
  const userId: UuidHex | undefined = options.userId !== undefined ? options.userId : auth.userId;

  const result: pg.QueryResult = await queryable.query(
    `UPDATE identities
    SET
      user_id = $2::UUID
    WHERE
      identity_id = $1::UUID AND user_id IS NULL;`,
    [identityId, userId],
  );
  if (result.rowCount !== 1) {
    throw new incident.Incident("IndentityNotFoundOrAlreadyLinked", {identityId, userId});
  }
  await checkUniqueServerForLinkedHfestIdentities(queryable, userId);
  throw new Error("AuthMigration: User linkage is disabled.");
}

async function checkUniqueServerForLinkedHfestIdentities(queryable: Queryable, userId: string): Promise<void> {
  interface Row {
    max_identity_count_per_server: number;
  }

  const row: Row = await queryable.one(
    `WITH
    identities_per_server AS (
      SELECT COUNT(*) as identity_count, server
      FROM identities
             INNER JOIN hfest_identities USING (identity_id)
      WHERE user_id = $1::UUID
      GROUP BY server
    )
  SELECT COALESCE(MAX(identity_count), 0) as max_identity_count_per_server
  FROM identities_per_server;`,
    [userId],
  );

  if (row.max_identity_count_per_server > 1) {
    throw new incident.Incident("MultipleIdentitiesForTheSameServer", {maxCount: row.max_identity_count_per_server});
  }
}
