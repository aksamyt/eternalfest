import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { Game } from "@eternalfest/api-core/lib/game/game.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { LeaderboardEntry } from "@eternalfest/api-core/lib/leaderboard/leaderboard-entry.js";
import { Leaderboard } from "@eternalfest/api-core/lib/leaderboard/leaderboard.js";
import { LeaderboardService } from "@eternalfest/api-core/lib/leaderboard/service.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";

import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionMode } from "../db/transaction.js";

export class PgLeaderboardService implements LeaderboardService {
  private readonly db: Database;
  private readonly game: GameService;

  constructor(db: Database, game: GameService) {
    this.db = db;
    this.game = game;
  }

  async getLeaderboard(auth: AuthContext, gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard> {
    return this.db.transaction(
      TransactionMode.ReadOnly,
      async txCtx => getLeaderboardById(auth, txCtx, this.game, gameId, gameMode),
    );
  }
}

async function getLeaderboardById(
  auth: AuthContext,
  queryable: Queryable,
  gameService: GameService,
  gameId: UuidHex,
  gameMode: GameModeKey
): Promise<Leaderboard> {
  const game: Game | undefined = await gameService.getGameById(auth, gameId);
  if (game === undefined) {
    throw new incident.Incident("GameNotFound", {gameId: gameId});
  }

  type Row = { best_score: string, user_id: string, run_id: string }
    & Pick<dbSchema.User, "user_id" | "display_name">
    & Pick<dbSchema.RunResult, "max_level">
    & Pick<dbSchema.Run, "game_options">;
  const rows: AsyncIterable<Row> = queryable.many(
    `WITH
      scores AS (
        SELECT json_array_elements(scores)::TEXT::INT as score, run_id, game_id, user_id, game_mode, run_results.created_at
        FROM run_results INNER JOIN runs USING (run_id) INNER JOIN games USING (game_id)
        WHERE game_id = $1::UUID AND game_mode = $2::VARCHAR(100) AND (games.public_access_from IS NULL OR runs.created_at >= games.public_access_from)
      ),
      total_scores AS (
        SELECT SUM(score) as total_score, run_id, user_id, created_at
        FROM scores
        GROUP BY (run_id, user_id, created_at)
      ),
      user_best_runs AS (
        SELECT max(total_score) as best_score, user_id
        FROM total_scores
        GROUP BY (user_id)
      ),
      dated_user_best_runs AS (
        SELECT best_score, user_best_runs.user_id, total_scores.created_at
        FROM user_best_runs
        INNER JOIN total_scores ON (user_best_runs.user_id = total_scores.user_id AND user_best_runs.best_score = total_scores.total_score)
      ),
      oldest_best_runs AS (
        SELECT min(created_at) as oldest_best_date, best_score, user_id
        FROM dated_user_best_runs
        GROUP BY (best_score, user_id)
      ),
      oldest_best_runs_with_id AS (
        SELECT best_score, oldest_best_runs.user_id, oldest_best_date, runs.run_id
        FROM oldest_best_runs
        INNER JOIN total_scores ON (oldest_best_runs.user_id = total_scores.user_id AND oldest_best_runs.best_score = total_scores.total_score)
        INNER JOIN runs ON (total_scores.run_id = runs.run_id AND total_scores.run_id = runs.run_id)
        INNER JOIN run_results ON (total_scores.run_id = run_results.run_id AND oldest_best_date = run_results.created_at)
      ),
      unique_best_run AS (
        SELECT CAST(max(CAST(run_id AS VARCHAR(36))) AS UUID) as run_id, best_score, user_id
        FROM oldest_best_runs_with_id
        GROUP BY (best_score, user_id)
      )
      SELECT best_score, unique_best_run.user_id, unique_best_run.run_id, display_name, max_level, game_options
      FROM unique_best_run
      INNER JOIN users ON (unique_best_run.user_id = users.user_id)
      INNER JOIN run_results ON (unique_best_run.run_id = run_results.run_id)
      INNER JOIN runs ON (unique_best_run.run_id = runs.run_id)
      ORDER BY best_score DESC`,
    [gameId, gameMode],
  );

  const results: LeaderboardEntry[] = [];
  for await (const row of rows) {
    results.push({
      score: parseFloat(row.best_score),
      user: {type: ActorType.User, id: row.user_id, displayName: row.display_name},
      run: {id: row.run_id, maxLevel: row.max_level, gameOptions: row.game_options},
    });
  }

  return {game: game, gameMode: gameMode, results: results};
}
