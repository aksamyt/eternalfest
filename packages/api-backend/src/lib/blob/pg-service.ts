import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { CreateBlobOptions } from "@eternalfest/api-core/lib/blob/create-blob-options.js";
import { CreateUploadSessionOptions } from "@eternalfest/api-core/lib/blob/create-upload-session-options.js";
import { MediaType } from "@eternalfest/api-core/lib/blob/media-type.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { UploadBytesOptions } from "@eternalfest/api-core/lib/blob/upload-bytes-options.js";
import { UploadSession } from "@eternalfest/api-core/lib/blob/upload-session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import bufferService from "@eternalfest/buffer-service/buffer-service.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import stream from "stream";
import uuidjs from "uuidjs";

import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionMode } from "../db/transaction.js";

export class PgBlobService implements BlobService {
  private readonly db: Database;
  private readonly bufferService: bufferService.BufferService;
  private readonly maxBlobSize: number;
  /**
   * In ms
   */

  private readonly uploadSessionMaxAge: number;

  /**
   *
   * @param db Database access object.
   * @param bufferService Buffer service used to store the blobs.
   */
  constructor(db: Database, bufferService: bufferService.BufferService) {
    this.db = db;
    this.bufferService = bufferService;
    this.maxBlobSize = 100 * 1024 * 1024;
    this.uploadSessionMaxAge = 24 * 3600 * 1000;
  }

  async createBlob(_auth: AuthContext, options: CreateBlobOptions): Promise<Blob> {
    if (options.data.length > this.maxBlobSize) {
      throw new Error("MaxSizeExceeded");
    }

    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createBlobFromBuffer(txCtx, this.bufferService, options.data, options.mediaType),
    );
  }

  async getBlobById(_auth: AuthContext, blobId: UuidHex): Promise<Blob> {
    return this.db.transaction(
      TransactionMode.ReadOnly,
      async txCtx => getBlobById(txCtx, blobId),
    );
  }

  async readBlobData(_auth: AuthContext, blobId: UuidHex): Promise<stream.Readable> {
    return this.db.transaction(
      TransactionMode.ReadOnly,
      async txCtx => readBlobData(txCtx, this.bufferService, blobId),
    );
  }

  async createUploadSession(_auth: AuthContext, options: CreateUploadSessionOptions): Promise<UploadSession> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createUploadSession(txCtx, this.bufferService, this.uploadSessionMaxAge, options),
    );
  }

  async uploadBytes(_auth: AuthContext, uploadSessionId: UuidHex, options: UploadBytesOptions): Promise<UploadSession> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => uploadBytes(txCtx, this.bufferService, this.uploadSessionMaxAge, uploadSessionId, options),
    );
  }

  async markUnusedBlobsForDeletion(auth: AuthContext): Promise<void> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => markUnusedBlobsForDeletion(auth, txCtx),
    );
  }

  async execBlobDeletions(auth: AuthContext): Promise<void> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => execBlobDeletions(auth, txCtx, this.bufferService),
    );
  }
}

/**
 * @internal
 */
async function createUploadSession(
  queryable: Queryable,
  bufferService: bufferService.BufferService,
  maxAge: number,
  options: CreateUploadSessionOptions,
): Promise<UploadSession> {
  type Row = Pick<dbSchema.UploadSession, "created_at">;

  const uploadSessionId: UuidHex = uuidjs.genV4().toString();

  const bufferId: string = await bufferService.createBuffer(options.byteSize);

  const row: Row = await queryable.one(
    `INSERT INTO upload_sessions(
      upload_session_id, media_type, byte_size, written_bytes, created_at, updated_at, buffer_id, blob_id
    )
    VALUES ($1::UUID, $2::VARCHAR(100), $3::INT4, 0, NOW(), NOW(), $4::VARCHAR, null)
    RETURNING created_at;`,
    [uploadSessionId, options.mediaType, options.byteSize, bufferId],
  );

  return {
    id: uploadSessionId,
    expiresAt: new Date(row.created_at.getTime() + maxAge),
    remainingRange: {start: 0, end: options.byteSize},
    blob: undefined,
  };
}

async function getBlobBufferId(
  queryable: Queryable,
  blobId: UuidHex,
): Promise<string> {
  type Row = Pick<dbSchema.Blob, "buffer_id">;

  const row: Row = await queryable.one(
    `SELECT buffer_id
    FROM blobs
    WHERE blobs.blob_id = $1::UUID;`,
    [blobId],
  );

  return row.buffer_id;
}

async function getBlobById(
  queryable: Queryable,
  blobId: UuidHex,
): Promise<Blob> {
  type Row = Pick<dbSchema.Blob, "byte_size" | "media_type">;

  const row: Row = await queryable.one(
    `SELECT byte_size, media_type
    FROM blobs
    WHERE blobs.blob_id = $1::UUID;`,
    [blobId],
  );

  return {
    id: blobId,
    mediaType: row.media_type,
    byteSize: row.byte_size,
  };
}

async function readBlobData(
  queryable: Queryable,
  bufferService: bufferService.BufferService,
  blobId: UuidHex,
): Promise<stream.Readable> {
  const bufferId: string = await getBlobBufferId(queryable, blobId);
  return bufferService.readStream(bufferId);
}

/**
 * @internal
 */
async function uploadBytes(
  queryable: Queryable,
  bufferService: bufferService.BufferService,
  maxAge: number,
  uploadSessionId: UuidHex,
  options: UploadBytesOptions,
): Promise<UploadSession> {
  type Row = Pick<dbSchema.UploadSession, "byte_size" | "written_bytes" | "created_at" | "media_type" | "buffer_id">;

  const row: Row = await queryable.one(
    `SELECT byte_size, written_bytes, created_at, media_type, buffer_id
    FROM upload_sessions
    WHERE upload_session_id = $1::UUID;`,
    [uploadSessionId],
  );

  if (Date.now() >= row.created_at.getTime() + maxAge) {
    throw new incident.Incident("ExpiredUploadSession");
  }

  if (options.offset !== row.written_bytes) {
    throw new incident.Incident("InvalidOffset", `Expected offset to be ${options.offset}`);
  }

  const bytesAfterWrite: number = row.written_bytes + options.data.length;

  // TODO: Support empty buffers
  if (bytesAfterWrite === row.written_bytes) {
    throw new incident.Incident("EmptyBuffer", "The buffer must be non-empty");
  }

  if (bytesAfterWrite > row.byte_size) {
    throw new incident.Incident("DataOverflow", "The provided offset and range exceeds the anounced total size.");
  }

  await bufferService.writeBytes(row.buffer_id, options.data, options.offset);

  let blob: Blob | undefined = undefined;
  if (bytesAfterWrite === row.byte_size) {
    blob = await createBlob(queryable, row.buffer_id, row.byte_size, row.media_type);
  }

  await queryable.countOne(
    `UPDATE upload_sessions
    SET written_bytes = $3::INT4, updated_at = NOW(), blob_id = $4::UUID
    WHERE upload_session_id = $1::UUID AND written_bytes = $2::INT4 AND blob_id IS NULL;`,
    [uploadSessionId, row.written_bytes, bytesAfterWrite, blob !== undefined ? blob.id : null],
  );

  return {
    id: uploadSessionId,
    expiresAt: new Date(row.created_at.getTime() + maxAge),
    remainingRange: {start: bytesAfterWrite, end: row.byte_size},
    blob,
  };
}

async function createBlobFromBuffer(
  queryable: Queryable,
  bufferService: bufferService.BufferService,
  buffer: Uint8Array,
  mediaType: MediaType,
): Promise<Blob> {
  const byteSize: number = buffer.length;
  const bufferId: string = await bufferService.createBuffer(byteSize);
  await bufferService.writeBytes(bufferId, buffer, 0);
  return createBlob(queryable, bufferId, byteSize, mediaType);
}

async function createBlob(
  queryable: Queryable,
  bufferId: string,
  byteSize: number,
  mediaType: MediaType,
): Promise<Blob> {
  const blobId: UuidHex = uuidjs.genV4().toString();

  await queryable.countOne(
    `INSERT INTO blobs (blob_id, media_type, byte_size, created_at, updated_at, buffer_id)
    VALUES ($1::UUID, $2::VARCHAR, $3::INT4, NOW(), NOW(), $4::VARCHAR);`,
    [blobId, mediaType, byteSize, bufferId],
  );

  return {
    id: blobId,
    mediaType,
    byteSize,
  };
}

async function markUnusedBlobsForDeletion(auth: AuthContext, queryable: Queryable): Promise<void> {
  if (auth.type !== ActorType.System) {
    throw new incident.Incident("Unauthorized");
  }

  // `ON CONFLICT` makes the insertion idempotent (should not be needed in practice)
  await queryable.query(
    `WITH unused_blobs AS (
      SELECT blob_id, media_type, byte_size, buffer_id, created_at, updated_at
      FROM blobs
      WHERE NOT EXISTS (
        SELECT 1 FROM files WHERE files.blob_id = blobs.blob_id
      )
    )
    INSERT INTO blob_deletion_queue(blob_id, media_type, byte_size, buffer_id, created_at, updated_at, deleted_at)
    SELECT blob_id, media_type, byte_size, buffer_id, created_at, updated_at, NOW() AS deleted_at FROM unused_blobs
    ON CONFLICT (blob_id) DO NOTHING;`,
    [],
  );

  await queryable.query(
    `DELETE FROM blobs
    WHERE blob_id IN (SELECT blob_id FROM blob_deletion_queue);`,
    [],
  );
}

async function execBlobDeletions(
  auth: AuthContext,
  queryable: Queryable,
  bufferService: bufferService.BufferService,
): Promise<void> {
  if (auth.type !== ActorType.System) {
    throw new incident.Incident("Unauthorized");
  }

  for await (const untypedRow of queryable.many("SELECT buffer_id FROM blob_deletion_queue;", [])) {
    type Row = Pick<dbSchema.Blob, "buffer_id">;
    const row: Row = untypedRow;
    await bufferService.deleteBufferIfExists(row.buffer_id);
  }

  await queryable.query("DELETE FROM blob_deletion_queue;", []);
}
