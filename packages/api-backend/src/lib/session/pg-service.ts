import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import uuidjs from "uuidjs";

import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionMode } from "../db/transaction.js";

export class PgSessionService implements SessionService {
  private readonly db: Database;
  private readonly sessionMaxAge: number;

  constructor(db: Database, sessionMaxAge: number) {
    this.db = db;
    this.sessionMaxAge = sessionMaxAge;
    if (this.sessionMaxAge < 0) {
      // TODO: Remove this hack and really use this property.
      console.log(this.sessionMaxAge);
    }
  }

  async createSession(_auth: AuthContext, userId: UuidHex): Promise<Session> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createSession(txCtx, userId),
    );
  }

  async getSessionById(_auth: AuthContext, sessionId: UuidHex): Promise<Session | undefined> {
    return this.db.transaction(
      TransactionMode.ReadOnly,
      async txCtx => getSessionById(txCtx, sessionId),
    );
  }

  async getAndTouchSessionById(_auth: AuthContext, sessionId: UuidHex): Promise<Session | undefined> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => getAndTouchSessionById(txCtx, sessionId),
    );
  }

  async deleteSessionById(_auth: AuthContext, sessionId: UuidHex): Promise<void> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => deleteSessionById(txCtx, sessionId),
    );
  }
}

/**
 * @internal
 */
async function createSession(
  queryable: Queryable,
  userId: UuidHex,
): Promise<Session> {
  type Row = Pick<dbSchema.Session, "created_at">;

  const sessionId: UuidHex = uuidjs.genV4().toString();

  const row: Row = await queryable.one(
    `INSERT INTO sessions(
      session_id, user_id, created_at, updated_at, data
    )
    VALUES ($1::UUID, $2::UUID, NOW(), NOW(), '{}')
    RETURNING created_at;`,
    [sessionId, userId],
  );

  return {
    id: sessionId,
    user: {id: userId},
    createdAt: row.created_at,
    updatedAt: row.created_at,
  };
}

/**
 * @internal
 */
async function getSessionById(
  queryable: Queryable,
  sessionId: UuidHex,
): Promise<Session | undefined> {
  type Row = Pick<dbSchema.Session, "user_id" | "created_at" | "updated_at">;

  const row: Row | undefined = await queryable.oneOrNone(
    `SELECT user_id, created_at, updated_at
    FROM sessions
    WHERE session_id = $1::UUID;`,
    [sessionId],
  );

  if (row === undefined) {
    return undefined;
  }

  return {
    id: sessionId,
    user: {id: row.user_id},
    createdAt: row.created_at,
    updatedAt: row.updated_at,
  };
}

/**
 * @internal
 */
async function getAndTouchSessionById(
  queryable: Queryable,
  sessionId: UuidHex,
): Promise<Session | undefined> {
  type Row = Pick<dbSchema.Session, "user_id" | "created_at" | "updated_at">;

  const row: Row | undefined = await queryable.oneOrNone(
    `UPDATE sessions
    SET updated_at = NOW()
    WHERE session_id = $1::UUID
    RETURNING user_id, created_at, updated_at;`,
    [sessionId],
  );

  if (row === undefined) {
    return undefined;
  }

  return {
    id: sessionId,
    user: {id: row.user_id},
    createdAt: row.created_at,
    updatedAt: row.updated_at,
  };
}

/**
 * @internal
 */
async function deleteSessionById(
  queryable: Queryable,
  sessionId: UuidHex,
): Promise<void> {
  await queryable.oneOrNone(
    `DELETE FROM sessions
    WHERE session_id = $1::UUID;`,
    [sessionId],
  );
}
