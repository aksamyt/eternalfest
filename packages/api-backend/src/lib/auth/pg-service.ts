import { AuthContext, SystemAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { AuthService } from "@eternalfest/api-core/lib/auth/service.js";
import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity.js";
import { HfestLogin } from "@eternalfest/api-core/lib/hfest-identity/hfest-login.js";
import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { UserDisplayName } from "@eternalfest/api-core/lib/user/user-display-name";
import { UserId } from "@eternalfest/api-core/lib/user/user-id";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import hfApiTypes from "@eternalfest/hammerfest-api/types/api.js";
import incident from "incident";

import { hfestServerToUrl } from "../utils/hfest-server-url.js";

const SYSTEM_AUTH_CONTEXT: SystemAuthContext = {
  type: ActorType.System,
};

export class PgAuthService implements AuthService {
  private readonly hfestApi: hfApiTypes.HammerfestApi;
  private readonly hfestIdentityService: HfestIdentityService;
  private readonly sessionService: SessionService;
  private readonly userService: UserService;

  constructor(
    hfestApi: hfApiTypes.HammerfestApi,
    hfestIdentityService: HfestIdentityService,
    sessionService: SessionService,
    userService: UserService,
  ) {
    this.hfestApi = hfestApi;
    this.hfestIdentityService = hfestIdentityService;
    this.sessionService = sessionService;
    this.userService = userService;
  }

  async etwinOauth(userId: UserId, userDisplayName: UserDisplayName): Promise<AuthContext> {
    const user: User = await this.userService.getOrCreateUserWithEtwin(SYSTEM_AUTH_CONTEXT, userId, userDisplayName);
    return {
      type: ActorType.User,
      userId: user.id,
      displayName: user.displayName,
      isAdministrator: user.isAdministrator,
      isTester: user.isTester!,
      scope: AuthScope.Default,
    };
  }

  async session(sessionId: string): Promise<AuthContext> {
    const session: Session | undefined = await this.sessionService.getAndTouchSessionById(
      SYSTEM_AUTH_CONTEXT,
      sessionId,
    );
    if (session === undefined) {
      throw new incident.Incident("InvalidSession");
    }
    const user: User | undefined = await this.userService.getUserById(SYSTEM_AUTH_CONTEXT, session.user.id);
    if (user === undefined) {
      throw new incident.Incident("InvalidSession: AuthenticatedUserNotFound");
    }
    return {
      type: ActorType.User,
      userId: user.id,
      displayName: user.displayName,
      isAdministrator: user.isAdministrator,
      isTester: user.isTester!,
      scope: AuthScope.Default,
    };
  }

  async hfestCredentials(server: HfestServer, login: HfestLogin, password: Uint8Array): Promise<AuthContext> {
    const session: hfApiTypes.Session = await this.hfestApi.createSession({
      server: hfestServerToUrl(server),
      username: login,
      password: Buffer.from(password).toString("utf-8"),
    });
    return this.hfestSession(this.hfestApi.exportSession(session));
  }

  async hfestSession(hfestSessionString: string): Promise<AuthContext> {
    const identity: HfestIdentity = await this.hfestIdentityService.createOrUpdateHfestIdentity(hfestSessionString);
    let user: User;
    if (typeof identity.userId === "string") {
      const tmpUser: User | undefined = await this.userService.getUserById(SYSTEM_AUTH_CONTEXT, identity.userId);
      if (tmpUser === undefined) {
        throw new incident.Incident("UnableToRetrieveLinkedUser", {userId: identity.userId, identityId: identity.id});
      }
      user = tmpUser;
    } else {
      user = await this.userService.createUserWithHfestIdentity(SYSTEM_AUTH_CONTEXT, identity);
    }
    return {
      type: ActorType.User,
      userId: user.id,
      displayName: user.displayName,
      isAdministrator: user.isAdministrator,
      isTester: user.isTester!,
      scope: AuthScope.Default,
    };
  }
}
