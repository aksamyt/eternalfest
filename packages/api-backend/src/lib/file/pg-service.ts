import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { createUnauthenticatedError } from "@eternalfest/api-core/lib/errors/unauthenticated.js";
import { CreateDirectoryOptions } from "@eternalfest/api-core/lib/file/create-directory-options.js";
import { CreateFileOptions } from "@eternalfest/api-core/lib/file/create-file-options.js";
import { Directory } from "@eternalfest/api-core/lib/file/directory.js";
import { DriveItemType } from "@eternalfest/api-core/lib/file/drive-item-type.js";
import { DriveItem } from "@eternalfest/api-core/lib/file/drive-item.js";
import { Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { File } from "@eternalfest/api-core/lib/file/file.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import { Readable as ReadableStream } from "stream";
import uuidjs from "uuidjs";

import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionMode } from "../db/transaction.js";
import { createDriveNotFoundError } from "../errors/drive-not-found.js";
import { dbEnumFromDriveItemType, dbEnumToDriveItemType } from "../utils/db-enum.js";

export class PgFileService implements FileService {
  private readonly db: Database;
  private readonly blobService: BlobService;

  constructor(db: Database, blobService: BlobService) {
    this.db = db;
    this.blobService = blobService;
  }

  async getDriveById(auth: AuthContext, driveId: UuidHex): Promise<Drive> {
    return this.db.transaction(TransactionMode.ReadOnly, async txCtx => getDriveById(auth, txCtx, driveId));
  }

  async getDriveByOwnerId(auth: AuthContext, ownerId: UuidHex): Promise<Drive> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => getOrCreateDriveByOwnerId(auth, txCtx, ownerId),
    );
  }

  async getItemByPath(_auth: AuthContext, _driveId: UuidHex, _itemPath: ReadonlyArray<string>): Promise<DriveItem> {
    throw new incident.Incident("NotImplemented", "PgFileService::FileService");
  }

  async getDirectoryById(auth: AuthContext, directoryId: UuidHex): Promise<Directory> {
    return this.db.transaction(TransactionMode.ReadOnly, async txCtx => getDirectoryById(auth, txCtx, directoryId));
  }

  async getDirectoryChildrenById(_auth: AuthContext, _directoryId: UuidHex): Promise<readonly DriveItem[]> {
    throw new incident.Incident("NotImplemented", "PgFileService::getDirectoryChildrenById");
  }

  async getFileById(auth: AuthContext, fileId: UuidHex): Promise<File> {
    return this.db.transaction(TransactionMode.ReadOnly, async txCtx => getFileById(auth, txCtx, fileId));
  }

  async createFile(auth: AuthContext, options: CreateFileOptions): Promise<File> {
    const params: CreateFileParams = {
      blobService: this.blobService,
    };
    return this.db.transaction(TransactionMode.ReadWrite, async txCtx => createFile(auth, txCtx, params, options));
  }

  async createDirectory(auth: AuthContext, options: CreateDirectoryOptions): Promise<Directory> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => createDirectory(auth, txCtx, options),
    );
  }

  async deleteItem(auth: AuthContext, itemId: UuidHex): Promise<void> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => deleteItem(auth, txCtx, itemId),
    );
  }

  async readFileContent(auth: AuthContext, fileId: UuidHex): Promise<ReadableStream> {
    const params: ReadFileContentParams = {
      blobService: this.blobService,
    };
    return this.db.transaction(
      TransactionMode.ReadOnly,
      async txCtx => readFileContent(auth, txCtx, params, fileId),
    );
  }

  async deleteUnusedFiles(auth: AuthContext): Promise<void> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => deleteUnusedFiles(auth, txCtx),
    );
  }
}

async function getDriveById(
  auth: AuthContext,
  queryable: Queryable,
  driveId: UuidHex,
): Promise<Drive> {
  type Row = dbSchema.Drive;

  const row: Row | undefined = await queryable.oneOrNone<Row>(
    `SELECT drive_id, owner_id, created_at, updated_at, root_id
    FROM drives
    WHERE drives.drive_id = $1::uuid;`,
    [driveId],
  );

  if (row === undefined) {
    throw createDriveNotFoundError(driveId);
  }

  if (auth.type !== ActorType.User || (auth.userId !== row.owner_id && !auth.isAdministrator)) {
    throw new incident.Incident("Unauthorized");
  }

  return {
    id: row.drive_id,
    createdAt: row.created_at,
    updatedAt: row.updated_at,
    root: {id: row.root_id},
    owner: {id: row.owner_id},
  };
}

async function getOrCreateDriveByOwnerId(
  auth: AuthContext,
  queryable: Queryable,
  ownerId: UuidHex,
): Promise<Drive> {
  // TODO: Throw `OwnerNotFound` before if the owner id does not exist.
  if (auth.type !== ActorType.User || (auth.userId !== ownerId && !auth.isAdministrator)) {
    throw new incident.Incident("Unauthorized");
  }

  {
    type Row = dbSchema.Drive;
    const row: Row | undefined = await queryable.oneOrNone<Row>(
      `SELECT drive_id, owner_id, created_at, updated_at, root_id
    FROM drives
    WHERE drives.owner_id = $1::uuid;`,
      [ownerId],
    );

    if (row !== undefined) {
      return {
        id: row.drive_id,
        createdAt: row.created_at,
        updatedAt: row.updated_at,
        root: {id: row.root_id},
        owner: {id: row.owner_id},
      };
    }
  }

  type Row = Pick<dbSchema.Drive, "created_at" | "updated_at">;

  const rootId: UuidHex = await createRootDirectory(queryable);
  const driveId: UuidHex = uuidjs.genV4().toString();

  const row: Row = await queryable.one(
    `INSERT INTO drives (drive_id, created_at, updated_at, owner_id, root_id)
    VALUES ($1::UUID, NOW(), NOW(), $2::UUID, $3::UUID)
    RETURNING created_at, updated_at;`,
    [driveId, ownerId, rootId],
  );

  return {
    id: driveId,
    createdAt: row.created_at,
    updatedAt: row.updated_at,
    root: {id: rootId},
    owner: {id: ownerId},
  };
}

/**
 * @internal
 */
async function createRootDirectory(queryable: Queryable): Promise<UuidHex> {
  const rootId: UuidHex = uuidjs.genV4().toString();

  await queryable.countOne(
    `INSERT INTO
      drive_items(drive_item_id, type, created_at, updated_at, display_name)
    VALUES ($1::UUID, $2::DRIVE_ITEM_TYPE, NOW(), NOW(), $3::VARCHAR);`,
    [rootId, dbEnumFromDriveItemType(DriveItemType.Directory), "$root"],
  );
  await queryable.countOne(
    `INSERT INTO
      directories(drive_item_id, type)
    VALUES ($1::UUID, $2::DRIVE_ITEM_TYPE);`,
    [rootId, dbEnumFromDriveItemType(DriveItemType.Directory)],
  );

  await queryable.countOne(
    `INSERT INTO
      drive_item_closure(ancestor_id,  descendant_id, distance)
    VALUES ($1::UUID, $1::uuid, 0);`,
    [rootId],
  );

  return rootId;
}

async function getDriveItemById(
  auth: AuthContext,
  queryable: Queryable,
  itemId: UuidHex,
): Promise<DriveItem> {
  if (auth.type === ActorType.Guest) {
    throw createUnauthenticatedError({driveItemId: itemId});
  }

  type Row = dbSchema.DriveItem & Pick<dbSchema.Blob, "media_type" | "byte_size">;

  const row: Row = await queryable.one(
    `SELECT di.drive_item_id, di.type, di.created_at, di.updated_at, di.display_name, b.media_type, b.byte_size
    FROM drive_items di
      LEFT OUTER JOIN files f ON (di.drive_item_id = f.drive_item_id)
      LEFT OUTER JOIN blobs b ON (f.blob_id = b.blob_id)
    WHERE di.drive_item_id = $1::uuid;`,
    [itemId],
  );

  switch (dbEnumToDriveItemType(row.type)) {
    case DriveItemType.Directory:
      return <Directory> {
        id: row.drive_item_id,
        type: DriveItemType.Directory,
        createdAt: row.created_at,
        updatedAt: row.updated_at,
        displayName: row.display_name,
      };
    case DriveItemType.File:
      return <File> {
        id: row.drive_item_id,
        type: DriveItemType.File,
        createdAt: row.created_at,
        updatedAt: row.updated_at,
        displayName: row.display_name,
        mediaType: row.media_type,
        byteSize: row.byte_size,
      };
    default:
      throw new incident.Incident("UnexpectedVariant", {row});
  }
}

async function getDirectoryById(
  auth: AuthContext,
  queryable: Queryable,
  directoryId: UuidHex,
): Promise<Directory> {
  const item: DriveItem = await getDriveItemById(auth, queryable, directoryId);
  if (item.type !== DriveItemType.Directory) {
    throw new incident.Incident("NotADirectory", {item});
  }
  return item;
}

async function getFileById(
  auth: AuthContext,
  queryable: Queryable,
  fileId: UuidHex,
): Promise<File> {
  const item: DriveItem = await getDriveItemById(auth, queryable, fileId);
  if (item.type !== DriveItemType.File) {
    throw new incident.Incident("NotAFile", {item});
  }
  return item;
}

async function createDirectory(
  auth: AuthContext,
  queryable: Queryable,
  options: CreateDirectoryOptions,
): Promise<Directory> {
  const details: GetDetailsResult = await getDetails(queryable, options.parentId);
  if (details.type !== DriveItemType.Directory) {
    throw new incident.Incident("Unauthorized");
  }
  if (auth.type !== ActorType.User || (!auth.isAdministrator && auth.userId !== details.ownerId)) {
    throw new incident.Incident("Unauthorized");
  }

  type DriveItemRow = Pick<dbSchema.DriveItem, "created_at" | "updated_at">;

  const itemId: UuidHex = uuidjs.genV4().toString();

  const driveItemRow: DriveItemRow = await queryable.one(
    `INSERT INTO
      drive_items(drive_item_id, type, created_at, updated_at, display_name)
    VALUES ($1::UUID, $2::DRIVE_ITEM_TYPE, NOW(), NOW(), $3::VARCHAR)
    RETURNING created_at, updated_at;`,
    [itemId, dbEnumFromDriveItemType(DriveItemType.Directory), options.displayName],
  );
  await queryable.countOne(
    `INSERT INTO
      directories(drive_item_id, type)
    VALUES ($1::UUID, $2::DRIVE_ITEM_TYPE);`,
    [itemId, dbEnumFromDriveItemType(DriveItemType.Directory)],
  );

  // Add one link for each ancestor of the parent, and the link to itself
  await queryable.query(
    `INSERT INTO
      drive_item_closure(ancestor_id,  descendant_id, distance)
    (
      SELECT ancestor_id, $1::UUID, distance + 1
      FROM drive_item_closure
      WHERE descendant_id = $2::UUID
    )
    UNION ALL
    SELECT $1::UUID, $1::UUID, 0;`,
    [itemId, options.parentId],
  );

  return {
    id: itemId,
    type: DriveItemType.Directory,
    displayName: options.displayName,
    children: [],
    createdAt: driveItemRow.created_at,
    updatedAt: driveItemRow.updated_at,
  };
}

interface CreateFileParams {
  readonly blobService: BlobService;
}

async function createFile(
  auth: AuthContext,
  queryable: Queryable,
  params: CreateFileParams,
  options: CreateFileOptions,
): Promise<File> {
  const details: GetDetailsResult = await getDetails(queryable, options.parentId);
  if (details.type !== DriveItemType.Directory) {
    throw new incident.Incident("Unauthorized");
  }
  if (auth.type !== ActorType.User || (!auth.isAdministrator && auth.userId !== details.ownerId)) {
    throw new incident.Incident("Unauthorized");
  }

  type DriveItemRow = Pick<dbSchema.DriveItem, "created_at" | "updated_at">;

  const itemId: UuidHex = uuidjs.genV4().toString();

  const driveItemRow: DriveItemRow = await queryable.one(
    `INSERT INTO
      drive_items(drive_item_id, type, created_at, updated_at, display_name)
    VALUES ($1::UUID, $2::DRIVE_ITEM_TYPE, NOW(), NOW(), $3::VARCHAR)
    RETURNING created_at, updated_at;`,
    [itemId, dbEnumFromDriveItemType(DriveItemType.File), options.displayName],
  );

  const blob: Blob = await params.blobService.getBlobById(auth, options.blobId);

  // TODO: Media type
  await queryable.countOne(
    `INSERT INTO
      files(drive_item_id, type, blob_id)
    VALUES ($1::UUID, $2::DRIVE_ITEM_TYPE, $3::UUID);`,
    [itemId, dbEnumFromDriveItemType(DriveItemType.File), blob.id],
  );

  // Add one link for each ancestor of the parent, and the link to itself
  await queryable.query(
    `INSERT INTO
      drive_item_closure(ancestor_id,  descendant_id, distance)
    (
      SELECT ancestor_id, $1::UUID, distance + 1
      FROM drive_item_closure
      WHERE descendant_id = $2::UUID
    )
    UNION ALL
    SELECT $1::UUID, $1::UUID, 0;`,
    [itemId, options.parentId],
  );

  return {
    id: itemId,
    type: DriveItemType.File,
    displayName: options.displayName,
    createdAt: driveItemRow.created_at,
    updatedAt: driveItemRow.updated_at,
    mediaType: blob.mediaType,
    byteSize: blob.byteSize,
  };
}

async function deleteItem(
  _auth: AuthContext,
  _queryable: Queryable,
  _itemId: UuidHex,
): Promise<void> {
  throw new Error("NotImplemented: PgFileService/deleteItem");
}

async function deleteUnusedFiles(auth: AuthContext, queryable: Queryable): Promise<void> {
  if (auth.type !== ActorType.System) {
    throw new incident.Incident("Unauthorized");
  }

  await queryable.query(
    `WITH unused_files AS (
      SELECT files.drive_item_id
      FROM files
      WHERE NOT EXISTS (
        SELECT 1 FROM game_resources WHERE game_resources.file_id = files.drive_item_id
        UNION
        SELECT 1 FROM games WHERE games.icon_file_id = files.drive_item_id
      )
    ),
    old_unused_files AS (
      SELECT f.drive_item_id
      FROM unused_files f INNER JOIN drive_items di ON di.drive_item_id = f.drive_item_id
      WHERE di.updated_at < (NOW() - INTERVAL '30 days')
    )
    DELETE FROM drive_items WHERE type = 'file' AND drive_item_id IN (SELECT drive_item_id FROM old_unused_files);`,
    [],
  );
}

// // Ordered from current node to root
// async function* getAncestorIds(
//   queryable: Queryable,
//   itemId: UuidHex,
//   withSelf: boolean = false,
// ): AsyncIterable<UuidHex> {
//   type Row = Pick<dbSchema.DriveItem, "drive_item_id" | "display_name">;
//
//   const rows: AsyncIterable<Row> = await queryable.many(`
//     SELECT drive_item_id, display_name
//     FROM drive_item_closure
//       INNER JOIN drive_items ON (drive_item_closure.ancestor_id = drive_items.drive_item_id)
//     WHERE drive_items_closure.descendant_id = $1::UUID
//     ORDER BY drive_item_closure.distance;`,
//     [itemId],
//   );
//
//   let selfNotFound: boolean = true;
//   for await (const row of rows) {
//     if (row.drive_item_id === itemId) {
//       selfNotFound = false;
//       if (!withSelf) {
//         continue;
//       }
//     }
//     yield row.drive_item_id;
//   }
//   if (selfNotFound) {
//     throw new incident.Incident("DriveItemNotFound", {itemId});
//   }
// }

// // Unordered
// async function* authorizedGetDescendants(
//   queryable: Queryable,
//   itemId: UuidHex,
//   withSelf: boolean = false,
// ): AsyncIterable<UuidHex> {
//   type Row = Pick<dbSchema.DriveItem, "drive_item_id" | "display_name">;
//
//   const rows: AsyncIterable<Row> = await queryable.many(`
//     SELECT drive_item_id, display_name
//     FROM drive_item_closure
//       INNER JOIN drive_items ON (drive_item_closure.descendant_id = drive_items.drive_item_id)
//     WHERE drive_item_closure.ancestor_id = $1::UUID;`,
//     [itemId],
//   );
//
//   let selfNotFound: boolean = true;
//   for await (const row of rows) {
//     if (row.drive_item_id === itemId) {
//       selfNotFound = false;
//       if (!withSelf) {
//         continue;
//       }
//     }
//     yield row.drive_item_id;
//   }
//   if (selfNotFound) {
//     throw new incident.Incident("DriveItemNotFound", {itemId});
//   }
// }

// async function* getChildren(
//   queryable: Queryable,
//   directoryId: UuidHex,
// ): AsyncIterable<UuidHex> {
//   type Row = Pick<dbSchema.DriveItem, "drive_item_id" | "display_name" | "type">;
//
//   const rows: AsyncIterable<Row> = await queryable.many(`
//     SELECT drive_item_id, type, display_name
//     FROM drive_item_closure
//       INNER JOIN drive_items ON (drive_item_closure.descendant_id = drive_items.drive_item_id)
//     WHERE drive_item_closure.ancestor_id = $1::UUID AND drive_item_closure.length <= 1;`,
//     [directoryId],
//   );
//
//   let selfNotFound: boolean = true;
//   for await (const row of rows) {
//     if (row.drive_item_id === directoryId) {
//       selfNotFound = true;
//       continue;
//     }
//     yield row.drive_item_id;
//   }
//   if (selfNotFound) {
//     throw new incident.Incident("DriveItemNotFound", {itemId: directoryId});
//   }
// }

interface GetDetailsResult {
  id: UuidHex;
  ownerId: UuidHex;
  driveId: UuidHex;
  rootId: UuidHex;
  type: DriveItemType;
}

/**
 * @internal
 * @throws OwnerNotFound If the drive item does not exist or its owner cannot be retrieved.
 */
async function getDetails(queryable: Queryable, driveItemId: UuidHex): Promise<GetDetailsResult> {
  type Row = Pick<dbSchema.Drive, "drive_id" | "owner_id" | "root_id"> & Pick<dbSchema.DriveItem, "type">;

  const row: Row | undefined = await queryable.oneOrNone<Row>(
    `SELECT drive_id, owner_id, root_id, drive_item_id, type
    FROM drives
      INNER JOIN drive_item_closure ON (drives.root_id = drive_item_closure.ancestor_id)
      INNER JOIN drive_items ON (drive_item_closure.descendant_id = drive_items.drive_item_id)
    WHERE drive_item_closure.descendant_id = $1::UUID;`,
    [driveItemId],
  );

  if (row === undefined) {
    throw new incident.Incident("OwnerNotFound");
  }

  return {
    id: driveItemId,
    ownerId: row.owner_id,
    driveId: row.drive_id,
    rootId: row.root_id,
    type: dbEnumToDriveItemType(row.type),
  };
}

interface ReadFileContentParams {
  readonly blobService: BlobService;
}

async function readFileContent(
  auth: AuthContext,
  queryable: Queryable,
  params: ReadFileContentParams,
  fileId: UuidHex,
): Promise<ReadableStream> {
  type Row = Pick<dbSchema.File, "blob_id">;
  const row: Row = await queryable.one(
    `SELECT blob_id
    FROM files
    WHERE drive_item_id = $1::UUID`,
    [fileId],
  );
  return params.blobService.readBlobData(auth, row.blob_id);
}
