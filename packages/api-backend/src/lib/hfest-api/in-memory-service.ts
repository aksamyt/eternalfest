import hfApi from "@eternalfest/hammerfest-api/types/api.js";
import incident from "incident";

export interface HammerfestApiState {
  readonly servers: Map<string, HammerfestApiServerState>;
}

export interface HammerfestApiServerState {
  readonly users: Map<string, HammerfestApiUserState>;
  readonly sessions: Map<string, HammerfestApiSessionState>;
}

export interface HammerfestApiUserState {
  readonly id: string;
  readonly username: string;
  readonly password: string;
}

export interface HammerfestApiSessionState {
  readonly userId: string;
}

export class InMemoryHfestApiService implements hfApi.HammerfestApi {
  readonly servers: Map<string, HammerfestApiServerState>;

  constructor(initialState?: HammerfestApiState) {
    this.servers = initialState !== undefined ? initialState.servers : new Map();
  }

  async createSession(options: hfApi.CreateSessionOptions): Promise<hfApi.Session> {
    const server: HammerfestApiServerState | undefined = this.servers.get(options.server);
    if (server === undefined) {
      throw new incident.Incident("ServerNotFound", {server: options.server});
    }
    for (const [userId, user] of server.users) {
      if (user.username === options.username && user.password === options.password) {
        const key: string = Math.random().toString(10);
        server.sessions.set(key, {userId});
        return {
          server: options.server,
          userId,
          key,
        };
      }
    }
    throw incident.Incident("BadCredentials", {server: options.server, username: options.username});
  }

  async getItems(_session: hfApi.Session): Promise<Map<number, number>> {
    return new Map();
  }

  async getPublicProfile(session: hfApi.Session): Promise<hfApi.Profile> {
    const server: HammerfestApiServerState | undefined = this.servers.get(session.server);
    if (server === undefined) {
      throw new incident.Incident("ServerNotFound", {server: session.server});
    }
    const sessionState: HammerfestApiSessionState | undefined = server.sessions.get(session.key);
    if (sessionState === undefined) {
      throw new incident.Incident("InvalidSession", {session});
    }
    const user: HammerfestApiUserState | undefined = server.users.get(sessionState.userId);
    if (user === undefined) {
      throw new incident.Incident("UserNotFound", {session});
    }
    return {
      id: user.id,
      username: user.username,
      email: null,
      bestScore: 10,
      bestLevel: 10,
      carrotRecovered: true,
      scoreOfTheWeek: 10,
      pyramidLevel: 1,
      hallOfFame: null,
      items: [],
      quests: {
        inProgress: [],
        completed: [],
      },
    };
  }

  async getPublicProfileById(_server: string, _userId: string): Promise<hfApi.Profile> {
    throw new Error("NotImplemented");
  }

  async getThreadPage(_session: hfApi.Session, _threadId: string, _page: number): Promise<hfApi.ThreadPage> {
    throw new Error("NotImplemented");
  }

  exportSession(session: hfApi.Session): string {
    return JSON.stringify({server: session.server, userId: session.userId, key: session.key});
  }

  importSession(sessionString: string): hfApi.Session {
    return JSON.parse(sessionString);
  }

  async isSessionActive(_session: hfApi.Session): Promise<boolean> {
    return true;
  }
}
