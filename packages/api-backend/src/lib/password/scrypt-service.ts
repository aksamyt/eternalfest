import { PasswordService } from "@eternalfest/api-core/lib/password/service.js";
import scryptKdf from "scrypt-kdf";

const DEFAULT_MAX_TIME_SECONDS: number = 1;

export class ScryptPasswordService implements PasswordService {
  private readonly params: scryptKdf.ScryptParams;

  /**
   * Creates a new password service using the scrypt algorithm.
   *
   * @param maxTimeSeconds Maximum time used by the key derivation function, in seconds
   * @param maxMemBytes Maximum memory used by the key derivation function, in bytes. Any value greater than
   *                    half of the total RAM is equivalent to half of the available RAM.
   *                    You can omit it to let `scrypt` determine the value automatically.
   */
  constructor(maxTimeSeconds?: number, maxMemBytes?: number) {
    if (maxTimeSeconds === undefined) {
      maxTimeSeconds = DEFAULT_MAX_TIME_SECONDS;
    }
    this.params = scryptKdf.pickParams(maxTimeSeconds, maxMemBytes);
  }

  async hash(clear: Buffer): Promise<Uint8Array> {
    return scryptKdf.kdf(clear, this.params);
  }

  async verify(hash: Uint8Array, clear: Uint8Array): Promise<boolean> {
    return scryptKdf.verify(hash, clear);
  }
}
