import incident from "incident";
import pg from "pg";

import { Database } from "./database.js";
import { Queryable, wrap } from "./queryable.js";

/**
 * Represents a transaction context. You can perform queries on it.
 */
export interface TransactionContext extends Queryable {
  readonly mode: TransactionMode;
}

export enum TransactionMode {
  ReadWrite,
  ReadOnly,
}

/**
 * Represents a function executed in the context of a transaction.
 */
export type TransactionHandler<T> = (txCtx: TransactionContext) => Promise<T>;

/**
 * Represents a function executed in the context of a transaction and returning an iterable
 * stream of results. (Not supported yet).
 */
export type StreamTransactionHandler<T> = (txCtx: TransactionContext) => AsyncIterable<T>;

/**
 * Represents a task that can be executed in the context of a transaction.
 */
export class TransactionTask<T> {
  /**
   * The mode of the transaction. Indicates if the transaction is read only or not.
   */
  readonly mode: TransactionMode;

  /**
   * The function to call during the transaction.
   * Its result will be the result of the transaction.
   * If it throws an error, the transaction will be rollback' ed.
   */
  readonly handler: TransactionHandler<T>;

  constructor(txMode: TransactionMode, handler: TransactionHandler<T>) {
    this.mode = txMode;
    this.handler = handler;
  }

  /**
   * Executes this transaction task on the provided database.
   *
   * @param db Database object providing clients allowing to do top-level transactions.
   * @return The result of the `handler` function.
   */
  async apply(db: Database): Promise<T> {
    return db.withClient<T>(async (client: pg.ClientBase): Promise<T> => {
      await begin(client, this.mode);
      let result: T;
      try {
        const transactionContext: TransactionContext = createTransactionContext(client, this.mode);
        result = await this.handler(transactionContext);
        await commit(client);
      } catch (err) {
        try {
          await rollback(client);
        } catch (rollbackError) {
          throw new incident.Incident(
            rollbackError,
            "RollbackFailedTransaction",
            {rollbackReason: err},
            "CRITICAL: Unable to rollback failed transaction!",
          );
        }
        throw err;
      }
      return result;
    });
  }
}

/**
 * Represents a task that can be executed in the context of a stream transaction.
 */
export class StreamTransactionTask<T> {
  /**
   * The mode of the transaction. Indicates if the transaction is read only or not.
   */
  readonly mode: TransactionMode.ReadOnly;

  /**
   * The function to call during the transaction.
   * Its result will be the result of the transaction.
   * If it throws an error, the transaction will be rollback' ed.
   */
  readonly handler: StreamTransactionHandler<T>;

  constructor(txMode: TransactionMode.ReadOnly, handler: StreamTransactionHandler<T>) {
    this.mode = txMode;
    this.handler = handler;
  }

  /**
   * Executes this transaction task on the provided database.
   *
   * @param db Database object providing clients allowing to do top-level transactions.
   * @return The result of the `handler` function.
   */
  apply(db: Database): AsyncIterable<T> {
    // tslint:disable-next-line:no-this-assignment
    const self: this = this;
    return db.streamWithClient(async function* (client: pg.ClientBase): AsyncIterable<T> {
      await begin(client, self.mode);
      try {
        const transactionContext: TransactionContext = createTransactionContext(client, self.mode);
        for await (const result of self.handler(transactionContext)) {
          yield result;
        }
        await commit(client);
      } catch (err) {
        try {
          await rollback(client);
        } catch (rollbackError) {
          throw new incident.Incident(
            rollbackError,
            "RollbackFailedTransaction",
            {rollbackReason: err},
            "CRITICAL: Unable to rollback failed transaction!",
          );
        }
        throw err;
      }
    });
  }
}

async function begin(client: pg.ClientBase, transactionMode: TransactionMode): Promise<void> {
  let query: string;
  switch (transactionMode) {
  case TransactionMode.ReadWrite:
    query = "BEGIN READ WRITE;";
    break;
  case TransactionMode.ReadOnly:
    query = "BEGIN READ ONLY;";
    break;
  default:
    throw new incident.Incident("Assertion", "Unexpected transaction mode");
  }
  try {
    await client.query(query);
  } catch (err) {
    throw new incident.Incident(err, "BeginTransaction", {transactionMode}, "Unable to begin transaction");
  }
}

async function commit(client: pg.ClientBase): Promise<void> {
  try {
    await client.query("COMMIT;");
  } catch (err) {
    throw new incident.Incident(err, "CommitTransaction", "Unable to commit transaction");
  }
}

async function rollback(client: pg.ClientBase): Promise<void> {
  try {
    await client.query("ROLLBACK;");
  } catch (err) {
    throw new incident.Incident(err, "RollbackTransaction", "Unable to rollback transaction");
  }
}

function createTransactionContext(client: pg.ClientBase, transactionMode: TransactionMode): TransactionContext {
  return {...wrap(client), mode: transactionMode};
}
