import incident from "incident";
import pg from "pg";

/**
 * Represents an object allowing to do SQL queries.
 *
 * This is either a database, a client, a transaction, etc.
 */
export interface SimpleQueryable {
  /**
   * Execute a parametrized SQL query
   *
   * @see https://node-postgres.com/features/queries#parameterized-query
   *
   * @param query The SQL query with the parameter placeholders.
   * @param values List of parameters, can be empty
   * @return The result of the query
   */
  query(query: string, values: any[]): Promise<pg.QueryResult>;
}

/**
 * Represents an object with advanced query utilities
 */
export interface Queryable extends SimpleQueryable {
  /**
   * Execute the provided parametrized SQL query.
   * If the query returns:
   * - 0 rows, then this method returns `undefined`
   * - 1 row, then this method returns this row
   * - 2 or more rows, then this methods throws an Error named `TooManyRows`.
   *
   * @param query
   * @param values
   */
  oneOrNone<T>(query: string, values: any[]): Promise<T | undefined>;

  /**
   * Execute the provided parametrized SQL query.
   * If the query returns:
   * - 0 rows, then this method throws an error named `ZeroRows`
   * - 1 row, then this method returns this row
   * - 2 or more rows, then this methods throws an Error named `TooManyRows`.
   *
   * @param query
   * @param values
   */
  one(query: string, values: any[]): Promise<any>;

  /**
   * Execute the provided parametrized SQL query and returns the rows as an async iterable.
   *
   * @param query
   * @param values
   */
  many(query: string, values: any[]): AsyncIterable<any>;

  /**
   * Execute the provided parametrized SQL query and assert that it affects exactly one row.
   *
   * @param query
   * @param values
   */
  countOne(query: string, values: any[]): Promise<void>;
}

export async function oneOrNone<T>(simple: SimpleQueryable, query: string, values: any[]): Promise<T | undefined> {
  const result: pg.QueryResult = await simple.query(query, values);

  switch (result.rows.length) {
    case 0:
      return undefined;
    case 1:
      return result.rows[0];
    default:
      throw new incident.Incident(
        "TooManyRows",
        {query, values, result},
        "Expected zero or one rows",
      );
  }
}

export async function one<T>(simple: SimpleQueryable, query: string, values: any[]): Promise<T> {
  const result: T | undefined = await oneOrNone<T>(simple, query, values);
  if (result === undefined) {
    throw new incident.Incident(
      "ZeroRows",
      {query, values, result},
      "Expected query to return a single row",
    );
  }
  return result;
}

export async function* many<T>(simple: SimpleQueryable, query: string, values: any[]): AsyncIterable<T> {
  const result: pg.QueryResult = await simple.query(query, values);
  for (const row of result.rows) {
    yield row;
  }
}

export async function countOne(simple: SimpleQueryable, query: string, values: any[]): Promise<void> {
  const result: pg.QueryResult = await simple.query(query, values);

  if (result.rowCount !== 1) {
    throw new incident.Incident(
      "ExpectOneAffectedRow",
      {query, values, result},
      "Expect the query to affect excatly one row",
    );
  }
}

export function wrap(simple: SimpleQueryable): Queryable {
  return {
    query: async (query: string, values: any[]) => simple.query(query, values),
    oneOrNone: async <R>(query: string, values: any[]): Promise<R | undefined> => oneOrNone<R>(simple, query, values),
    one: async (query: string, values: any[]) => one(simple, query, values),
    many: (query: string, values: any[]) => many(simple, query, values),
    countOne: async (query: string, values: any[]) => countOne(simple, query, values),
  };
}
