import pg from "pg";

import { countOne, oneOrNone, Queryable } from "./queryable.js";
import {
  StreamTransactionHandler,
  StreamTransactionTask,
  TransactionHandler,
  TransactionMode,
  TransactionTask,
} from "./transaction.js";

export interface DatabaseOptions {
  user: string;
  password: string;
  host: string;
  port: number;
  dbName: string;
}

export class Database implements Queryable {
  private pool: pg.Pool;

  constructor(pool: pg.Pool) {
    this.pool = pool;
  }

  static create(options: DatabaseOptions): Database {
    const dbConfig: pg.PoolConfig = {
      user: options.user,
      password: options.password,
      host: options.host,
      port: options.port,
      database: options.dbName,
      max: 10,
      idleTimeoutMillis: 1000,
    };

    return new Database(new pg.Pool(dbConfig));
  }

  async query(queryText: string, params: any[]): Promise<pg.QueryResult> {
    return this.pool.query(queryText, params);
  }

  async oneOrNone<T>(query: string, values: any[]): Promise<T | undefined> {
    return oneOrNone<T>(this.pool, query, values);
  }

  async one<T>(query: string, values: any[]): Promise<T> {
    const result: T | undefined = await oneOrNone<T>(this.pool, query, values);
    if (result === undefined) {
      throw new Error("Expected exactly one affected row");
    }
    return result;
  }

  async * many<T>(query: string, values: any[]): AsyncIterable<T> {
    const result: pg.QueryResult = await this.pool.query(query, values);
    for (const row of result.rows) {
      yield row;
    }
  }

  async countOne(query: string, values: any[]): Promise<void> {
    return countOne(this.pool, query, values);
  }

  async withClient<T>(executor: (client: pg.PoolClient) => Promise<T>): Promise<T> {
    const client: pg.PoolClient = await this.pool.connect();
    let result: T;
    try {
      result = await executor(client);
      client.release();
    } catch (err) {
      client.release(err);
      throw err;
    }
    return result;
  }

  async transaction<T>(txMode: TransactionMode, handler: TransactionHandler<T>): Promise<T> {
    return new TransactionTask(txMode, handler).apply(this);
  }

  async * streamWithClient<T>(handler: (client: pg.PoolClient) => AsyncIterable<T>): AsyncIterable<T> {
    const client: pg.PoolClient = await this.pool.connect();
    try {
      for await (const result of handler(client)) {
        yield result;
      }
      client.release();
    } catch (err) {
      client.release(err);
      throw err;
    }
  }

  streamTransaction<T>(
    txMode: TransactionMode.ReadOnly,
    handler: StreamTransactionHandler<T>,
  ): AsyncIterable<T> {
    return new StreamTransactionTask(txMode, handler).apply(this);
  }
}
