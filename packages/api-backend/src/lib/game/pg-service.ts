import { AuthContext, SystemAuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { createForbiddenError } from "@eternalfest/api-core/lib/errors/forbidden.js";
import { createUnauthenticatedError } from "@eternalfest/api-core/lib/errors/unauthenticated.js";
import { createUserNotFoundError } from "@eternalfest/api-core/lib/errors/user-not-found.js";
import { File } from "@eternalfest/api-core/lib/file/file.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { ReadonlyCreateGameOptions } from "@eternalfest/api-core/lib/game/create-game-options.js";
import { GameAuthor } from "@eternalfest/api-core/lib/game/game-author.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { NullableGameKey } from "@eternalfest/api-core/lib/game/game-key.js";
import { GameModeDisplayName } from "@eternalfest/api-core/lib/game/game-mode-display-name.js";
import { GameModeLocale } from "@eternalfest/api-core/lib/game/game-mode-locale.js";
import { GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state.js";
import { GameMode, ReadonlyGameMode } from "@eternalfest/api-core/lib/game/game-mode.js";
import { GameOptionDisplayName } from "@eternalfest/api-core/lib/game/game-option-display-name.js";
import { GameOptionState } from "@eternalfest/api-core/lib/game/game-option-state.js";
import { GameOption } from "@eternalfest/api-core/lib/game/game-option.js";
import { GameResource } from "@eternalfest/api-core/lib/game/game-resource.js";
import { Game } from "@eternalfest/api-core/lib/game/game.js";
import { InputGameResource } from "@eternalfest/api-core/lib/game/input-game-resource.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game.js";
import { ReadonlyUpdateGameOptions } from "@eternalfest/api-core/lib/game/update-game-options.js";
import { RunItems } from "@eternalfest/api-core/lib/run/run-items.js";
import { IdRef } from "@eternalfest/api-core/lib/types/id-ref.js";
import { LocaleId } from "@eternalfest/api-core/lib/types/locale-id.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import * as dbSchema from "@eternalfest/eternalfest-db/lib/schema.js";
import incident from "incident";
import { UuidHex } from "kryo/lib/uuid-hex.js";
import squel from "squel";
import uuidjs from "uuidjs";

import { Database } from "../db/database.js";
import { Queryable } from "../db/queryable.js";
import { TransactionContext, TransactionMode } from "../db/transaction.js";
import { getUserItems } from "../run/pg-service.js";
import {
  dbEnumFromGameCategory,
  dbEnumFromLocaleId,
  dbEnumToGameCategory,
  dbEnumToLocaleId
} from "../utils/db-enum.js";

const PG_SQUEL: squel.PostgresSquel = squel.useFlavour("postgres");
const SYSTEM_AUTH: SystemAuthContext = {type: ActorType.System};

/**
 * Make all properties in T nullable
 */
type NullableProperties<T> = {
  [P in keyof T]: T[P] | null;
};

export class PgGameService implements GameService {
  private readonly db: Database;
  private readonly fileService: FileService;
  private readonly userService: UserService;

  constructor(db: Database, userService: UserService, fileService: FileService) {
    this.db = db;
    this.fileService = fileService;
    this.userService = userService;
  }

  async getGames(auth: AuthContext): Promise<ShortGame[]> {
    const params: GetGameByIdParams = {
      fileService: this.fileService,
      userService: this.userService,
    };
    return this.db.transaction(
      TransactionMode.ReadOnly,
      txCtx => getGames(auth, txCtx, params),
    );
  }

  async getGameById(auth: AuthContext, gameId: UuidHex): Promise<Game | undefined> {
    const params: GetGameByIdParams = {
      fileService: this.fileService,
      userService: this.userService,
    };
    return this.db.transaction(
      TransactionMode.ReadOnly,
      async txCtx => getGameById(auth, txCtx, params, gameId),
    );
  }

  async deleteGameById(auth: AuthContext, gameId: UuidHex): Promise<void> {
    return this.db.transaction(
      TransactionMode.ReadWrite,
      async txCtx => deleteGameById(auth, txCtx, this.userService, gameId),
    );
  }

  async createGame(auth: AuthContext, options: ReadonlyCreateGameOptions): Promise<Game> {
    const params: GetGameByIdParams = {
      fileService: this.fileService,
      userService: this.userService,
    };
    return this.db.transaction<Game>(
      TransactionMode.ReadWrite,
      async txCtx => createGame(auth, txCtx, params, options),
    );
  }

  async updateGame(auth: AuthContext, gameId: UuidHex, options: ReadonlyUpdateGameOptions): Promise<Game> {
    return this.db.transaction(TransactionMode.ReadWrite, async (txCtx: TransactionContext) => {
      const params: GetGameByIdParams = {
        fileService: this.fileService,
        userService: this.userService,
      };
      const updatedId: IdRef = await updateGame(this.fileService, auth, txCtx, gameId, options);
      const updated: Game | undefined = await getGameById(auth, txCtx, params, updatedId.id);
      if (updated === undefined) {
        throw new incident.Incident("GameNotFound", {id: updatedId.id});
      }
      return updated;
    });
  }
}

enum PermissionResult {
  Ok,
  Unauthorized,
  Forbidden,
}

function getGameReadPermission(
  auth: AuthContext,
  row: Pick<dbSchema.Game, "is_private" | "author_id" | "public_access_from">,
): PermissionResult {
  if (auth.type === ActorType.System) {
    return PermissionResult.Ok;
  }
  if (row.is_private) {
    // Only owner and administrators can access private games
    if (auth.type === ActorType.Guest) {
      return PermissionResult.Unauthorized;
    } else if (row.author_id !== auth.userId && !auth.isAdministrator) {
      return PermissionResult.Forbidden;
    }
  } else if (row.public_access_from === null || row.public_access_from.getTime() > Date.now()) {
    // No publication date, or publication date in the future
    // Only eternalfest members (testers) can access the game
    if (auth.type === ActorType.Guest) {
      return PermissionResult.Unauthorized;
    } else if (!auth.isTester) {
      return PermissionResult.Forbidden;
    }
  }
  // Public game
  return PermissionResult.Ok;
}

function getGameDeletionPermission(
  auth: AuthContext,
  row: Pick<dbSchema.Game, "author_id">,
): PermissionResult {
  if (auth.type === ActorType.Guest) {
    // Authentication required
    return PermissionResult.Unauthorized;
  }

  // Only system, owner or administrators can delete games
  if (auth.type === ActorType.System || row.author_id === auth.userId || auth.isAdministrator) {
    return PermissionResult.Ok;
  }

  return PermissionResult.Forbidden;
}

async function getGames(
  auth: AuthContext,
  queryable: Queryable,
  params: GetGameByIdParams,
): Promise<ShortGame[]> {
  type GameRow = Pick<dbSchema.Game,
    "game_id" | "key" | "author_id" | "category" | "created_at"
    | "main_locale" | "display_name" | "description"
    | "families" | "icon_file_id" | "is_private"
    | "public_access_from" | "updated_at">;
  const gRows: AsyncIterable<GameRow> = queryable.many(
    `SELECT
      game_id, key, author_id, category, created_at,
      main_locale, display_name, description,
      families, icon_file_id, is_private,
      public_access_from, updated_at
    FROM games
    ORDER BY updated_at DESC;`,
    [],
  );

  const gamePromises: Promise<ShortGame>[] = [];
  for await (const gRow of gRows) {
    const permissionResult: PermissionResult = getGameReadPermission(auth, gRow);
    if (permissionResult !== PermissionResult.Ok) {
      continue;
    }
    gamePromises.push(getShortGame(gRow));
  }

  const games: ShortGame[] = await Promise.all(gamePromises);

  async function getShortGame(gRow: GameRow): Promise<ShortGame> {
    const author: GameAuthor | undefined = await params.userService.getUserRefById(auth, gRow.author_id);
    if (author === undefined) {
      throw createUserNotFoundError(gRow.author_id);
    }

    const iconFile: File | undefined = gRow.icon_file_id !== null
      ? await params.fileService.getFileById(SYSTEM_AUTH, gRow.icon_file_id)
      : undefined;

    const shortGame: ShortGame = {
      id: gRow.game_id,
      key: gRow.key,
      createdAt: gRow.created_at,
      updatedAt: gRow.updated_at,
      mainLocale: dbEnumToLocaleId(gRow.main_locale),
      displayName: gRow.display_name,
      description: gRow.description,
      publicationDate: gRow.public_access_from,
      author: {
        id: author.id,
        displayName: author.displayName,
      },
      iconFile,
      isPrivate: gRow.is_private,
      category: dbEnumToGameCategory(gRow.category),
      locales: new Map(),
    };
    return shortGame;
  }

  sortGames(games);

  return games;
}

function sortGames(games: ShortGame[]): void {
  let latestPublishedGame: ShortGame | undefined = undefined;
  for (const game of games) {
    if (game.publicationDate !== null) {
      if (
        latestPublishedGame === undefined
        || game.publicationDate.getTime() > latestPublishedGame.publicationDate!.getTime()
      ) {
        latestPublishedGame = game;
      }
    }
  }
  games.sort((left: ShortGame, right: ShortGame): -1 | 0 | 1 => {
    if (latestPublishedGame !== undefined) {
      if (left.id === latestPublishedGame.id) {
        return -1;
      } else if (right.id === latestPublishedGame.id) {
        return 1;
      }
    }
    if (left.displayName === "Niveau du Jour") {
      return -1;
    } else if (right.displayName === "Niveau du Jour") {
      return 1;
    }
    if (left.updatedAt.getTime() === right.updatedAt.getTime()) {
      return 0;
    } else {
      return left.updatedAt.getTime() > right.updatedAt.getTime() ? -1 : 1;
    }
  });
}

interface GetGameByIdParams {
  fileService: FileService;
  userService: UserService;
}

async function getGameById(
  auth: AuthContext,
  queryable: Queryable,
  params: GetGameByIdParams,
  gameId: UuidHex,
): Promise<Game | undefined> {
  type GameRow = Pick<dbSchema.Game,
    "key" | "author_id" | "category" | "created_at"
    | "main_locale" | "display_name" | "description"
    | "families" | "icon_file_id" | "is_private"
    | "public_access_from" | "updated_at">;
  const gRow: GameRow | undefined = await queryable.oneOrNone(
    `SELECT
      key, author_id, category, created_at,
      main_locale, display_name, description,
      families, icon_file_id, is_private,
      public_access_from, updated_at
    FROM games
    WHERE game_id = $1::UUID;`,
    [gameId],
  );
  if (gRow === undefined) {
    return undefined;
  }

  const isHackfest: boolean = gRow.key === "hackfest" || gRow.key === "hackfest_beta";
  let items: RunItems = new Map();
  if (isHackfest && auth.type === ActorType.User) {
    items = await getUserItems(queryable, auth.userId, gameId);
  }

  const permissionResult: PermissionResult = getGameReadPermission(auth, gRow);
  switch (permissionResult) {
    case PermissionResult.Unauthorized:
      throw createUnauthenticatedError({gameId});
    case PermissionResult.Forbidden:
      throw incident.Incident("Forbidden", {gameId});
    case PermissionResult.Ok:
      break;
    default:
      throw new incident.Incident("UnexpectedPermissionResult", {permissionResult});
  }

  const author: GameAuthor | undefined = await params.userService.getUserRefById(auth, gRow.author_id);
  if (author === undefined) {
    throw createUserNotFoundError(gRow.author_id);
  }

  const iconFile: File | undefined = gRow.icon_file_id !== null
    ? await params.fileService.getFileById(SYSTEM_AUTH, gRow.icon_file_id)
    : undefined;

  const resourcePromises: Promise<GameResource>[] = [];
  {
    type Row = Pick<dbSchema.GameResource, "file_id" | "resource_tag">;
    const rows: AsyncIterable<Row> = queryable.many(
      `SELECT file_id, resource_tag
      FROM game_resources
      WHERE game_id = $1::UUID
      ORDER BY _rank;`,
      [gameId],
    );

    for await (const row of rows) {
      resourcePromises.push(
        params.fileService.getFileById(SYSTEM_AUTH, row.file_id)
          .then((file: File): GameResource => ({
            file,
            tag: row.resource_tag !== null ? row.resource_tag : undefined,
            priority: "high",
          })),
      );
    }
  }
  const resources: GameResource[] = await Promise.all(resourcePromises);

  const gameModes: GameMode[] = await getGameModes();

  async function getGameModes(): Promise<GameMode[]> {
    interface GameModeRow {
      mode_key: dbSchema.GameOption["mode_key"];
      mode_display_name: dbSchema.GameMode["display_name"];
      mode_is_enabled: dbSchema.GameMode["is_enabled"];
    }

    interface GameOptionRow {
      option_key: dbSchema.GameOption["option_key"];
      option_display_name: dbSchema.GameOption["display_name"];
      option_is_enabled: dbSchema.GameOption["is_enabled"];
    }

    type Row = GameModeRow & NullableProperties<GameOptionRow>;
    const rows: AsyncIterable<Row> = queryable.many(
      `SELECT
        gm.mode_key, gm.display_name AS mode_display_name, gm.is_enabled AS mode_is_enabled,
        go.option_key, go.display_name AS option_display_name, go.is_enabled AS option_is_enabled
      FROM game_modes AS gm
        LEFT OUTER JOIN game_options AS go
        ON go.game_id = gm.game_id AND go.mode_key = gm.mode_key
      WHERE gm.game_id = $1::UUID
      ORDER BY gm._rank, go._rank;`,
      [gameId],
    );

    const gameModes: GameMode[] = [];

    let oldGameModeRow: GameModeRow | undefined;
    let curOptions: GameOption[] = [];

    for await (const row of rows) {
      if (oldGameModeRow === undefined) {
        oldGameModeRow = row;
      }
      if (row.mode_key !== oldGameModeRow.mode_key) {
        endGameMode(oldGameModeRow);
        oldGameModeRow = row;
      }
      if (row.option_key !== null) {
        curOptions.push({
          key: row.option_key!,
          displayName: row.option_display_name!,
          state: row.option_is_enabled! ? GameOptionState.Enabled : GameOptionState.Disabled,
          locales: new Map(),
        });
      }
    }
    if (oldGameModeRow !== undefined) {
      endGameMode(oldGameModeRow);
    }

    function endGameMode(gmRow: GameModeRow): void {
      const mode = {
        key: gmRow.mode_key,
        displayName: gmRow.mode_display_name,
        state: gmRow.mode_is_enabled ? GameModeState.Enabled : GameModeState.Disabled,
        options: curOptions,
        locales: new Map(),
      };
      if (isHackfest && mode.key === "prelude") {
        const INVISIBILITY_CLOAK_ID: string = "1262";
        const cloakCount = items.get(INVISIBILITY_CLOAK_ID) ?? 0;
        if (cloakCount >= 1) {
          gameModes.push(mode);
        }
      } else {
        gameModes.push(mode);
      }

      curOptions = [];
    }

    return gameModes;
  }

  return {
    id: gameId,
    key: gRow.key,
    mainLocale: dbEnumToLocaleId(gRow.main_locale),
    displayName: gRow.display_name,
    description: gRow.description,
    createdAt: gRow.created_at,
    updatedAt: gRow.updated_at,
    publicationDate: gRow.public_access_from,
    author: {
      id: author.id,
      displayName: author.displayName,
    },
    iconFile,
    families: gRow.families,
    isPrivate: gRow.is_private,
    category: dbEnumToGameCategory(gRow.category),
    resources,
    gameModes,
    engines: {
      loader: "3.0.0" as "3.0.0",
    },
    locales: new Map(),
  };
}

async function deleteGameById(
  auth: AuthContext,
  queryable: Queryable,
  _userService: UserService,
  gameId: UuidHex,
): Promise<void> {
  type Row = Pick<dbSchema.Game, "game_id" | "author_id">;
  const row: Row = await queryable.one(
    `SELECT game_id, author_id
    FROM games
    WHERE game_id = $1::UUID;`,
    [gameId],
  );

  const permissionResult: PermissionResult = getGameDeletionPermission(auth, row);
  switch (permissionResult) {
    case PermissionResult.Unauthorized:
      throw createUnauthenticatedError({gameId});
    case PermissionResult.Forbidden:
      throw incident.Incident("Forbidden", {gameId});
    case PermissionResult.Ok:
      break;
    default:
      throw new incident.Incident("UnexpectedPermissionResult", {permissionResult});
  }

  await queryable.countOne(
    `DELETE FROM games
    WHERE game_id = $1::UUID;`,
    [gameId],
  );
}

interface CreateGameParams {
  fileService: FileService;
  userService: UserService;
}

async function createGame(
  auth: AuthContext,
  queryable: Queryable,
  params: CreateGameParams,
  options: ReadonlyCreateGameOptions,
): Promise<Game> {
  if (auth.type !== ActorType.User) {
    throw new incident.Incident("Unauthorized");
  } else if (!auth.isTester) {
    throw new incident.Incident("Forbidden");
  }

  const gameId: UuidHex = uuidjs.genV4().toString();
  const key: NullableGameKey = null;
  const mainLocale: LocaleId = options.mainLocale;
  const displayName: string = options.displayName;
  const authorId: UuidHex = auth.userId;
  let iconFileId: UuidHex | null = null;
  let iconFile: File | undefined = undefined;
  if (options.iconFileId !== undefined) {
    iconFileId = options.iconFileId;
    iconFile = await params.fileService.getFileById(auth, iconFileId);
  }
  const category: GameCategory = options.category !== undefined ? options.category : GameCategory.Other;
  const description: string = options.description;
  const families: string = options.families;
  const loaderVersion: "3.0.0" = options.engines.loader;
  const isPrivate: boolean = options.isPrivate;

  type GameRow = Pick<dbSchema.Game, "created_at" | "updated_at">;
  const gameRow: GameRow = await queryable.one(
    `INSERT INTO games(
      game_id, key, created_at, updated_at,
      main_locale, display_name, description,
      public_access_from, author_id, icon_file_id, category,
      families, loader_version, is_private
    )
    VALUES (
      $1::UUID, $2::VARCHAR, NOW(), NOW(),
      $3::LOCALE_ID, $4::VARCHAR, $5::TEXT,
      NULL, $6::UUID, $7::UUID, $8::GAME_CATEGORY,
      $9::TEXT, $10::VARCHAR, $11::BOOLEAN
    )
    RETURNING created_at, updated_at;`,
    [
      gameId, key,
      dbEnumFromLocaleId(mainLocale), displayName, description,
      authorId, iconFileId, dbEnumFromGameCategory(category),
      families, loaderVersion, isPrivate,
    ],
  );

  const resources: GameResource[] = await createGameResources(
    params.fileService,
    auth,
    queryable,
    gameId,
    options.resources,
  );

  const gameModes: GameMode[] = await createGameModes(queryable, gameId, mainLocale, options.gameModes);

  return {
    id: gameId,
    key,
    mainLocale,
    displayName,
    description,
    createdAt: gameRow.created_at,
    updatedAt: gameRow.updated_at,
    publicationDate: null, // gameRow.public_access_from,
    author: {
      id: auth.userId,
      displayName: auth.displayName,
    },
    iconFile,
    families,
    isPrivate,
    category,
    resources,
    gameModes,
    engines: {
      loader: loaderVersion,
    },
    locales: new Map(),
  };
}

async function updateGame(
  fileService: FileService,
  auth: AuthContext,
  queryable: Queryable,
  gameId: UuidHex,
  options: ReadonlyUpdateGameOptions,
): Promise<IdRef> {
  if (auth.type !== ActorType.User) {
    throw createUnauthenticatedError({action: "updateGame"});
  }

  let mainLocale: LocaleId;
  {
    type Row = Pick<dbSchema.Game, "author_id" | "main_locale">;
    const row: Row | undefined = await queryable.oneOrNone(
      `SELECT
      author_id, main_locale
    FROM games
    WHERE game_id = $1::UUID;`,
      [gameId],
    );
    if (row === undefined) {
      throw new incident.Incident("GameNotFound", {id: gameId});
    }
    const authErr: Error | undefined = testUpdateGameAuthorization(auth, row.author_id, options);
    if (authErr !== undefined) {
      throw authErr;
    }
    mainLocale = dbEnumToLocaleId(row.main_locale);
  }

  // tslint:disable-next-line:max-line-length
  type Fields = Partial<Pick<dbSchema.Game, "main_locale" | "display_name" | "description" | "icon_file_id" | "families" | "is_private" | "public_access_from">>;
  const fields: Fields = Object.create(null);
  if (options.mainLocale !== undefined) {
    fields.main_locale = dbEnumFromLocaleId(options.mainLocale);
    mainLocale = options.mainLocale;
  }
  if (options.displayName !== undefined) {
    fields.display_name = options.displayName;
  }
  if (options.description !== undefined) {
    fields.description = options.description;
  }
  if (options.iconFileId !== undefined) {
    fields.icon_file_id = options.iconFileId;
  }
  if (options.families !== undefined) {
    fields.families = options.families;
  }
  if (options.isPrivate !== undefined) {
    fields.is_private = options.isPrivate;
  }
  if (options.publicationDate !== undefined) {
    fields.public_access_from = options.publicationDate !== null
      ? (options.publicationDate as Date).toISOString() as any
      : null;
  }
  if (Object.keys(fields).length > 0) {
    const query: squel.ParamString = PG_SQUEL.update()
      .table("games")
      .setFields(fields)
      .set("updated_at", PG_SQUEL.str("NOW()"))
      .where("games.game_id = ?::UUID", gameId)
      .toParam();
    await queryable.countOne(query.text, query.values);
  }

  if (options.resources !== undefined) {
    await deleteGameResources(queryable, gameId);
    await createGameResources(fileService, auth, queryable, gameId, options.resources);
  }

  if (options.gameModes !== undefined) {
    await deleteGameModes(queryable, gameId);
    await createGameModes(queryable, gameId, mainLocale, options.gameModes);
  }

  return {id: gameId};
}

function testUpdateGameAuthorization(
  auth: UserAuthContext,
  authorId: UuidHex,
  options: ReadonlyUpdateGameOptions,
): Error | undefined {
  if (auth.userId !== authorId && !auth.isAdministrator) {
    return createForbiddenError({
      action: "updateGame",
      message: "Only administrators and authors can update games",
    });
  }
  if (options.publicationDate !== undefined && !auth.isAdministrator) {
    return createForbiddenError({
      action: "updateGame",
      message: "Only administrators can set the `publicationDate` value",
    });
  }
  return undefined;
}

async function createGameResources(
  fileService: FileService,
  auth: AuthContext,
  queryable: Queryable,
  gameId: UuidHex,
  resources: ReadonlyArray<InputGameResource>,
): Promise<GameResource[]> {
  return Promise.all(resources.map(async (resource, i): Promise<GameResource> => {
    const file: File = await fileService.getFileById(auth, resource.fileId);
    const tag: string | null = resource.tag !== undefined ? resource.tag : null;

    await queryable.countOne(
      `INSERT INTO game_resources(game_id, file_id, resource_tag, _rank)
      VALUES ($1::UUID, $2::UUID, $3::VARCHAR, $4::INT4);`,
      [gameId, file.id, tag, i],
    );

    return {file, tag: resource.tag, priority: "high"};
  }));
}

async function deleteGameResources(
  queryable: Queryable,
  gameId: UuidHex,
): Promise<void> {
  await queryable.query(
    `DELETE FROM game_resources
    WHERE game_id = $1::UUID;`,
    [gameId],
  );
}

async function createGameModes(
  queryable: Queryable,
  gameId: UuidHex,
  mainLocale: LocaleId,
  gameModes: readonly ReadonlyGameMode[],
): Promise<GameMode[]> {
  return Promise.all(gameModes.map(async (gameMode, i): Promise<GameMode> => {
    const displayName: GameModeDisplayName = gameMode.displayName;
    const isEnabled: boolean = gameMode.state !== GameModeState.Disabled;
    const state: GameModeState = isEnabled ? GameModeState.Enabled : GameModeState.Disabled;
    const locales: Map<LocaleId, GameModeLocale> = new Map();

    await queryable.countOne(
      `INSERT INTO game_modes(game_id, mode_key, main_locale, display_name, is_enabled, _rank)
      VALUES ($1::UUID, $2::VARCHAR, $3::LOCALE_ID, $4::VARCHAR, $5::BOOLEAN, $6::INT4);`,
      [gameId, gameMode.key, dbEnumFromLocaleId(mainLocale), displayName, isEnabled, i],
    );

    const options: GameOption[] = await Promise.all(gameMode.options.map(async (gameOption, i): Promise<GameOption> => {
      const displayName: GameOptionDisplayName = gameOption.displayName;
      const isEnabled: boolean = gameOption.state !== GameOptionState.Disabled;
      const state: GameOptionState = isEnabled ? GameOptionState.Enabled : GameOptionState.Disabled;
      const locales: Map<LocaleId, GameModeLocale> = new Map();

      await queryable.countOne(
        `INSERT INTO game_options(game_id, mode_key, option_key, main_locale, display_name, is_enabled, _rank)
        VALUES ($1::UUID, $2::VARCHAR, $3::VARCHAR, $4::LOCALE_ID, $5::VARCHAR, $6::BOOLEAN, $7::INT4);`,
        [gameId, gameMode.key, gameOption.key, dbEnumFromLocaleId(mainLocale), displayName, isEnabled, i],
      );

      return {key: gameOption.key, displayName, state, locales};
    }));

    return {key: gameMode.key, displayName, state, options, locales};
  }));
}

async function deleteGameModes(
  queryable: Queryable,
  gameId: UuidHex,
): Promise<void> {
  await queryable.query(
    `DELETE FROM game_modes
    WHERE game_id = $1::UUID;`,
    [gameId],
  );
}
