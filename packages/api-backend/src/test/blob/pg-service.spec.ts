import { PgBlobService } from "../../lib/blob/pg-service.js";
import { DbState, StateService, withTestState } from "../test-db.js";
import { testBlobService } from "./test.js";

async function withPgBlobService<R>(handler: (blobService: PgBlobService) => Promise<R>): Promise<R> {
  return withTestState(DbState.Empty, async ({db, buffer}: StateService): Promise<R> => {
    const blobService: PgBlobService = new PgBlobService(db, buffer);
    return handler(blobService);
  });
}

describe("PgBlobService", function () {
  testBlobService(withPgBlobService);
});
