import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { UploadSession } from "@eternalfest/api-core/lib/blob/upload-session.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import chai from "chai";
import stream from "stream";

import { readIntoBuffer } from "../read-into-buffer.js";
import { TIMEOUT } from "../timeout.js";

const GUEST_AUTH: AuthContext = {type: ActorType.Guest, scope: AuthScope.Default};

export function testBlobService(withBlobService: AsyncResourceManager<BlobService, void>) {
  it("Create a `text/plain` blob", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const data: Buffer = Buffer.from("Hello, World!", "UTF-8" as any);
      const blob: Blob = await blobService.createBlob(GUEST_AUTH, {mediaType: "text/plain", data});

      chai.assert.isString(blob.id);
      chai.assert.strictEqual(blob.byteSize, 13);
      chai.assert.strictEqual(blob.mediaType, "text/plain");
    });
  });

  it("Create an empty blob", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const data: Buffer = Buffer.alloc(0);
      const blob: Blob = await blobService.createBlob(GUEST_AUTH, {mediaType: "application/octet-stream", data});

      chai.assert.isString(blob.id);
      chai.assert.strictEqual(blob.byteSize, 0);
      chai.assert.strictEqual(blob.mediaType, "application/octet-stream");
    });
  });

  it("Create a blob and retrieve it by id", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const data: Buffer = Buffer.from("Hello, World!", "UTF-8" as any);
      const blob: Blob = await blobService.createBlob(GUEST_AUTH, {mediaType: "text/plain", data});

      const retrievedBlob: Blob = await blobService.getBlobById(GUEST_AUTH, blob.id);

      chai.assert.strictEqual(retrievedBlob.id, blob.id);
      chai.assert.strictEqual(retrievedBlob.byteSize, 13);
      chai.assert.strictEqual(retrievedBlob.mediaType, "text/plain");
    });
  });

  it("Create a blob and read it", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const data: Buffer = Buffer.from("Hello, World!", "UTF-8" as any);
      const blob: Blob = await blobService.createBlob(GUEST_AUTH, {mediaType: "text/plain", data});

      const stream: stream.Readable = await blobService.readBlobData(GUEST_AUTH, blob.id);
      const buffer: Buffer = await readIntoBuffer(stream);
      chai.assert.deepEqual(buffer, Buffer.from("Hello, World!", "UTF-8" as any));
    });
  });

  it("Create an upload session", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const startTime: number = Date.now();
      const session: UploadSession = await blobService.createUploadSession(
        GUEST_AUTH,
        {mediaType: "text/plain", byteSize: 13},
      );

      chai.assert.isString(session.id);
      chai.assert.deepEqual(session.remainingRange, {start: 0, end: 13});
      chai.assert.isTrue(session.expiresAt.getTime() >= startTime);
      chai.assert.isUndefined(session.blob);
    });
  });

  it("Create an upload session and upload some bytes", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const session1: UploadSession = await blobService.createUploadSession(
        GUEST_AUTH,
        {mediaType: "text/plain", byteSize: 13},
      );

      const data: Buffer = Buffer.from("Hello", "UTF-8" as any);
      const session2: UploadSession = await blobService.uploadBytes(
        GUEST_AUTH,
        session1.id,
        {data, offset: 0},
      );

      chai.assert.strictEqual(session2.id, session1.id);
      chai.assert.deepEqual(session2.remainingRange, {start: 5, end: 13});
      chai.assert.isTrue(session2.expiresAt.getTime() >= session1.expiresAt.getTime());
      chai.assert.isUndefined(session2.blob);
    });
  });

  it("Create an upload session and upload all chunks", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const session1: UploadSession = await blobService.createUploadSession(
        GUEST_AUTH,
        {mediaType: "text/plain", byteSize: 13},
      );

      const data1: Buffer = Buffer.from("Hello", "UTF-8" as any);
      const session2: UploadSession = await blobService.uploadBytes(
        GUEST_AUTH,
        session1.id,
        {data: data1, offset: 0},
      );

      const data2: Buffer = Buffer.from(", World!", "UTF-8" as any);
      const session3: UploadSession = await blobService.uploadBytes(
        GUEST_AUTH,
        session2.id,
        {data: data2, offset: 5},
      );

      chai.assert.strictEqual(session3.id, session2.id);
      chai.assert.deepEqual(session3.remainingRange, {start: 13, end: 13});
      chai.assert.isTrue(session3.expiresAt.getTime() >= session2.expiresAt.getTime());
      chai.assert.isDefined(session3.blob, "expected `session3.blob` to be defined");
      chai.assert.isString(session3.blob!.id);
      chai.assert.strictEqual(session3.blob!.byteSize, 13);
      chai.assert.strictEqual(session3.blob!.mediaType, "text/plain");
    });
  });

  it("Create a blob with an upload session and read it", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withBlobService(async (blobService: BlobService): Promise<void> => {
      const session: UploadSession = await blobService.createUploadSession(
        GUEST_AUTH,
        {mediaType: "text/plain", byteSize: 13},
      );

      const data1: Buffer = Buffer.from("Hello", "UTF-8" as any);
      await blobService.uploadBytes(
        GUEST_AUTH,
        session.id,
        {data: data1, offset: 0},
      );

      const data2: Buffer = Buffer.from(", World!", "UTF-8" as any);
      const completeSession: UploadSession = await blobService.uploadBytes(
        GUEST_AUTH,
        session.id,
        {data: data2, offset: 5},
      );

      chai.assert.isDefined(completeSession.blob, "expected `completeSession.blob` to be defined");

      const stream: stream.Readable = await blobService.readBlobData(GUEST_AUTH, completeSession.blob!.id);
      const buffer: Buffer = await readIntoBuffer(stream);
      chai.assert.deepEqual(buffer, Buffer.from("Hello, World!", "UTF-8" as any));
    });
  });
}
