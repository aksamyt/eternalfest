import { UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { RunStart } from "@eternalfest/api-core/lib/run/run-start.js";
import { Run } from "@eternalfest/api-core/lib/run/run.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";

import { createSampleGames } from "../populate/games.js";
import { populateWithUsers } from "../populate/users.js";
import { TIMEOUT } from "../timeout.js";

export interface Context {
  blob: BlobService;
  file: FileService;
  game: GameService;
  run: RunService;
  user: UserService;
}

function getAuthContext(user: User): UserAuthContext {
  return {
    type: ActorType.User,
    userId: user.id,
    displayName: user.displayName,
    isAdministrator: Boolean(user.isAdministrator),
    isTester: Boolean(user.isTester),
    scope: AuthScope.Default,
  };
}

export function testRunService(withContext: AsyncResourceManager<Context, void>) {
  it("Start a run", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);

      const createRunOptions: CreateRunOptions = {
        userId: alice.id,
        gameId: alpha.id,
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      const actualRun: Run = await ctx.run.createRun(aliceAuth, createRunOptions);

      const expectedRun: Run = {
        id: actualRun.id,
        createdAt: actualRun.createdAt,
        startedAt: null,
        result: null,
        user: {id: alice.id, type: ActorType.User, displayName: alice.displayName},
        game: {id: alpha.id, displayName: alpha.displayName},
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      chai.assert.deepEqual(actualRun, expectedRun);

      const actualRunStart: RunStart = await ctx.run.startRun(aliceAuth, actualRun.id);

      const expectedRunStart: RunStart = {
        run: {id: actualRun.id},
        key: actualRunStart.key,
        families: alpha.families,
        items: new Map(),
      };

      chai.assert.deepEqual(actualRunStart, expectedRunStart);
    });
  });

  it("Sets the result of a run", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);

      const createRunOptions: CreateRunOptions = {
        userId: alice.id,
        gameId: alpha.id,
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      const actualRun: Run = await ctx.run.createRun(aliceAuth, createRunOptions);

      const expectedRun: Run = {
        id: actualRun.id,
        createdAt: actualRun.createdAt,
        startedAt: null,
        result: null,
        user: {id: alice.id, type: ActorType.User, displayName: alice.displayName},
        game: {id: alpha.id, displayName: alpha.displayName},
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      chai.assert.deepEqual(actualRun, expectedRun);

      const actualRunStart: RunStart = await ctx.run.startRun(aliceAuth, actualRun.id);

      const expectedRunStart: RunStart = {
        run: {id: actualRun.id},
        key: actualRunStart.key,
        families: alpha.families,
        items: new Map(),
      };

      chai.assert.deepEqual(actualRunStart, expectedRunStart);

      const actualRunWithResult: Run = await ctx.run.setRunResult(
        aliceAuth,
        actualRun.id,
        {
          isVictory: true,
          maxLevel: 10,
          scores: [43500],
          items: new Map([["1000", 10]]),
          stats: {jumps: 10},
        });

      const expectedRunWithResults: Run = {
        ...expectedRun,
        startedAt: actualRunWithResult.startedAt,
        result: {
          createdAt: actualRunWithResult.result!.createdAt,
          isVictory: true,
          maxLevel: 10,
          scores: [43500],
          items: new Map([["1000", 10]]),
          stats: {jumps: 10},
        },
      };

      chai.assert.deepEqual(actualRunWithResult, expectedRunWithResults);
    });
  });

  it("Provides user progress to the run start", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);

      const createRunOptions: CreateRunOptions = {
        userId: alice.id,
        gameId: alpha.id,
        gameMode: "solo",
        gameOptions: ["boost"],
        settings: {
          detail: false,
          music: true,
          shake: false,
          sound: true,
          volume: 100,
        },
      };

      {
        const run0: Run = await ctx.run.createRun(aliceAuth, createRunOptions);

        const actualRunStart0: RunStart = await ctx.run.startRun(aliceAuth, run0.id);

        const expectedRunStart0: RunStart = {
          run: {id: run0.id},
          key: actualRunStart0.key,
          families: alpha.families,
          items: new Map(),
        };

        chai.assert.deepEqual(actualRunStart0, expectedRunStart0);

        await ctx.run.setRunResult(
          aliceAuth,
          run0.id,
          {
            isVictory: true,
            maxLevel: 10,
            scores: [43500],
            items: new Map([["1000", 10]]),
            stats: {jumps: 10},
          },
        );
      }

      {
        const run1: Run = await ctx.run.createRun(aliceAuth, createRunOptions);

        const actualRunStart1: RunStart = await ctx.run.startRun(aliceAuth, run1.id);

        const expectedRunStart1: RunStart = {
          run: {id: run1.id},
          key: actualRunStart1.key,
          families: alpha.families,
          items: new Map([["1000", 10]]),
        };

        chai.assert.deepEqual(actualRunStart1, expectedRunStart1);

        await ctx.run.setRunResult(
          aliceAuth,
          run1.id,
          {
            isVictory: true,
            maxLevel: 12,
            scores: [12800],
            items: new Map([["1000", 3], ["1002", 2]]),
            stats: {jumps: 5},
          },
        );
      }

      {
        const run2: Run = await ctx.run.createRun(aliceAuth, createRunOptions);

        const actualRunStart2: RunStart = await ctx.run.startRun(aliceAuth, run2.id);

        const expectedRunStart2: RunStart = {
          run: {id: run2.id},
          key: actualRunStart2.key,
          families: alpha.families,
          items: new Map([["1000", 13], ["1002", 2]]),
        };

        chai.assert.deepEqual(actualRunStart2, expectedRunStart2);

        const actualRunWithResult2: Run = await ctx.run.setRunResult(
          aliceAuth,
          run2.id,
          {
            isVictory: true,
            maxLevel: 7,
            scores: [378215],
            items: new Map([["1000", 100], ["1012", 24]]),
            stats: {jumps: 20},
          },
        );

        const expectedRunWithResults2: Run = {
          ...run2,
          startedAt: actualRunWithResult2.startedAt,
          result: {
            createdAt: actualRunWithResult2.result!.createdAt,
            isVictory: true,
            maxLevel: 7,
            scores: [378215],
            items: new Map([["1000", 100], ["1012", 24]]),
            stats: {jumps: 20},
          },
        };

        chai.assert.deepEqual(actualRunWithResult2, expectedRunWithResults2);
      }
    });
  });
}
