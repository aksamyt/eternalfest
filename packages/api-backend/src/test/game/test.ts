import { GuestAuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { File } from "@eternalfest/api-core/lib/file/file.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { CreateGameOptions } from "@eternalfest/api-core/lib/game/create-game-options.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { GameModeState } from "@eternalfest/api-core/lib/game/game-mode-state.js";
import { GameOptionState } from "@eternalfest/api-core/lib/game/game-option-state.js";
import { Game } from "@eternalfest/api-core/lib/game/game.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { ShortGame } from "@eternalfest/api-core/lib/game/short-game.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";
import { UuidHex } from "kryo/lib/uuid-hex.js";

import { getTestResource } from "../get-test-resource.js";
import { createSampleGames } from "../populate/games.js";
import { populateWithUsers } from "../populate/users.js";
import { readIntoBuffer } from "../read-into-buffer.js";
import { TIMEOUT } from "../timeout.js";

export interface Context {
  blob: BlobService;
  file: FileService;
  game: GameService;
  user: UserService;
}

const GUEST_AUTH: GuestAuthContext = {type: ActorType.Guest, scope: AuthScope.Default};

function getAuthContext(user: User): UserAuthContext {
  return {
    type: ActorType.User,
    userId: user.id,
    displayName: user.displayName,
    isAdministrator: Boolean(user.isAdministrator),
    isTester: Boolean(user.isTester),
    scope: AuthScope.Default,
  };
}

export function testGameService(withContext: AsyncResourceManager<Context, void>) {
  it("Guests cannot create a game", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {

      const options: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        resources: [],
        gameModes: [],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
      };

      try {
        await ctx.game.createGame(GUEST_AUTH, options);
        chai.assert.fail("Expected game creation to fail with guest auth");
      } catch (err) {
        chai.assert.instanceOf(err, Error);
        chai.assert.propertyVal(err, "name", "Unauthorized");
      }
    });
  });

  it("Simple users cannot create a game", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {charlie} = await populateWithUsers(ctx.user);
      const charlieAuth: UserAuthContext = getAuthContext(charlie);

      const options: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        resources: [],
        gameModes: [],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
      };

      try {
        await ctx.game.createGame(charlieAuth, options);
        chai.assert.fail("Expected game creation to fail with simple user auth");
      } catch (err) {
        chai.assert.instanceOf(err, Error);
        chai.assert.propertyVal(err, "name", "Forbidden");
      }
    });
  });

  it("Create a game without resources", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);

      const options: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        resources: [],
        gameModes: [],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
      };

      const actual: Game = await ctx.game.createGame(aliceAuth, options);

      chai.assert.isString(actual.id);
      chai.assert.instanceOf(actual.createdAt, Date);
      chai.assert.instanceOf(actual.updatedAt, Date);
      chai.assert.isTrue(actual.updatedAt.getTime() >= actual.createdAt.getTime());

      const expected: Game = {
        id: actual.id,
        key: null,
        createdAt: actual.createdAt,
        updatedAt: actual.updatedAt,
        publicationDate: null,
        author: {
          id: aliceAuth.userId,
          displayName: aliceAuth.displayName,
        },
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        iconFile: undefined,
        resources: [],
        gameModes: [],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
        locales: new Map(),
      };

      chai.assert.deepEqual(actual, expected);
    });
  });

  it("Create a game with game modes without options", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);

      const options: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        resources: [],
        gameModes: [
          {key: "solo", displayName: "Aventure", state: GameModeState.Enabled, options: [], locales: new Map()},
        ],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
      };

      const actual: Game = await ctx.game.createGame(aliceAuth, options);

      chai.assert.isString(actual.id);
      chai.assert.instanceOf(actual.createdAt, Date);
      chai.assert.instanceOf(actual.updatedAt, Date);
      chai.assert.isTrue(actual.updatedAt.getTime() >= actual.createdAt.getTime());

      const expected: Game = {
        id: actual.id,
        key: null,
        createdAt: actual.createdAt,
        updatedAt: actual.updatedAt,
        publicationDate: null,
        author: {
          id: aliceAuth.userId,
          displayName: aliceAuth.displayName,
        },
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        iconFile: undefined,
        resources: [],
        gameModes: [
          {key: "solo", displayName: "Aventure", state: GameModeState.Enabled, options: [], locales: new Map()},
        ],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
        locales: new Map(),
      };

      chai.assert.deepEqual(actual, expected);

      const retrieved: Game | undefined = await ctx.game.getGameById(aliceAuth, actual.id);
      chai.assert.deepEqual(retrieved, expected);
    });
  });

  it("Create a game with all game mode and option states", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);

      const options: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        resources: [],
        gameModes: [
          {key: "m0", displayName: "EnabledVoid", state: GameModeState.Enabled, options: [], locales: new Map()},
          {key: "m1", displayName: "DisabledVoid", state: GameModeState.Disabled, options: [], locales: new Map()},
          {
            key: "m2",
            displayName: "EnabledOptions",
            state: GameModeState.Enabled,
            options: [
              {key: "o20", displayName: "EnabledEnabled", state: GameOptionState.Enabled, locales: new Map()},
              {key: "o21", displayName: "EnabledDisabled", state: GameOptionState.Disabled, locales: new Map()},
              {key: "o22", displayName: "EnabledLocked", state: GameOptionState.Locked, locales: new Map()},
            ],
            locales: new Map(),
          },
          {
            key: "m3",
            displayName: "DisabledOptions",
            state: GameModeState.Disabled,
            options: [
              {key: "o30", displayName: "DisabledEnabled", state: GameOptionState.Enabled, locales: new Map()},
              {key: "o31", displayName: "DisabledDisabled", state: GameOptionState.Disabled, locales: new Map()},
              {key: "o32", displayName: "DisabledLocked", state: GameOptionState.Locked, locales: new Map()},
            ],
            locales: new Map(),
          },
        ],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
      };

      const actual: Game = await ctx.game.createGame(aliceAuth, options);

      chai.assert.isString(actual.id);
      chai.assert.instanceOf(actual.createdAt, Date);
      chai.assert.instanceOf(actual.updatedAt, Date);
      chai.assert.isTrue(actual.updatedAt.getTime() >= actual.createdAt.getTime());

      const expected: Game = {
        id: actual.id,
        key: null,
        createdAt: actual.createdAt,
        updatedAt: actual.updatedAt,
        publicationDate: null,
        author: {
          id: aliceAuth.userId,
          displayName: aliceAuth.displayName,
        },
        mainLocale: "fr-FR",
        displayName: "Test game",
        description: "Test game to test the game service",
        engines: {
          loader: "3.0.0",
        },
        iconFile: undefined,
        resources: [],
        gameModes: [
          {key: "m0", displayName: "EnabledVoid", state: GameModeState.Enabled, options: [], locales: new Map()},
          {key: "m1", displayName: "DisabledVoid", state: GameModeState.Disabled, options: [], locales: new Map()},
          {
            key: "m2",
            displayName: "EnabledOptions",
            state: GameModeState.Enabled,
            options: [
              {key: "o20", displayName: "EnabledEnabled", state: GameOptionState.Enabled, locales: new Map()},
              {key: "o21", displayName: "EnabledDisabled", state: GameOptionState.Disabled, locales: new Map()},
              {key: "o22", displayName: "EnabledLocked", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
          {
            key: "m3",
            displayName: "DisabledOptions",
            state: GameModeState.Disabled,
            options: [
              {key: "o30", displayName: "DisabledEnabled", state: GameOptionState.Enabled, locales: new Map()},
              {key: "o31", displayName: "DisabledDisabled", state: GameOptionState.Disabled, locales: new Map()},
              {key: "o32", displayName: "DisabledLocked", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
        ],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
        locales: new Map(),
      };

      chai.assert.deepEqual(actual, expected);

      const retrieved: Game | undefined = await ctx.game.getGameById(aliceAuth, actual.id);
      chai.assert.deepEqual(retrieved, expected);
    });
  });

  it("Create \"Sous la colline\"", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);

      const drive: Drive = await ctx.file.getDriveByOwnerId(aliceAuth, alice.id);

      const {
        icon: iconFile,
        gameSwf: gameSwfFile,
        gameXml: gameXmlFile,
        music: musicFile,
      } = await uploadSousLaCollineFiles(ctx, aliceAuth, drive.root.id);

      const options: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Sous la colline",
        description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
        engines: {
          loader: "3.0.0",
        },
        iconFileId: iconFile.id,
        resources: [
          {fileId: gameSwfFile.id, tag: "game-swf"},
          {fileId: gameXmlFile.id, tag: "game-xml"},
          {fileId: musicFile.id, tag: "music"},
          {fileId: musicFile.id, tag: "music"},
          {fileId: musicFile.id, tag: "music"},
        ],
        gameModes: [
          {
            key: "solo",
            displayName: "Aventure",
            state: GameModeState.Enabled,
            options: [
              {key: "mirror", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmare", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "ninja", displayName: "Ninjutsu", state: GameOptionState.Enabled, locales: new Map()},
              {
                key: "bombexpert",
                displayName: "Explosifs Instables",
                state: GameOptionState.Enabled,
                locales: new Map(),
              },
              {key: "boost", displayName: "Tornade", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
          {
            key: "multicoop",
            displayName: "Multi coopératif",
            state: GameModeState.Enabled,
            options: [
              {key: "mirrormulti", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmaremulti", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "lifesharing", displayName: "Partage de vies", state: GameOptionState.Enabled, locales: new Map()},
              {key: "bombotopia", displayName: "Bombotopia", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
        ],
        // tslint:disable-next-line:max-line-length
        families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
        category: GameCategory.Small,
        isPrivate: true,
      };

      const actual: Game = await ctx.game.createGame(aliceAuth, options);

      chai.assert.isString(actual.id);
      chai.assert.instanceOf(actual.createdAt, Date);
      chai.assert.instanceOf(actual.updatedAt, Date);
      chai.assert.isTrue(actual.updatedAt.getTime() >= actual.createdAt.getTime());

      const expected: Game = {
        id: actual.id,
        key: null,
        createdAt: actual.createdAt,
        updatedAt: actual.updatedAt,
        publicationDate: null,
        author: {
          id: aliceAuth.userId,
          displayName: aliceAuth.displayName,
        },
        mainLocale: "fr-FR",
        displayName: "Sous la colline",
        description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques !",
        engines: {
          loader: "3.0.0",
        },
        iconFile,
        resources: [
          {file: gameSwfFile, tag: "game-swf", priority: "high"},
          {file: gameXmlFile, tag: "game-xml", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
        ],
        gameModes: [
          {
            key: "solo",
            displayName: "Aventure",
            state: GameModeState.Enabled,
            options: [
              {key: "mirror", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmare", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "ninja", displayName: "Ninjutsu", state: GameOptionState.Enabled, locales: new Map()},
              {
                key: "bombexpert",
                displayName: "Explosifs Instables",
                state: GameOptionState.Enabled,
                locales: new Map(),
              },
              {key: "boost", displayName: "Tornade", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
          {
            key: "multicoop",
            displayName: "Multi coopératif",
            state: GameModeState.Enabled,
            options: [
              {key: "mirrormulti", displayName: "Miroir", state: GameOptionState.Enabled, locales: new Map()},
              {key: "nightmaremulti", displayName: "Cauchemar", state: GameOptionState.Enabled, locales: new Map()},
              {key: "lifesharing", displayName: "Partage de vies", state: GameOptionState.Enabled, locales: new Map()},
              {key: "bombotopia", displayName: "Bombotopia", state: GameOptionState.Enabled, locales: new Map()},
            ],
            locales: new Map(),
          },
        ],
        // tslint:disable-next-line:max-line-length
        families: "0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030,5000,5001,5002,5003,5004,5005,5006,5007,5008,5009,5010,5012,5014",
        category: GameCategory.Small,
        isPrivate: true,
        locales: new Map(),
      };

      chai.assert.deepEqual(actual, expected);

      const retrieved: Game | undefined = await ctx.game.getGameById(aliceAuth, actual.id);

      chai.assert.deepEqual(retrieved, expected);

      const foo: Buffer = await readIntoBuffer(await ctx.file.readFileContent(aliceAuth, gameXmlFile.id));

      chai.assert.instanceOf(foo, Buffer);
    });
  });

  it("Returns an empty list when there are no games", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);

      const games: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);

      chai.assert.isEmpty(games);
    });
  });

  it("Returns all the games to administrators", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);

      const firstGameOptions: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "First game",
        description: "My first game",
        engines: {
          loader: "3.0.0",
        },
        resources: [],
        gameModes: [],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
      };

      const firstGame: Game = await ctx.game.createGame(aliceAuth, firstGameOptions);

      const secondGameOptions: CreateGameOptions = {
        mainLocale: "fr-FR",
        displayName: "Second game",
        description: "My second game",
        engines: {
          loader: "3.0.0",
        },
        resources: [],
        gameModes: [],
        families: "1,2,4",
        category: GameCategory.Big,
        isPrivate: true,
      };

      const secondGame: Game = await ctx.game.createGame(aliceAuth, secondGameOptions);

      const games: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);

      chai.assert.deepEqual(games, [toShortGame(secondGame), toShortGame(firstGame)]);
    });
  });

  it("Should be possible to delete games", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha, beta} = await createSampleGames(aliceAuth, ctx.game);
      {
        const games: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);
        chai.assert.deepEqual(games, [toShortGame(beta), toShortGame(alpha)]);
      }
      await ctx.game.deleteGameById(aliceAuth, beta.id);
      {
        const games: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);
        chai.assert.deepEqual(games, [toShortGame(alpha)]);
      }
    });
  });

  it("Authors should be able to edit games (resources)", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {bob} = await populateWithUsers(ctx.user);
      const bobAuth: UserAuthContext = getAuthContext(bob);
      const {alpha} = await createSampleGames(bobAuth, ctx.game);
      const bobDrive: Drive = await ctx.file.getDriveByOwnerId(bobAuth, bob.id);

      chai.assert.isEmpty(alpha.resources);

      const {
        icon: iconFile,
        gameSwf: gameSwfFile,
        gameXml: gameXmlFile,
        music: musicFile,
      } = await uploadSousLaCollineFiles(ctx, bobAuth, bobDrive.root.id);

      const updatedAlpha: Game = await ctx.game.updateGame(bobAuth, alpha.id, {
        description: "foo'bar",
        iconFileId: iconFile.id,
        resources: [
          {fileId: gameSwfFile.id, tag: "game-swf", priority: "high"},
          {fileId: gameXmlFile.id, tag: "game-xml", priority: "high"},
          {fileId: musicFile.id, tag: "music", priority: "high"},
          {fileId: musicFile.id, tag: "music", priority: "high"},
          {fileId: musicFile.id, tag: "music", priority: "high"},
        ],
      });

      const expectedUpdatedAlpha: Game = {
        ...alpha,
        description: "foo'bar",
        updatedAt: updatedAlpha.updatedAt,
        iconFile,
        resources: [
          {file: gameSwfFile, tag: "game-swf", priority: "high"},
          {file: gameXmlFile, tag: "game-xml", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
          {file: musicFile, tag: "music", priority: "high"},
        ],
      };

      chai.assert.deepEqual(updatedAlpha, expectedUpdatedAlpha);
    });
  });

  it("Administrators can set the `publicationDate` value", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);

      chai.assert.propertyVal(alpha, "publicationDate", null);
      const now: Date = new Date();
      const updatedAlpha: Game = await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: now});
      chai.assert.deepEqual(updatedAlpha.id, alpha.id);
      chai.assert.deepEqual(updatedAlpha.publicationDate, now);
      const updatedAlpha2: Game = await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: null});
      chai.assert.deepEqual(updatedAlpha2.id, alpha.id);
      chai.assert.deepEqual(updatedAlpha2.publicationDate, null);
    });
  });

  it("Getting all games should respect authorizations", async function (this: Mocha.Context) {
    // TODO: Test with game with resources
    this.timeout(TIMEOUT);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice, bob, charlie} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const bobAuth: UserAuthContext = getAuthContext(bob);
      const charlieAuth: UserAuthContext = getAuthContext(charlie);

      chai.assert.propertyVal(aliceAuth, "isAdministrator", true);
      chai.assert.propertyVal(aliceAuth, "isTester", true);
      chai.assert.propertyVal(bobAuth, "isAdministrator", false);
      chai.assert.propertyVal(bobAuth, "isTester", true);
      chai.assert.propertyVal(charlieAuth, "isAdministrator", false);
      chai.assert.propertyVal(charlieAuth, "isTester", false);

      const {alpha, beta} = await createSampleGames(aliceAuth, ctx.game);

      chai.assert.propertyVal(alpha, "publicationDate", null);
      chai.assert.propertyVal(alpha, "isPrivate", true);
      chai.assert.propertyVal(beta, "publicationDate", null);
      chai.assert.propertyVal(beta, "isPrivate", true);

      {
        const aliceGames: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);
        const bobGames: readonly ShortGame[] = await ctx.game.getGames(bobAuth);
        const charlieGames: readonly ShortGame[] = await ctx.game.getGames(charlieAuth);
        const guestGames: readonly ShortGame[] = await ctx.game.getGames(GUEST_AUTH);
        chai.assert.lengthOf(aliceGames, 2, "Admin should see private non-published games");
        chai.assert.lengthOf(bobGames, 0, "Tester should not see private non-published games");
        chai.assert.lengthOf(charlieGames, 0, "Simple user should not see private non-published games");
        chai.assert.lengthOf(guestGames, 0, "Guest should not see private non-published games");
      }

      const DAY_MS: number = 24 * 3600 * 1000;
      const past: Date = new Date(Date.now() - DAY_MS);
      const future: Date = new Date(Date.now() + DAY_MS);

      await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: past});
      await ctx.game.updateGame(aliceAuth, beta.id, {publicationDate: future});

      {
        const aliceGames: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);
        const bobGames: readonly ShortGame[] = await ctx.game.getGames(bobAuth);
        const charlieGames: readonly ShortGame[] = await ctx.game.getGames(charlieAuth);
        const guestGames: readonly ShortGame[] = await ctx.game.getGames(GUEST_AUTH);
        chai.assert.lengthOf(aliceGames, 2, "Admin should see private published (past and future) games");
        chai.assert.lengthOf(bobGames, 0, "Tester should not see private published (past and future) games");
        chai.assert.lengthOf(charlieGames, 0, "Simple user should not see private published (past and future) games");
        chai.assert.lengthOf(guestGames, 0, "Guest should not see private published (past and future) games");
      }

      await ctx.game.updateGame(aliceAuth, alpha.id, {isPrivate: false});
      await ctx.game.updateGame(aliceAuth, beta.id, {isPrivate: false});

      {
        const aliceGames: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);
        const bobGames: readonly ShortGame[] = await ctx.game.getGames(bobAuth);
        const charlieGames: readonly ShortGame[] = await ctx.game.getGames(charlieAuth);
        const guestGames: readonly ShortGame[] = await ctx.game.getGames(GUEST_AUTH);
        chai.assert.lengthOf(aliceGames, 2, "Admin should see public published (past and future) games");
        chai.assert.lengthOf(bobGames, 2, "Tester should see public published (past and future) games");
        chai.assert.lengthOf(charlieGames, 1, "Simple user should see public already published (past) games");
        chai.assert.strictEqual(charlieGames[0].id, alpha.id);
        chai.assert.lengthOf(guestGames, 1, "Guest user should see public published games");
        chai.assert.strictEqual(guestGames[0].id, alpha.id);
      }

      await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: null});
      await ctx.game.updateGame(aliceAuth, beta.id, {publicationDate: null});
      {
        const aliceGames: readonly ShortGame[] = await ctx.game.getGames(aliceAuth);
        const bobGames: readonly ShortGame[] = await ctx.game.getGames(bobAuth);
        const charlieGames: readonly ShortGame[] = await ctx.game.getGames(charlieAuth);
        const guestGames: readonly ShortGame[] = await ctx.game.getGames(GUEST_AUTH);
        chai.assert.lengthOf(aliceGames, 2, "Admin should see public non-published games");
        chai.assert.lengthOf(bobGames, 2, "Tester should see public non-published games");
        chai.assert.lengthOf(charlieGames, 0, "Simple user should not see public non-published games");
        chai.assert.lengthOf(guestGames, 0, "Guest should not see public non-published games");
      }
    });
  });
}

interface SousLaCollineFiles {
  icon: File;
  gameSwf: File;
  gameXml: File;
  music: File;
}

async function uploadSousLaCollineFiles(
  ctx: Context,
  userAuth: UserAuthContext,
  directoryId: UuidHex,
): Promise<SousLaCollineFiles> {
  async function createFile(components: string[], mediaType: string): Promise<File> {
    const buffer: Buffer = await getTestResource(["example-games", "sous-la-colline", ...components]);
    const blob: Blob = await ctx.blob.createBlob(userAuth, {mediaType, data: buffer});
    return ctx.file.createFile(userAuth, {
      parentId: directoryId,
      displayName: components[components.length - 1],
      blobId: blob.id,
    });
  }

  const icon: File = await createFile(["icon.png"], "image/png");
  const gameSwf: File = await createFile(["game.swf"], "application/x-shockwave-flash");
  const gameXml: File = await createFile(["game.xml"], "application/xml");
  const music: File = await createFile(["music", "rourou.mp3"], "music/mp3");

  return {icon, gameSwf, gameXml, music};
}

function toShortGame(game: Game): ShortGame {
  return {
    id: game.id,
    key: game.key,
    createdAt: game.createdAt,
    updatedAt: game.updatedAt,
    publicationDate: game.publicationDate,
    author: game.author,
    mainLocale: game.mainLocale,
    displayName: game.displayName,
    description: game.description,
    category: game.category,
    iconFile: game.iconFile,
    isPrivate: game.isPrivate,
    locales: game.locales,
  };
}
