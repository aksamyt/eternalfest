import { PgBlobService } from "../../lib/blob/pg-service.js";
import { PgFileService } from "../../lib/file/pg-service.js";
import { PgGameService } from "../../lib/game/pg-service.js";
import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { PgUserService } from "../../lib/user/pg-service.js";
import { DbState, StateService, withTestState } from "../test-db.js";
import { Context, testGameService } from "./test.js";

describe("PgGameService", function () {
  testGameService(withPgGameService);
});

async function withPgGameService<R>(handler: (context: Context) => Promise<R>): Promise<R> {
  return withTestState(DbState.Empty, async ({db, buffer}: StateService): Promise<R> => {
    const blob: PgBlobService = new PgBlobService(db, buffer);
    const password: ScryptPasswordService = new ScryptPasswordService(0.1);
    const user: PgUserService = new PgUserService(db, password);
    const file: PgFileService = new PgFileService(db, blob);
    const game: PgGameService = new PgGameService(db, user, file);
    return handler({blob, file, game, user});
  });
}
