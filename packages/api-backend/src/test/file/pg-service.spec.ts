import { PgBlobService } from "../../lib/blob/pg-service.js";
import { PgFileService } from "../../lib/file/pg-service.js";
import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { PgUserService } from "../../lib/user/pg-service.js";
import { DbState, StateService, withTestState } from "../test-db.js";
import { Context, testFileService } from "./test.js";

async function withPgFileService<R>(handler: (context: Context) => Promise<R>): Promise<R> {
  return withTestState(DbState.Empty, async ({db, buffer}: StateService): Promise<R> => {
    const blobService: PgBlobService = new PgBlobService(db, buffer);
    const passwordService: ScryptPasswordService = new ScryptPasswordService(0.1);
    const userService: PgUserService = new PgUserService(db, passwordService);
    const fileService: PgFileService = new PgFileService(db, blobService);
    return handler({blobService, fileService, userService});
  });
}

describe("PgFileService", function () {
  testFileService(withPgFileService);
});
