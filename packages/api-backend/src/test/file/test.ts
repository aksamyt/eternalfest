import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { Blob } from "@eternalfest/api-core/lib/blob/blob.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { Directory } from "@eternalfest/api-core/lib/file/directory.js";
import { DriveItemType } from "@eternalfest/api-core/lib/file/drive-item-type.js";
import { Drive } from "@eternalfest/api-core/lib/file/drive.js";
import { File } from "@eternalfest/api-core/lib/file/file.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";
import stream from "stream";

import { readIntoBuffer } from "../read-into-buffer.js";
import { TIMEOUT } from "../timeout.js";

export interface Context {
  userService: UserService;
  fileService: FileService;
  blobService: BlobService;
}

const GUEST_AUTH: AuthContext = {type: ActorType.Guest, scope: AuthScope.Default};

async function createUsers(userService: UserService): Promise<[User, User]> {
  const alice: User = await userService.createUserWithCredentials(GUEST_AUTH, {
    displayName: "Alice",
    login: "alice",
    password: Buffer.from("aaaaaa"),
  });
  const bob: User = await userService.createUserWithCredentials(GUEST_AUTH, {
    displayName: "Bob",
    login: "bob",
    password: Buffer.from("bbbbbb"),
  });
  return [alice, bob];
}

function getAuthContext(user: User): AuthContext {
  return {
    type: ActorType.User,
    userId: user.id,
    displayName: user.displayName,
    isAdministrator: Boolean(user.isAdministrator),
    isTester: Boolean(user.isTester),
    scope: AuthScope.Default,
  };
}

export function testFileService(withContext: AsyncResourceManager<Context, void>) {
  it("Get your own drive", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      chai.assert.isString(drive.id);
      chai.assert.isObject(drive.root);
      chai.assert.isString(drive.root.id);
      chai.assert.instanceOf(drive.createdAt, Date);
      chai.assert.instanceOf(drive.updatedAt, Date);
      chai.assert.isTrue(drive.updatedAt.getTime() >= drive.createdAt.getTime());
      chai.assert.isObject(drive.owner);
      chai.assert.strictEqual(drive.owner.id, alice.id);
    });
  });

  it("Getting the drive of a user as a guest should file (first access)", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];

      try {
        await fileService.getDriveByOwnerId(GUEST_AUTH, alice.id);
        chai.assert.fail(undefined, undefined, "expected an error");
      } catch (err) {
        chai.assert.instanceOf(err, Error);
        chai.assert.strictEqual(err.name, "Unauthorized");
      }
    });
  });

  it("Getting the drive of a user as a guest should fail (subsequent access)", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);
      await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      try {
        await fileService.getDriveByOwnerId(GUEST_AUTH, alice.id);
        chai.assert.fail(undefined, undefined, "expected an error");
      } catch (err) {
        chai.assert.instanceOf(err, Error);
        chai.assert.strictEqual(err.name, "Unauthorized");
      }
    });
  });

  it("Geting the drive by owner twice should return the same drive", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const firstDrive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);
      const secondDrive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      chai.assert.deepEqual(secondDrive, firstDrive);
    });
  });

  it("Geting the drive by owner and then by id should return the same drive", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const firstDrive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);
      const secondDrive: Drive = await fileService.getDriveById(aliceAuth, firstDrive.id);

      chai.assert.deepEqual(secondDrive, firstDrive);
    });
  });

  it("Should be possible to get the root directory of a drive", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);
      const directory: Directory = await fileService.getDirectoryById(aliceAuth, drive.root.id);

      chai.assert.strictEqual(directory.id, drive.root.id);
      chai.assert.isString(directory.displayName);
      chai.assert.instanceOf(directory.createdAt, Date);
      chai.assert.instanceOf(directory.updatedAt, Date);
      chai.assert.isTrue(directory.updatedAt.getTime() >= directory.createdAt.getTime());

      const expected: Directory = {
        id: drive.root.id,
        type: DriveItemType.Directory,
        displayName: directory.displayName,
        createdAt: directory.createdAt,
        updatedAt: directory.updatedAt,
      };

      chai.assert.deepEqual(directory, expected);
    });
  });

  it("Should be possible to create a directory at the root of your own drive", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      const subDir: Directory = await fileService.createDirectory(
        aliceAuth,
        {parentId: drive.root.id, displayName: "Images"},
      );

      chai.assert.isString(subDir.id);
      chai.assert.instanceOf(subDir.createdAt, Date);
      chai.assert.instanceOf(subDir.updatedAt, Date);
      chai.assert.isTrue(subDir.updatedAt.getTime() >= subDir.createdAt.getTime());

      const expected: Directory = {
        id: subDir.id,
        type: DriveItemType.Directory,
        displayName: "Images",
        createdAt: subDir.createdAt,
        updatedAt: subDir.updatedAt,
        children: [],
      };

      chai.assert.deepEqual(subDir, expected);

      chai.assert.isTrue(subDir.createdAt.getTime() >= drive.updatedAt.getTime());
    });
  });

  it(
    "Should be prevent simple users from creating directories in drives they don't own",
    async function (this: Mocha.Context) {
      this.timeout(TIMEOUT);
      return withContext(async ({userService, fileService}: Context): Promise<void> => {
        const [alice, bob] = await createUsers(userService);
        const aliceAuth: AuthContext = getAuthContext(alice);
        const bobAuth: AuthContext = getAuthContext(bob);

        const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

        try {
          await fileService.createDirectory(bobAuth, {parentId: drive.root.id, displayName: "Images"});
          chai.assert.fail(undefined, undefined, "expected an error");
        } catch (err) {
          chai.assert.instanceOf(err, Error);
          chai.assert.strictEqual(err.name, "Unauthorized");
        }
      });
    },
  );

  it("Should be possible to create a sub-directories in your own drive", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      const imgDir: Directory = await fileService.createDirectory(
        aliceAuth,
        {parentId: drive.root.id, displayName: "Images"},
      );

      const subDir: Directory = await fileService.createDirectory(
        aliceAuth,
        {parentId: imgDir.id, displayName: "Vacations"},
      );

      chai.assert.isString(subDir.id);
      chai.assert.instanceOf(subDir.createdAt, Date);
      chai.assert.instanceOf(subDir.updatedAt, Date);
      chai.assert.isTrue(subDir.updatedAt.getTime() >= subDir.createdAt.getTime());

      const expected: Directory = {
        id: subDir.id,
        type: DriveItemType.Directory,
        displayName: "Vacations",
        createdAt: subDir.createdAt,
        updatedAt: subDir.updatedAt,
        children: [],
      };

      chai.assert.deepEqual(subDir, expected);

      chai.assert.isTrue(subDir.createdAt.getTime() >= imgDir.updatedAt.getTime());
    });
  });

  it("Should be possible to create files in your drive", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({blobService, userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const blob: Blob = await blobService.createBlob(
        GUEST_AUTH,
        {mediaType: "text/plain", data: Buffer.from("Hello, World!", "UTF-8" as any)},
      );

      const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      const file: File = await fileService.createFile(
        aliceAuth,
        {parentId: drive.root.id, displayName: "hello.txt", blobId: blob.id},
      );

      chai.assert.isString(file.id);
      chai.assert.instanceOf(file.createdAt, Date);
      chai.assert.instanceOf(file.updatedAt, Date);
      chai.assert.isTrue(file.updatedAt.getTime() >= file.createdAt.getTime());

      const expected: File = {
        id: file.id,
        type: DriveItemType.File,
        displayName: "hello.txt",
        createdAt: file.createdAt,
        updatedAt: file.updatedAt,
        mediaType: "text/plain",
        byteSize: 13,
      };

      chai.assert.deepEqual(file, expected);
    });
  });

  it("Should not be possible to create a file in a file", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({blobService, userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const blob: Blob = await blobService.createBlob(
        GUEST_AUTH,
        {mediaType: "text/plain", data: Buffer.from("Hello, World!", "UTF-8" as any)},
      );

      const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      const file: File = await fileService.createFile(
        aliceAuth,
        {parentId: drive.root.id, displayName: "hello.txt", blobId: blob.id},
      );

      try {
        await fileService.createFile(aliceAuth, {parentId: file.id, displayName: "hello.txt", blobId: blob.id});
        chai.assert.fail(undefined, undefined, "expected an error");
      } catch (err) {
        chai.assert.instanceOf(err, Error);
        chai.assert.strictEqual(err.name, "Unauthorized");
      }
    });
  });

  it("Should be possible to read a file", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({blobService, userService, fileService}: Context): Promise<void> => {
      const alice: User = (await createUsers(userService))[0];
      const aliceAuth: AuthContext = getAuthContext(alice);

      const blob: Blob = await blobService.createBlob(
        GUEST_AUTH,
        {mediaType: "text/plain", data: Buffer.from("Hello, World!", "UTF-8" as any)},
      );

      const drive: Drive = await fileService.getDriveByOwnerId(aliceAuth, alice.id);

      const file: File = await fileService.createFile(
        aliceAuth,
        {parentId: drive.root.id, displayName: "hello.txt", blobId: blob.id},
      );

      const stream: stream.Readable = await fileService.readFileContent(GUEST_AUTH, file.id);
      const buffer: Buffer = await readIntoBuffer(stream);

      chai.assert.deepEqual(buffer, Buffer.from("Hello, World!", "UTF-8" as any));
    });
  });
}
