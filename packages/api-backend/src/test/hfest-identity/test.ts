// import { UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
// import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
// import { HfestIdentity } from "@eternalfest/api-core/lib/hfest-identity/hfest-identity.js";
import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
// import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
// import hfServer from "@eternalfest/hammerfest-api/api/get-server.js";
import hfApiTypes from "@eternalfest/hammerfest-api/types/api.js";

// import chai from "chai";
// import { populateWithUsers } from "../populate/users.js";
import { TIMEOUT } from "../timeout.js";

export interface Context {
  hfestApi: hfApiTypes.HammerfestApi;
  hfestIdentity: HfestIdentityService;
  user: UserService;
}

export function testHfestIdentityService(_withContext: AsyncResourceManager<Context, void>) {
  it("Create a new Hammerfest identity", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    this.skip(); // AuthMigration
    // return withContext(async (ctx: Context): Promise<void> => {
    //   const hfestSession: hfApiTypes.Session = await ctx.hfestApi.createSession({
    //     server: hfServer.SERVER_FR,
    //     username: "bob",
    //     password: "BOB",
    //   });
    //   const identity: HfestIdentity = await ctx.hfestIdentity.createOrUpdateHfestIdentity(
    //     ctx.hfestApi.exportSession(hfestSession),
    //   );
    //   chai.assert.isString(identity.id);
    //   chai.assert.instanceOf(identity.createdAt, Date);
    //   chai.assert.instanceOf(identity.updatedAt, Date);
    //   chai.assert.isTrue(identity.updatedAt.getTime() >= identity.createdAt.getTime());
    // });
  });

  it("Links an Hammerfest identity to a user", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    this.skip(); // AuthMigration
    // return withContext(async (ctx: Context): Promise<void> => {
    //   const {bob} = await populateWithUsers(ctx.user);
    //   const bobAuth: UserAuthContext = {
    //     type: ActorType.User,
    //     scope: AuthScope.Default,
    //     isAdministrator: false,
    //     isTester: true,
    //     displayName: "bob",
    //     userId: bob.id,
    //   };
    //   const hfestSession: hfApiTypes.Session = await ctx.hfestApi.createSession({
    //     server: hfServer.SERVER_FR,
    //     username: "bob",
    //     password: "BOB",
    //   });
    //   const identity: HfestIdentity = await ctx.hfestIdentity.createOrUpdateHfestIdentity(
    //     ctx.hfestApi.exportSession(hfestSession),
    //   );
    //   await ctx.hfestIdentity.linkUser(bobAuth, {identityId: identity.id});
    // });
  });

  it("Links an Hammerfest identity from all EN, ES and FR servers to a user", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    this.skip(); // AuthMigration
    // return withContext(async (ctx: Context): Promise<void> => {
    //   const {bob} = await populateWithUsers(ctx.user);
    //   const bobAuth: UserAuthContext = {
    //     type: ActorType.User,
    //     scope: AuthScope.Default,
    //     isAdministrator: false,
    //     isTester: true,
    //     displayName: "bob",
    //     userId: bob.id,
    //   };
    //
    //   async function link(server: string, username: string, password: string): Promise<void> {
    //     const hfestSession: hfApiTypes.Session = await ctx.hfestApi.createSession({server, username, password});
    //     const identity: HfestIdentity = await ctx.hfestIdentity.createOrUpdateHfestIdentity(
    //       ctx.hfestApi.exportSession(hfestSession),
    //     );
    //     await ctx.hfestIdentity.linkUser(bobAuth, {identityId: identity.id});
    //   }
    //
    //   await link(hfServer.SERVER_EN, "robert", "ROBERT");
    //   await link(hfServer.SERVER_ES, "roberto", "ROBERTO");
    //   await link(hfServer.SERVER_FR, "bob", "BOB");
    // });
  });

  // tslint:disable-next-line:max-line-length
  it("Rejects linking multiple hfest identities from the same server to the same user", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    this.skip(); // AuthMigration
    // return withContext(async (ctx: Context): Promise<void> => {
    //   const {bob} = await populateWithUsers(ctx.user);
    //   const bobAuth: UserAuthContext = {
    //     type: ActorType.User,
    //     scope: AuthScope.Default,
    //     isAdministrator: false,
    //     isTester: true,
    //     displayName: "bob",
    //     userId: bob.id,
    //   };
    //
    //   async function link(server: string, username: string, password: string): Promise<void> {
    //     const hfestSession: hfApiTypes.Session = await ctx.hfestApi.createSession({server, username, password});
    //     const identity: HfestIdentity = await ctx.hfestIdentity.createOrUpdateHfestIdentity(
    //       ctx.hfestApi.exportSession(hfestSession),
    //     );
    //     await ctx.hfestIdentity.linkUser(bobAuth, {identityId: identity.id});
    //   }
    //
    //   await link(hfServer.SERVER_EN, "robert", "ROBERT");
    //   await link(hfServer.SERVER_ES, "roberto", "ROBERTO");
    //   await link(hfServer.SERVER_FR, "bob", "BOB");
    //
    //   const extrSession: hfApiTypes.Session = await ctx.hfestApi.createSession({
    //     server: hfServer.SERVER_FR,
    //     username: "bob2",
    //     password: "BOB2",
    //   });
    //   const extraIdentity: HfestIdentity = await ctx.hfestIdentity.createOrUpdateHfestIdentity(
    //     ctx.hfestApi.exportSession(extrSession),
    //   );
    //   let error: Error | undefined;
    //   try {
    //     await ctx.hfestIdentity.linkUser(bobAuth, {identityId: extraIdentity.id});
    //   } catch (err) {
    //     error = err;
    //   }
    //   chai.assert.instanceOf(error, Error, "Expected `linkUser` to fail");
    // });
  });
}
