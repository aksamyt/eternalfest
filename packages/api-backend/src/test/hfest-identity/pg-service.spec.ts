import hfApi from "@eternalfest/hammerfest-api/types/api.js";

import { Database } from "../../lib/db/database.js";
import { PgHfestIdentityService } from "../../lib/hfest-identity/pg-service.js";
import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { PgUserService } from "../../lib/user/pg-service.js";
import { createMockHammerfestApi } from "../mock-hammerfest-api.js";
import { DbState, withTestDb } from "../test-db.js";
import { Context, testHfestIdentityService } from "./test.js";

describe("PgHfestIdentityService", function () {
  testHfestIdentityService(withPgHfestIdentityService);
});

async function withPgHfestIdentityService<R>(
  handler: (context: Context) => Promise<R>,
): Promise<R> {
  return withTestDb(DbState.Empty, async (db: Database): Promise<R> => {
    const hfestApi: hfApi.HammerfestApi = createMockHammerfestApi();
    const hfestIdentity: PgHfestIdentityService = new PgHfestIdentityService(db, hfestApi);
    const password: ScryptPasswordService = new ScryptPasswordService(0.1);
    const user: PgUserService = new PgUserService(db, password);
    return handler({hfestApi, hfestIdentity, user});
  });
}
