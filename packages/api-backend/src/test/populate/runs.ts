import { UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { Run } from "@eternalfest/api-core/lib/run/run.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { SetRunResultOptions } from "@eternalfest/api-core/lib/run/set-run-result-options.js";

import { UuidHex } from "kryo/lib/uuid-hex.js";

export interface SampleRuns {
  createdSoloRun: Run;
  startedSoloRun: Run;
  finishedSoloRun: Run;
}

export async function createRunWithResult(userAuth: UserAuthContext, runService: RunService, runOptions: CreateRunOptions, resultOptions: SetRunResultOptions): Promise<Run> {
  const run: Run = await runService.createRun(userAuth, runOptions);
  await runService.startRun(userAuth, run.id);
  return runService.setRunResult(userAuth, run.id, resultOptions);
}

export async function createSampleRuns(userAuth: UserAuthContext, runService: RunService, gameId: UuidHex): Promise<SampleRuns> {
  const createRunOptions: CreateRunOptions = {
    userId: userAuth.userId,
    gameId: gameId,
    gameMode: "solo",
    gameOptions: ["boost"],
    settings: {
      detail: false,
      music: true,
      shake: false,
      sound: false,
      volume: 100,
    },
  };

  const createdSoloRun: Run = await runService.createRun(userAuth, createRunOptions);

  const startedSoloRun: Run = await runService.createRun(userAuth, createRunOptions);
  await runService.startRun(userAuth, startedSoloRun.id);

  const finishedSoloRun: Run = await createRunWithResult(userAuth, runService, createRunOptions, {
    isVictory: false,
    maxLevel: 44,
    scores: [4],
    items: new Map<string, number>(),
    stats: {},
  });

  return {createdSoloRun, startedSoloRun, finishedSoloRun};
}
