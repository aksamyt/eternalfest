import { UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { GameCategory } from "@eternalfest/api-core/lib/game/game-category.js";
import { Game } from "@eternalfest/api-core/lib/game/game.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";

export interface SampleGames {
  alpha: Game;
  beta: Game;
}

export async function createSampleGames(userAuth: UserAuthContext, gameService: GameService): Promise<SampleGames> {
  const alpha: Game = await gameService.createGame(userAuth, {
    mainLocale: "fr-FR",
    displayName: "Alpha",
    description: "Alpha game description.",
    engines: {
      loader: "3.0.0",
    },
    iconFileId: undefined,
    resources: [],
    gameModes: [],
    families: "1,2,4",
    category: GameCategory.Big,
    isPrivate: true,
  });
  const beta: Game = await gameService.createGame(userAuth, {
    mainLocale: "fr-FR",
    displayName: "Beta",
    description: "Beta game description.",
    engines: {
      loader: "3.0.0",
    },
    iconFileId: undefined,
    resources: [],
    gameModes: [],
    families: "10,20,40",
    category: GameCategory.Big,
    isPrivate: true,
  });
  return {alpha, beta};
}
