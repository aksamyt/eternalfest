import { AuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";

export interface PopulateUsers {
  alice: User;
  bob: User;
  charlie: User;
}

const GUEST_AUTH: AuthContext = {type: ActorType.Guest, scope: AuthScope.Default};

function toUserAuth(user: User): UserAuthContext {
  return {
    type: ActorType.User,
    scope: AuthScope.Default,
    displayName: user.displayName,
    userId: user.id,
    isTester: user.isTester!,
    isAdministrator: user.isAdministrator,
  };
}

export async function populateWithUsers(userService: UserService): Promise<PopulateUsers> {
  const alice: User = await userService.createUserWithCredentials(GUEST_AUTH, {
    displayName: "Alice",
    login: "alice",
    password: Buffer.from("ALICE"),
  });
  const aliceAuth: UserAuthContext = toUserAuth(alice);
  const simpleBob: User = await userService.createUserWithCredentials(GUEST_AUTH, {
    displayName: "Bob",
    login: "bob",
    password: Buffer.from("BOB"),
  });
  const bob: User = await userService.updateUser(aliceAuth, {userId: simpleBob.id, isTester: true});
  const charlie: User = await userService.createUserWithCredentials(GUEST_AUTH, {
    displayName: "Charlie",
    login: "charlie",
    password: Buffer.from("CHARLIE"),
  });

  return {alice, bob, charlie};
}
