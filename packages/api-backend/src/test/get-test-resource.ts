import fs from "fs";
import sysPath from "path";

import meta from "./meta.js";

export async function getTestResource(components: string[]): Promise<Buffer> {
  return new Promise<Buffer>((resolve, reject): void => {
    const resolvedPath: string = sysPath.resolve(meta.dirname, "..", "test-resources", ...components);
    fs.readFile(resolvedPath, (err: Error | null, buffer: Buffer): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(buffer);
      }
    });
  });
}
