import { PasswordService } from "@eternalfest/api-core/lib/password/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";

import { Database } from "../../lib/db/database.js";
import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { PgSessionService } from "../../lib/session/pg-service.js";
import { PgUserService } from "../../lib/user/pg-service.js";
import { DbState, withTestDb } from "../test-db.js";
import { Context, testSessionService } from "./test.js";

describe("PgSessionService", function () {
  testSessionService(withPgSessionService);
});

async function withPgSessionService<R>(
  handler: (context: Context) => Promise<R>,
): Promise<R> {
  return withTestDb(DbState.Empty, async (db: Database): Promise<R> => {
    const password: PasswordService = new ScryptPasswordService();
    const user: UserService = new PgUserService(db, password);
    const session: SessionService = new PgSessionService(db, 86400);
    return handler({session, user});
  });
}
