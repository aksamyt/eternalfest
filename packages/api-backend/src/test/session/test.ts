import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import { Session } from "@eternalfest/api-core/lib/types/session.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";
import { TIMEOUT } from "../timeout.js";

export interface Context {
  session: SessionService;
  user: UserService;
}

export function testSessionService(withContext: AsyncResourceManager<Context, void>) {
  it("Create a new session", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withContext(async ({session, user}): Promise<void> => {
      const alice: User = await user.createUserWithCredentials(
        {type: ActorType.System},
        {
          login: "alice",
          password: Buffer.from("ALICE"),
          displayName: "Alice",
          email: "alice@example.com",
        },
      );

      const aliceSession: Session = await session.createSession({type: ActorType.System}, alice.id);

      chai.assert.isObject(aliceSession.user);
      chai.assert.propertyVal(aliceSession.user, "id", alice.id);
    });
  });
}
