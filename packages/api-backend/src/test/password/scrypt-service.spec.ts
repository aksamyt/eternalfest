import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { Context, testPasswordService } from "./test.js";

describe("ScryptPasswordService", function () {
  testPasswordService(withScryptPasswordService);
});

async function withScryptPasswordService<R>(handler: (ctx: Context) => Promise<R>): Promise<R> {
  const password: ScryptPasswordService = new ScryptPasswordService(0.1);
  return handler({password});
}
