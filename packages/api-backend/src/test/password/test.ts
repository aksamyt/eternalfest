import { PasswordService } from "@eternalfest/api-core/lib/password/service.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import chai from "chai";

export interface Context {
  password: PasswordService;
}

export function testPasswordService(withPasswordService: AsyncResourceManager<Context, void>) {
  it("Hash and verify the result", async function () {
    return withPasswordService(async (ctx: Context): Promise<void> => {
      const clearText: Buffer = Buffer.from("hunter2");
      const hash: Uint8Array = await ctx.password.hash(clearText);
      const verified: boolean = await ctx.password.verify(hash, Buffer.from("hunter2"));
      chai.assert.isTrue(verified);
    });
  });
}
