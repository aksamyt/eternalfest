import { PasswordService } from "@eternalfest/api-core/lib/password/service.js";

import { Database } from "../../lib/db/database.js";
import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { PgUserService } from "../../lib/user/pg-service.js";
import { DbState, withTestDb } from "../test-db.js";
import { Context, testUserService } from "./test.js";

describe("PgUserService", function () {
  testUserService(withPgUserService);
});

async function withPgUserService<R>(handler: (ctx: Context) => Promise<R>): Promise<R> {
  return withTestDb(DbState.Empty, async (db: Database): Promise<R> => {
    const password: PasswordService = new ScryptPasswordService(0.1);
    const user: PgUserService = new PgUserService(db, password);
    return handler({user});
  });
}
