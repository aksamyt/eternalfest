import { GuestAuthContext, UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";

import { TIMEOUT } from "../timeout.js";

const GUEST_AUTH_CONTEXT: GuestAuthContext = {
  type: ActorType.Guest,
  scope: AuthScope.Default,
};

function toUserAuth(user: User): UserAuthContext {
  return {
    type: ActorType.User,
    scope: AuthScope.Default,
    displayName: user.displayName,
    userId: user.id,
    isTester: user.isTester!,
    isAdministrator: user.isAdministrator,
  };
}

export interface Context {
  user: UserService;
}

export function testUserService(withUserService: AsyncResourceManager<Context, void>) {
  it("Create a user", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withUserService(async (ctx: Context): Promise<void> => {
      const user: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Demurgos",
        login: "demurgos",
        password: Buffer.from("hunter2"),
      });

      chai.assert.isString(user.id);
      chai.assert.instanceOf(user.createdAt, Date);
      chai.assert.instanceOf(user.updatedAt, Date);
      chai.assert.isTrue(user.updatedAt.getTime() >= user.createdAt.getTime());

      const expected: User = {
        id: user.id,
        type: ActorType.User,
        displayName: "Demurgos",
        isAdministrator: true,
        isTester: true,
        hasPassword: true,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
        identities: [],
      };

      chai.assert.deepEqual(user, expected);
    });
  });

  it("Create a user and retrieve it as a guest", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withUserService(async (ctx: Context): Promise<void> => {
      const user: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Demurgos",
        login: "demurgos",
        password: Buffer.from("hunter2"),
      });

      const retrieved: User | undefined = await ctx.user.getUserById(GUEST_AUTH_CONTEXT, user.id);

      const expected: User = {
        id: user.id,
        type: ActorType.User,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
        identities: [],
        isAdministrator: true,
        isTester: undefined,
        hasPassword: undefined,
        displayName: "Demurgos",
      };

      chai.assert.deepEqual(retrieved, expected);
    });
  });

  it("Create a user and retrieve it as himself", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withUserService(async (ctx: Context): Promise<void> => {
      const user: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Demurgos",
        login: "demurgos",
        password: Buffer.from("hunter2"),
      });

      const userAuth: UserAuthContext = toUserAuth(user);

      const retrieved: User | undefined = await ctx.user.getUserById(userAuth, user.id);

      const expected: User = {
        id: user.id,
        type: ActorType.User,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
        identities: [],
        isAdministrator: true,
        isTester: true,
        hasPassword: true,
        displayName: "Demurgos",
      };

      chai.assert.deepEqual(retrieved, expected);
    });
  });

  it("Create a user and changes his display name", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withUserService(async (ctx: Context): Promise<void> => {
      const aliceUser: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Alice",
        login: "alice",
        password: Buffer.from("ALICE"),
      });

      const aliceAuth: UserAuthContext = toUserAuth(aliceUser);

      const updatedUser: User = await ctx.user.updateUser(aliceAuth, {displayName: "Demurgos"});

      const expected: User = {
        id: aliceUser.id,
        type: ActorType.User,
        createdAt: aliceUser.createdAt,
        updatedAt: updatedUser.updatedAt,
        identities: [],
        isAdministrator: true,
        isTester: true,
        hasPassword: true,
        displayName: "Demurgos",
      };

      chai.assert.deepEqual(updatedUser, expected);
    });
  });

  it("Administrators can set the `isTester` value for users", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withUserService(async (ctx: Context): Promise<void> => {
      const aliceUser: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Alice",
        login: "alice",
        password: Buffer.from("ALICE"),
      });

      const aliceAuth: UserAuthContext = toUserAuth(aliceUser);

      const bobUser: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Bob",
        login: "bob",
        password: Buffer.from("BOB"),
      });

      chai.assert.isFalse(bobUser.isTester);

      const updatedBob: User = await ctx.user.updateUser(aliceAuth, {userId: bobUser.id, isTester: true});
      chai.assert.strictEqual(updatedBob.id, bobUser.id);
      chai.assert.isTrue(updatedBob.isTester);

      const updatedBob2: User = await ctx.user.updateUser(aliceAuth, {userId: bobUser.id, isTester: false});
      chai.assert.strictEqual(updatedBob2.id, bobUser.id);
      chai.assert.isFalse(updatedBob2.isTester);
    });
  });

  it("The `isTester` value cannot be set to `false` for administrators", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withUserService(async (ctx: Context): Promise<void> => {
      const aliceUser: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Alice",
        login: "alice",
        password: Buffer.from("ALICE"),
      });

      const aliceAuth: UserAuthContext = toUserAuth(aliceUser);

      let error: Error | undefined;
      try {
        await ctx.user.updateUser(aliceAuth, {isTester: false});
      } catch (err) {
        error = err;
      }

      chai.assert.instanceOf(error, Error);
    });
  });

  it("Non-administrators cannot set their `isTester` value", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    return withUserService(async (ctx: Context): Promise<void> => {
      await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Alice",
        login: "alice",
        password: Buffer.from("ALICE"),
      });

      const bobUser: User = await ctx.user.createUserWithCredentials(GUEST_AUTH_CONTEXT, {
        displayName: "Bob",
        login: "bob",
        password: Buffer.from("BOB"),
      });
      const bobAuth: UserAuthContext = toUserAuth(bobUser);

      chai.assert.isFalse(bobUser.isTester);

      let error: Error | undefined;
      try {
        await ctx.user.updateUser(bobAuth, {isTester: true});
      } catch (err) {
        error = err;
      }
      chai.assert.instanceOf(error, Error);
    });
  });
}
