import { PgBlobService } from "../../lib/blob/pg-service.js";
import { PgFileService } from "../../lib/file/pg-service.js";
import { PgGameService } from "../../lib/game/pg-service.js";
import { PgLeaderboardService } from "../../lib/leaderboard/pg-service.js";
import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { PgRunService } from "../../lib/run/pg-service.js";
import { PgUserService } from "../../lib/user/pg-service.js";
import { DbState, StateService, withTestState } from "../test-db.js";
import { Context, testLeaderboardService } from "./test.js";

describe("PgLeaderboardService", function () {
  testLeaderboardService(withPgLeaderboardService);
});

async function withPgLeaderboardService<R>(handler: (context: Context) => Promise<R>): Promise<R> {
  return withTestState(DbState.Empty, async ({db, buffer}: StateService): Promise<R> => {
    const blob: PgBlobService = new PgBlobService(db, buffer);
    const password: ScryptPasswordService = new ScryptPasswordService(0.1);
    const user: PgUserService = new PgUserService(db, password);
    const file: PgFileService = new PgFileService(db, blob);
    const game: PgGameService = new PgGameService(db, user, file);
    const run: PgRunService = new PgRunService(db, game, user);
    const leaderboard: PgLeaderboardService = new PgLeaderboardService(db, game);
    return handler({blob, file, game, run, user, leaderboard});
  });
}
