import { UserAuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
import { AuthScope } from "@eternalfest/api-core/lib/auth/auth-scope.js";
import { BlobService } from "@eternalfest/api-core/lib/blob/service.js";
import { FileService } from "@eternalfest/api-core/lib/file/service.js";
import { GameModeKey } from "@eternalfest/api-core/lib/game/game-mode-key.js";
import { GameService } from "@eternalfest/api-core/lib/game/service.js";
import { LeaderboardService } from "@eternalfest/api-core/lib/leaderboard/service.js";
import { CreateRunOptions } from "@eternalfest/api-core/lib/run/create-run-options.js";
import { Run } from "@eternalfest/api-core/lib/run/run.js";
import { RunService } from "@eternalfest/api-core/lib/run/service.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import { User } from "@eternalfest/api-core/lib/user/user.js";
import chai from "chai";
import { UuidHex } from "kryo/lib/uuid-hex.js";

import { createSampleGames } from "../populate/games.js";
import { createRunWithResult, createSampleRuns } from "../populate/runs.js";
import { populateWithUsers } from "../populate/users.js";

export interface Context {
  blob: BlobService;
  file: FileService;
  game: GameService;
  run: RunService;
  user: UserService;
  leaderboard: LeaderboardService;
}

function getAuthContext(user: User): UserAuthContext {
  return {
    type: ActorType.User,
    userId: user.id,
    displayName: user.displayName,
    isAdministrator: true,
    isTester: true,
    scope: AuthScope.Default,
  };
}

function getAuthContextNoRights(user: User): UserAuthContext {
  return {
    type: ActorType.User,
    userId: user.id,
    displayName: user.displayName,
    isAdministrator: false,
    isTester: false,
    scope: AuthScope.Default,
  };
}

function createRun(userAuth: UserAuthContext, runService: RunService, gameId: UuidHex, gameMode: GameModeKey, score: number): Promise<Run> {
  const createRunOptions: CreateRunOptions = {
    userId: userAuth.userId,
    gameId: gameId,
    gameMode: gameMode,
    gameOptions: [],
    settings: {
      detail: false,
      music: false,
      shake: false,
      sound: false,
      volume: 100,
    },
  };
  return createRunWithResult(userAuth, runService, createRunOptions, {
    isVictory: false,
    maxLevel: 0,
    scores: [score],
    items: new Map<string, number>(),
    stats: {},
  });
}

async function createRuns(userAuth: UserAuthContext, runService: RunService, gameId: UuidHex, gameMode: GameModeKey, scores: number[]): Promise<Run[]> {
  const runs: Run[] = [];
  for (const score of scores)
  {runs.push(await createRun(userAuth, runService, gameId, gameMode, score));}
  return runs;
}

async function delay(timeout: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), timeout);
  });
}

export function testLeaderboardService(withContext: AsyncResourceManager<Context, void>) {
  it("Handles an unknown game", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const unknownGameUuid = "c0582612-093f-4b4c-99c4-75d3f28604a7";
      try {
        await ctx.leaderboard.getLeaderboard(aliceAuth, unknownGameUuid, "solo");
        chai.assert.fail("Expected getGameResults to throw.");
      }
      catch (error) {
        chai.assert.deepEqual(error.name, "GameNotFound");
        chai.assert.deepEqual(error.data, {gameId: unknownGameUuid});
      }
    });
  });

  it("Returns an empty array for a game with no run", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results, []);
    });
  });

  it("Only takes finished runs into account", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      const {finishedSoloRun} = await createSampleRuns(aliceAuth, ctx.run, alpha.id);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results, [{score: 4,
        user: {type: ActorType.User, id: alice.id, displayName: "Alice"},
        run: {id: finishedSoloRun.id, maxLevel: 44, gameOptions: ["boost"]}
      }]);
    });
  });

  it("Only takes runs of the right mode into account", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "multi", [0, 4, 10]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results, []);
    });
  });

  it("Returns only the best score for a player", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      const runs = await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [10, 3, 25, 7]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results, [{score: 25,
        user: {type: ActorType.User, id: alice.id, displayName: "Alice"},
        run: {id: runs[2].id, maxLevel: 0, gameOptions: []}
      }]);
    });
  });

  it("Returns the best scores by decreasing order", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice, bob, charlie} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const bobAuth: UserAuthContext = getAuthContext(bob);
      const charlieAuth: UserAuthContext = getAuthContext(charlie);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      const aliceRuns = await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [1, 5]);
      const bobRuns = await createRuns(bobAuth, ctx.run, alpha.id, "solo", [7, 4, 8]);
      const charlieRuns = await createRuns(charlieAuth, ctx.run, alpha.id, "solo", [10]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results, [{score: 10,
        user: {type: ActorType.User, id: charlie.id, displayName: "Charlie"},
        run: {id: charlieRuns[0].id, maxLevel: 0, gameOptions: []}},
      {score: 8,
        user: {type: ActorType.User, id: bob.id, displayName: "Bob"},
        run: {id: bobRuns[2].id, maxLevel: 0, gameOptions: []}},
      {score: 5,
        user: {type: ActorType.User, id: alice.id, displayName: "Alice"},
        run: {id: aliceRuns[1].id, maxLevel: 0, gameOptions: []}
      }]);
    });
  });

  it("Returns the oldest run for a player having duplicate best scores", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      const oldestRun = (await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [0]))[0];

      /* It should return the oldest finished best run, however there is a precision of a second in the database.
       * So wait 1s between creating both runs. */
      await delay(1000);

      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [0]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results, [{score: 0,
        user: {type: ActorType.User, id: alice.id, displayName: "Alice"},
        run: {id: oldestRun.id, maxLevel: 0, gameOptions: []}
      }]);
    });
  });

  it("Handles a player having 2 simultaneous best scores", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [0, 0]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results.length, 1);
      chai.assert.deepEqual(result.results[0].score, 0);
      chai.assert.deepEqual(result.results[0].user.id, alice.id);
    });
  });

  it("Only serves players with rights to see the leaderboard", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice, bob} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const bobAuth: UserAuthContext = getAuthContextNoRights(bob);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [0]);
      try {
        await ctx.leaderboard.getLeaderboard(bobAuth, alpha.id, "solo");
        chai.assert.fail("Expected getGameResults to throw.");
      }
      catch (error) {
        chai.assert.deepEqual(error.name, "Forbidden");
        chai.assert.deepEqual(error.data, {gameId: alpha.id});
      }
    });
  });

  it("Returns all runs for a game with no publication date", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice, bob, charlie} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const bobAuth: UserAuthContext = getAuthContext(bob);
      const charlieAuth: UserAuthContext = getAuthContext(charlie);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [1, 5]);
      await createRuns(bobAuth, ctx.run, alpha.id, "solo", [7, 4, 8]);
      await createRuns(charlieAuth, ctx.run, alpha.id, "solo", [10]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results.length, 3);
    });
  });

  it("Doesn't return runs before the publication date if the game is public (no runs after)", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice, bob, charlie} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const bobAuth: UserAuthContext = getAuthContext(bob);
      const charlieAuth: UserAuthContext = getAuthContext(charlie);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [1, 5]);
      await createRuns(bobAuth, ctx.run, alpha.id, "solo", [7, 4, 8]);
      await createRuns(charlieAuth, ctx.run, alpha.id, "solo", [10]);
      await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: new Date()});
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results.length, 0);
    });
  });

  it("Doesn't return runs before the publication date if the game is public (some runs after)", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice, bob, charlie} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const bobAuth: UserAuthContext = getAuthContext(bob);
      const charlieAuth: UserAuthContext = getAuthContext(charlie);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [1, 5]);
      await createRuns(bobAuth, ctx.run, alpha.id, "solo", [7, 4, 8]);
      await createRuns(charlieAuth, ctx.run, alpha.id, "solo", [10]);
      await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: new Date()});
      await createRuns(bobAuth, ctx.run, alpha.id, "solo", [15]);
      await createRuns(charlieAuth, ctx.run, alpha.id, "solo", [6]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results.length, 2);
    });
  });

  it("Returns runs from after the publication date if the game is public", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice, bob, charlie} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const bobAuth: UserAuthContext = getAuthContext(bob);
      const charlieAuth: UserAuthContext = getAuthContext(charlie);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: new Date()});
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [1, 5]);
      await createRuns(bobAuth, ctx.run, alpha.id, "solo", [7, 4, 8]);
      await createRuns(charlieAuth, ctx.run, alpha.id, "solo", [10]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results.length, 3);
    });
  });

  it("Only returns runs from after the publication date even if there is a better score before", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [10]);
      await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: new Date()});
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [5]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results.length, 1);
      chai.assert.deepEqual(result.results[0].score, 5);
    });
  });

  it("Only returns runs from after the publication date even if there is an identical score before", async function (this: Mocha.Context) {
    this.timeout(30000);
    return withContext(async (ctx: Context): Promise<void> => {
      const {alice} = await populateWithUsers(ctx.user);
      const aliceAuth: UserAuthContext = getAuthContext(alice);
      const {alpha} = await createSampleGames(aliceAuth, ctx.game);
      await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [10]);
      await ctx.game.updateGame(aliceAuth, alpha.id, {publicationDate: new Date()});
      const afterRuns = await createRuns(aliceAuth, ctx.run, alpha.id, "solo", [10]);
      const result = await ctx.leaderboard.getLeaderboard(aliceAuth, alpha.id, "solo");
      chai.assert.deepEqual(result.game.id, alpha.id);
      chai.assert.deepEqual(result.gameMode, "solo");
      chai.assert.deepEqual(result.results.length, 1);
      chai.assert.deepEqual(result.results[0].run.id, afterRuns[0].id);
    });
  });
}
