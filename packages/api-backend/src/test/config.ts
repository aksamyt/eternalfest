import dotenv from "dotenv";
import fs from "fs";
import furi from "furi";
import incident from "incident";
import { QsValueReader } from "kryo-qs/lib/qs-value-reader.js";
import { $Uint16 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";
import url from "url";
import urlJoin from "url-join";

export interface Config {
  readonly user: string;
  readonly password: string;
  readonly host: string;
  readonly port: number;
  readonly name: string;
  readonly dataRoot: string;
}

export const $Config: RecordIoType<Config> = new RecordType<Config>({
  properties: {
    user: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_USER"},
    password: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_PASSWORD"},
    host: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_HOST"},
    port: {type: $Uint16, rename: "EF_DB_PORT"},
    name: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_NAME"},
    dataRoot: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DATA_ROOT"},
  },
});

const QS_READER: QsValueReader = new QsValueReader();

export function getConfigFromEnv(cwd: url.URL, env: NodeJS.ProcessEnv): Config {
  const userConfig: Config = $Config.read(QS_READER, env);
  return {
    ...userConfig,
    dataRoot: new url.URL(urlJoin(cwd.toString(), userConfig.dataRoot)).toString(),
  };
}

export async function getLocalConfig(): Promise<Config> {
  const cwdUri: url.URL = furi.fromSysPath(process.cwd());
  const dotEnvUri: url.URL = furi.join(cwdUri, [".env"]);
  let dotEnvText: string;
  try {
    dotEnvText = await readTextAsync(dotEnvUri);
  } catch (err) {
    if (err.code === "ENOENT") {
      throw new incident.Incident(
        err,
        "DotEnvNotFound",
        {cwdUri},
        ({cwdUri}: {cwdUri: url.URL}) => `Missing \`.env\` file in current workind directory: ${cwdUri}`,
      );
    }
    throw err;
  }
  const parsedDotEnv: Record<string, string> = dotenv.parse(dotEnvText);
  for (const [key, value] of Object.entries(parsedDotEnv)) {
    Reflect.set(process.env, key, value);
  }
  return getConfigFromEnv(cwdUri, process.env);
}

async function readTextAsync(fileUri: url.URL): Promise<string> {
  return new Promise<string>((resolve, reject): void => {
    fs.readFile(fileUri, {encoding: "utf-8"}, (err: NodeJS.ErrnoException | null, text: string): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(text);
      }
    });
  });
}
