// import { AuthContext } from "@eternalfest/api-core/lib/auth/auth-context.js";
// import { HfestServer } from "@eternalfest/api-core/lib/hfest-identity/hfest-server.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { AsyncResourceManager } from "@eternalfest/api-core/lib/types/resource-manager.js";
// import { Session } from "@eternalfest/api-core/lib/types/session.js";
// import { ActorType } from "@eternalfest/api-core/lib/types/user/actor-type.js";
import hfApi from "@eternalfest/hammerfest-api/types/api.js";

// import chai from "chai";
// import incident from "incident";
import { PgAuthService } from "../../lib/auth/pg-service.js";
import { TIMEOUT } from "../timeout.js";

export interface Context {
  auth: PgAuthService;
  hfestApi: hfApi.HammerfestApi;
  session: SessionService;
}

export function testAuthService(_withContext: AsyncResourceManager<Context, void>) {
  it("Authenticate with Hammerfest credentials", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    this.skip(); // AuthMigration
    // return withContext(async (ctx: Context): Promise<void> => {
    //   const auth: AuthContext = await ctx.auth.hfestCredentials(
    //     HfestServer.Fr,
    //     "bob",
    //     Buffer.from("BOB"),
    //   );
    //   chai.assert.propertyVal(auth, "type", ActorType.User);
    // });
  });

  it("Authenticate with a session id", async function (this: Mocha.Context) {
    this.timeout(TIMEOUT);
    this.skip(); // AuthMigration
    // return withContext(async (ctx: Context): Promise<void> => {
    //   const hfestAuth: AuthContext = await ctx.auth.hfestCredentials(
    //     HfestServer.Fr,
    //     "bob",
    //     Buffer.from("BOB"),
    //   );
    //   if (hfestAuth.type !== ActorType.User) {
    //     throw new incident.Incident("Expected `type` to be `ActorType.User`");
    //   }
    //   const session: Session = await ctx.session.createSession({type: ActorType.System}, hfestAuth.userId);
    //   const sessionAuth: AuthContext = await ctx.auth.session(session.id);
    //   chai.assert.propertyVal(sessionAuth, "type", ActorType.User);
    // });
  });
}
