import { HfestIdentityService } from "@eternalfest/api-core/lib/hfest-identity/service.js";
import { PasswordService } from "@eternalfest/api-core/lib/password/service.js";
import { SessionService } from "@eternalfest/api-core/lib/session/service.js";
import { UserService } from "@eternalfest/api-core/lib/user/service.js";
import hammerfestApi from "@eternalfest/hammerfest-api/types/api.js";

import { PgAuthService } from "../../lib/auth/pg-service.js";
import { Database } from "../../lib/db/database.js";
import { PgHfestIdentityService } from "../../lib/hfest-identity/pg-service.js";
import { ScryptPasswordService } from "../../lib/password/scrypt-service.js";
import { PgSessionService } from "../../lib/session/pg-service.js";
import { PgUserService } from "../../lib/user/pg-service.js";
import { createMockHammerfestApi } from "../mock-hammerfest-api.js";
import { DbState, withTestDb } from "../test-db.js";
import { Context, testAuthService } from "./test.js";

describe("PgAuthService", function () {
  testAuthService(withPgAuthService);
});

async function withPgAuthService<R>(
  handler: (context: Context) => Promise<R>,
): Promise<R> {
  return withTestDb(DbState.Empty, async (db: Database): Promise<R> => {
    const hfestApi: hammerfestApi.HammerfestApi = createMockHammerfestApi();
    const hfestIdentity: HfestIdentityService = new PgHfestIdentityService(db, hfestApi);
    const password: PasswordService = new ScryptPasswordService();
    const user: UserService = new PgUserService(db, password);
    const session: SessionService = new PgSessionService(db, 86400);
    const auth: PgAuthService = new PgAuthService(hfestApi, hfestIdentity, session, user);
    return handler({auth, hfestApi, session});
  });
}
