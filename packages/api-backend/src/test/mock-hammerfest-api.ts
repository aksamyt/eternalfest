import hfServer from "@eternalfest/hammerfest-api/api/get-server.js";

import { HammerfestApiState, InMemoryHfestApiService } from "../lib/hfest-api/in-memory-service.js";

// const profiles: Map<number, hfestInterfaces.Profile> = new Map<number, hfestInterfaces.Profile>([
//   [
//     50313,
//     {
//       id: 50313,
//       username: "bob",
//       email: null,
//       bestScore: 0,
//       bestLevel: 0,
//       carrotRecovered: true,
//       scoreOfTheWeek: 0,
//       pyramidLevel: 4,
//       hallOfFame: null,
//       items: [],
//       quests: {
//         inProgress: [],
//         completed: [],
//       },
//     },
//   ],
//   [
//     38,
//     {
//       id: 38,
//       username: "alice",
//       email: null,
//       bestScore: 10,
//       bestLevel: 10,
//       carrotRecovered: true,
//       scoreOfTheWeek: 10,
//       pyramidLevel: 2,
//       hallOfFame: null,
//       items: [],
//       quests: {
//         inProgress: [],
//         completed: [],
//       },
//     },
//   ],
// ]);

export function createMockHammerfestApi(): InMemoryHfestApiService {
  const INITIAL_STATE: HammerfestApiState = {
    servers: new Map([
      [
        hfServer.SERVER_EN,
        {
          users: new Map([
            [
              "1",
              {
                id: "1",
                username: "robert",
                password: "ROBERT",
              },
            ],
          ]),
          sessions: new Map(),
        },
      ],
      [
        hfServer.SERVER_ES,
        {
          users: new Map([
            [
              "111",
              {
                id: "111",
                username: "roberto",
                password: "ROBERTO",
              },
            ],
          ]),
          sessions: new Map(),
        },
      ],
      [
        hfServer.SERVER_FR,
        {
          users: new Map([
            [
              "50313",
              {
                id: "50313",
                username: "bob",
                password: "BOB",
              },
            ],
            [
              "50314",
              {
                id: "50314",
                username: "bob2",
                password: "BOB2",
              },
            ],
          ]),
          sessions: new Map(),
        },
      ],
    ]),
  };

  return new InMemoryHfestApiService(INITIAL_STATE);
}
