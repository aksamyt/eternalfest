import * as actorType from "@eternalfest/api-core/lib/types/user/actor-type.js";
import fsBufferService from "@eternalfest/buffer-service/fs-buffer-service.js";
import furi from "furi";
import path from "path";
import pg from "pg";

import { PgBlobService } from "../lib/blob/pg-service.js";
import { Database } from "../lib/db/database.js";
import { PgFileService } from "../lib/file/pg-service.js";
import { Config, getLocalConfigOrDie } from "./config.js";

async function main(): Promise<void> {
  const config: Config = getLocalConfigOrDie();
  const bufferRoot: string = path.posix.join(config.dataRoot, "buffers");

  const poolConfig: pg.PoolConfig = {
    user: config.user,
    password: config.password,
    host: config.host,
    port: config.port,
    database: config.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);
  const db: Database = new Database(pool);

  const bufferService: fsBufferService.FsBufferService = new fsBufferService.FsBufferService(furi.fromSysPath(bufferRoot));
  const blob: PgBlobService = new PgBlobService(db, bufferService);
  const file: PgFileService = new PgFileService(db, blob);

  console.log("Starting Clean-up");

  await file.deleteUnusedFiles({type: actorType.ActorType.System});
  await blob.markUnusedBlobsForDeletion({type: actorType.ActorType.System});
  await blob.execBlobDeletions({type: actorType.ActorType.System});
}

main()
  .catch((err: Error): never => {
    console.error(err.stack);
    return process.exit(1) as never;
  });
