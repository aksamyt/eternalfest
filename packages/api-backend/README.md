# API Backend

[![build status](https://gitlab.com/eternalfest/api-backend/badges/master/build.svg)](https://gitlab.com/eternalfest/api-backend/commits/master)

Backend implementation of the API.

## Changelog

See [CHANGELOG.md](./CHANGELOG.md).

## License

[MIT License © 2018 Eternalfest](./LICENSE.md)
