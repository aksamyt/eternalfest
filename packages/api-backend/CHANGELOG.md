# 0.14.0 (2020-06-01)

- **[Breaking change]** Update to `api-core@0.14`.

# 0.13.0 (2020-04-27)

- **[Breaking change]** Use native ESM.
- **[Breaking change]** Update to `api-core@0.13`.

# 0.12.1 (2019-12-31)

- **[Fix]** Update dependencies.

# 0.12.0 (2019-12-04)

- **[Breaking change]** Update to `api-core@0.12`.

# 0.11.3 (2019-12-04)

- **[Fix]** Compute `getGames` concurrently.

# 0.11.2 (2019-11-29)

- **[Fix]** Optimize `getGames`.

# 0.11.1 (2019-10-23)

- **[Fix]** Inline user items SQL query.
- **[Fix]** Fix support for locked options.
- **[Fix]** Update to `api-core@0.11.2`.

# 0.11.0 (2019-10-22)

- **[Breaking change]** Update to `api-core@0.11`.
- **[Breaking change]** Remove author anonymization.
- **[Feature]** Return user progress in `RunStart`.
- **[Fix]** Force latest published game at the top of the list.

# 0.10.0 (2019-08-22)

- **[Breaking change]** Update to `api-core@0.10.0`.

# 0.9.2 (2019-05-17)

- **[Fix]** Fix support for game modes without options.
- **[Fix]** Update dependencies.

# 0.9.1 (2019-05-17)

- **[Fix]** Skip permission checks for game resource files.
- **[Fix]** Reduce SQL requests to fetch game modes.
- **[Fix]** Read game resources concurrently.

# 0.9.0 (2019-05-17)

- **[Breaking change]** Update to `api-core@0.9.0`.
- **[Fix]** Refactor game permissions.

# 0.8.1 (2019-05-14)

- **[Feature]** Implement clean-up functions.

# 0.8.0 (2019-05-12)

- **[Breaking change]** Update to `api-core@0.8.0`.

# 0.5.2 (2019-04-01)

- **[Fix]** Fix value escaping using squel.

# 0.5.1 (2019-03-31)

- **[Fix]** Enable game icon file updates
- **[Fix]** Properly update `updated_at` for games.

# 0.5.0 (2019-01-10)

- **[Breaking change]** Update to `api-core@0.7.0`.
- **[Feature]** Add support for updating game resources.

# 0.4.0 (2019-01-03)

- **[Breaking change]** Update to `api-core@0.6.0`

# 0.3.0 (2018-12-31)

- **[Breaking change]** Update to `api-core@0.5.0`
- **[Feature]** Add support for run results.
- **[Internal]** Increase test timeout for `file` service.

# 0.2.2 (2018-12-24)

- **[Fix]** Rename anonymous game author to `TeamEternalfest`.

# 0.2.1 (2018-12-24)

- **[Fix]** Update `scrypt-kdf` dependency.
- **[Fix]** Update `@eternalfest/api-core` dependency.
- **[Fix]** Ensure `PgAuthService` implements `AuthService`.

# 0.2.0 (2018-12-20)

- **[Breaking change]** Update to `api-core@0.4.0`.
- **[Breaking change]** Refactor lib structure.
- **[Feature]** Add support for linking hfest identities to an existing user.
- **[Feature]** Add support for updating `User#displayName`.
- **[Feature]** Add support for updating `User#isTester`.
- **[Feature]** Add support for updating games.
- **[Fix]** Fix `ScryptPasswordService`.
- **[Fix]** Fix game access authorizations.
- **[Internal]** Populate DB with hfest items before running tests.

# 0.1.3 (2018-12-16)

- **[Feature]** Add `PgRunService`
- **[Fix]** Fix game access authorizations.

# 0.1.2 (2018-12-15)

- **[Feature]** Add `publicAccessFrom` support.
- **[Fix]** Normalize use of DB enums.
- **[Internal]** Use Kryo to load test config from environment.
- **[Internal]** Remove unused files and dependencies.

# 0.1.1 (2018-12-12)

- **[Feature]** Expose `hasPassword`

# 0.1.0 (2018-12-12)

- **[Feature]** First release.
