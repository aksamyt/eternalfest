import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/lib/ts-enum.js";

export enum GameModeState {
  Disabled = "disabled",
  Enabled = "enabled",
}

export const $GameModeState: TsEnumType<GameModeState> = new TsEnumType<GameModeState>({
  enum: GameModeState,
  changeCase: CaseStyle.KebabCase,
});
