import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type GameModeKey = string;

export const $GameModeKey: Ucs2StringType = new Ucs2StringType({maxLength: 50});
