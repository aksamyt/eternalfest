import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/lib/ts-enum.js";

export enum GameCategory {
  Big,
  Small,
  Puzzle,
  Challenge,
  Fun,
  Lab,
  Other,
}

export const $GameCategory: TsEnumType<GameCategory> = new TsEnumType<GameCategory>({
  enum: GameCategory,
  changeCase: CaseStyle.KebabCase,
});
