import { CaseStyle } from "kryo";
import { $Null } from "kryo/lib/null.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TryUnionType } from "kryo/lib/try-union.js";

import { $GameDisplayName, GameDisplayName } from "./game-display-name.js";
import { $GameId, GameId } from "./game-id.js";

export interface GameRef {
  id: GameId;
  displayName: GameDisplayName;
}

export const $GameRef: RecordIoType<GameRef> = new RecordType<GameRef>({
  properties: {
    id: {type: $GameId},
    displayName: {type: $GameDisplayName},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableGameRef = null | GameRef;

export const $NullableGameRef: TryUnionType<NullableGameRef> = new TryUnionType({
  variants: [$Null, $GameRef],
});
