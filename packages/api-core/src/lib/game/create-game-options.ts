import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { $Boolean } from "kryo/lib/boolean.js";
import { MapType } from "kryo/lib/map.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { DeepReadonly } from "../types/deep-readonly.js";
import { $LocaleId, LocaleId } from "../types/locale-id.js";
import { $FamiliesString, FamiliesString } from "./families-string.js";
import { $GameCategory, GameCategory } from "./game-category.js";
import { $GameDescription, GameDescription } from "./game-description.js";
import { $GameDisplayName, GameDisplayName } from "./game-display-name.js";
import { $GameEngines, GameEngines } from "./game-engines.js";
import { $GameLocale, GameLocale } from "./game-locale.js";
import { $GameMode, GameMode } from "./game-mode.js";
import { $InputGameResource, InputGameResource } from "./input-game-resource.js";

export interface CreateGameOptions {
  mainLocale: LocaleId;
  displayName: GameDisplayName;
  description: GameDescription;
  iconFileId?: UuidHex;
  engines: GameEngines;
  resources: ReadonlyArray<InputGameResource>;
  gameModes: GameMode[];
  families: FamiliesString;
  category: GameCategory;
  isPrivate: boolean;
  locales?: Map<LocaleId, GameLocale>;
}

export type ReadonlyCreateGameOptions = DeepReadonly<CreateGameOptions>;

export const $CreateGameOptions: RecordIoType<CreateGameOptions> = new RecordType<CreateGameOptions>({
  properties: {
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    iconFileId: {type: $UuidHex, optional: true},
    engines: {type: $GameEngines},
    resources: {type: new ArrayType({itemType: $InputGameResource, maxLength: 50})},
    gameModes: {type: new ArrayType({itemType: $GameMode, minLength: 1, maxLength: 50})},
    families: {type: $FamiliesString},
    category: {type: $GameCategory},
    isPrivate: {type: $Boolean},
    locales: {
      type: new MapType({
        keyType: $LocaleId,
        valueType: $GameLocale,
        maxSize: 500,
        assumeStringKey: true,
      }),
      optional: true,
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
