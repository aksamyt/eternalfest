import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { MapType } from "kryo/lib/map.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { DeepReadonly } from "../types/deep-readonly.js";
import { $LocaleId, LocaleId } from "../types/locale-id.js";
import { $GameModeDisplayName, GameModeDisplayName } from "./game-mode-display-name.js";
import { $GameModeKey, GameModeKey } from "./game-mode-key.js";
import { $GameModeLocale, GameModeLocale } from "./game-mode-locale.js";
import { $GameModeState, GameModeState } from "./game-mode-state.js";
import { $GameOption, GameOption } from "./game-option.js";

export interface GameMode {
  key: GameModeKey;
  displayName: GameModeDisplayName;
  state: GameModeState;
  options: GameOption[];
  locales: Map<LocaleId, GameModeLocale>;
}

export type ReadonlyGameMode = DeepReadonly<GameMode>;

export const $GameMode: RecordIoType<GameMode> = new RecordType<GameMode>({
  properties: {
    key: {type: $GameModeKey},
    displayName: {type: $GameModeDisplayName},
    state: {type: $GameModeState},
    options: {type: new ArrayType({itemType: $GameOption, maxLength: 50})},
    locales: {
      type: new MapType({
        keyType: $LocaleId,
        valueType: $GameModeLocale,
        maxSize: 500,
        assumeStringKey: true,
      }),
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
