import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/lib/boolean.js";
import { $Date } from "kryo/lib/date.js";
import { MapType } from "kryo/lib/map.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { DeepReadonly } from "../types/deep-readonly.js";
import { $IdRef, IdRef } from "../types/id-ref.js";
import { $LocaleId, LocaleId } from "../types/locale-id.js";
import { $NullableDate, NullableDate } from "../types/nullable-date.js";
import { $GameAuthor, GameAuthor } from "./game-author.js";
import { $GameCategory, GameCategory } from "./game-category.js";
import { $GameDescription, GameDescription } from "./game-description.js";
import { $GameDisplayName, GameDisplayName } from "./game-display-name.js";
import { $GameId, GameId } from "./game-id.js";
import { $NullableGameKey, NullableGameKey } from "./game-key.js";
import { $GameLocale, GameLocale } from "./game-locale.js";

export interface ShortGame {
  id: GameId;
  key: NullableGameKey;
  createdAt: Date;
  updatedAt: Date;
  publicationDate: NullableDate;
  author: GameAuthor;
  mainLocale: LocaleId;
  displayName: GameDisplayName;
  description: GameDescription;
  category: GameCategory;
  iconFile?: IdRef;
  isPrivate: boolean;
  locales: Map<LocaleId, GameLocale>;
}

export type ReadonlyShortGame = DeepReadonly<ShortGame>;

export const $ShortGame: RecordIoType<ShortGame> = new RecordType<ShortGame>({
  properties: {
    id: {type: $GameId},
    key: {type: $NullableGameKey},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
    publicationDate: {type: $NullableDate},
    author: {type: $GameAuthor},
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    category: {type: $GameCategory},
    iconFile: {type: $IdRef, optional: true},
    isPrivate: {type: $Boolean},
    locales: {
      type: new MapType({
        keyType: $LocaleId,
        valueType: $GameLocale,
        maxSize: 500,
        assumeStringKey: true,
      }),
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
