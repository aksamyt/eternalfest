import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { $Boolean } from "kryo/lib/boolean.js";
import { MapType } from "kryo/lib/map.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { DeepReadonly } from "../types/deep-readonly.js";
import { $LocaleId, LocaleId } from "../types/locale-id.js";
import { $NullableDate, NullableDate } from "../types/nullable-date.js";
import { $FamiliesString, FamiliesString } from "./families-string.js";
import { $GameCategory, GameCategory } from "./game-category.js";
import { $GameDescription, GameDescription } from "./game-description.js";
import { $GameDisplayName, GameDisplayName } from "./game-display-name.js";
import { $GameLocale, GameLocale } from "./game-locale.js";
import { $GameMode, GameMode } from "./game-mode.js";
import { $InputGameResource, InputGameResource } from "./input-game-resource.js";

export interface UpdateGameOptions {
  mainLocale?: LocaleId;
  displayName?: GameDisplayName;
  description?: GameDescription;
  iconFileId?: UuidHex;
  resources?: InputGameResource[];
  gameModes?: GameMode[];
  families?: FamiliesString;
  category?: GameCategory;
  isPrivate?: boolean;

  /**
   * Only administrators can set this field.
   */
  publicationDate?: NullableDate;
  locales?: Map<LocaleId, GameLocale>;
}

export type ReadonlyUpdateGameOptions = DeepReadonly<UpdateGameOptions>;

export const $UpdateGameOptions: RecordIoType<UpdateGameOptions> = new RecordType<UpdateGameOptions>({
  properties: {
    mainLocale: {type: $LocaleId, optional: true},
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    iconFileId: {type: $UuidHex, optional: true},
    resources: {type: new ArrayType({itemType: $InputGameResource, maxLength: 50}), optional: true},
    gameModes: {type: new ArrayType({itemType: $GameMode, maxLength: 50}), optional: true},
    families: {type: $FamiliesString, optional: true},
    category: {type: $GameCategory, optional: true},
    isPrivate: {type: $Boolean, optional: true},
    publicationDate: {type: $NullableDate, optional: true},
    locales: {
      type: new MapType({
        keyType: $LocaleId,
        valueType: $GameLocale,
        maxSize: 500,
        assumeStringKey: true,
      }),
      optional: true,
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
