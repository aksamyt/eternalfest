import { $Null } from "kryo/lib/null.js";
import { TryUnionType } from "kryo/lib/try-union.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type GameKey = string;

export const $GameKey: Ucs2StringType = new Ucs2StringType({maxLength: 32});

export type NullableGameKey = null | GameKey;

export const $NullableGameKey: TryUnionType<NullableGameKey> = new TryUnionType({variants: [$Null, $GameKey]});
