import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

import { $File, File } from "../file/file.js";

export interface GameResource {
  readonly file: File;
  readonly tag?: string;
  readonly priority: string;
}

export const $GameResource: RecordIoType<GameResource> = new RecordType<GameResource>({
  properties: {
    file: {type: $File},
    tag: {type: new Ucs2StringType({maxLength: 50}), optional: true},
    priority: {type: new Ucs2StringType({maxLength: 50})},
  },
  changeCase: CaseStyle.SnakeCase,
});
