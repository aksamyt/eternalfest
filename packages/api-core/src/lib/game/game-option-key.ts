import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type GameOptionKey = string;

export const $GameOptionKey: Ucs2StringType = new Ucs2StringType({maxLength: 50});
