import { UuidHex } from "kryo/lib/uuid-hex.js";

import { AuthContext } from "../auth/auth-context.js";
import { ReadonlyCreateGameOptions } from "./create-game-options.js";
import { Game } from "./game.js";
import { ShortGame } from "./short-game.js";
import { ReadonlyUpdateGameOptions } from "./update-game-options.js";

export interface ReadonlyGameService {
  getGames(auth: AuthContext): Promise<readonly ShortGame[]>;

  getGameById(auth: AuthContext, gameId: UuidHex): Promise<Game | undefined>;
}

export interface GameService extends ReadonlyGameService {
  createGame(auth: AuthContext, options: ReadonlyCreateGameOptions): Promise<Game>;

  updateGame(auth: AuthContext, gameId: UuidHex, options: ReadonlyUpdateGameOptions): Promise<Game>;

  deleteGameById(auth: AuthContext, gameId: UuidHex): Promise<void>;
}
