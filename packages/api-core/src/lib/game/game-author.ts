import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $UserDisplayName, UserDisplayName } from "../user/user-display-name.js";

export interface GameAuthor {
  readonly id: UuidHex;
  readonly displayName: UserDisplayName;
}

export const $GameAuthor: RecordIoType<GameAuthor> = new RecordType<GameAuthor>({
  properties: {
    id: {type: $UuidHex},
    displayName: {type: $UserDisplayName},
  },
  changeCase: CaseStyle.SnakeCase,
});
