import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $GameDescription, GameDescription } from "./game-description.js";
import { $GameDisplayName, GameDisplayName } from "./game-display-name.js";

export interface GameLocale {
  displayName?: GameDisplayName;
  description?: GameDescription;
}

export const $GameLocale: RecordIoType<GameLocale> = new RecordType<GameLocale>({
  properties: {
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
