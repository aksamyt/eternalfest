import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $VersionString, VersionString } from "../types/version-string.js";

export interface GameEngines {
  readonly loader: "3.0.0"; // VersionString;
  readonly game?: VersionString;
  readonly external?: VersionString;
  readonly eternaldev?: VersionString;
}

export const $GameEngines: RecordIoType<GameEngines> = new RecordType<GameEngines>({
  properties: {
    loader: {type: new LiteralType({type: $VersionString, value: "3.0.0" as "3.0.0"})},
    game: {type: $VersionString, optional: true},
    external: {type: $VersionString, optional: true},
    eternaldev: {type: $VersionString, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
