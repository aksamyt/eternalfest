import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

export interface InputGameResource {
  readonly fileId: UuidHex;
  readonly tag?: string;
  readonly priority?: string;
}

export const $InputGameResource: RecordIoType<InputGameResource> = new RecordType<InputGameResource>({
  properties: {
    fileId: {type: $UuidHex},
    tag: {type: new Ucs2StringType({maxLength: 50}), optional: true},
    priority: {type: new Ucs2StringType({maxLength: 50}), optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
