import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type GameModeDisplayName = string;

export const $GameModeDisplayName: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 64,
});
