import { UuidHex } from "kryo/lib/uuid-hex.js";

export interface UnlinkUserOptions {
  identityId: UuidHex;
}
