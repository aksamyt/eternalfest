import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/lib/boolean.js";
import { $Uint32 } from "kryo/lib/integer.js";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

import { $Email, Email } from "../types/email.js";
import { $AbstractIdentity, IdentityBase } from "../types/user/identity-base.js";
import { $IdentityType, IdentityType } from "../types/user/identity-type.js";
import { $HfestLogin, HfestLogin } from "./hfest-login.js";
import { $HfestServer, HfestServer } from "./hfest-server.js";

export interface HfestIdentity extends IdentityBase {
  type: IdentityType.Hfest;
  server: HfestServer;
  hfestId: string;
  username: HfestLogin;
  email?: Email;
  bestScore: number;
  bestLevel: number;
  gameCompleted: boolean;
}

export const $HfestIdentity: RecordIoType<HfestIdentity> = new RecordType<HfestIdentity>({
  properties: {
    ...$AbstractIdentity.properties,
    type: {type: new LiteralType({type: $IdentityType, value: IdentityType.Hfest as IdentityType.Hfest})},
    server: {type: $HfestServer},
    hfestId: {type: new Ucs2StringType({maxLength: 10, trimmed: true, pattern: /^\d+$/})},
    username: {type: $HfestLogin},
    email: {type: $Email, optional: true},
    bestScore: {type: $Uint32},
    bestLevel: {type: $Uint32},
    gameCompleted: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});
