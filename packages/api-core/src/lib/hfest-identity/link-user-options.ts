import { UuidHex } from "kryo/lib/uuid-hex.js";

export interface LinkUserOptions {
  userId?: UuidHex;

  identityId: UuidHex;
}
