import { AuthContext } from "../auth/auth-context.js";
import { HfestIdentity } from "./hfest-identity.js";
import { LinkUserOptions } from "./link-user-options.js";
import { UnlinkUserOptions } from "./unlink-user-options.js";

export interface HfestIdentityService {
  createOrUpdateHfestIdentity(hfestSessionString: string): Promise<HfestIdentity>;

  linkUser(auth: AuthContext, linkUserOptions: LinkUserOptions): Promise<void>;

  unlinkUser(auth: AuthContext, unlinkUserOptions: UnlinkUserOptions): Promise<void>;
}
