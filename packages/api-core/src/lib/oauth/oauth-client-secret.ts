import { Ucs2StringType } from "kryo/lib/ucs2-string.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

export type OauthClientSecret = UuidHex;

export const $OauthClientSecret: Ucs2StringType = $UuidHex;
