// Consider using `https://www.npmjs.com/package/concat-stream`?
import stream from "stream";

export async function readIntoBuffer(stream: stream.Readable): Promise<Buffer> {
  return new Promise<Buffer>((resolve, reject): void => {
    const chunks: Buffer[] = [];

    function ondata(data: Buffer) {
      chunks.push(data);
    }

    function onEnd() {
      stream.removeListener("data", ondata);
      stream.removeListener("error", onError);
      resolve(Buffer.concat(chunks));
    }

    function onError(err: Error) {
      stream.removeListener("data", ondata);
      stream.removeListener("error", onError);
      reject(err);
    }

    stream.on("data", ondata);
    stream.once("end", onEnd);
    stream.once("error", onError);
    stream.resume();
  });
}
