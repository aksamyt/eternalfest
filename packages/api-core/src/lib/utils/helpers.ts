import crypto from "crypto";
import { UuidHex } from "kryo/lib/uuid-hex.js";

function md5ToUuid(hash: string): UuidHex {
  const result: string[] = [];
  const partSizes: number[] = [8, 4, 4, 4, 12];
  let index: number = 0;
  for (const partSize of partSizes) {
    result.push(hash.substr(index, partSize));
    index += partSize;
  }
  return result.join("-");
}

export function toUuid(string: string): UuidHex {
  return md5ToUuid(crypto.createHmac("md5", string).digest("hex"));
}
