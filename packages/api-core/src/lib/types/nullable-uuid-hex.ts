import { $Null } from "kryo/lib/null.js";
import { TryUnionType } from "kryo/lib/try-union.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

export type NullableUuidHex = null | UuidHex;

export const $NullableUuidHex: TryUnionType<NullableUuidHex> = new TryUnionType({variants: [$Null, $UuidHex]});
