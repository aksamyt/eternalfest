import { $Date } from "kryo/lib/date.js";
import { $Null } from "kryo/lib/null.js";
import { TryUnionType } from "kryo/lib/try-union.js";

export type NullableDate = null | Date;

export const $NullableDate: TryUnionType<NullableDate> = new TryUnionType({variants: [$Null, $Date]});
