import { $Uint32 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

/**
 * Represents the `[start, end[` range.
 */
export interface Range {
  start: number;
  end: number;
}

export const $Range: RecordIoType<Range> = new RecordType<Range>(() => ({
  properties: {
    start: {type: $Uint32},
    end: {type: $Uint32},
  },
}));
