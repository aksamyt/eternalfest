import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type VersionString = string;

export const $VersionString: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
