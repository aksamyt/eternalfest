import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type LocaleId = string;

export const $LocaleId: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 2,
  maxLength: 7,
  pattern: /^[a-z]{2,3}(?:-[A-Z]{1,3})$/,
});
