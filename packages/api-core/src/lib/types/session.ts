import { CaseStyle } from "kryo";
import { $Date } from "kryo/lib/date.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $IdRef, IdRef } from "./id-ref.js";

export interface Session {
  id: UuidHex;

  user: IdRef;

  createdAt: Date;

  updatedAt: Date;
}

export const $Session: RecordIoType<Session> = new RecordType<Session>({
  properties: {
    id: {type: $UuidHex},
    user: {type: $IdRef},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
});
