import { UuidHex } from "kryo/lib/uuid-hex.js";

import { EternalfestJson } from "./eternalfest-json.js";

export interface Project extends EternalfestJson {
  id: UuidHex;
  rootId: UuidHex;
}
