import { GameEngines } from "../../game/game-engines.js";
import { GameMode } from "../../game/game-mode.js";

export type ResourcesDictionary = Record<string, string | null>;

export interface Resource {
  path: string;
  tag?: string;
  priority?: string;
}

export type Resources = ResourcesDictionary | Resource[];

export interface LocalEternalfestJson {
  name?: string;
  description?: string;
  icon?: string;
  engines?: Partial<GameEngines>;
  resources?: Resources;
  families?: string;
  modes?: GameMode[];
}
