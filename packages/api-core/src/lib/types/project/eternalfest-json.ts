import { UuidHex } from "kryo/lib/uuid-hex.js";

import { GameEngines } from "../../game/game-engines.js";
import { GameMode } from "../../game/game-mode.js";
import { GameResource } from "../../game/game-resource.js";
import { LocalEternalfestJson } from "./local-eternalfest-json.js";

export interface EternalfestJson {
  _local?: LocalEternalfestJson;
  name?: string;
  description?: string;
  iconFileId?: UuidHex;
  engines: GameEngines;
  resources: GameResource[];
  families?: string;
  modes: GameMode[];
}
