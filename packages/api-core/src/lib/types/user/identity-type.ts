import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/lib/ts-enum.js";

export enum IdentityType {
  Hfest,
  Ef,
}

export const $IdentityType: TsEnumType<IdentityType> = new TsEnumType({
  enum: IdentityType,
  rename: CaseStyle.KebabCase,
});
