import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/lib/ts-enum.js";

export enum ActorType {
  Guest,
  User,
  System,
}

export const $ActorType: TsEnumType<ActorType> = new TsEnumType<ActorType>({
  enum: ActorType,
  changeCase: CaseStyle.KebabCase,
});
