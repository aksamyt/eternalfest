import { CaseStyle } from "kryo";
import { $Uint32 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

export interface ItemCount {
  count: number;
  maxCount: number;
}

export const $ItemCount: RecordIoType<ItemCount> = new RecordType<ItemCount>({
  properties: {
    count: {type: $Uint32},
    maxCount: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
});
