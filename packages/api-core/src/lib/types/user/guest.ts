import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $ActorType, ActorType } from "./actor-type.js";

/**
 * Represents an unauthenticated actor
 */
export interface Guest {
  type: ActorType.Guest;
}

export const $Guest: RecordIoType<Guest> = new RecordType<Guest>({
  properties: {
    type: {type: new LiteralType({type: $ActorType, value: ActorType.Guest as ActorType.Guest})},
  },
  changeCase: CaseStyle.SnakeCase,
});
