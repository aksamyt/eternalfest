import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $AbstractIdentity, IdentityBase } from "./identity-base.js";
import { $IdentityType, IdentityType } from "./identity-type.js";

export interface EternalfestIdentity extends IdentityBase {
  type: IdentityType.Ef;
}

export const $EternalfestIdentity: RecordIoType<EternalfestIdentity> = new RecordType<EternalfestIdentity>({
  properties: {
    ...$AbstractIdentity.properties,
    type: {type: new LiteralType({type: $IdentityType, value: IdentityType.Ef as IdentityType.Ef})},
  },
  changeCase: CaseStyle.SnakeCase,
});
