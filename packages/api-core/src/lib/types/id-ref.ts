import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

export interface IdRef {
  id: UuidHex;
}

export const $IdRef: RecordIoType<IdRef> = new RecordType<IdRef>({
  properties: {
    id: {type: $UuidHex},
  },
  changeCase: CaseStyle.SnakeCase,
});
