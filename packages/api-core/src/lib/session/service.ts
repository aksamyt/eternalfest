import { UuidHex } from "kryo/lib/uuid-hex.js";

import { AuthContext } from "../auth/auth-context";
import { Session } from "../types/session";

export interface SessionService {
  createSession(authContext: AuthContext, userId: UuidHex): Promise<Session>;

  getSessionById(authContext: AuthContext, sessionId: UuidHex): Promise<Session | undefined>;

  getAndTouchSessionById(authContext: AuthContext, sessionId: UuidHex): Promise<Session | undefined>;

  deleteSessionById(authContext: AuthContext, sessionId: UuidHex): Promise<void>;
}
