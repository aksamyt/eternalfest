import { UuidHex } from "kryo/lib/uuid-hex.js";

import { AuthContext } from "../auth/auth-context.js";
import { HfestIdentity } from "../hfest-identity/hfest-identity.js";
import { CreateUserWithCredentialsOptions } from "./create-user-with-credentials-options.js";
import { UpdateUserOptions } from "./update-user-options.js";
import { UserDisplayName } from "./user-display-name.js";
import { UserRef } from "./user-ref.js";
import { User } from "./user.js";

export interface UserService {
  createUserWithCredentials(auth: AuthContext, options: CreateUserWithCredentialsOptions): Promise<User>;

  createUserWithHfestIdentity(auth: AuthContext, hfestIdentity: HfestIdentity): Promise<User>;

  getOrCreateUserWithEtwin(auth: AuthContext, userId: UuidHex, userDisplay: UserDisplayName): Promise<User>;

  getUserById(auth: AuthContext, userId: UuidHex): Promise<User | undefined>;

  getUserRefById(auth: AuthContext, userId: UuidHex): Promise<UserRef | undefined>;

  getUserRefsById(auth: AuthContext, userIds: ReadonlySet<UuidHex>): Promise<Map<UuidHex, UserRef | undefined>>;

  getUsers(auth: AuthContext): Promise<ReadonlyArray<Partial<User>>>;

  updateUser(auth: AuthContext, options: UpdateUserOptions): Promise<User>;
}
