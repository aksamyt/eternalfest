import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/lib/boolean.js";
import { $Bytes } from "kryo/lib/bytes.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $Email, Email } from "../types/email.js";
import { $Login, Login } from "./login.js";
import { $UserDisplayName, UserDisplayName } from "./user-display-name.js";

export interface UpdateUserOptions {
  /**
   * Only administrator are allowed to specify a userId different from their own.
   */
  readonly userId?: UuidHex;

  readonly displayName?: UserDisplayName;
  readonly email?: Email;
  readonly login?: Login;
  readonly password?: Uint8Array;

  /**
   * Only administrators are allowed to change the `isTester` flag.
   */
  readonly isTester?: boolean;
}

export const $UpdateUserOptions: RecordIoType<UpdateUserOptions> = new RecordType<UpdateUserOptions>({
  properties: {
    userId: {type: $UuidHex, optional: true},
    displayName: {type: $UserDisplayName, optional: true},
    email: {type: $Email, optional: true},
    login: {type: $Login, optional: true},
    password: {type: $Bytes, optional: true},
    isTester: {type: $Boolean, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
