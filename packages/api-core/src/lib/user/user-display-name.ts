import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type UserDisplayName = string;

/**
 * Type of a user's display name.
 * The display name is only used to represent the user when generating views (such as the profile
 * page or a message). This is not a unique value.
 */
export const $UserDisplayName: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 3,
  maxLength: 64,
  pattern: /^[0-9A-Za-z_ ()]{3,64}$/,
});
