import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/lib/literal.js";
import { $Null } from "kryo/lib/null.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TryUnionType } from "kryo/lib/try-union.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $ActorType, ActorType } from "../types/user/actor-type.js";
import { $UserDisplayName, UserDisplayName } from "./user-display-name.js";

export interface UserRef {
  id: UuidHex;

  type: ActorType.User;

  displayName: UserDisplayName;
}

export const $UserRef: RecordIoType<UserRef> = new RecordType<UserRef>({
  properties: {
    id: {type: $UuidHex},
    type: {type: new LiteralType({type: $ActorType, value: ActorType.User as ActorType.User})},
    displayName: {type: $UserDisplayName},
  },
  changeCase: CaseStyle.SnakeCase,
});

export const $NullableUserRef: TryUnionType<null | UserRef> = new TryUnionType({variants: [$Null, $UserRef]});
