import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type Login = string;

export const $Login: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 3,
  maxLength: 64,
  lowerCase: true,
  pattern: /^[a-z][0-9a-z]{2,63}$/,
});
