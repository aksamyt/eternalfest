import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { $Boolean } from "kryo/lib/boolean.js";
import { $Date } from "kryo/lib/date.js";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $ActorType, ActorType } from "../types/user/actor-type.js";
import { $Identity, Identity } from "../types/user/identity.js";
import { $UserDisplayName, UserDisplayName } from "./user-display-name.js";

export interface User {
  /**
   * UUID of this user
   */
  id: UuidHex;

  type: ActorType.User;

  displayName: UserDisplayName;
  createdAt: Date;
  updatedAt: Date;
  identities: Identity[];
  isAdministrator: boolean;

  /**
   * Indicates that this user has access to the closed beta of Eternalfest
   */
  isTester?: boolean;

  /**
   * Indicates if the user has set an Eternalfest password for this account.
   * Only visible for the user itself or administrators.
   */
  hasPassword?: boolean;
}

export const $User: RecordIoType<User> = new RecordType<User>({
  properties: {
    id: {type: $UuidHex},
    type: {type: new LiteralType({type: $ActorType, value: ActorType.User as ActorType.User})},
    displayName: {type: $UserDisplayName},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
    identities: {type: new ArrayType({itemType: $Identity, maxLength: 5})},
    isAdministrator: {type: $Boolean},
    isTester: {type: $Boolean, optional: true},
    hasPassword: {type: $Boolean, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
