import { CaseStyle } from "kryo";
import { $Date } from "kryo/lib/date.js";
import { $Null } from "kryo/lib/null.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TryUnionType } from "kryo/lib/try-union.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

/**
 * Represents a reference to a revision (version) of a wiki page.
 */
export interface RevisionRef {
  /**
   * Revision id
   */
  id: UuidHex;

  /**
   * Date of the revision.
   */
  date: Date;
}

export const $RevisionRef: RecordIoType<RevisionRef> = new RecordType<RevisionRef>({
  properties: {
    id: {type: $UuidHex},
    date: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
});

export const $NullableRevisionRef: TryUnionType<null | RevisionRef> = new TryUnionType({
  variants: [$Null, $RevisionRef],
});
