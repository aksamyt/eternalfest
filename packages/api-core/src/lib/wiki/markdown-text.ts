import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

/**
 * Raw markdown content.
 */
export type MarkdownText = string;

export const $MarkdownText: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
