import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $WikiContentType, WikiContentType } from "./wiki-content-type.js";

export interface DeletedRevisionContent {
  type: WikiContentType.Deleted;
}

// tslint:disable-next-line:max-line-length
export const $DeletedRevisionContent: RecordIoType<DeletedRevisionContent> = new RecordType<DeletedRevisionContent>({
  properties: {
    type: {type: new LiteralType({type: $WikiContentType, value: WikiContentType.Deleted as const})},
  },
  changeCase: CaseStyle.SnakeCase,
});
