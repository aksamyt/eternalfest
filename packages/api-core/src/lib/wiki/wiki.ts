import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $NullableGameRef, GameRef } from "../game/game-ref.js";
import { $WikiPage, WikiPage } from "./wiki-page.js";

export interface Wiki {
  id: UuidHex;
  game: GameRef | null;
  home: WikiPage;
}

export const $Wiki: RecordIoType<Wiki> = new RecordType<Wiki>({
  properties: {
    id: {type: $UuidHex},
    game: {type: $NullableGameRef},
    home: {type: $WikiPage},
  },
  changeCase: CaseStyle.SnakeCase,
});
