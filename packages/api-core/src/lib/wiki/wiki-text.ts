import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $Ucs2String } from "kryo/lib/ucs2-string.js";

import { $MarkdownText, MarkdownText } from "./markdown-text.js";

export interface WikiText {
  markdown: MarkdownText;
  html: string;
}

export const $WikiText: RecordIoType<WikiText> = new RecordType<WikiText>({
  properties: {
    markdown: {type: $MarkdownText},
    html: {type: $Ucs2String},
  },
  changeCase: CaseStyle.SnakeCase,
});
