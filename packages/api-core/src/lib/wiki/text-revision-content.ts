import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $WikiContentType, WikiContentType } from "./wiki-content-type.js";
import { $WikiPageTitle, WikiPageTitle } from "./wiki-page-title.js";
import { $WikiText, WikiText } from "./wiki-text.js";

export interface TextRevisionContent {
  type: WikiContentType.Text;
  title: WikiPageTitle;
  body: WikiText;
}

export const $TextRevisionContent: RecordIoType<TextRevisionContent> = new RecordType<TextRevisionContent>({
  properties: {
    type: {type: new LiteralType({type: $WikiContentType, value: WikiContentType.Text as const})},
    title: {type: $WikiPageTitle},
    body: {type: $WikiText},
  },
  changeCase: CaseStyle.SnakeCase,
});
