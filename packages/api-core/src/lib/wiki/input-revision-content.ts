import { TaggedUnionType } from "kryo/lib/tagged-union.js";

import { $DeletedRevisionContent, DeletedRevisionContent } from "./deleted-revision-content.js";
import { $InputTextRevisionContent, InputTextRevisionContent } from "./input-text-revision-content.js";

/**
 * Revision content provided by the user.
 */
export type InputRevisionContent = DeletedRevisionContent | InputTextRevisionContent;

export const $InputRevisionContent: TaggedUnionType<InputRevisionContent> = new TaggedUnionType<InputRevisionContent>({
  variants: [$DeletedRevisionContent, $InputTextRevisionContent],
  tag: "type",
});
