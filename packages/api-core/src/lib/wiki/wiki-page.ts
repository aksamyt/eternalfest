import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $Revision, Revision } from "./revision.js";
import { $WikiSlug, WikiSlug } from "./wiki-slug.js";

export interface WikiPage {
  id: UuidHex;
  slug: WikiSlug;
  revision: Revision;
}

export const $WikiPage: RecordIoType<WikiPage> = new RecordType<WikiPage>({
  properties: {
    id: {type: $UuidHex},
    slug: {type: $WikiSlug},
    revision: {type: $Revision},
  },
  changeCase: CaseStyle.SnakeCase,
});
