import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $NullableUuidHex, NullableUuidHex } from "../types/nullable-uuid-hex.js";
import { $WikiSlug, WikiSlug } from "./wiki-slug.js";

export interface GetPageBySlugOptions {
  gameId: NullableUuidHex;
  slug: WikiSlug;
  revision?: UuidHex;
}

export const $GetPageBySlugOptions: RecordIoType<GetPageBySlugOptions> = new RecordType<GetPageBySlugOptions>({
  properties: {
    gameId: {type: $NullableUuidHex},
    slug: {type: $WikiSlug},
    revision: {type: $UuidHex, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
