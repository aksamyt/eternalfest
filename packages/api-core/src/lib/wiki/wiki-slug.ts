import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

/**
 * Represents wiki page slug.
 *
 * The page slug represents the part of the URL identifying a given wiki page.
 *
 * - `""` represents the home page
 * - `"a"` represents the page `a`
 * - `"a/b"` represents the page `a/b`
 */
export type WikiSlug = string;

export const $WikiSlug: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  maxLength: 100,
  pattern: /(?:^[a-z0-9.-]+(?:\/[a-z0-9.-]+)*$|^$)/,
});
