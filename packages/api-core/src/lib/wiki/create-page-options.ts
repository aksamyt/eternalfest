import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $InputTextRevisionContent, InputTextRevisionContent } from "./input-text-revision-content.js";
import { $RevisionComment, RevisionComment } from "./revision-comment.js";
import { $WikiSlug, WikiSlug } from "./wiki-slug.js";

export interface CreatePageOptions {
  /**
   * Slug of the page to create.
   */
  slug: WikiSlug;

  /**
   * Initial content of the page.
   */
  content: InputTextRevisionContent;

  /**
   * Comment describing the initial content of the page.
   */
  comment: RevisionComment;
}

export const $CreatePageOptions: RecordIoType<CreatePageOptions> = new RecordType<CreatePageOptions>({
  properties: {
    slug: {type: $WikiSlug},
    content: {type: $InputTextRevisionContent},
    comment: {type: $RevisionComment},
  },
  changeCase: CaseStyle.SnakeCase,
});
