import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $MarkdownText, MarkdownText } from "./markdown-text.js";
import { $WikiContentType, WikiContentType } from "./wiki-content-type.js";
import { $WikiPageTitle, WikiPageTitle } from "./wiki-page-title.js";

/**
 * A text revision content provided by the user.
 */
export interface InputTextRevisionContent {
  type: WikiContentType.Text;
  title: WikiPageTitle;
  body: MarkdownText;
}

// tslint:disable-next-line:max-line-length
export const $InputTextRevisionContent: RecordIoType<InputTextRevisionContent> = new RecordType<InputTextRevisionContent>({
  properties: {
    type: {type: new LiteralType({type: $WikiContentType, value: WikiContentType.Text as const})},
    title: {type: $WikiPageTitle},
    body: {type: $MarkdownText},
  },
  changeCase: CaseStyle.SnakeCase,
});
