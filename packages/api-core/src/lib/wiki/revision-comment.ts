import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

/**
 * Represents the content of a comment describing the changes of a revision.
 */
export type RevisionComment = string;

export const $RevisionComment: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 500,
});
