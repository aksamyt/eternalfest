import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $GameModeKey, GameModeKey } from "../game/game-mode-key.js";
import { $GameOptionKey, GameOptionKey } from "../game/game-option-key.js";
import { $RunSettings, RunSettings } from "./run-settings.js";

export interface CreateRunOptions {
  readonly gameId: UuidHex;
  readonly userId: UuidHex;
  readonly gameMode: GameModeKey;
  readonly gameOptions: ReadonlyArray<GameOptionKey>;
  readonly settings: RunSettings;
}

export const $CreateRunOptions: RecordIoType<CreateRunOptions> = new RecordType<CreateRunOptions>({
  properties: {
    gameId: {type: $UuidHex},
    userId: {type: $UuidHex},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 10})},
    settings: {type: $RunSettings},
  },
  changeCase: CaseStyle.SnakeCase,
});
