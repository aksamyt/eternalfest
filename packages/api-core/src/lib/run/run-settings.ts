import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/lib/boolean.js";
import { IntegerType } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

export interface RunSettings {
  readonly detail: boolean;
  readonly shake: boolean;
  readonly sound: boolean;
  readonly music: boolean;
  readonly volume: number;
}

export const $RunSettings: RecordIoType<RunSettings> = new RecordType<RunSettings>({
  properties: {
    detail: {type: $Boolean},
    shake: {type: $Boolean},
    sound: {type: $Boolean},
    music: {type: $Boolean},
    volume: {type: new IntegerType({min: 0, max: 100})},
  },
  changeCase: CaseStyle.SnakeCase,
});
