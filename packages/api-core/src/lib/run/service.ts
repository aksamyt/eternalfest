import { UuidHex } from "kryo/lib/uuid-hex.js";

import { AuthContext } from "../auth/auth-context.js";
import { CreateRunOptions } from "./create-run-options.js";
import { RunStart } from "./run-start.js";
import { Run } from "./run.js";
import { SetRunResultOptions } from "./set-run-result-options.js";

export interface RunService {
  createRun(auth: AuthContext, options: CreateRunOptions): Promise<Run>;

  getRunById(auth: AuthContext, runId: UuidHex): Promise<Run>;

  startRun(auth: AuthContext, runId: UuidHex): Promise<RunStart>;

  setRunResult(auth: AuthContext, runId: UuidHex, result: SetRunResultOptions): Promise<Run>;
}
