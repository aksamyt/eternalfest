import { ArrayIoType, ArrayType } from "kryo/lib/array.js";
import { $Sint32 } from "kryo/lib/integer.js";

export type RunScores = ReadonlyArray<number>;

export const $RunScores: ArrayIoType<number> = new ArrayType({
  itemType: $Sint32,
  minLength: 1,
  maxLength: 2,
});
