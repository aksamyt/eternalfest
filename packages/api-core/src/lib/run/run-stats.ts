import { AnyType } from "kryo/lib/any.js";

export type RunStats = unknown;

export const $RunStats: AnyType = new AnyType();
