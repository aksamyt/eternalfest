import { $Uint32 } from "kryo/lib/integer.js";
import { MapType } from "kryo/lib/map.js";

import { $HfestItemId, HfestItemId } from "../hfest-api/hfest-item-id.js";

export type RunItems = ReadonlyMap<HfestItemId, number>;

export const $RunItems: MapType<HfestItemId, number> = new MapType({
  keyType: $HfestItemId,
  valueType: $Uint32,
  maxSize: 500,
  assumeStringKey: true,
});
