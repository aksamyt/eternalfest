import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { $Date } from "kryo/lib/date.js";
import { $Null } from "kryo/lib/null.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TryUnionType } from "kryo/lib/try-union.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $GameRef, GameRef } from "../game/game-ref.js";
import { $GameModeKey, GameModeKey } from "../game/game-mode-key.js";
import { $NullableDate, NullableDate } from "../types/nullable-date.js";
import { $UserRef, UserRef } from "../user/user-ref.js";
import { $RunResult, RunResult } from "./run-result.js";
import { $RunSettings, RunSettings } from "./run-settings.js";

export interface Run {
  id: UuidHex;
  createdAt: Date;
  startedAt: NullableDate;
  result: null | RunResult;
  game: GameRef;
  user: UserRef;
  gameMode: GameModeKey;
  gameOptions: ReadonlyArray<string>;
  settings: RunSettings;
}

export const $Run: RecordIoType<Run> = new RecordType<Run>({
  properties: {
    id: {type: $UuidHex},
    createdAt: {type: $Date},
    startedAt: {type: $NullableDate},
    result: {type: new TryUnionType({variants: [$Null, $RunResult]})},
    game: {type: $GameRef},
    user: {type: $UserRef},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: new Ucs2StringType({maxLength: 50}), maxLength: 10})},
    settings: {type: $RunSettings},
  },
  changeCase: CaseStyle.SnakeCase,
});
