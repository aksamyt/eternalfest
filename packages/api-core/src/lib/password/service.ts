export interface PasswordService {
  verify(hash: Uint8Array, clearText: Uint8Array): Promise<boolean>;

  hash(clearText: Uint8Array): Promise<Uint8Array>;
}
