import { CaseStyle } from "kryo";
import { $Uint32 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $MediaType, MediaType } from "./media-type.js";

export interface CreateUploadSessionOptions {
  readonly mediaType: MediaType;
  readonly byteSize: number;
}

// tslint:disable-next-line:max-line-length
export const $CreateUploadSessionOptions: RecordIoType<CreateUploadSessionOptions> = new RecordType<CreateUploadSessionOptions>({
  properties: {
    mediaType: {type: $MediaType},
    byteSize: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
});
