import { CaseStyle } from "kryo";
import { $Bytes } from "kryo/lib/bytes.js";
import { $Uint32 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

export interface UploadBytesOptions {
  readonly offset: number;
  readonly data: Uint8Array;
}

export const $UploadBytesOptions: RecordIoType<UploadBytesOptions> = new RecordType<UploadBytesOptions>({
  properties: {
    offset: {type: $Uint32},
    data: {type: $Bytes},
  },
  changeCase: CaseStyle.SnakeCase,
});
