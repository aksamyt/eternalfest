import { UuidHex } from "kryo/lib/uuid-hex.js";
import stream from "stream";

import { AuthContext } from "../auth/auth-context.js";
import { Blob } from "./blob.js";
import { CreateBlobOptions } from "./create-blob-options.js";
import { CreateUploadSessionOptions } from "./create-upload-session-options.js";
import { UploadBytesOptions } from "./upload-bytes-options.js";
import { UploadSession } from "./upload-session.js";

export interface BlobService {
  createBlob(auth: AuthContext, options: CreateBlobOptions): Promise<Blob>;

  getBlobById(auth: AuthContext, blobId: UuidHex): Promise<Blob>;

  readBlobData(auth: AuthContext, blobId: UuidHex): Promise<stream.Readable>;

  createUploadSession(auth: AuthContext, options: CreateUploadSessionOptions): Promise<UploadSession>;

  uploadBytes(auth: AuthContext, uploadSessionId: UuidHex, options: UploadBytesOptions): Promise<UploadSession>;
}
