import { CaseStyle } from "kryo";
import { $Bytes } from "kryo/lib/bytes.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $MediaType, MediaType } from "./media-type.js";

export interface CreateBlobOptions {
  readonly mediaType: MediaType;
  readonly data: Uint8Array;
}

export const $CreateBlobOptions: RecordIoType<CreateBlobOptions> = new RecordType<CreateBlobOptions>({
  properties: {
    mediaType: {type: $MediaType},
    data: {type: $Bytes},
  },
  changeCase: CaseStyle.SnakeCase,
});
