import { CaseStyle } from "kryo";
import { $Uint32 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $MediaType, MediaType } from "./media-type.js";

/**
 * Placing the mediatype here (instead of the file interface) is consistent with the web Blob
 * object.
 *
 * - https://developer.mozilla.org/en-US/docs/Web/API/Blob
 */
export interface Blob {
  id: UuidHex;
  mediaType: MediaType;
  byteSize: number;
  // dataUrl?: string;
}

export const $Blob: RecordIoType<Blob> = new RecordType<Blob>(() => ({
  properties: {
    id: {type: $UuidHex},
    mediaType: {type: $MediaType},
    byteSize: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
}));
