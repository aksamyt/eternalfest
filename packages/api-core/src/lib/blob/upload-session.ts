import { CaseStyle } from "kryo";
import { DateType } from "kryo/lib/date.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $Range, Range } from "../types/range.js";
import { $Blob, Blob } from "./blob.js";

export interface UploadSession {
  id: UuidHex;
  expiresAt: Date;
  remainingRange: Range;
  blob?: Blob;
}

export const $UploadSession: RecordIoType<UploadSession> = new RecordType<UploadSession>(() => ({
  properties: {
    id: {type: $UuidHex},
    expiresAt: {type: new DateType()},
    remainingRange: {type: $Range},
    blob: {type: $Blob, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
}));
