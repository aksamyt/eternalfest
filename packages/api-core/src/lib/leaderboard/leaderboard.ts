import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $GameModeKey, GameModeKey } from "../game/game-mode-key.js";
import { $GameRef, GameRef } from "../game/game-ref.js";
import { $LeaderboardEntry, LeaderboardEntry } from "./leaderboard-entry.js";

export interface Leaderboard {
  game: GameRef;
  gameMode: GameModeKey;
  results: LeaderboardEntry[];
}

export const $Leaderboard: RecordIoType<Leaderboard> = new RecordType<Leaderboard>({
  properties: {
    game: {type: $GameRef},
    gameMode: {type: $GameModeKey},
    results: {type: new ArrayType({itemType: $LeaderboardEntry, maxLength: Infinity})},
  },
  changeCase: CaseStyle.SnakeCase,
});
