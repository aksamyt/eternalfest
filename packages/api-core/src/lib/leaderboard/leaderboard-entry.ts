import { CaseStyle } from "kryo";
import { $Float64 } from "kryo/lib/float64.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";

import { $UserRef, UserRef } from "../user/user-ref.js";
import { $LeaderboardEntryRun, LeaderboardEntryRun } from "./leaderboard-entry-run.js";

export interface LeaderboardEntry {
  score: number;
  user: UserRef;
  run: LeaderboardEntryRun;
}

export const $LeaderboardEntry: RecordIoType<LeaderboardEntry> = new RecordType<LeaderboardEntry>({
  properties: {
    score: {type: $Float64},
    user: {type: $UserRef},
    run: {type: $LeaderboardEntryRun},
  },
  changeCase: CaseStyle.SnakeCase,
});
