import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/lib/array.js";
import { $Uint32 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $GameOptionKey, GameOptionKey } from "../game/game-option-key.js";

export interface LeaderboardEntryRun {
  id: UuidHex;
  maxLevel: number;
  gameOptions: GameOptionKey[];
}

export const $LeaderboardEntryRun: RecordIoType<LeaderboardEntryRun> = new RecordType<LeaderboardEntryRun>({
  properties: {
    id: {type: $UuidHex},
    maxLevel: {type: $Uint32},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 10})},
  },
  changeCase: CaseStyle.SnakeCase,
});
