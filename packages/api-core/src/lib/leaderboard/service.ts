import { UuidHex } from "kryo/lib/uuid-hex.js";

import { AuthContext } from "../auth/auth-context.js";
import { GameModeKey } from "../game/game-mode-key.js";
import { Leaderboard } from "./leaderboard.js";

export interface LeaderboardService {
  getLeaderboard(auth: AuthContext, gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard>;
}
