import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/lib/ts-enum.js";

export enum AuthScope {
  Default,
}

export const $AuthScope: TsEnumType<AuthScope> = new TsEnumType<AuthScope>({
  enum: AuthScope,
  changeCase: CaseStyle.KebabCase,
});
