import { HfestLogin } from "../hfest-identity/hfest-login.js";
import { HfestServer } from "../hfest-identity/hfest-server.js";
import { UserDisplayName } from "../user/user-display-name.js";
import { UserId } from "../user/user-id.js";
import { AuthContext } from "./auth-context.js";

export interface AuthService {
  session(sessionId: string): Promise<AuthContext>;

  hfestCredentials(server: HfestServer, login: HfestLogin, password: Uint8Array): Promise<AuthContext>;

  hfestSession(hfestSessionString: string): Promise<AuthContext>;

  etwinOauth(userId: UserId, userDisplayName: UserDisplayName): Promise<AuthContext>;
}
