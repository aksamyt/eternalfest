import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/lib/boolean.js";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { TaggedUnionType } from "kryo/lib/tagged-union.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $ActorType, ActorType } from "../types/user/actor-type.js";
import { $UserDisplayName, UserDisplayName } from "../user/user-display-name.js";
import { $AuthScope, AuthScope } from "./auth-scope.js";

export interface GuestAuthContext {
  readonly type: ActorType.Guest;
  readonly scope: AuthScope;
}

export const $GuestAuthContext: RecordIoType<GuestAuthContext> = new RecordType<GuestAuthContext>({
  properties: {
    type: {type: new LiteralType({type: $ActorType, value: ActorType.Guest as ActorType.Guest})},
    scope: {type: $AuthScope},
  },
  changeCase: CaseStyle.KebabCase,
});

export interface UserAuthContext {
  readonly type: ActorType.User;
  readonly scope: AuthScope;
  readonly userId: UuidHex;
  readonly displayName: UserDisplayName;
  readonly isAdministrator: boolean;
  readonly isTester: boolean;
}

export const $UserAuthContext: RecordIoType<UserAuthContext> = new RecordType<UserAuthContext>({
  properties: {
    type: {type: new LiteralType({type: $ActorType, value: ActorType.User as ActorType.User})},
    scope: {type: $AuthScope},
    userId: {type: $UuidHex},
    displayName: {type: $UserDisplayName},
    isAdministrator: {type: $Boolean},
    isTester: {type: $Boolean},
  },
  changeCase: CaseStyle.KebabCase,
});

export interface SystemAuthContext {
  readonly type: ActorType.System;
}

export const $SystemAuthContext: RecordIoType<SystemAuthContext> = new RecordType<SystemAuthContext>({
  properties: {
    type: {type: new LiteralType({type: $ActorType, value: ActorType.System as ActorType.System})},
  },
  changeCase: CaseStyle.KebabCase,
});

export type AuthContext = GuestAuthContext | UserAuthContext | SystemAuthContext;

export const $AuthContext: TaggedUnionType<AuthContext> = new TaggedUnionType<AuthContext>({
  variants: [$GuestAuthContext, $UserAuthContext, $SystemAuthContext],
  tag: "type",
});
