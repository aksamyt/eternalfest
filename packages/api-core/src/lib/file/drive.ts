import { CaseStyle } from "kryo";
import { $Date } from "kryo/lib/date.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $IdRef, IdRef } from "../types/id-ref.js";

export interface Drive {
  id: UuidHex;
  owner: IdRef;
  root: IdRef;
  createdAt: Date;
  updatedAt: Date;
}

export const $Drive: RecordIoType<Drive> = new RecordType<Drive>(() => ({
  properties: {
    id: {type: $UuidHex},
    owner: {type: $IdRef},
    root: {type: $IdRef},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
}));
