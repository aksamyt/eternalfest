import { Ucs2StringType } from "kryo/lib/ucs2-string.js";

export type DriveItemDisplayName = string;

export const $DriveItemDisplayName: Ucs2StringType = new Ucs2StringType({
  maxLength: 100,
});
