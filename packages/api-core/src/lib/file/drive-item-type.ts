import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/lib/ts-enum.js";

export enum DriveItemType {
  Directory,
  File,
}

export const $DriveItemType: TsEnumType<DriveItemType> = new TsEnumType(() => ({
  enum: DriveItemType,
  changeCase: CaseStyle.KebabCase,
}));
