import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $DriveItemDisplayName, DriveItemDisplayName } from "./drive-item-display-name.js";

export interface CreateDirectoryOptions {
  parentId: UuidHex;

  displayName: DriveItemDisplayName;
}

// tslint:disable-next-line:max-line-length
export const $CreateDirectoryOptions: RecordIoType<CreateDirectoryOptions> = new RecordType<CreateDirectoryOptions>(() => ({
  properties: {
    parentId: {type: $UuidHex},
    displayName: {type: $DriveItemDisplayName},
  },
}));
