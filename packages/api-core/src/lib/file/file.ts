import { CaseStyle } from "kryo";
import { $Date } from "kryo/lib/date.js";
import { $Uint32 } from "kryo/lib/integer.js";
import { LiteralType } from "kryo/lib/literal.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $MediaType, MediaType } from "../blob/media-type.js";
import { $DriveItemDisplayName, DriveItemDisplayName } from "./drive-item-display-name.js";
import { $DriveItemType, DriveItemType } from "./drive-item-type.js";

export interface File {
  type: DriveItemType.File;
  id: UuidHex;
  displayName: DriveItemDisplayName;
  byteSize: number;
  mediaType: MediaType;
  createdAt: Date;
  updatedAt: Date;
}

export const $File: RecordIoType<File> = new RecordType<File>(() => ({
  properties: {
    type: {type: new LiteralType({type: $DriveItemType, value: DriveItemType.File as DriveItemType.File})},
    id: {type: $UuidHex},
    displayName: {type: $DriveItemDisplayName},
    byteSize: {type: $Uint32},
    mediaType: {type: $MediaType},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
}));
