import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { $UuidHex, UuidHex } from "kryo/lib/uuid-hex.js";

import { $DriveItemDisplayName, DriveItemDisplayName } from "./drive-item-display-name.js";

export interface CreateFileOptions {
  parentId: UuidHex;

  /**
   * Id of the blob for the content of this file.
   */
  blobId: UuidHex;

  displayName: DriveItemDisplayName;
}

export const $CreateFileOptions: RecordIoType<CreateFileOptions> = new RecordType<CreateFileOptions>(() => ({
  properties: {
    parentId: {type: $UuidHex},
    blobId: {type: $UuidHex},
    displayName: {type: $DriveItemDisplayName},
  },
}));
