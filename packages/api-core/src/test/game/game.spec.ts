import { DriveItemType } from "../../lib/file/drive-item-type.js";
import { GameCategory } from "../../lib/game/game-category.js";
import { GameModeState } from "../../lib/game/game-mode-state.js";
import { GameOptionState } from "../../lib/game/game-option-state.js";
import { $Game } from "../../lib/game/game.js";
import { testKryoType } from "../test-kryo.js";

describe("Game", function () {
  testKryoType(
    {
      type: $Game,
      valid: [
        {
          value: {
            id: "23eb2994-129b-42a7-893a-337612b71b99",
            key: null,
            createdAt: new Date("2015-09-08T12:14:36.017Z"),
            updatedAt: new Date("2019-04-02T12:12:48.400Z"),
            publicationDate: new Date("2019-03-05T18:00:00.000Z"),
            author: {
              id: "9f310484-963b-446b-af69-797feec6813f",
              displayName: "Demurgos",
            },
            mainLocale: "fr-FR" as const,
            displayName: "Sous la colline",
            description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques\u202f!",
            category: GameCategory.Small,
            iconFile: {
              id: "256faf1b-6c40-4e9e-83ba-44d9a9dd9baf",
            },
            isPrivate: false,
            resources: [
              {
                file: {
                  type: DriveItemType.File as const,
                  id: "850e5401-5cc0-4739-be3f-7000d84cc82c",
                  displayName: "tmp-20190402121109320--game.swf",
                  byteSize: 2319913,
                  mediaType: "application/x-shockwave-flash",
                  createdAt: new Date("2019-04-02T12:12:13.141Z"),
                  updatedAt: new Date("2019-04-02T12:12:13.141Z"),
                },
                tag: "game-swf",
                priority: "high",
              },
              {
                file: {
                  type: DriveItemType.File as const,
                  id: "105cbe47-d9c1-4a59-b2aa-7bd309c300b4",
                  displayName: "tmp-20190402121109320--game.xml",
                  byteSize: 65055,
                  mediaType: "application/xml",
                  createdAt: new Date("2019-04-02T12:12:15.619Z"),
                  updatedAt: new Date("2019-04-02T12:12:15.619Z"),
                },
                tag: "game-xml",
                priority: "high",
              },
              {
                file: {
                  type: DriveItemType.File as const,
                  id: "c454ca7c-44f6-4beb-b274-4d1164f3962d",
                  displayName: "tmp-20190402121109320--music.mp3",
                  byteSize: 594256,
                  mediaType: "audio/mp3",
                  createdAt: new Date("2019-04-02T12:12:32.516Z"),
                  updatedAt: new Date("2019-04-02T12:12:32.516Z"),
                },
                tag: "music",
                priority: "high",
              },
              {
                file: {
                  type: DriveItemType.File as const,
                  id: "36fdde08-ed27-4ad4-bb6f-576c6681739c",
                  displayName: "tmp-20190402121109320--hurryUp.mp3",
                  byteSize: 142652,
                  mediaType: "audio/mp3",
                  createdAt: new Date("2019-04-02T12:12:36.847Z"),
                  updatedAt: new Date("2019-04-02T12:12:36.847Z"),
                },
                tag: "music",
                priority: "high",
              },
              {
                file: {
                  type: DriveItemType.File as const,
                  id: "a3b70718-2b42-4af3-a724-3da2e61f218b",
                  displayName: "tmp-20190402121109320--boss.mp3",
                  byteSize: 320284,
                  mediaType: "audio/mp3",
                  createdAt: new Date("2019-04-02T12:12:45.862Z"),
                  updatedAt: new Date("2019-04-02T12:12:45.862Z"),
                },
                tag: "music",
                priority: "high",
              },
            ],
            engines: {
              loader: "3.0.0" as const,
            },
            families: "0,7,1000,1001,1002,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19",
            gameModes: [
              {
                key: "solo",
                displayName: "Aventure",
                state: GameModeState.Enabled,
                options: [
                  {
                    key: "mirror",
                    displayName: "Miroir",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                  {
                    key: "nightmare",
                    displayName: "Cauchemar",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                  {
                    key: "ninja",
                    displayName: "Ninjutsu",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                  {
                    key: "bombexpert",
                    displayName: "Explosifs Instables",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                  {
                    key: "boost",
                    displayName: "Tornade",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                ],
                locales: new Map(),
              },
              {
                key: "multicoop",
                displayName: "Multi Coopératif",
                state: GameModeState.Enabled,
                options: [
                  {
                    key: "mirrormulti",
                    displayName: "Miroir",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                  {
                    key: "nightmaremulti",
                    displayName: "Cauchemar",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                  {
                    key: "lifesharing",
                    displayName: "Partage de vies",
                    state: GameOptionState.Enabled,
                    locales: new Map(),
                  },
                ],
                locales: new Map(),
              },
            ],
            locales: new Map(),
          },
        },
      ],
      invalid: [],
    });
});
