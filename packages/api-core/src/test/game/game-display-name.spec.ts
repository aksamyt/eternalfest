import chai from "chai";
import { JSON_READER } from "kryo-json/lib/json-reader.js";

import { $GameDisplayName, GameDisplayName } from "../../lib/game/game-display-name.js";

describe("GameDisplayName", function () {
  describe("invalid", function () {
    interface Item {
      name?: string;
      value: GameDisplayName;
    }

    const items: Item[] = [
      {name: "empty string", value: ""},
      {name: "1 space character", value: " "},
      {name: "2 space characters", value: "  "},
      {name: "65 (max + 1) letters", value: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"},
      {
        name: "Non-trimmed name",
        value: "  \n \t Hackfest ",
      },
      {
        name: "64 letters and whitespace",
        value: "  Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa  ",
      },
    ];

    for (const item of items) {
      it(`should reject ${JSON.stringify(item.value)}: ${item.name}`, function () {
        chai.assert.throw(() => $GameDisplayName.read(JSON_READER, item.value));
      });
    }
  });
  describe("valid", function () {
    interface Item {
      name?: string;
      value: GameDisplayName;
      rawJson: string;
    }

    const items: Item[] = [
      {
        name: "1 letter",
        value: "A",
        rawJson: "\"A\"",
      },
      {
        name: "1 special character",
        value: "?",
        rawJson: "\"?\"",
      },
      {
        name: "64 letters",
        value: "Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        rawJson: "\"Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\"",
      },
    ];

    for (const item of items) {
      it(`should accept ${JSON.stringify(item.value)}: ${item.name}`, function () {
        const actualValue: string = $GameDisplayName.read(JSON_READER, item.rawJson);
        chai.assert.isTrue($GameDisplayName.equals(actualValue, item.value));
      });
    }
  });
});
