import { GameCategory } from "../../lib/game/game-category.js";
import { $ShortGame } from "../../lib/game/short-game.js";
import { testKryoType } from "../test-kryo.js";

describe("ShortGame", function () {
  testKryoType(
    {
      type: $ShortGame,
      valid: [
        {
          value: {
            id: "23eb2994-129b-42a7-893a-337612b71b99",
            key: null,
            createdAt: new Date("2015-09-08T12:14:36.017Z"),
            updatedAt: new Date("2019-04-02T12:12:48.400Z"),
            publicationDate: new Date("2019-03-05T18:00:00.000Z"),
            author: {
              id: "9f310484-963b-446b-af69-797feec6813f",
              displayName: "Demurgos",
            },
            mainLocale: "fr-FR" as const,
            displayName: "Sous la colline",
            description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques\u202f!",
            category: GameCategory.Small,
            iconFile: {
              id: "256faf1b-6c40-4e9e-83ba-44d9a9dd9baf",
            },
            isPrivate: false,
            locales: new Map(),
          },
        },
      ],
      invalid: [],
    });
});
