import { $Login } from "../../lib/user/login.js";
import { testKryoType } from "../test-kryo.js";

describe("Login", function () {
  testKryoType({
    type: $Login,
    valid: [
      {value: "abc"},
      {value: "demurgos"},
      {value: "demu1"},
      {value: "a23"},
      {value: "ab3"},
    ],
    invalid: [
      {value: ""},
      {value: "a"},
      {value: "ab"},
      {value: "123"},
      {value: "Demurgos"},
      {value: " demurgos"},
      {value: "demurgos "},
      {value: "demurgos\t"},
      {value: "demü"},
      {value: "1demu"},
    ],
  });
});
