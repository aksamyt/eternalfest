import chai from "chai";
import { JSON_READER } from "kryo-json/lib/json-reader.js";

import { $GameDisplayName } from "../../lib/game/game-display-name.js";
import { $InputTextRevisionContent, InputTextRevisionContent } from "../../lib/wiki/input-text-revision-content.js";
import { WikiContentType } from "../../lib/wiki/wiki-content-type.js";

describe("InputTextRevisionContent", function () {
  describe("invalid", function () {
    interface Item {
      name?: string;
      value: InputTextRevisionContent;
    }

    const items: Item[] = [
      {
        name: "Eternoel description",
        value: {
          type: WikiContentType.Text,
          title: "",
          body: "Eternoël est la première contrée publiée sur Eternalfest.",
        },
      },
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      it(`should reject ${valueStr}: ${item.name ?? valueStr}`, function () {
        chai.assert.throw(() => $GameDisplayName.read(JSON_READER, item.value));
      });
    }
  });
  describe("valid", function () {
    interface Item {
      name?: string;
      value: InputTextRevisionContent;
      rawJson: string;
    }

    const items: Item[] = [
      {
        name: "Eternoel description",
        value: {
          type: WikiContentType.Text,
          title: "Contrée - Eternoël",
          body: "Eternoël est la première contrée publiée sur Eternalfest.",
        },
        rawJson: "{\"type\":\"text\",\"title\":\"Contrée - Eternoël\",\"body\":\"Eternoël est la première contrée publiée sur Eternalfest.\"}",
      },
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      it(`should accept ${valueStr}: ${item.name ?? valueStr}`, function () {
        const actualValue: InputTextRevisionContent = $InputTextRevisionContent.read(JSON_READER, item.rawJson);
        chai.assert.isTrue($InputTextRevisionContent.equals(actualValue, item.value));
      });
    }
  });
});
