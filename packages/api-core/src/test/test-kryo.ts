import chai from "chai";
import { Type } from "kryo";
import { JSON_READER } from "kryo-json/lib/json-reader.js";
import objectInspect from "object-inspect";

export interface InvalidValue {
  readonly value: any;
  readonly name?: string;
}

export interface ValidValue<T> {
  readonly value: T;
  readonly rawJson?: string;
  readonly name?: string;
}

export interface TestKryoTypeOptions<T> {
  readonly type: Type<T>;
  readonly valid: ReadonlyArray<ValidValue<T>>;
  readonly invalid: ReadonlyArray<InvalidValue>;
}

export function testKryoType<T = unknown>(options: TestKryoTypeOptions<T>): void {
  for (const valid of options.valid) {
    const name: string = valid.name !== undefined ? valid.name : objectInspect(valid.value);
    it(`accepts: ${name}`, () => {
      const err: Error | undefined = options.type.testError!(valid.value);
      if (err !== undefined) {
        throw err;
      }
    });

    if (valid.rawJson !== undefined) {
      it(`reads raw JSON: ${name}`, () => {
        const actual: T = options.type.read!(JSON_READER, valid.rawJson);
        chai.assert.isTrue(options.type.equals(actual, valid.value));
      });
    }
  }

  for (const invalid of options.invalid) {
    const name: string = invalid.name !== undefined ? invalid.name : objectInspect(invalid.value);
    it(`rejects: ${name}`, () => {
      chai.assert.isFalse(options.type.test(invalid.value));
    });
  }
}
