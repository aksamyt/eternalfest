import { $RunSettings } from "../../lib/run/run-settings.js";
import { testKryoType } from "../test-kryo.js";

describe("RunSettings", function () {
  testKryoType({
    type: $RunSettings,
    valid: [
      {value: {detail: false, shake: false, sound: false, music: false, volume: 0}},
      {value: {detail: true, shake: true, sound: true, music: true, volume: 100}},
      {value: {detail: true, shake: false, sound: false, music: false, volume: 0}},
      {value: {detail: false, shake: true, sound: false, music: false, volume: 0}},
      {value: {detail: false, shake: false, sound: true, music: false, volume: 0}},
      {value: {detail: false, shake: false, sound: false, music: true, volume: 0}},
      {value: {detail: false, shake: false, sound: false, music: false, volume: 100}},
    ],
    invalid: [
      {value: {detail: false, shake: false, sound: false, music: true, volume: -1}},
      {value: {detail: false, shake: false, sound: false, music: true, volume: 101}},
      {value: {shake: false, sound: false, music: false, volume: 0}},
      {value: {detail: false, sound: false, music: false, volume: 0}},
      {value: {detail: false, shake: false, music: false, volume: 0}},
      {value: {detail: false, shake: false, sound: false, volume: 0}},
      {value: {detail: false, shake: false, sound: false, music: false}},
    ],
  });
});
