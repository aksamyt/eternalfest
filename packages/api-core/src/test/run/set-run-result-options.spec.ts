import { $SetRunResultOptions } from "../../lib/run/set-run-result-options.js";
import { testKryoType } from "../test-kryo.js";

describe("SetRunResultOptions", function () {
  testKryoType({
    type: $SetRunResultOptions,
    valid: [
      {value: {isVictory: false, maxLevel: 0, scores: [0], items: new Map(), stats: {jumps: 10}}},
    ],
    invalid: [
      {value: {maxLevel: 0, scores: [0], items: new Map()}},
      {value: {isVictory: false, scores: [0], items: new Map()}},
      {value: {isVictory: false, maxLevel: 0, items: new Map()}},
      {value: {isVictory: false, maxLevel: 0, scores: [0]}},
    ],
  });
});
