import { $LeaderboardEntry } from "../../lib/leaderboard/leaderboard-entry.js";
import { ActorType } from "../../lib/types/user/actor-type.js";
import { testKryoType } from "../test-kryo.js";

describe("LeaderboardEntry", function () {
  testKryoType({
    type: $LeaderboardEntry,
    valid: [
      {
        value: {
          score: 514233,
          user: {
            type: ActorType.User,
            id: "00000000-0000-0000-0000-000000000000",
            displayName: "SkyWasHere",
          },
          run: {
            id: "00000000-0000-0000-0000-000000000000",
            maxLevel: 0,
            gameOptions: ["[0]"],
          },
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"score\":514233,\"user\":{\"type\":\"user\",\"id\":\"00000000-0000-0000-0000-000000000000\",\"display_name\":\"SkyWasHere\"},\"run\":{\"id\":\"00000000-0000-0000-0000-000000000000\",\"max_level\":0,\"game_options\":[\"[0]\"]}}",
      },
    ],
    invalid: [],
  });
});
