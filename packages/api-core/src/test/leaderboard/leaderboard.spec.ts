import { $Leaderboard } from "../../lib/leaderboard/leaderboard.js";
import { ActorType } from "../../lib/types/user/actor-type.js";
import { testKryoType } from "../test-kryo.js";

describe("Leaderboard", function () {
  testKryoType({
    type: $Leaderboard,
    valid: [
      {
        value: {
          game: {
            id: "00000000-0000-0000-0000-000000000000",
            displayName: "A descent into darkness ~ Final Chapter",
          },
          gameMode: "solo",
          results: [
            {
              score: 500,
              user: {
                type: ActorType.User,
                id: "00000000-0000-0000-0000-000000000000",
                displayName: "Demurgos",
              },
              run: {
                id: "00000000-0000-0000-0000-000000000000",
                maxLevel: 2,
                gameOptions: [],
              },
            },
            {
              score: 1580250,
              user: {
                type: ActorType.User,
                id: "00000000-0000-0000-0000-000000000001",
                displayName: "Elagea",
              },
              run: {
                id: "00000000-0000-0000-0000-000000000001",
                maxLevel: 374,
                gameOptions: ["nightmare"],
              },
            },
          ]
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"game\":{\"id\":\"00000000-0000-0000-0000-000000000000\",\"display_name\":\"A descent into darkness ~ Final Chapter\"},\"game_mode\":\"solo\",\"results\":[{\"score\":500,\"user\":{\"type\":\"user\",\"id\":\"00000000-0000-0000-0000-000000000000\",\"display_name\":\"Demurgos\"},\"run\":{\"id\":\"00000000-0000-0000-0000-000000000000\",\"max_level\":2,\"game_options\":[]}}, {\"score\":1580250,\"user\":{\"type\":\"user\",\"id\":\"00000000-0000-0000-0000-000000000001\",\"display_name\":\"Elagea\"},\"run\":{\"id\":\"00000000-0000-0000-0000-000000000001\",\"max_level\":374,\"game_options\":[\"nightmare\"]}}]}",
      },
    ],
    invalid: [],
  });
});
