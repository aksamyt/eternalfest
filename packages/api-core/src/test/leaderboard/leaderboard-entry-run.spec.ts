import { $LeaderboardEntryRun } from "../../lib/leaderboard/leaderboard-entry-run.js";
import { testKryoType } from "../test-kryo.js";

describe("LeaderboardEntryRun", function () {
  testKryoType({
    type: $LeaderboardEntryRun,
    valid: [
      {
        value: {
          id: "00000000-0000-0000-0000-000000000000",
          maxLevel: 4,
          gameOptions: ["boost", "nightmare"],
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"id\":\"00000000-0000-0000-0000-000000000000\",\"max_level\":4,\"game_options\":[\"boost\",\"nightmare\"]}",
      },
    ],
    invalid: [],
  });
});
