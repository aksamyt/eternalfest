export enum DbVersion {
  /**
   * Users, identities
   */
  V001 = "001",

  /**
   * Blobs, files, games.
   */
  V002 = "002",

  /**
   * Run results, run volume, details -> detail, hfest_items#is_hidden.
   */
  V003 = "003",

  /**
   * Updated run results: score, max_level, items.
   */
  V004 = "004",

  /**
   * Use `Sint32` for `run_result.scores[i]`
   */
  V005 = "005",

  /**
   * Schema version, blob deletion queue, FK constraints with restricted deletion.
   */
  V006 = "006",

  /**
   * Run stats, `mode_tag` -> `mode_key`, `option_tag` -> `option_key`
   */
  V007 = "007",

  /**
   * Locale identifiers, game locales, user items.
   */
  V008 = "008",

  /**
   * Game key.
   */
  V009 = "009",
}

export const LATEST_DB_VERSION: DbVersion = DbVersion.V009;
