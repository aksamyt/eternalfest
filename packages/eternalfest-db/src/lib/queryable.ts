import fs from "fs";
import url from "url";

export interface QueryResultRow {
  [column: string]: unknown;
}

export interface QueryResult<R extends QueryResultRow = QueryResultRow> {
  command: string;
  rowCount: number;
  oid: number;
  fields: unknown[];
  rows: R[];
}

/**
 * Represents a simple queryable object for a database.
 */
export interface Queryable {
  query(query: string, values: any[]): Promise<QueryResult>;
}

/**
 * Execute one of the SQL files bundled in this lib.
 *
 * @param queryable Queryable object for the database.
 * @param furi File URI for the SQL file to execute.
 */
export async function execSqlFile(queryable: Queryable, furi: url.URL): Promise<QueryResult> {
  const sqlBuffer: Buffer = await fs.promises.readFile(furi);
  const sqlText: string = sqlBuffer.toString("utf-8");
  return execSql(queryable, sqlText);
}

/**
 * Execute an SQL script.
 *
 * @param queryable Queryable object for the database.
 * @param sqlText The source text of the SQL script.
 */
export async function execSql(queryable: Queryable, sqlText: string): Promise<QueryResult> {
  return queryable.query(sqlText, []);
}
