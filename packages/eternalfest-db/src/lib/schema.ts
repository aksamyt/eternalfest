export interface Inventory {
  /**
   * Id of the Identity owning this item (inventory row)
   */
  identity_id: string;

  /**
   * Hfest ID of the item
   */
  hfest_item_id: number;

  /**
   * Max count of the item (to support resets)
   */
  max_count: number;

  /**
   * Current count of the item
   */
  count: number;
}

export interface Session {
  session_id: string;
  user_id: string;
  created_at: Date;
  updated_at: Date;
  data: any;
}

export type IdentityType = typeof IDENTITY_TYPE_HFEST | typeof IDENTITY_TYPE_EF;
export const IDENTITY_TYPE_HFEST: "hfest" = "hfest";
export const IDENTITY_TYPE_EF: "ef" = "ef";

export interface Identity {
  /**
   * UUID of the identity
   */
  identity_id: string;

  /**
   * Type of this identity
   */
  type: IdentityType;

  /**
   * Date of the creation of the identity
   */
  created_at: Date;

  /**
   * Date of the last modification of the identity
   */
  updated_at: Date;

  /**
   * UUIDv4 of the user attached to this identity.
   * This can be null if the identity is not attached to anyone.
   */
  user_id: string | null;
}

export type HfestServer = typeof HFEST_SERVER_EN | typeof HFEST_SERVER_ES | typeof HFEST_SERVER_FR;
export const HFEST_SERVER_EN: "en" = "en";
export const HFEST_SERVER_ES: "es" = "es";
export const HFEST_SERVER_FR: "fr" = "fr";

export type LocaleId = typeof LOCALE_ID_FR_FR | typeof LOCALE_ID_EN_US | typeof LOCALE_ID_ES_SP;
export const LOCALE_ID_FR_FR: "fr-FR" = "fr-FR";
export const LOCALE_ID_EN_US: "en-US" = "en-US";
export const LOCALE_ID_ES_SP: "es-SP" = "es-SP";

export interface HfestIdentity {
  /**
   * UUID of the HfestIdentity, references a base identity in Identity
   */
  identity_id: string;

  server: HfestServer;

  /**
   * Hammerfest ID of the user
   */
  hfest_id: number;

  /**
   * Hammerfest username
   */
  username: string;

  email: string | null;
  best_score: number;
  best_level: number;
  game_completed: boolean;
}

export interface User {
  /**
   * UUIDv4 of the user
   */
  user_id: string;

  /**
   * Date of the creation of the user
   */
  created_at: Date;

  /**
   * Date of the last modification of the user
   */
  updated_at: Date;

  /**
   * Name to display for this user
   */
  display_name: string;

  /**
   * Boolean indicating if this user is an administrator
   */
  is_administrator: boolean;

  /**
   * Boolean indicating if this user has access to beta-test features (games)
   */
  is_tester: boolean;

  /**
   * Primary email of the user
   */
  email: string | null;

  /**
   * Use for authentication and custom URL
   */
  username: string | null;

  /**
   * 96 bytes (scrypt hash)
   */
  password_hash: Buffer | null;
}

export type CompleteHfestIdentity = Identity & HfestIdentity;

export type DriveItemType = typeof DRIVE_ITEM_TYPE_DIRECTORY | typeof DRIVE_ITEM_TYPE_FILE;
export const DRIVE_ITEM_TYPE_DIRECTORY: "directory" = "directory";
export const DRIVE_ITEM_TYPE_FILE: "file" = "file";

export interface Blob {
  blob_id: string;
  media_type: string;
  byte_size: number;
  buffer_id: string;
  created_at: Date;
  updated_at: Date;
}

export interface DriveItem {
  drive_item_id: string;
  type: DriveItemType;
  created_at: Date;
  updated_at: Date;
  display_name: string;
}

export interface DriveItemLink {
  ancestor_id: string;
  descendant_id: string;
}

export interface Directory {
  drive_item_id: string;
  type: "directory";
}

export interface File {
  drive_item_id: string;
  type: "file";
  blob_id: string;
}

export interface Drive {
  drive_id: string;
  owner_id: string;
  created_at: Date;
  updated_at: Date;
  root_id: string;
}

export interface UploadSession {
  upload_session_id: string;
  media_type: string;
  byte_size: number;
  written_bytes: number;
  created_at: Date;
  updated_at: Date;
  buffer_id: string;
  blob_id: string | null;
}

export type GameCategory = "big" | "challenge" | "fun" | "lab" | "puzzle" | "other" | "small";
export const GAME_CATEGORY_BIG: "big" = "big";
export const GAME_CATEGORY_CHALLENGE: "challenge" = "challenge";
export const GAME_CATEGORY_FUN: "fun" = "fun";
export const GAME_CATEGORY_LAB: "lab" = "lab";
export const GAME_CATEGORY_PUZZLE: "puzzle" = "puzzle";
export const GAME_CATEGORY_OTHER: "other" = "other";
export const GAME_CATEGORY_SMALL: "small" = "small";

export interface Game {
  game_id: string;
  key: string | null;
  main_locale: LocaleId;
  display_name: string;
  description: string;
  created_at: Date;
  updated_at: Date;
  public_access_from: Date | null;
  author_id: string;
  icon_file_id: string | null;
  category: GameCategory;
  families: string;
  loader_version: string;
  is_private: boolean;
}

export interface GameLocale {
  game_id: string;
  locale: LocaleId;
  display_name: string | null;
  description: string | null;
}

export interface GameMode {
  game_id: string;
  mode_key: string;
  main_locale: LocaleId;
  display_name: string;
  is_enabled: boolean;
  _rank: number;
}

export interface GameModeLocale {
  game_id: string;
  mode_key: string;
  locale: LocaleId;
  display_name: string | null;
}

export interface GameOption {
  game_id: string;
  mode_key: string;
  option_key: string;
  main_locale: LocaleId;
  display_name: string;
  is_enabled: boolean;
  _rank: number;
}

export interface GameOptionLocale {
  game_id: string;
  mode_key: string;
  option_key: string;
  locale: LocaleId;
  display_name: string | null;
}

export interface GameResource {
  game_id: string;
  file_id: string;
  resource_tag: string | null;
  _rank: number;
}

export interface Run {
  run_id: string;
  game_id: string;
  user_id: string;
  created_at: Date;
  started_at: Date | null;
  game_mode: string;
  game_options: any;
  detail: boolean;
  shake: boolean;
  sound: boolean;
  music: boolean;
  volume: number;
}

export interface RunResult {
  run_id: string;
  created_at: Date;
  is_victory: boolean;
  max_level: number;
  scores: [number] | [number, number];
  items: Record<string, number>;
  stats: any;
}

export interface UserItem {
  user_id: string;
  game_id: string;
  item_id: string;
  item_count: number;
}
