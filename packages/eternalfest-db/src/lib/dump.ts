import cp from "child_process";
import stream from "stream";
import { ConnectionConfig } from "./connection-config.js";

const PG_DUMP_BIN: string = "pg_dump";

export async function dumpData(config: ConnectionConfig, outStream: stream.Writable): Promise<void> {
  const args: ReadonlyArray<string> = [
    "--data-only",
    "--host", config.host,
    "--port", config.port.toString(10),
    "--username", config.user,
    "--dbname", config.name,
    "--format", "custom",
  ];

  const env: NodeJS.ProcessEnv = {
    ...process.env,
    PGPASSWORD: config.password,
  };

  const proc: cp.ChildProcess = cp.spawn(PG_DUMP_BIN, args, {env, stdio: ["ignore", "pipe", "inherit"]});
  proc.stdout!.pipe(outStream);
}
