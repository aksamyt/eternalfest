export interface ConnectionConfig {
  user: string;
  password: string;
  host: string;
  port: number;

  /**
   * Database name
   */
  // TODO: Rename to `database`.
  name: string;
}
