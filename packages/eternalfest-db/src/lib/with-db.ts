import pg from "pg";
import { ConnectionConfig } from "./connection-config.js";

export async function withDb<R>(config: ConnectionConfig, handler: (pool: pg.Pool) => Promise<R>): Promise<R> {
  const poolConfig: pg.PoolConfig = {
    user: config.user,
    password: config.password,
    host: config.host,
    port: config.port,
    database: config.name,
    max: 10,
    idleTimeoutMillis: 1000,
  };

  const pool: pg.Pool = new pg.Pool(poolConfig);

  try {
    return await handler(pool);
  } finally {
    await pool.end();
  }
}
