import cp from "child_process";
import stream from "stream";

import { ConnectionConfig } from "./connection-config.js";
import { DbVersion } from "./db-version.js";
import { Queryable } from "./queryable.js";
import { setState } from "./scripts.js";
import { withDb } from "./with-db.js";

const PG_RESTORE_BIN: string = "pg_restore";

export async function restoreData(
  config: ConnectionConfig,
  version: DbVersion,
  inStream: stream.Readable,
): Promise<void> {
  await withDb(config, async (queryable: Queryable) => setState(queryable, version, true));

  const args: ReadonlyArray<string> = [
    "--host", config.host,
    "--port", config.port.toString(10),
    "--username", config.user,
    "--dbname", config.name,
  ];

  const env: NodeJS.ProcessEnv = {
    ...process.env,
    PGPASSWORD: config.password,
  };

  const proc: cp.ChildProcess = cp.spawn(PG_RESTORE_BIN, args, {env, stdio: ["pipe", "inherit", "inherit"]});
  inStream.pipe(proc.stdin!);
}
