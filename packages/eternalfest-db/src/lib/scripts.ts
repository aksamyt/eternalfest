import hfItems from "@eternalfest/hammerfest-db/items.js";
import fs from "fs";
import furi from "furi";
import url from "url";

import { DbVersion, Queryable } from "./index.js";
import { execSql, QueryResult } from "./queryable.js";

const PROJECT_ROOT: url.URL = furi.join(import.meta.url, "..", "..");

export function resolveScriptFuri(...components: readonly string[]): url.URL {
  return furi.join(PROJECT_ROOT, "scripts", ...components);
}

function resolveCreateScript(version: DbVersion): url.URL {
  return resolveScriptFuri("create", `${version}.sql`);
}

function resolveUpgradeScript(startVersion: DbVersion, endVersion: DbVersion): url.URL {
  return resolveScriptFuri("upgrade", `${startVersion}-${endVersion}.sql`);
}

async function readCreateScript(version: DbVersion): Promise<string> {
  const scriptFuri: url.URL = resolveCreateScript(version);
  return await fs.promises.readFile(scriptFuri, {encoding: "utf-8"});
}

async function readUpgradeScript(startVersion: DbVersion, endVersion: DbVersion): Promise<string> {
  const scriptFuri: url.URL = resolveUpgradeScript(startVersion, endVersion);
  return await fs.promises.readFile(scriptFuri, {encoding: "utf-8"});
}

export async function drop(queryable: Queryable): Promise<void> {
  const scriptFuri: url.URL = resolveScriptFuri("drop.sql");
  const script: string = await fs.promises.readFile(scriptFuri, {encoding: "utf-8"});
  await execSql(queryable, script);
}

async function create1(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V001));
  if (!isVoid) {
    await populateItems1(queryable);
  }
}

async function create2(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V002));
  if (!isVoid) {
    await populateItems1(queryable);
  }
}

async function create3(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V003));
  if (!isVoid) {
    await populateItems3(queryable);
  }
}

async function create4(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V004));
  if (!isVoid) {
    await populateItems3(queryable);
  }
}

async function create5(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V005));
  if (!isVoid) {
    await populateItems3(queryable);
  }
}

async function create6(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V006));
  if (!isVoid) {
    await populateItems3(queryable);
  }
}

async function create7(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V007));
  if (!isVoid) {
    await populateItems3(queryable);
  }
}

async function create8(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V008));
  if (!isVoid) {
    await populateItems3(queryable);
  }
}

async function create9(queryable: Queryable, isVoid: boolean): Promise<void> {
  await execSql(queryable, await readCreateScript(DbVersion.V009));
  if (!isVoid) {
    await populateItems3(queryable);
  }
}

async function populateItems1(queryable: Queryable): Promise<void> {
  const promises: Promise<unknown>[] = [];
  for (const family of hfItems.getFamilies(false)) {
    for (const item of family.items) {
      promises.push(queryable.query("INSERT INTO hfest_items (hfest_item_id) VALUES ($1::int)", [item.id]));
    }
  }
  await Promise.all(promises);
}

async function populateItems3(queryable: Queryable): Promise<void> {
  const promises: Promise<unknown>[] = [];
  for (const family of hfItems.getFamilies(true)) {
    for (const item of family.items) {
      promises.push(queryable.query(
        "INSERT INTO hfest_items (hfest_item_id, is_hidden) VALUES ($1::INT, $2::BOOLEAN)",
        [item.id, family.hidden],
      ));
    }
  }
  await Promise.all(promises);
}

async function upgrade1to2(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V001, DbVersion.V002));
}

async function upgrade2to3(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V002, DbVersion.V003));

  const hasItemsRows = await queryable.query("SELECT * FROM hfest_items", []);
  const hasItems = hasItemsRows.rowCount > 0;

  if (!hasItems) {
    return;
  }

  const promises: Promise<unknown>[] = [];
  for (const family of hfItems.getFamilies(true)) {
    for (const item of family.items) {
      promises.push(queryable.query(`
        INSERT INTO hfest_items (hfest_item_id, is_hidden)
        VALUES ($1::VARCHAR, $2::BOOLEAN)
        ON CONFLICT (hfest_item_id)
        DO UPDATE SET
          is_hidden = $2::BOOLEAN`,
      [item.id, family.hidden],
      ));
    }
  }
  await Promise.all(promises);
}

async function upgrade3to4(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V003, DbVersion.V004));
}

async function upgrade4to5(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V004, DbVersion.V005));
}

async function upgrade5to6(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V005, DbVersion.V006));
}

async function upgrade6to7(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V006, DbVersion.V007));
}

async function upgrade7to8(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V007, DbVersion.V008));
}

async function upgrade8to9(queryable: Queryable): Promise<void> {
  await execSql(queryable, await readUpgradeScript(DbVersion.V008, DbVersion.V009));
}

type CreateFunction = (queryable: Queryable, isVoid: boolean) => Promise<void>;
type UpgradeFunction = (queryable: Queryable) => Promise<void>;

const CREATE_FUNCTIONS: ReadonlyMap<DbVersion, CreateFunction> = new Map([
  [DbVersion.V001, create1],
  [DbVersion.V002, create2],
  [DbVersion.V003, create3],
  [DbVersion.V004, create4],
  [DbVersion.V005, create5],
  [DbVersion.V006, create6],
  [DbVersion.V007, create7],
  [DbVersion.V008, create8],
  [DbVersion.V009, create9],
]);

const UPGRADE_FUNCTIONS: ReadonlyMap<DbVersion, ReadonlyMap<DbVersion, UpgradeFunction>> = new Map([
  [DbVersion.V001, new Map([[DbVersion.V002 as DbVersion, upgrade1to2]])],
  [DbVersion.V002, new Map([[DbVersion.V003, upgrade2to3]])],
  [DbVersion.V003, new Map([[DbVersion.V004, upgrade3to4]])],
  [DbVersion.V004, new Map([[DbVersion.V005, upgrade4to5]])],
  [DbVersion.V005, new Map([[DbVersion.V006, upgrade5to6]])],
  [DbVersion.V006, new Map([[DbVersion.V007, upgrade6to7]])],
  [DbVersion.V007, new Map([[DbVersion.V008, upgrade7to8]])],
  [DbVersion.V008, new Map([[DbVersion.V009, upgrade8to9]])],
]);

export function create(queryable: Queryable, version: DbVersion, isVoid: boolean): Promise<void> {
  const fn: CreateFunction | undefined = CREATE_FUNCTIONS.get(version);
  if (fn === undefined) {
    throw new Error("CreateFunctionNotFound");
  }
  return fn(queryable, isVoid);
}

export async function upgrade(queryable: Queryable, src: DbVersion, dest: DbVersion): Promise<void> {
  const fn: UpgradeFunction | undefined = UPGRADE_FUNCTIONS.get(src)?.get(dest);
  if (fn === undefined) {
    throw new Error(`UpgradeFunctionNotFound: ${src} -> ${dest}`);
  }
  await fn(queryable);
}

/**
 * Sets the state of the DB.
 *
 * @param queryable Queryable for the DB.
 * @param version Version of the schema to use
 * @param isVoid Only create the schema, do not put any data.
 */
export async function setState(queryable: Queryable, version: DbVersion, isVoid: boolean): Promise<void> {
  await drop(queryable);
  await create(queryable, version, isVoid);
}

export async function getVersion(queryable: Queryable): Promise<DbVersion> {
  const result: QueryResult = await queryable.query(
    `SELECT description
FROM   pg_catalog.pg_namespace INNER JOIN pg_catalog.pg_description ON (oid = objoid)
WHERE  nspname = current_schema();
`, []
  );
  if (result.rows.length !== 1) {
    throw new Error("Failed to retrieve schema metadata");
  }
  const row = result.rows[0];
  const descriptionObj = JSON.parse((row as any).description);
  const versionName: unknown = descriptionObj.version;
  if (typeof versionName !== "string" || !/V\d{3}/.test(versionName)) {
    throw new Error("Invalid version name");
  }
  const version: unknown = Reflect.get(DbVersion, versionName);
  if (typeof version !== "string") {
    throw new Error("Version resolution failure");
  }
  return version as DbVersion;
}
