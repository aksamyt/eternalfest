import fs from "fs";
import incident from "incident";
import pg from "pg";
import stream from "stream";

import { ConnectionConfig } from "../lib/connection-config.js";
import { dumpData } from "../lib/dump.js";
import { DbVersion, LATEST_DB_VERSION, setState } from "../lib/index.js";
import { restoreData } from "../lib/restore.js";
import { getVersion, upgrade } from "../lib/scripts.js";
import { withDb } from "../lib/with-db.js";
import { getLocalConfig } from "./config.js";

async function run(): Promise<void> {
  const args: ReadonlyArray<string> = process.argv.slice(2);
  switch (args[0]) {
    case "create":
      return runCreate(LATEST_DB_VERSION);
    case "dump":
      return runDumpData(args[1] !== undefined ? args[1] : "eternalfest.pgdump");
    case "restore": {
      if (args.length < 3) {
        throw new Error("NotEnoughArguments");
      }
      const inFile: string = args[1];
      const inVersion: DbVersion = stringToDbVersion(args[2]);
      return runRestoreData(inFile, inVersion);
    }
    case "test": {
      return runTest();
    }
    case "upgrade":
      await runUpgrade(null, null);
      return;
    default:
      throw new incident.Incident(
        "NotImplemented",
        "This script should allow you to set the db to a given state or perform backups",
      );
  }
}

function stringToDbVersion(str: string): DbVersion {
  const version: any = Reflect.get(DbVersion, str);
  if (version === undefined) {
    throw new Error(`InvalidDbVersion: ${str}, available: ${Object.keys(DbVersion).join(", ")}`);
  }
  return version;
}

async function runCreate(dbVersion: DbVersion): Promise<void> {
  const config: ConnectionConfig = getLocalConfig();
  return withDb(config, async (pool: pg.Pool) => {
    await setState(pool, dbVersion, false);
  });
}

async function runDumpData(outFile: string): Promise<void> {
  const config: ConnectionConfig = getLocalConfig();
  const outStream: stream.Writable = fs.createWriteStream(outFile);
  await dumpData(config, outStream);
}

async function runRestoreData(inFile: string, dbVersion: DbVersion): Promise<void> {
  const inStream: stream.Readable = fs.createReadStream(inFile);
  const config: ConnectionConfig = getLocalConfig();
  await restoreData(config, dbVersion, inStream);
}

async function runTest(): Promise<void> {
  const config: ConnectionConfig = getLocalConfig();
  return withDb(config, async (pool: pg.Pool) => {
    const version = await getVersion(pool);
    console.log(`Host: ${config.host}`);
    console.log(`Port: ${config.port}`);
    console.log(`Name: ${config.name}`);
    console.log(`User: ${config.user}`);
    console.log(`Schema version: ${version}`);
  });
}

async function runUpgrade(src: DbVersion | null, dest: DbVersion | null): Promise<void> {
  const config: ConnectionConfig = getLocalConfig();
  return withDb(config, async (pool: pg.Pool) => {
    if (src === null) {
      src = await getVersion(pool);
    }
    if (dest === null) {
      dest = LATEST_DB_VERSION;
    }
    console.log(`Start version: ${src}`);
    console.log(`End version: ${dest}`);
    if (src === dest) {
      console.log("(Nothing to do)");
      return;
    }
    await upgrade(pool, src, dest);
    console.log("Upgrade complete");
  });
}

run().catch((err: Error): never => {
  console.error(err.stack);
  return process.exit(1);
});
