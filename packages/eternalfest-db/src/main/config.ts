import dotenv from "dotenv";
import fs from "fs";
import incident from "incident";
import path from "path";

import { ConnectionConfig } from "../lib/connection-config.js";

const REQUIRED_VARIABLES: string[] = [
  "EF_DB_USER",
  "EF_DB_PASSWORD",
  "EF_DB_HOST",
  "EF_DB_PORT",
  "EF_DB_NAME",
];

export function getLocalConfig(): ConnectionConfig {
  let dotEnvContent: string;
  try {
    dotEnvContent = fs.readFileSync(path.resolve(process.cwd(), ".env"), {encoding: "utf8"});
  } catch (err) {
    if (err.code === "ENOENT") {
      throw new incident.Incident(err, "MissingEnvFile", {cwd: process.cwd()});
    }
    throw err;
  }

  const parsedDotEnv: { [name: string]: string } = dotenv.parse(dotEnvContent);
  for (const key in parsedDotEnv) {
    if (!(key in process.env)) {
      process.env[key] = parsedDotEnv[key];
    }
  }

  const missingVariables: string[] = REQUIRED_VARIABLES.filter(envVar => !(envVar in process.env));
  if (missingVariables.length > 0) {
    throw new Error(`Missing environmnet variable: ${missingVariables.join(", ")}`);
  }

  return {
    user: process.env["EF_DB_USER"]!,
    password: process.env["EF_DB_PASSWORD"]!,
    host: process.env["EF_DB_HOST"]!,
    port: parseInt(process.env["EF_DB_PORT"]!, 10),
    name: process.env["EF_DB_NAME"]!,
  };
}
