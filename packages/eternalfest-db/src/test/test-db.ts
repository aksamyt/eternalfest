import pg from "pg";

import { ConnectionConfig } from "../lib/connection-config.js";
import { withDb } from "../lib/with-db.js";
import { getLocalConfig } from "./config.js";

export async function withTestDb(handler: (pool: pg.Pool) => Promise<void>): Promise<void> {
  const config: ConnectionConfig = await getLocalConfig();
  return withDb(config, handler);
}
