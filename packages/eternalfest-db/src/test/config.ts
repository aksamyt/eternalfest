import dotenv from "dotenv";
import fs from "fs";
import furi from "furi";
import incident from "incident";
import { QsValueReader } from "kryo-qs/lib/qs-value-reader.js";
import { $Uint16 } from "kryo/lib/integer.js";
import { RecordIoType, RecordType } from "kryo/lib/record.js";
import { Ucs2StringType } from "kryo/lib/ucs2-string.js";
import url from "url";

export interface ConnectionConfig {
  readonly user: string;
  readonly password: string;
  readonly host: string;
  readonly port: number;
  readonly name: string;
}

export const $ConnectionConfig: RecordIoType<ConnectionConfig> = new RecordType<ConnectionConfig>({
  properties: {
    user: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_USER"},
    password: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_PASSWORD"},
    host: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_HOST"},
    port: {type: $Uint16, rename: "EF_DB_PORT"},
    name: {type: new Ucs2StringType({maxLength: Infinity}), rename: "EF_DB_NAME"},
  },
});

const QS_READER: QsValueReader = new QsValueReader();

export function getConfigFromEnv(env: NodeJS.ProcessEnv): ConnectionConfig {
  return $ConnectionConfig.read(QS_READER, env);
}

export async function getLocalConfig(): Promise<ConnectionConfig> {
  const cwdUri: url.URL = furi.fromSysPath(process.cwd());
  const dotEnvUri: url.URL = furi.join(cwdUri, [".env"]);
  let dotEnvText: string;
  try {
    dotEnvText = await readTextAsync(dotEnvUri);
  } catch (err) {
    if (err.code === "ENOENT") {
      throw new incident.Incident(
        err,
        "DotEnvNotFound",
        {cwdUri},
        ({cwdUri}: {cwdUri: url.URL}) => `Missing \`.env\` file in current working directory: ${cwdUri}`,
      );
    }
    throw err;
  }
  const parsedDotEnv: Record<string, string> = dotenv.parse(dotEnvText);
  for (const [key, value] of Object.entries(parsedDotEnv)) {
    Reflect.set(process.env, key, value);
  }
  return getConfigFromEnv(process.env);
}

async function readTextAsync(fileUri: url.URL): Promise<string> {
  return new Promise<string>((resolve, reject): void => {
    fs.readFile(fileUri, {encoding: "utf-8"}, (err: NodeJS.ErrnoException | null, text: string): void => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(text);
      }
    });
  });
}
