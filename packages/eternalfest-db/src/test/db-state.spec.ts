import { DbVersion, Queryable, setState } from "../lib/index.js";
import { upgrade } from "../lib/scripts.js";
import { withTestDb } from "./test-db.js";

describe("db-state", function () {
  it("001", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V001, false);
    });
  });
  it("001 (empty)", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V001, true);
    });
  });
  it("002", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V002, false);
    });
  });
  it("002 (empty)", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V002, true);
    });
  });
  it("003", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V003, false);
    });
  });
  it("upgrade 002-003", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V002, false);
      await upgrade(db, DbVersion.V002, DbVersion.V003);
    });
  });
  it("004", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V004, false);
    });
  });
  it("005", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V005, false);
    });
  });
  it("006", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V006, false);
    });
  });
  it("007", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V007, false);
    });
  });
  it("008", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V008, false);
    });
  });
  it("009", async function (this: Mocha.Context) {
    this.timeout(30000);
    await withTestDb(async (db: Queryable): Promise<void> => {
      await setState(db, DbVersion.V009, false);
    });
  });
});
