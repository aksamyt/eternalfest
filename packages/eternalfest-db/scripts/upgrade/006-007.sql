COMMENT ON SCHEMA public IS '{"version": "V007"}';

ALTER TABLE game_modes
  RENAME COLUMN mode_tag TO mode_key;

ALTER TABLE game_options
  RENAME COLUMN mode_tag TO mode_key;
ALTER TABLE game_options
  RENAME COLUMN option_tag TO option_key;

ALTER TABLE run_results
  ADD COLUMN stats JSON;
