# play.eternalfest.net

## Get Started

```sh
npm run start
```

## Create a DB user

```sh
# Run as the Postgres user
createuser --encrypted --interactive --pwprompt
```

Example

```sh
postgres@host $ createuser --encrypted --interactive --pwprompt
Enter name of role to add: eternalfest
Shall the new role be a superuser? (y/n) n
Shall the new role be allowed to create databases? (y/n) y
Shall the new role be allowed to create more new roles? (y/n) n
```

## Create a DB

```sh
createdb --owner=dbuser dbname
psql dbname
ALTER SCHEMA public OWNER TO dbuser;
```

Example:

```sh
$ createdb --owner=eternalfest eternalfestdb
$ psql eternalfestdb
psql (9.6.10)
Type "help" for help.

eternalfestdb=# ALTER SCHEMA public OWNER TO eternalfest;
```
