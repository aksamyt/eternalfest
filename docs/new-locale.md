# To add a new locale:

1. Copy src/locales/messages
2. Translate
3. Add the locale id to webpack.config.js
4. Update the scripts in package.json
5. Update src/locales
6. Add the locale to src/app/translation-providers 
7. Add a flag to src/app/app.component.pug
8. And update the CSS flag in static/main
